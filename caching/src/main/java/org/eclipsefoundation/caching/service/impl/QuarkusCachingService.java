/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.caching.service.impl;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.caching.config.CacheKeyClassTagResolver;
import org.eclipsefoundation.caching.exception.CacheCalculationException;
import org.eclipsefoundation.caching.model.CacheWrapper;
import org.eclipsefoundation.caching.model.CacheWrapperBuilder;
import org.eclipsefoundation.caching.model.ParameterizedCacheKey;
import org.eclipsefoundation.caching.model.ParameterizedCacheKeyBuilder;
import org.eclipsefoundation.caching.namespaces.CachingPropertyNames;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.micrometer.core.annotation.Timed;
import io.micrometer.core.aop.MeterTag;
import io.quarkus.cache.Cache;
import io.quarkus.cache.CacheInvalidate;
import io.quarkus.cache.CacheInvalidateAll;
import io.quarkus.cache.CacheKey;
import io.quarkus.cache.CacheName;
import io.quarkus.cache.CacheResult;
import io.quarkus.cache.CaffeineCache;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * Utililzes Quarkus caching extensions in order to cache and retrieve data. Reason we augment over the Quarkus core cache (caffeine) is so
 * that we can record TTLs for cache objects which aren't exposed by base cache.
 * 
 * @author Martin Lowe
 *
 */
@ApplicationScoped
public class QuarkusCachingService implements CachingService {
    private static final Logger LOGGER = LoggerFactory.getLogger(QuarkusCachingService.class);

    private final long ttlWrite;
    private final RequestWrapper wrapper;
    private final Cache cache;

    public QuarkusCachingService(RequestWrapper wrapper, @CacheName("default") Cache cache,
            @ConfigProperty(name = CachingPropertyNames.CACHE_TTL_MAX_SECONDS, defaultValue = "900") long ttlWrite) {
        this.cache = cache;
        this.wrapper = wrapper;
        this.ttlWrite = ttlWrite;
    }

    @Override
    public <T> CacheWrapper<T> get(String id, MultivaluedMap<String, String> params, Class<?> rawType, Callable<? extends T> callable) {
        Objects.requireNonNull(callable);
        // create the cache key for the entry, cloning the map for key integrity
        ParameterizedCacheKey cacheKey = ParameterizedCacheKeyBuilder.builder().clazz(rawType).id(id).params(cloneMap(params)).build();
        LOGGER.debug("Retrieving cache value for '{}'", cacheKey);
        return get(cacheKey, callable);
    }

    @Override
    public <T> CacheWrapper<T> getPaginatedData(String id, MultivaluedMap<String, String> params, Class<?> rawType,
            Callable<? extends T> callable) {
        // we need pagination context to add it implicitly, so we should error out otherwise
        if (wrapper == null || wrapper.asMap() == null) {
            throw new IllegalStateException("Cached paginated data can only be used in the context of a request");
        }
        // copy the map to not mutate the passed reference when adding the pagination parameter
        MultivaluedMap<String, String> paramMap = new MultivaluedHashMap<>();
        if (params == null) {
            paramMap
                    .add(DefaultUrlParameterNames.PAGE_PARAMETER_NAME,
                            wrapper.getFirstParam(DefaultUrlParameterNames.PAGE).orElseGet(() -> "1"));
        } else {
            params.forEach(paramMap::addAll);
            // only add the page if it hasn't been set upstream by another call
            paramMap
                    .computeIfAbsent(DefaultUrlParameterNames.PAGE_PARAMETER_NAME,
                            k -> Arrays.asList(wrapper.getFirstParam(DefaultUrlParameterNames.PAGE).orElseGet(() -> "1")));
        }
        return get(id, paramMap, rawType, callable);
    }

    @Override
    public Optional<Long> getExpiration(String id, MultivaluedMap<String, String> params, Class<?> type) {
        ParameterizedCacheKey cacheKey = ParameterizedCacheKeyBuilder.builder().clazz(type).id(id).params(params).build();
        try {
            return Optional.ofNullable(getExpiration(true, cacheKey));
        } catch (Exception e) {
            throw new CacheCalculationException("Error while retrieving expiration for cachekey: " + cacheKey.toString(), e);
        }
    }

    @Override
    public Set<ParameterizedCacheKey> getCacheKeys() {
        return cache.as(CaffeineCache.class).keySet().stream().map(o -> {
            if (o instanceof ParameterizedCacheKey k) {
                return k;
            } else {
                // dont record broken keys, and filter null values out
                LOGGER.debug("Found illegal cache key value in default cache: {}", o);
                return null;
            }
        }).dropWhile(Predicate.isEqual(null)).collect(Collectors.toSet());

    }

    @Override
    @CacheInvalidate(cacheName = "default")
    @CacheInvalidate(cacheName = "ttl")
    public void remove(@CacheKey ParameterizedCacheKey key) {
        LOGGER.debug("Removed cache key '{}' from TTL and default cache regions", key);
    }

    @Override
    public void fuzzyRemove(String id, Class<?> type) {
        this.getCacheKeys().stream().filter(k -> k.id().equals(id) && k.clazz().equals(type)).forEach(this::remove);
    }

    @Override
    @CacheInvalidateAll(cacheName = "default")
    @CacheInvalidateAll(cacheName = "ttl")
    public void removeAll() {
        LOGGER.debug("Cleared TTL and default cache regions");
    }

    @Override
    public long getMaxAge() {
        return TimeUnit.MILLISECONDS.convert(ttlWrite, TimeUnit.SECONDS);
    }

    @Override
    public Cache getCache() {
        return this.cache;
    }

    /**
     * Soft check of expiration, does not create new entries if no entry is present.
     * 
     * @param cacheKey key of cache item to retrieve TTL of.
     * @return the epoch time that the given cache key expires, or null if the key is not set.
     */
    @SuppressWarnings("java:S3516") // suppressed as the cached entry can be returned when present
    public Long checkExpiration(ParameterizedCacheKey cacheKey) {
        try {
            // check for existing value, throwing out if none is found.
            return getExpiration(true, cacheKey);
        } catch (Exception e) {
            // no result found
            return null;
        }
    }

    @Timed(value = "eclipse_cache_timing")
    @CacheResult(cacheName = "default")
    <T> CacheWrapper<T> get(@MeterTag(resolver = CacheKeyClassTagResolver.class) @CacheKey ParameterizedCacheKey cacheKey,
            Callable<? extends T> callable) {

        CacheWrapperBuilder<T> cacheWrap = CacheWrapperBuilder.builder();
        try {
            T data = callable.call();
            cacheWrap.data(Optional.ofNullable(data));
        } catch (Exception e) {
            LOGGER.error("Error while creating cache entry for key '{}': \n", cacheKey, e);
            cacheWrap.errorType(Optional.of(e.getClass()));
        }
        getExpiration(false, cacheKey);
        return cacheWrap.build();
    }

    @CacheResult(cacheName = "ttl")
    public long getExpiration(boolean throwIfMissing, @CacheKey ParameterizedCacheKey cacheKey) {
        if (throwIfMissing) {
            throw new CacheCalculationException("No TTL present for cache key '{}', not generating");
        }
        LOGGER.debug("Timeout for {}: {}", cacheKey, System.currentTimeMillis() + getMaxAge());
        return System.currentTimeMillis() + getMaxAge();
    }

    /**
     * To prevent modification of maps/cache key entries post retrieval, we should be referencing copies of the maps rather than the direct
     * passed reference for safety.
     * 
     * @return the copied map, or an empty map if null
     */
    private MultivaluedMap<String, String> cloneMap(MultivaluedMap<String, String> paramMap) {
        MultivaluedMap<String, String> out = new MultivaluedHashMap<>();
        if (paramMap != null) {
            paramMap.entrySet().forEach(e -> out.addAll(e.getKey(), e.getValue()));
        }
        return out;
    }

}
