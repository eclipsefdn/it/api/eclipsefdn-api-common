/**
 * Copyright (c) 2023, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.caching.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import org.eclipse.microprofile.context.ManagedExecutor;
import org.eclipsefoundation.caching.config.LoadingCacheConfig;
import org.eclipsefoundation.caching.config.LoadingCacheConfig.LoaderDefinition;
import org.eclipsefoundation.caching.model.ParameterizedCacheKey;
import org.eclipsefoundation.caching.model.ParameterizedCacheKeyBuilder;
import org.eclipsefoundation.caching.service.LoadingCacheManager;

import com.github.benmanes.caffeine.cache.AsyncLoadingCache;
import com.github.benmanes.caffeine.cache.Caffeine;

import io.quarkus.arc.All;
import io.quarkus.arc.InstanceHandle;
import io.quarkus.runtime.Startup;
import io.quarkus.scheduler.Scheduled;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;

/**
 * Service layer for defined loading caches. This centralizes the loading caches to reduce potential for inconsistent or incorrect
 * implementations of loading caches in downstream applications.
 * 
 * @author Martin Lowe
 *
 */
@Startup
@ApplicationScoped
public class DefaultLoadingCacheManager implements LoadingCacheManager {

    private final LoadingCacheConfig config;
    private final List<InstanceHandle<LoadingCacheProvider<?>>> providers;
    private final ManagedExecutor executor;

    // internal list of managed loading caches
    private List<LoadingCacheWrapper<?>> caches;

    /**
     * Default constructor for LCM, includes required configs, providers, and the executor for processing.
     * 
     * @param config loading cache configuration mapping for runtime
     * @param providers list of all available loaders for the cache
     * @param executor context-aware executor for loading cache async
     */
    public DefaultLoadingCacheManager(LoadingCacheConfig config, @All List<InstanceHandle<LoadingCacheProvider<?>>> providers,
            ManagedExecutor executor) {
        this.config = config;
        this.providers = providers;
        this.executor = executor;
    }

    /**
     * Using the loading cache providers, generate active loading caches for the application.
     */
    @PostConstruct
    void generateCaches() {
        // instantiate the caches, using the handles to build named caches
        this.caches = providers.stream().map(p -> buildWrapper(p.get(), p.getBean().getName())).collect(Collectors.toList());

        // grouping by inner type, ensure that there is only 1 cache per type.
        // We could support multiple caches for the same type eventually, but there is no current use case
        if (this.caches
                .stream()
                .collect(Collectors.groupingBy(LoadingCacheWrapper::getInnerType))
                .entrySet()
                .stream()
                .anyMatch(e -> e.getValue().size() > 1)) {
            throw new IllegalStateException("Ambiguous loading caches found, will not continue to load.");
        }
        // do the preloading keys if present, discarding the unneeded result after calculation
        this.caches
                .stream()
                .filter(c -> c.config.startAtBoot())
                .forEach(c -> c.cache
                        .get(ParameterizedCacheKeyBuilder.builder().clazz(c.innerType).id(c.config.runtimeBootKey().get()).build()));
    }

    @Override
    public <T> List<T> getList(ParameterizedCacheKey k) {
        Optional<LoadingCacheWrapper<?>> cache = caches.stream().filter(c -> c.getInnerType().equals(k.clazz())).findFirst();
        if (cache.isEmpty()) {
            throw new IllegalStateException("Could not find a provided loading cache mapped for type: " + k.clazz().getSimpleName());
        }
        // do the checked lookup of the cache data
        return lookupLoadingCache(k, cache.get());
    }

    @Override
    public List<LoadingCacheWrapper<?>> getManagedCaches() {
        return Collections.unmodifiableList(caches);
    }

    /**
     * Using the key and the discovered cache, do a lookup of the key, using the configured timeout and rethrowing any exceptions in
     * execution upstream.
     * 
     * @param <T> the type of data returned by the cache
     * @param k the cache key to pass to the loading cache for value retrieval
     * @param cache the cache wrapper that can retrieve the value for the current request
     * @return the list of results for the call to the cache
     * @throws RuntimeException when there is a timeout or error in the execution of loading the cache value
     */
    @SuppressWarnings("unchecked")
    private <T> List<T> lookupLoadingCache(ParameterizedCacheKey k, LoadingCacheWrapper<?> cache) {
        // this is checked through the above inner class comparison, so this should be a safe lookup
        LoadingCacheWrapper<T> typedCache = (LoadingCacheWrapper<T>) cache;

        try {
            // do the lookup of the cached data, using the configured timeout
            return typedCache.cache.get(k).get(typedCache.config.timeout(), TimeUnit.SECONDS);
        } catch (TimeoutException | ExecutionException e) {
            // rethrow upstream. In the future, we may add handlers here
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            // reinterrupt, as we don't care about our state in this case
            Thread.currentThread().interrupt();
        }
        // this is only technically possible but shouldn't happen, as interrupts shouldn't resolve
        return Collections.emptyList();
    }

    /**
     * Checks if a cache should be refreshed by checking the last refresh time, and if so iterating all active keys and attempt a refresh.
     * This is used for cases where we need to force the cache to refresh before it is called by downstream methods to have the most
     * accurate data possible.
     */
    @Scheduled(every = "5m")
    void refreshKeys() {
        caches
                .stream()
                .filter(wrapper -> wrapper.config.forceRefresh())
                .forEach(wrapper -> wrapper.cache.getAll(wrapper.cache.asMap().keySet()));
    }

    /**
     * Builds a typed wrapper for safer lookups and to hold the configurations for easier access.
     * 
     * @param <T> the type of data returned by the cache
     * @param provider the loading cache building method, populates cache values
     * @param name the name of the cache, used to lookup loading cache configurations.
     * @return the cache wrapper containing the instantiated cache, the associated configuration, and expected inner types
     */
    private <T> LoadingCacheWrapper<T> buildWrapper(LoadingCacheProvider<T> provider, String name) {
        // get the loader definition to build the initial cache and be set along side cache in wrapper
        LoaderDefinition ld = config.loaders().getOrDefault(name, config.defaults());
        return new LoadingCacheWrapper<>(Caffeine
                .newBuilder()
                .executor(executor)
                .refreshAfterWrite(ld.refreshAfter())
                .maximumSize(ld.maximumSize())
                .buildAsync(provider::fetchData), provider.getType(), ld);
    }

    /**
     * Typed generic wrapper for loading caches. This will help match on internal types of expected output and increase type safety around
     * lookups, as well as provide best matching configuration values for the given cache.
     * 
     * @author Martin Lowe
     *
     * @param <T> the internal type of content produced by the loading cache
     */
    public final class LoadingCacheWrapper<T> {
        private final AsyncLoadingCache<ParameterizedCacheKey, List<T>> cache;
        private final Class<T> innerType;
        private final LoaderDefinition config;

        /**
         * Builds the wrapper with assembled cache, the configurations that apply to the cache to reduce need for reflection, and the
         * internal type for easier matching on incoming requests.
         * 
         * @param cache the wrapped asynchronous cache
         * @param innerType the return type of the loading cache
         * @param config relevant settings for the current cache
         */
        private LoadingCacheWrapper(AsyncLoadingCache<ParameterizedCacheKey, List<T>> cache, Class<T> innerType, LoaderDefinition config) {
            this.cache = Objects.requireNonNull(cache);
            this.innerType = Objects.requireNonNull(innerType);
            this.config = Objects.requireNonNull(config);
        }

        /**
         * The loading cache used to maintain preloaded assets.
         * 
         * @return the loading cache for the wrapper.
         */
        public AsyncLoadingCache<ParameterizedCacheKey, List<T>> getCache() {
            return this.cache;
        }

        /**
         * Retrieves a view of the cache keys for the current cache. Will only show keys that have realized values and not values that are
         * in the process of being fetched.
         * 
         * @return list of cache keys for finished cache results.
         */
        public List<ParameterizedCacheKey> getCacheKeys() {
            return new ArrayList<>(this.cache.asMap().keySet());
        }

        /**
         * The internal type of content returned by the loading cache operations.
         * 
         * @return literal class for internal content type.
         */
        public Class<T> getInnerType() {
            return this.innerType;
        }

        /**
         * Gets the pre-loading cache configuration that defines refresh periods, whether to enforce forced refreshes, and the timeout
         * periods to reduce risk of stalled operations.
         * 
         * @return the preloading configuration for the wrapped cache.
         */
        public LoaderDefinition getLoaderConfig() {
            return this.config;
        }
    }
}
