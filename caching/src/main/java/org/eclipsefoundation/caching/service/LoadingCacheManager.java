/**
 * Copyright (c) 2023, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.caching.service;

import java.util.List;

import org.eclipsefoundation.caching.model.ParameterizedCacheKey;
import org.eclipsefoundation.caching.service.impl.DefaultLoadingCacheManager.LoadingCacheWrapper;

/**
 * Interface for service that manages loading caches within applications. This will remove the burden of managing best implementations and
 * improve consistency when accessing these caches.
 * 
 * @author Martin Lowe
 *
 */
public interface LoadingCacheManager {

    /**
     * <p>
     * Retrieves a list of cached results for the given key.
     * 
     * <p>
     * If there is no cache value populated, it will be generated. If the value exists but is expired, then the cache will return the
     * current value and begin recalculations in the background.
     * 
     * @param <T> the type of data returned by the given cache
     * @param k the key to use in cache lookups
     * @return the list of results for the given cache key
     */
    <T> List<T> getList(ParameterizedCacheKey k);

    /**
     * Retrieves an unmodifiable list of generic caches managed by this service. Used in testing and management of the caches.
     * 
     * @return list of caches for the current application
     */
    List<LoadingCacheWrapper<?>> getManagedCaches();

    /**
     * Provides the binding for registering managed loading caches automatically.
     * 
     * @author Martin Lowe
     *
     */
    public interface LoadingCacheProvider<T> {

        public static final String METRICS_REGION_NAME = "eclipse_precache_timing";
        public static final String METRICS_KEY_TAG_NAME = "type";

        /**
         * Retrieves raw data given a cache key.
         * 
         * @param k the cache key with context for the current request
         * @return the list of results to cache in the upstream loading cache
         */
        List<T> fetchData(ParameterizedCacheKey k);

        /**
         * Indicates the type of data returned by the cache for the provider.
         * 
         * @return class literal for result type.
         */
        Class<T> getType();
    }
}
