/*********************************************************************
* Copyright (c) 2019, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.caching.service;

import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;

import org.eclipsefoundation.caching.model.CacheWrapper;
import org.eclipsefoundation.caching.model.ParameterizedCacheKey;

import jakarta.ws.rs.core.MultivaluedMap;
import io.quarkus.arc.Unremovable;
import io.quarkus.cache.Cache;

/**
 * Interface defining the caching service to be used within the application.
 * 
 * @author Martin Lowe
 * @param <T> the type of object to be stored in the cache.
 */
@Unremovable
public interface CachingService {

    /**
     * Returns an Optional object of type T, returning a cached object if available, otherwise using the callable to
     * generate a value to be stored in the cache and returned.
     * 
     * @param id the ID of the object to be stored in cache
     * @param params the query parameters for the current request
     * @param callable a runnable that returns an object of type T
     * @param rawType the rawtype of data returned, which may be different than the actual data type returned (such as a
     * returned list containing the raw type).
     * @return the cached result if it can be calculated, or empty
     */
    <T> CacheWrapper<T> get(String id, MultivaluedMap<String, String> params, Class<?> rawType, Callable<? extends T> callable);

    /**
     * <p>
     * Using the same retrieval logic as the get() call in this class, this call will add page parameters off of the request
     * to the parameter map if they exist. This will better cover cases where data needs to be chunked in large data sets.
     * 
     * <p>
     * This call introduces larger amounts of cache entries, so this should be used with caution. Pagesize has been omitted
     * to provide protection against spam requests that would slow down the request.
     * 
     * @param <T> the type of object to be stored in the cache.
     * @param id the ID of the object to be stored in cache
     * @param params the query parameters for the current request
     * @param callable a runnable that returns an object of type T
     * @param rawType the rawtype of data returned, which may be different than the actual data type returned (such as a
     * returned list containing the raw type).
     * @return the cached result if it can be calculated, or empty
     */
    <T> CacheWrapper<T> getPaginatedData(String id, MultivaluedMap<String, String> params, Class<?> rawType, Callable<? extends T> callable);

    /**
     * Returns the expiration date in millis since epoch.
     * 
     * @param id the ID of the object to be stored in cache
     * @param params the query parameters for the current request
     * @return an Optional expiration date for the current object if its set. If there is no underlying data, then empty
     * would be returned
     */
    Optional<Long> getExpiration(String id, MultivaluedMap<String, String> params, Class<?> type);

    /**
     * @return the max age of cache entries
     */
    long getMaxAge();

    /**
     * Retrieves a set of cache keys available to the current cache.
     * 
     * @return unmodifiable set of cache entry keys.
     */
    Set<ParameterizedCacheKey> getCacheKeys();

    /**
     * Removes cache entry for given cache entry key.
     * 
     * @param key cache entry key
     */
    void remove(ParameterizedCacheKey key);

    /**
     * Removes cache entries with matching ID and type, regardless of the parameters for the cache key.
     * 
     * @param id id of entries to remove
     * @param type cached object type to be removed
     */
    void fuzzyRemove(String id, Class<?> type);

    /**
     * Removes all cache entries.
     */
    void removeAll();

    /**
     * Get the main cache associated with this service. Should not be used in place of this service, as some of the
     * mechanisms like TTL won't work in such cases.
     * 
     * @return the main cache for this service
     */
    Cache getCache();

}
