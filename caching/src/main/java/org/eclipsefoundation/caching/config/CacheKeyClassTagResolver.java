package org.eclipsefoundation.caching.config;

import org.eclipsefoundation.caching.model.ParameterizedCacheKey;

import io.micrometer.common.annotation.ValueResolver;
import jakarta.inject.Singleton;

@Singleton
public class CacheKeyClassTagResolver implements ValueResolver {

    @Override
    public String resolve(Object parameter) {
        if (parameter instanceof ParameterizedCacheKey castKey) {
            return castKey.clazz().getSimpleName();
        }
        return null;
    }

}
