/**
 * Copyright (c) 2023, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.caching.config;

import java.time.Duration;
import java.util.Map;
import java.util.Optional;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;
import io.smallrye.config.WithParentName;

/**
 * Configuration mapping for loading cache service, provides ability to configure caches to be preloading or lazy init.
 * 
 * @author Martin Lowe
 *
 */
@ConfigMapping(prefix = "eclipse.cache.loading")
public interface LoadingCacheConfig {

    /**
     * @return map of additional loading configurations to use when generating loading cache set
     */
    @WithParentName
    Map<String, LoaderDefinition> loaders();

    /**
     * @return the default configuration for the loading caches when no name is set or given.
     */
    LoaderDefinition defaults();

    /**
     * Definition for managing a loading cache. Adds the ability to start at boot to form a preloading cache, or to lazy
     * init for simpler loading cache and high availability calls.
     */
    interface LoaderDefinition {

        /**
         * @return key used when populating map during boot, required if startAtBoot is true.
         */
        Optional<String> runtimeBootKey();

        @WithDefault("false")
        boolean startAtBoot();

        /**
         * @return true if the cache should forcefully refresh to maintain better accuracy in live calls.
         */
        @WithDefault("false")
        boolean forceRefresh();

        /**
         * The timeout duration for async lookups in seconds. Used to prevent out of control processes.
         */
        @WithDefault("5")
        Long timeout();

        /**
         * @return the duration a given cache element should exist before recalculation
         */
        @WithDefault("PT1H")
        Duration refreshAfter();

        /**
         * @return the number of keys that should be maintained in the cache
         */
        @WithDefault("10")
        int maximumSize();
    }
}
