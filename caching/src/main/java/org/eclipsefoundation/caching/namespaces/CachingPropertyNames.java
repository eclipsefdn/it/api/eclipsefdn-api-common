/*******************************************************************************
 * Copyright (c) 2024 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipsefoundation.caching.namespaces;

public class CachingPropertyNames {

    public static final String CACHE_TTL_MAX_SECONDS = "eclipse.cache.ttl-max-seconds";
    public static final String CACHE_RESOURCE_ENABLED = "eclipse.cache.resource.enabled";

    private CachingPropertyNames() {
    }
}
