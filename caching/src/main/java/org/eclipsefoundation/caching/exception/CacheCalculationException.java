/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.caching.exception;

/**
 * Thrown when there is a problem in calculating cache data
 * 
 * @author Martin Lowe
 *
 */
public class CacheCalculationException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * Include basic context as to why this cache calculation exception was thrown.
     * 
     * @param message error message to include in exception
     */
    public CacheCalculationException(String message) {
        super(message);
    }

    /**
     * Include stack context as to why this cache calculation exception was thrown.
     * 
     * @param message error message to include in exception
     * @param t the underlying cause of the exception
     */
    public CacheCalculationException(String message, Throwable t) {
        super(message, t);
    }
}
