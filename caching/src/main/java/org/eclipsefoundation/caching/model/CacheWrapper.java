/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.caching.model;

import java.util.Optional;

import io.soabase.recordbuilder.core.RecordBuilder;

@RecordBuilder
public record CacheWrapper<T>(Optional<T> data, Optional<Class<?>> errorType) {

    public CacheWrapper(Optional<T> data, Optional<Class<?>> errorType) {
        this.data = data != null ? data : Optional.empty();
        this.errorType = errorType != null ? errorType : Optional.empty();
    }
}
