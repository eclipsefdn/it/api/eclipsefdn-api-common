/**
 * Copyright (c) 2019, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.caching.model;

import org.eclipsefoundation.utils.helper.ParameterHelper;

import io.quarkus.arc.Arc;
import io.soabase.recordbuilder.core.RecordBuilder;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * @author Martin Lowe
 *
 */
@RecordBuilder
public record ParameterizedCacheKey(String id, Class<?> clazz, MultivaluedMap<String, String> params) {

    public ParameterizedCacheKey(String id, Class<?> clazz, MultivaluedMap<String, String> params) {
        this.id = id;
        this.clazz = clazz;
        this.params = params != null ? params : new MultivaluedHashMap<>();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('[').append(clazz.getSimpleName()).append(']');
        sb.append("id:").append(id);

        // use param helper to standardize parameters to registered params
        ParameterHelper paramHelper = Arc.container().instance(ParameterHelper.class).get();
        MultivaluedMap<String, String> filteredParams = paramHelper.filterUnknownParameters(params);
        // join all the non-empty params to the key to create distinct entries for
        // filtered values
        filteredParams
                .entrySet()
                .stream()
                .filter(e -> !e.getValue().isEmpty())
                .map(e -> e.getKey() + '=' + String.join(",", e.getValue()))
                .forEach(s -> sb.append('|').append(s));
        return sb.toString();
    }
}