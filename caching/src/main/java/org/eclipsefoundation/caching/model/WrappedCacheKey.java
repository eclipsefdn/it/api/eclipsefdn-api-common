/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
/**
 * 
 */
package org.eclipsefoundation.caching.model;

import java.util.Date;
import java.util.Objects;

/**
 * 
 */
public record WrappedCacheKey(ParameterizedCacheKey key, Date ttl) {

    public WrappedCacheKey(ParameterizedCacheKey key, Date ttl) {
        this.key = Objects.requireNonNull(key);
        this.ttl = ttl;
    }
}
