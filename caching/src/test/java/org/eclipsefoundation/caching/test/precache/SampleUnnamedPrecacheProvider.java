/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.caching.test.precache;

import java.util.Arrays;
import java.util.List;

import org.eclipsefoundation.caching.model.ParameterizedCacheKey;
import org.eclipsefoundation.caching.service.LoadingCacheManager.LoadingCacheProvider;
import org.eclipsefoundation.caching.test.models.SecondaryTestModel;

import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;

/**
 * Super basic precache example for testing purposes.
 * 
 * @author Martin Lowe
 *
 */
@ApplicationScoped
public class SampleUnnamedPrecacheProvider implements LoadingCacheProvider<SecondaryTestModel> {

    List<SecondaryTestModel> base;

    @PostConstruct
    void init() {
        this.base = Arrays
                .asList(testModel("Jimmy Baylet", "honeybadger@y-men.fcu"), testModel("Corduroy Blue", "thunderbird@y-men.fcu"),
                        testModel("Matthew the Black Rainfrog", "agent-q@theagency.ff"));
    }

    @Override
    public List<SecondaryTestModel> fetchData(ParameterizedCacheKey k) {
        return this.base
                .stream()
                .filter(b -> (!k.params().containsKey("name") || k.params().get("name").contains(b.name()))
                        && (!k.params().containsKey("mail") || k.params().get("mail").contains(b.name())))
                .toList();
    }

    @Override
    public Class<SecondaryTestModel> getType() {
        return SecondaryTestModel.class;
    }

    private SecondaryTestModel testModel(String name, String mail) {
        return new SecondaryTestModel(name, mail);
    }
}
