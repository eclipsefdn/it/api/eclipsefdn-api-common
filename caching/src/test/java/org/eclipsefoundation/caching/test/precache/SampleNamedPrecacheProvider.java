/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.caching.test.precache;

import java.util.Arrays;
import java.util.List;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.caching.model.ParameterizedCacheKey;
import org.eclipsefoundation.caching.service.LoadingCacheManager.LoadingCacheProvider;
import org.eclipsefoundation.caching.test.models.TestModel;
import org.eclipsefoundation.caching.test.namespaces.TestParameters;

import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Named;

/**
 * Super basic precache example for testing purposes.
 * 
 * @author Martin Lowe
 *
 */
@Named(TestParameters.SAMPLE_CONFIG_NAME)
@ApplicationScoped
public class SampleNamedPrecacheProvider implements LoadingCacheProvider<TestModel> {

    @ConfigProperty(name = "eclipse.caching.preloading-test.timeout", defaultValue = "5")
    Long timeoutInSeconds;

    List<TestModel> base;

    @PostConstruct
    void init() {
        this.base = Arrays
                .asList(testModel("Jimmy Baylet", "honeybadger@y-men.fcu"), testModel("Corduroy Blue", "thunderbird@y-men.fcu"),
                        testModel("", ""));
    }

    @Override
    public List<TestModel> fetchData(ParameterizedCacheKey k) {
        return this.base
                .stream()
                .filter(b -> (!k.params().containsKey("name") || k.params().get("name").contains(b.name()))
                        && (!k.params().containsKey("mail") || k.params().get("mail").contains(b.name())))
                .toList();
    }

    @Override
    public Class<TestModel> getType() {
        return TestModel.class;
    }

    private TestModel testModel(String name, String mail) {
        return new TestModel(name, mail);
    }
}
