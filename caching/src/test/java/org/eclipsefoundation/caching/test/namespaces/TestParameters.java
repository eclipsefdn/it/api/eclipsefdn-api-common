/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.caching.test.namespaces;

/**
 * @author martin
 *
 */
public final class TestParameters {

    public static final String SAMPLE_CONFIG_NAME = "sample";

    private TestParameters() {
    }
}
