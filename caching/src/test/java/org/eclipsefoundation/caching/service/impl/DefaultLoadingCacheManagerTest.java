/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.caching.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.eclipsefoundation.caching.config.LoadingCacheConfig;
import org.eclipsefoundation.caching.model.ParameterizedCacheKey;
import org.eclipsefoundation.caching.model.ParameterizedCacheKeyBuilder;
import org.eclipsefoundation.caching.service.LoadingCacheManager;
import org.eclipsefoundation.caching.service.impl.DefaultLoadingCacheManager.LoadingCacheWrapper;
import org.eclipsefoundation.caching.test.models.SecondaryTestModel;
import org.eclipsefoundation.caching.test.models.TestModel;
import org.eclipsefoundation.caching.test.namespaces.TestParameters;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;

/**
 * @author Martin Lowe
 *
 */
@QuarkusTest
class DefaultLoadingCacheManagerTest {

    @Inject
    LoadingCacheConfig config;
    @Inject
    LoadingCacheManager manager;

    @Test
    void init_success_mapsNamedConfigs() {
        // relies on the fact that the provider has a Named qualifier
        Optional<LoadingCacheWrapper<?>> namedCache = manager
                .getManagedCaches()
                .stream()
                .filter(c -> c.getInnerType() == TestModel.class)
                .findFirst();
        Assertions.assertTrue(namedCache.isPresent());
        Assertions
                .assertNotEquals(config.defaults(), namedCache.get().getLoaderConfig(),
                        "The named loading cache should not have the same configuration as the default");
        Assertions
                .assertEquals(config.loaders().get(TestParameters.SAMPLE_CONFIG_NAME), namedCache.get().getLoaderConfig(),
                        "The named loading cache should match the named sample loader configs");
    }

    @Test
    void init_success_mapsUnnamedConfigs() {
        // relies on the fact that the provider has no Named qualifier
        Optional<LoadingCacheWrapper<?>> unnamedCache = manager
                .getManagedCaches()
                .stream()
                .filter(c -> c.getInnerType() == SecondaryTestModel.class)
                .findFirst();
        Assertions.assertTrue(unnamedCache.isPresent());
        Assertions
                .assertEquals(config.defaults(), unnamedCache.get().getLoaderConfig(),
                        "The unnamed loading cache should have the same configuration as the default");
    }

    @Test
    void getList_success() {
        List<TestModel> out = manager.getList(ParameterizedCacheKeyBuilder.builder().clazz(TestModel.class).id("sample").build());
        Assertions.assertNotNull(out);
        Assertions.assertFalse(out.isEmpty());
    }

    @Test
    void getList_success_insertsIntoLoadingCache() {
        // relies on the fact that the provider has a Named qualifier
        LoadingCacheWrapper<?> namedCache = manager
                .getManagedCaches()
                .stream()
                .filter(c -> c.getInnerType() == TestModel.class)
                .findFirst()
                .get();
        // generate a random ID to check for cache presence
        String randomId = UUID.randomUUID().toString();
        ParameterizedCacheKey k = ParameterizedCacheKeyBuilder.builder().clazz(TestModel.class).id(randomId).build();
        // check that it doesn't exist
        Assertions.assertFalse(namedCache.getCache().asMap().containsKey(k));
        // run the cache op
        manager.getList(k);
        // check that it now exists after call
        Assertions.assertTrue(namedCache.getCache().asMap().containsKey(k));
    }

    @Test
    void getList_failure_noSupportedCache() {
        // create a cache key that would fetch an unsupported cache result type (this class)
        ParameterizedCacheKey k = ParameterizedCacheKeyBuilder.builder().clazz(getClass()).id("sample").build();
        Assertions
                .assertThrows(IllegalStateException.class, () -> manager.getList(k),
                        "Loading cache manager should have thrown error for unsupported type");
    }

    @Test
    void getManagedCaches_success_containsExpectedCache() {
        List<LoadingCacheWrapper<?>> out = manager.getManagedCaches();
        Assertions.assertNotNull(out);
        Assertions
                .assertTrue(!out.isEmpty() && out.stream().anyMatch(lcw -> lcw.getInnerType() == TestModel.class),
                        "Expected managed cache list to contain the sample loading cache that references TestModel objects");
    }

    @Test
    void getManagedCaches_success_listIsUnmodifiable() {
        List<LoadingCacheWrapper<?>> out = manager.getManagedCaches();
        // need to use existing reference as constructor is hidden and class is final
        LoadingCacheWrapper<?> sampleWrapper = out.get(0);
        Assertions.assertThrows(UnsupportedOperationException.class, () -> out.add(sampleWrapper));
        Assertions.assertThrows(UnsupportedOperationException.class, () -> out.remove(0));
    }
}
