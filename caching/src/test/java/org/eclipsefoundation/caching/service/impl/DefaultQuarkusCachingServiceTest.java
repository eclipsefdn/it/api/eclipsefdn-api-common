/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.caching.service.impl;

import java.util.Optional;
import java.util.UUID;

import org.eclipsefoundation.caching.model.CacheWrapper;
import org.eclipsefoundation.caching.model.ParameterizedCacheKey;
import org.eclipsefoundation.caching.model.ParameterizedCacheKeyBuilder;
import org.eclipsefoundation.caching.service.CachingService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.cache.Cache;
import io.quarkus.cache.CacheName;
import io.quarkus.cache.CaffeineCache;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * Tests the Quarkus Caching service for various calls.
 * 
 * @author Martin Lowe
 *
 */
@QuarkusTest
class DefaultQuarkusCachingServiceTest {

    private static final String PARAMETER_NAME = "id";
    private static final String SECOND_PARAMETER_NAME = "ids";
    
    @Inject
    @CacheName("default")
    Cache cache;
    @Inject
    @CacheName("ttl")
    Cache ttlCache;

    @Inject
    CachingService svc;

    /*
     * get() tests
     */

    @Test
    void get_success() {
        // generate a random key
        String id = UUID.randomUUID().toString();
        String cacheValue = "sample";
        // check that we have no match for ID before proceeding
        Assertions.assertFalse(lookupCacheKeyUsingMainCache(id, Optional.empty()), "Expected random UUID key to be empty, but found value");
        // do the lookup
        CacheWrapper<String> value = svc.get(id, new MultivaluedHashMap<>(), String.class, () -> "sample");

        Assertions
                .assertTrue(lookupCacheKeyUsingMainCache(id, Optional.empty()),
                        "Expected new cache value to have a key present in cache key set");
        Assertions.assertTrue(value.data().isPresent(), "Cache value should have been present but was missing");
        Assertions.assertEquals(cacheValue, value.data().get(), "Expected input and output value to be the same");
    }

    @Test
    void get_success_generatesTtlEntry() {
        // generate a random key
        String id = UUID.randomUUID().toString();
        String cacheValue = "sample";
        // check that we have no match for ID before proceeding
        Assertions.assertFalse(lookupCacheKeyUsingMainCache(id, Optional.empty()), "Expected random UUID key to be empty, but found value");
        Assertions
                .assertFalse(lookupCacheKeyUsingRawCache(id, Optional.empty(), ttlCache),
                        "Expected random UUID key to have no TTL key, but found value");

        // do the lookup that should generate a TTL cache entry
        svc.get(id, new MultivaluedHashMap<>(), String.class, () -> cacheValue);

        Assertions
                .assertTrue(lookupCacheKeyUsingRawCache(id, Optional.empty(), ttlCache),
                        "Expected new cache value to have a key present in TTL cache key set");
    }

    @Test
    void get_success_nullParamsSameAsEmptyMap() {
        // generate a random key
        String id = UUID.randomUUID().toString();
        String cacheValue = "sample";
        // create empty map to be used in comparisons
        MultivaluedMap<String, String> emptyMap = new MultivaluedHashMap<>();

        // check that we have no match for ID before proceeding
        Assertions.assertFalse(lookupCacheKeyUsingMainCache(id, Optional.empty()), "Expected random UUID key to be empty, but found value");

        // do the lookup
        CacheWrapper<String> value = svc.get(id, null, String.class, () -> cacheValue);

        Assertions
                .assertTrue(lookupCacheKeyUsingMainCache(id, Optional.of(emptyMap)),
                        "Expected new cache value to have a key present in cache key set");
        Assertions.assertTrue(value.data().isPresent(), "Cache value should have been present but was missing");
        Assertions.assertEquals(cacheValue, value.data().get(), "Expected input and output value to be the same");
    }

    @Test
    void get_success_paramsCreateDistinctKeys() {
        // generate a random key
        String id = UUID.randomUUID().toString();
        // create empty map to be used in comparisons
        MultivaluedMap<String, String> emptyMap = new MultivaluedHashMap<>();

        // create a param map to use in creating distinct keys
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(PARAMETER_NAME, "sample_value");

        // check that we have no match for ID before proceeding
        Assertions.assertFalse(lookupCacheKeyUsingMainCache(id, Optional.empty()), "Expected random UUID key to be empty, but found value");

        // do the cache ops for the different maps with the same ID
        CacheWrapper<String> value = svc.get(id, emptyMap, String.class, () -> "sample");
        CacheWrapper<String> value2 = svc.get(id, params, String.class, () -> "sample_value2");

        Assertions
                .assertTrue(lookupCacheKeyUsingMainCache(id, Optional.of(emptyMap)),
                        "Expected new cache value to have a key present in cache key set with empty map");
        Assertions
                .assertTrue(lookupCacheKeyUsingMainCache(id, Optional.of(params)),
                        "Expected new cache value to have a key present in cache key set with a populated map");
        Assertions.assertTrue(value2.data().isPresent(), "Cache value should have been present but was missing");
        Assertions.assertNotEquals(value2.data().get(), value.data().get(), "Expected output for different cache entries to be different");
    }

    @Test
    void get_success_returnTypeCreateDistinctKeys() {
        // generate a random key
        String id = UUID.randomUUID().toString();
        // create empty map to be used in comparisons
        MultivaluedMap<String, String> emptyMap = new MultivaluedHashMap<>();

        // check that we have no match for ID before proceeding
        Assertions.assertFalse(lookupCacheKeyUsingMainCache(id, Optional.empty()), "Expected random UUID key to be empty, but found value");

        // do the cache ops for the different maps with the same ID
        CacheWrapper<String> value = svc.get(id, emptyMap, String.class, () -> "sample");
        CacheWrapper<Integer> value2 = svc.get(id, emptyMap, Integer.class, () -> 11);

        Assertions
                .assertTrue(lookupCacheKeyUsingRawCache(id, Optional.of(emptyMap), Optional.of(String.class), cache),
                        "Expected new cache value to have a key present in cache key set with string type");
        Assertions
                .assertTrue(lookupCacheKeyUsingRawCache(id, Optional.of(emptyMap), Optional.of(Integer.class), cache),
                        "Expected new cache value to have a key present in cache key set with integer type");

        Assertions.assertTrue(value.data().isPresent(), "Integer cache value should have been present but was missing");
        Assertions.assertTrue(value2.data().isPresent(), "String cache value should have been present but was missing");
    }

    @Test
    void get_failure_errorThrown() {
        String id = UUID.randomUUID().toString();

        // check that we have no match for ID before proceeding
        Assertions.assertFalse(lookupCacheKeyUsingMainCache(id, Optional.empty()), "Expected random UUID key to be empty, but found value");

        // value should be null as there was an error
        CacheWrapper<String> value = svc.get(id, new MultivaluedHashMap<>(), String.class, () -> { throw new RuntimeException("Kaboom"); });
        Assertions.assertTrue(lookupCacheKeyUsingMainCache(id, Optional.empty()), "Expected random UUID key to have a value, but was empty");
        Assertions.assertTrue(value.data().isEmpty(), "Cache value should have been missing but was present");
        Assertions.assertEquals(RuntimeException.class, value.errorType().get(), "The exception type caught in the wrapper should be the same as thrown");
    }

    @Test
    void get_failure_errorGeneratesTTL() {
        String id = UUID.randomUUID().toString();

        // check that we have no match for ID before proceeding
        Assertions.assertFalse(lookupCacheKeyUsingMainCache(id, Optional.empty()), "Expected random UUID key to be empty, but found value");
        
        // value should be null as there was an error, but a TTL should be set
        svc.get(id, new MultivaluedHashMap<>(), String.class, () -> { throw new RuntimeException("Kaboom"); });
        Assertions.assertTrue(lookupCacheKeyUsingRawCache(id, Optional.empty(), ttlCache), "Expected random UUID key to have a TTL value, but was empty");
    }

    @Test
    void get_failure_nullValue() {
        String id = UUID.randomUUID().toString();

        // check that we have no match for ID before proceeding
        Assertions.assertFalse(lookupCacheKeyUsingMainCache(id, Optional.empty()), "Expected random UUID key to be empty, but found value");

        // value should be empty as null gets interpreted as an empty optional
        CacheWrapper<String> value = svc.get(id, new MultivaluedHashMap<>(), String.class, () -> null);

        Assertions.assertTrue(lookupCacheKeyUsingMainCache(id, Optional.empty()), "Expected random UUID key to have a value, but was empty");
        Assertions.assertTrue(value.data().isEmpty(), "Cache value should have been missing but was present");
        Assertions.assertTrue(value.errorType().isEmpty(), "There should be no exception caught in this case");
    }

    @Test
    void get_failure_nullGeneratesTTL() {
        String id = UUID.randomUUID().toString();

        // check that we have no match for ID before proceeding
        Assertions.assertFalse(lookupCacheKeyUsingMainCache(id, Optional.empty()), "Expected random UUID key to be empty, but found value");

        // value should be null as there was an error
        CacheWrapper<String> value = svc.get(id, new MultivaluedHashMap<>(), String.class, () -> null);
        Assertions.assertTrue(lookupCacheKeyUsingMainCache(id, Optional.empty()), "Expected random UUID key to have a value, but was empty");
        Assertions.assertTrue(value.data().isEmpty(), "Cache value should have been missing but was present");
        Assertions.assertTrue(value.errorType().isEmpty(), "There should be no exception caught in this case");
    }

    /*
     * getCacheKeys() tests
     */

    @Test
    void getCacheKeys_success() {
        String id = UUID.randomUUID().toString();

        // check that we have no match for ID before proceeding
        Assertions
                .assertFalse(svc.getCacheKeys().stream().anyMatch(k -> k.id().equals(id)),
                        "Expected random UUID key to be empty, but found key");
        // run the op to populate the cache value
        svc.get(id, new MultivaluedHashMap<>(), String.class, () -> "sample");

        // check that the cache value can be seen in the cache key set
        Assertions
                .assertTrue(svc.getCacheKeys().stream().anyMatch(k -> k.id().equals(id)),
                        "Expected random UUID key to be present, but found no key");
    }

    @Test
    void getCacheKeys_success_unknownKeysAreIgnored() {
        // register the number of keys before the test
        int keysLen = svc.getCacheKeys().size();

        // generate an 'unknown' key of a different format
        cache.get("foo", k -> "bar");

        // check that the cache value can be seen in the cache key set
        Assertions
                .assertEquals(keysLen, svc.getCacheKeys().size(),
                        "Unknown key should not have been returned by getCacheKey, but was returned");
    }

    /*
     * fuzzyRemove(id, type)
     */

    @Test
    void fuzzyRemove_success() {
        // generate a random key
        String id = UUID.randomUUID().toString();
        // create empty map for initial post
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        // check that we have no match for ID before proceeding
        Assertions.assertFalse(lookupCacheKeyUsingMainCache(id, Optional.empty()), "Expected random UUID key to be empty, but found value");
        // request + put the data in cache and check it's there
        svc.get(id, params, String.class, () -> "sample");
        Assertions
                .assertTrue(lookupCacheKeyUsingMainCache(id, Optional.empty()),
                        "Expected new cache value to have a key present in cache key set with empty map");

        // update the map and create another entry
        params = new MultivaluedHashMap<>();
        params.add(SECOND_PARAMETER_NAME, id);
        Assertions
                .assertFalse(lookupCacheKeyUsingMainCache(id, Optional.of(params)),
                        "Expected random UUID key to be empty, but found value");
        // request + put the data in cache and check it's there
        svc.get(id, params, String.class, () -> "sample");
        Assertions
                .assertTrue(lookupCacheKeyUsingMainCache(id, Optional.of(params)),
                        "Expected new cache value to have a key present in cache key set with empty map");

        // do the fuzzy remove operation
        svc.fuzzyRemove(id, String.class);

        // check that both of the values are gone
        Assertions
                .assertFalse(lookupCacheKeyUsingMainCache(id, Optional.empty()),
                        "Expected random UUID key to be empty after fuzzy remove, but found value");
        Assertions
                .assertFalse(lookupCacheKeyUsingMainCache(id, Optional.of(params)),
                        "Expected random UUID key + params to be empty after fuzzy remove, but found value");
    }

    @Test
    void fuzzyRemove_success_onlyRemovesForSameType() {
        // generate a random key
        String id = UUID.randomUUID().toString();
        // create empty map for initial post
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        // check that we have no match for ID before proceeding
        Assertions
                .assertFalse(lookupCacheKeyUsingRawCache(id, Optional.of(params), Optional.of(String.class), cache),
                        "Expected random UUID key to be empty, but found value");
        // request + put the data in cache and check it's there
        svc.get(id, params, String.class, () -> "sample");
        Assertions
                .assertTrue(lookupCacheKeyUsingRawCache(id, Optional.of(params), Optional.of(String.class), cache),
                        "Expected new cache value to have a key present in cache key set with empty map");

        // create another entry with the same ID for a different type
        Assertions
                .assertFalse(lookupCacheKeyUsingRawCache(id, Optional.of(params), Optional.of(Integer.class), cache),
                        "Expected random UUID key to be empty, but found value");
        // request + put the data in cache and check it's there
        svc.get(id, params, Integer.class, () -> 1);
        Assertions
                .assertTrue(lookupCacheKeyUsingRawCache(id, Optional.of(params), Optional.of(Integer.class), cache),
                        "Expected new cache value to have a key present in cache key set with empty map");

        // do the fuzzy remove operation
        svc.fuzzyRemove(id, String.class);

        // check that only the targeted type is removed
        Assertions
                .assertFalse(lookupCacheKeyUsingRawCache(id, Optional.of(params), Optional.of(String.class), cache),
                        "Expected random UUID key to be empty after fuzzy remove, but found value");
        Assertions
                .assertTrue(lookupCacheKeyUsingRawCache(id, Optional.of(params), Optional.of(Integer.class), cache),
                        "Expected random UUID key + params to be empty after fuzzy remove, but found value");
    }

    /*
     * remove(key) tests
     */

    @Test
    void remove_success() {
        // generate a random key
        String id = UUID.randomUUID().toString();
        // create empty map to be used in comparisons
        MultivaluedMap<String, String> emptyMap = new MultivaluedHashMap<>();

        // check that we have no match for ID before proceeding
        Assertions.assertFalse(lookupCacheKeyUsingMainCache(id, Optional.empty()), "Expected random UUID key to be empty, but found value");

        // do the cache ops to create the cache entry we will remove
        svc.get(id, emptyMap, String.class, () -> "sample");
        Assertions
                .assertTrue(lookupCacheKeyUsingMainCache(id, Optional.empty()),
                        "Expected new cache value to have a key present in cache key set with empty map");

        // build the key that we will be removing
        ParameterizedCacheKey key = ParameterizedCacheKeyBuilder.builder().id(id).clazz(String.class).params(emptyMap).build();

        // remove the cache key
        svc.remove(key);
        Assertions.assertFalse(lookupCacheKeyUsingMainCache(id, Optional.empty()), "Expected random UUID key to be empty, but found value");
    }

    @Test
    void remove_success_removesFromTtl() {
        // generate a random key
        String id = UUID.randomUUID().toString();
        // create empty map to be used in comparisons
        MultivaluedMap<String, String> emptyMap = new MultivaluedHashMap<>();

        // check that we have no match for ID before proceeding
        Assertions
                .assertFalse(lookupCacheKeyUsingRawCache(id, Optional.empty(), Optional.empty(), ttlCache),
                        "Expected random UUID key to be empty, but found value");

        // do the cache ops to create the cache entry we will remove
        svc.get(id, emptyMap, String.class, () -> "sample");
        Assertions
                .assertTrue(lookupCacheKeyUsingRawCache(id, Optional.empty(), Optional.empty(), ttlCache),
                        "Expected new cache value to have a key present in cache key set with empty map");

        // build the key that we will be removing
        ParameterizedCacheKey key = ParameterizedCacheKeyBuilder.builder().id(id).clazz(String.class).params(emptyMap).build();

        // remove the cache key
        svc.remove(key);
        Assertions
                .assertFalse(lookupCacheKeyUsingRawCache(id, Optional.empty(), Optional.empty(), ttlCache),
                        "Expected random UUID key to be empty, but found value");
    }

    @Test
    void remove_success_doesntFailOnMissingKey() {
        // generate a random key
        String id = UUID.randomUUID().toString();

        // check that we have no match for ID before proceeding
        Assertions.assertFalse(lookupCacheKeyUsingMainCache(id, Optional.empty()), "Expected random UUID key to be empty, but found value");

        // build the key that we will be removing
        ParameterizedCacheKey key = ParameterizedCacheKeyBuilder
                .builder()
                .id("other-key-not-present")
                .clazz(Integer.class)
                .params(new MultivaluedHashMap<>())
                .build();

        // attempt to remove the non-existent cache key
        svc.remove(key);
    }

    @Test
    void remove_success_keepsEntriesWithDifferentIds() {
        // generate a random key
        String id = UUID.randomUUID().toString();
        // create empty map to be used in comparisons
        MultivaluedMap<String, String> emptyMap = new MultivaluedHashMap<>();

        // check that we have no match for ID before proceeding
        Assertions.assertFalse(lookupCacheKeyUsingMainCache(id, Optional.empty()), "Expected random UUID key to be empty, but found value");

        // do the cache ops to create the cache entry we will remove
        svc.get(id, emptyMap, String.class, () -> "sample");
        Assertions
                .assertTrue(lookupCacheKeyUsingMainCache(id, Optional.empty()),
                        "Expected new cache value to have a key present in cache key set with empty map");

        // build the key that we will be removing
        ParameterizedCacheKey key = ParameterizedCacheKeyBuilder
                .builder()
                .id("other-key-not-present")
                .clazz(Integer.class)
                .params(emptyMap)
                .build();

        // attempt to remove the cache key
        svc.remove(key);
        Assertions
                .assertTrue(lookupCacheKeyUsingMainCache(id, Optional.empty()),
                        "Expected new cache value to have a key present in cache key set with empty map");
    }

    @Test
    void remove_success_keepsEntriesWithDifferentReturnTypes() {
        // generate a random key
        String id = UUID.randomUUID().toString();
        // create empty map to be used in comparisons
        MultivaluedMap<String, String> emptyMap = new MultivaluedHashMap<>();

        // check that we have no match for ID before proceeding
        Assertions.assertFalse(lookupCacheKeyUsingMainCache(id, Optional.empty()), "Expected random UUID key to be empty, but found value");

        // do the cache ops to create the cache entry we will remove
        svc.get(id, emptyMap, String.class, () -> "sample");
        Assertions
                .assertTrue(lookupCacheKeyUsingMainCache(id, Optional.empty()),
                        "Expected new cache value to have a key present in cache key set with empty map");

        // build the key that we will be removing
        ParameterizedCacheKey key = ParameterizedCacheKeyBuilder.builder().id(id).clazz(Integer.class).params(emptyMap).build();

        // attempt to remove the cache key
        svc.remove(key);
        Assertions
                .assertTrue(lookupCacheKeyUsingMainCache(id, Optional.empty()),
                        "Expected new cache value to have a key present in cache key set with empty map");
    }

    @Test
    void remove_success_keepsEntriesWithDifferentParams() {
        // generate a random key
        String id = UUID.randomUUID().toString();
        // create empty map to be used in comparisons
        MultivaluedMap<String, String> emptyMap = new MultivaluedHashMap<>();

        // create a param map to use in creating distinct keys
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(PARAMETER_NAME, "sample_value");

        // check that we have no match for ID before proceeding
        Assertions.assertFalse(lookupCacheKeyUsingMainCache(id, Optional.empty()), "Expected random UUID key to be empty, but found value");

        // do the cache ops to create the cache entry we will remove
        svc.get(id, emptyMap, String.class, () -> "sample");
        Assertions
                .assertTrue(lookupCacheKeyUsingMainCache(id, Optional.empty()),
                        "Expected new cache value to have a key present in cache key set with empty map");

        // build the key that we will be removing
        ParameterizedCacheKey key = ParameterizedCacheKeyBuilder.builder().id(id).clazz(String.class).params(params).build();

        // attempt to remove the cache key
        svc.remove(key);
        Assertions
                .assertTrue(lookupCacheKeyUsingMainCache(id, Optional.empty()),
                        "Expected new cache value to have a key present in cache key set with empty map");
    }

    /*
     * getExpiration(key) tests
     */

    @Test
    void getExpiration_success() {
        // generate a random key
        String id = UUID.randomUUID().toString();
        // create empty map to be used in comparisons
        MultivaluedMap<String, String> emptyMap = new MultivaluedHashMap<>();

        // check that we have no match for ID before proceeding
        Assertions.assertFalse(lookupCacheKeyUsingMainCache(id, Optional.empty()), "Expected random UUID key to be empty, but found value");

        // do the cache ops to create the cache entry we will check expiration for
        svc.get(id, emptyMap, String.class, () -> "sample");

        Optional<Long> ttl = svc.getExpiration(id, emptyMap, String.class);

        Assertions.assertTrue(ttl.isPresent(), "TTL was unexpectedly missing from valid cache entry");
        Assertions.assertTrue(ttl.get() > 0, "TTL was unexpectedly missing from valid cache entry");
    }

    @Test
    void getExpiration_failure_missingKey() {

        // generate a random key
        String id = UUID.randomUUID().toString();
        // check that we have no match for ID before proceeding
        Assertions.assertFalse(lookupCacheKeyUsingMainCache(id, Optional.empty()), "Expected random UUID key to be empty, but found value");

        // keep map separate to reduce assertion to have 1 actual call
        MultivaluedMap<String, String> emptyMap = new MultivaluedHashMap<>();
        Assertions
                .assertThrows(RuntimeException.class, () -> svc.getExpiration(id, emptyMap, String.class),
                        "Expected missing cache key to throw exception when there is no TTL present");
    }

    /**
     * Uses "default" cache to look for cache key values.
     * 
     * @param id the ID to lookup in cache keys
     * @param params optional map of params to match on
     * @return true if there is a match for the passed parameters, otherwise false
     */
    private boolean lookupCacheKeyUsingMainCache(String id, Optional<MultivaluedMap<String, String>> params) {
        return lookupCacheKeyUsingRawCache(id, params, cache);
    }

    /**
     * Using similar logic to whats in the key set retrieval logic in the caching service, iterate over keys and look for
     * matches for the passed parameters.
     * 
     * @param id the ID to lookup in cache keys
     * @param params optional map of params to match on
     * @param c the cache to check for the value
     * @return true if there is a match for the passed parameters, otherwise false
     */
    private boolean lookupCacheKeyUsingRawCache(String id, Optional<MultivaluedMap<String, String>> params, Cache c) {
        return lookupCacheKeyUsingRawCache(id, params, Optional.empty(), c);
    }

    /**
     * Using similar logic to whats in the key set retrieval logic in the caching service, iterate over keys and look for
     * matches for the passed parameters.
     * 
     * @param id the ID to lookup in cache keys
     * @param params optional map of params to match on
     * @param clazz the optional containing the return type to check for in keys if set
     * @param c the cache to check for the value
     * @return true if there is a match for the passed parameters, otherwise false
     */
    private boolean lookupCacheKeyUsingRawCache(String id, Optional<MultivaluedMap<String, String>> params, Optional<Class<?>> clazz,
            Cache c) {
        return c.as(CaffeineCache.class).keySet().stream().anyMatch(o -> {
            if (o instanceof ParameterizedCacheKey) {
                ParameterizedCacheKey key = (ParameterizedCacheKey) o;
                return key.id().equals(id) && (params.isEmpty() || key.params().equals(params.get()))
                        && (clazz.isEmpty() || key.clazz().equals(clazz.get()));
            }
            return false;
        });
    }
}
