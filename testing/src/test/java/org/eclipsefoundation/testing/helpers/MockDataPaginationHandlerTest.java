/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.testing.helpers;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.http.model.FlatRequestWrapper;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class MockDataPaginationHandlerTest {

    @Test
    void paginate_success() {
        // Create list with 20 items
        List<String> data = generateData(20);

        // Get first page containing only 5 items
        List<String> expected = data.subList(0, 5);
        BaseAPIParameters params = new BaseAPIParameters(1, 5, null);
        RestResponse<List<String>> response = MockDataPaginationHandler.paginateData(params, data);
        List<String> paginatedData = response.getEntity();

        Assertions.assertEquals(expected.size(), paginatedData.size());
        Assertions.assertEquals(expected, paginatedData);
        Assertions.assertNull(response.getLink("prev"));
        Assertions.assertNotNull(response.getLink("next"));
        Assertions.assertNotNull(response.getLink("first"));
        Assertions.assertNotNull(response.getLink("last"));
        Assertions.assertTrue(response.getLink("self").getUri().getQuery().contains("page=1&pagesize=5"));

        // Get second page with 5 items
        expected = data.subList(5, 10);
        params = new BaseAPIParameters(2, 5, null);
        response = MockDataPaginationHandler.paginateData(params, data);
        paginatedData = response.getEntity();

        Assertions.assertEquals(expected.size(), paginatedData.size());
        Assertions.assertEquals(expected, paginatedData);
        Assertions.assertNotNull(response.getLink("prev"));
        Assertions.assertNotNull(response.getLink("next"));
        Assertions.assertNotNull(response.getLink("first"));
        Assertions.assertNotNull(response.getLink("last"));
        Assertions.assertTrue(response.getLink("self").getUri().getQuery().contains("page=2&pagesize=5"));

        // Get all using higher limit than we have results
        params = new BaseAPIParameters(1, 50, null);
        response = MockDataPaginationHandler.paginateData(params, data);
        paginatedData = response.getEntity();
        Assertions.assertEquals(data.size(), paginatedData.size());
        Assertions.assertEquals(data, paginatedData);
        Assertions.assertNull(response.getLink("prev"));
        Assertions.assertNull(response.getLink("next"));
        Assertions.assertEquals(response.getLink("self").getUri(), response.getLink("first").getUri());
        Assertions.assertEquals(response.getLink("self").getUri(), response.getLink("last").getUri());
        Assertions.assertTrue(response.getLink("self").getUri().getQuery().contains("page=1&pagesize=50"));
    }

    @Test
    void paginate_success_defaultPageSize() {
        // Create list with 200 items
        List<String> data = generateData(200);

        // Get first page containing 100 items using default page size
        List<String> expected = data.subList(0, 100);
        BaseAPIParameters params = new BaseAPIParameters(1, null, null);
        RestResponse<List<String>> response = MockDataPaginationHandler.paginateData(params, data);
        List<String> paginatedData = response.getEntity();

        Assertions.assertEquals(expected.size(), paginatedData.size());
        Assertions.assertEquals(expected, paginatedData);
        Assertions.assertNull(response.getLink("prev"));
        Assertions.assertNotNull(response.getLink("next"));
        Assertions.assertNotNull(response.getLink("last"));
        Assertions.assertEquals(response.getLink("self").getUri(), response.getLink("first").getUri());
        Assertions.assertTrue(response.getLink("self").getUri().getQuery().contains("page=1&pagesize=100"));

        // Get second page with default of 100 items
        expected = data.subList(100, 200);
        params = new BaseAPIParameters(2, null, null);
        response = MockDataPaginationHandler.paginateData(params, data);
        paginatedData = response.getEntity();

        Assertions.assertEquals(expected.size(), paginatedData.size());
        Assertions.assertEquals(expected, paginatedData);
        Assertions.assertNull(response.getLink("next"));
        Assertions.assertNotNull(response.getLink("prev"));
        Assertions.assertNotNull(response.getLink("first"));
        Assertions.assertEquals(response.getLink("self").getUri(), response.getLink("last").getUri());
        Assertions.assertTrue(response.getLink("self").getUri().getQuery().contains("page=2&pagesize=100"));
    }

    @Test
    void paginate_success_usingRequestWrapper() {
        // Create list with 200 items
        List<String> data = generateData(200);

        // Get first page containing 50 items
        List<String> expected = data.subList(0, 50);
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("https://api.eclipse.org/testing"));
        wrap.addParam(DefaultUrlParameterNames.PAGE, Integer.toString(1));
        wrap.addParam(DefaultUrlParameterNames.PAGESIZE, Integer.toString(50));
        BaseAPIParameters params = BaseAPIParameters.buildFromWrapper(wrap);
        RestResponse<List<String>> response = MockDataPaginationHandler.paginateData(params, data);
        List<String> paginatedData = response.getEntity();

        Assertions.assertEquals(expected.size(), paginatedData.size());
        Assertions.assertEquals(expected, paginatedData);
        Assertions.assertNull(response.getLink("prev"));
        Assertions.assertNotNull(response.getLink("next"));
        Assertions.assertNotNull(response.getLink("last"));
        Assertions.assertEquals(response.getLink("self").getUri(), response.getLink("first").getUri());
        Assertions.assertTrue(response.getLink("self").getUri().getQuery().contains("page=1&pagesize=50"));

        // Get fourth page with 50
        expected = data.subList(150, 200);
        wrap.setParam(DefaultUrlParameterNames.PAGE, Integer.toString(4));
        wrap.setParam(DefaultUrlParameterNames.PAGESIZE, Integer.toString(50));
        params = BaseAPIParameters.buildFromWrapper(wrap);
        response = MockDataPaginationHandler.paginateData(params, data);
        paginatedData = response.getEntity();

        Assertions.assertEquals(expected.size(), paginatedData.size());
        Assertions.assertEquals(expected, paginatedData);
        Assertions.assertNull(response.getLink("next"));
        Assertions.assertNotNull(response.getLink("prev"));
        Assertions.assertNotNull(response.getLink("first"));
        Assertions.assertEquals(response.getLink("self").getUri(), response.getLink("last").getUri());
        Assertions.assertTrue(response.getLink("self").getUri().getQuery().contains("page=4&pagesize=50"));
    }

    @Test
    void paginate_sucess_highPageEmptyList() {
        // Create list with 200 items
        List<String> data = generateData(200);

        // Request page that is too high, empty list should return
        BaseAPIParameters params = new BaseAPIParameters(5, 50, null);
        RestResponse<List<String>> response = MockDataPaginationHandler.paginateData(params, data);
        List<String> paginatedData = response.getEntity();

        Assertions.assertTrue(paginatedData.isEmpty());
        Assertions.assertNull(response.getLink("next"));
        Assertions.assertNotNull(response.getLink("first"));
        Assertions.assertNotNull(response.getLink("last"));
        Assertions.assertTrue(response.getLink("last").getUri().getQuery().contains("page=4&pagesize=50"));
        Assertions.assertTrue(response.getLink("self").getUri().getQuery().contains("page=5&pagesize=50"));

        // Request page that is too high using default page size
        params = new BaseAPIParameters(10, null, null);
        response = MockDataPaginationHandler.paginateData(params, data);
        paginatedData = response.getEntity();

        Assertions.assertTrue(paginatedData.isEmpty());

        // Request page that is too high using wrap params
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("https://api.eclipse.org/testing"));
        wrap.addParam(DefaultUrlParameterNames.PAGE, Integer.toString(21));
        wrap.addParam(DefaultUrlParameterNames.PAGESIZE, Integer.toString(10));
        params = BaseAPIParameters.buildFromWrapper(wrap);
        response = MockDataPaginationHandler.paginateData(params, data);
        paginatedData = response.getEntity();

        Assertions.assertTrue(paginatedData.isEmpty());
        Assertions.assertNull(response.getLink("next"));
        Assertions.assertNotNull(response.getLink("first"));
        Assertions.assertNotNull(response.getLink("last"));
        Assertions.assertTrue(response.getLink("last").getUri().getQuery().contains("page=20&pagesize=10"));
        Assertions.assertTrue(response.getLink("self").getUri().getQuery().contains("page=21&pagesize=10"));
    }

    private List<String> generateData(int numItems) {
        List<String> out = new ArrayList<>(numItems);
        for (int i = 0; i < numItems; i++) {
            out.add("item-" + i);
        }
        return out;
    }
}
