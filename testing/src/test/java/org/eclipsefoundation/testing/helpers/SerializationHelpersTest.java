/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin<zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.testing.helpers;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.GZIPInputStream;

import jakarta.inject.Inject;

import org.eclipsefoundation.testing.test.models.TestModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class SerializationHelpersTest {

    @Inject
    SerializationHelpers serialization;
    @Inject
    ObjectMapper objectMapper;

    @Test
    void convert_success_GZIPContent() {
        try {
            List<TestModel> toCompress = new LinkedList<TestModel>();

            toCompress.add(TestModel.builder().setName("Jim Bob").setMail("fake@mail.com").build());
            toCompress.add(TestModel.builder().setName("Bim Job").setMail("false@mail.com").build());

            String listAsString = objectMapper.writeValueAsString(toCompress);
            List<TestModel> decompressed = objectMapper
                    .readerForListOf(TestModel.class)
                    .readValue(new GZIPInputStream(SerializationHelpers.writeAsGZIP(listAsString)));

            Assertions.assertEquals(toCompress.get(0), decompressed.get(0));
            Assertions.assertEquals(toCompress.get(1), decompressed.get(1));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void convertToJsonString_success_convertsToJson() {
        // hardcode the expected JSON as it shouldn't change
        Assertions
                .assertEquals("{\"name\":\"Sampleson\",\"mail\":\"sample@mail.com\"}",
                        serialization.convertToJsonString(TestModel.builder().setMail("sample@mail.com").setName("Sampleson").build()));
    }

    @Test
    void convertToJsonString_success_handlesNulls() {
        Assertions.assertNull(serialization.convertToJsonString(null));
    }
}
