/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *          Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.testing.models;

import java.util.Map;
import java.util.Optional;

import jakarta.inject.Inject;
import jakarta.ws.rs.HttpMethod;
import jakarta.ws.rs.core.Response.Status;

import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.test.models.TestModel;
import org.eclipsefoundation.testing.test.namespaces.SchemaNamespaceHelper;
import org.eclipsefoundation.testing.test.resources.SampleResource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.restassured.module.jsv.JsonSchemaValidationException;
import io.restassured.response.ValidatableResponse;

/**
 * Tests for the EndpointTestBuilder to validate functionality of the pattern.
 * 
 * @author Martin Lowe, Zachary Sabourin
 *
 */
@QuarkusTest
class EndpointTestBuilderTest {

    private static final Map<String, Object> TEST_HEADER = Map.of(SampleResource.TEST_HEADER_FIELD, "header-val");

    private static final EndpointTestCase SUCCESS_GET = TestCaseHelper
            .prepareTestCase("test/get?returned_status={code}", new String[] { Integer.toString(Status.OK.getStatusCode()) }, null)
            .setHeaderParams(Optional.of(TEST_HEADER))
            .setHeaderValidationParams(TEST_HEADER)
            .build();
    private static final EndpointTestCase GET_NOT_FOUND = TestCaseHelper
            .prepareTestCase("nope", new String[] {}, SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setStatusCode(Status.NOT_FOUND.getStatusCode())
            .setHeaderParams(Optional.of(Map.of("Accept", "application/json")))
            .setBodyValidationParams(Map.of("status_code", Status.NOT_FOUND.getStatusCode()))
            .build();
    private static final EndpointTestCase GET_BAD_REQUEST = TestCaseHelper
            .buildBadRequestCase("test/get?returned_status={code}", new String[] { Integer.toString(Status.BAD_REQUEST.getStatusCode()) },
                    null);

    private static final EndpointTestCase SUCCESS_PUT = TestCaseHelper
            .buildSuccessCase("test/put?returned_status={code}", new String[] { Integer.toString(Status.OK.getStatusCode()) }, null);
    private static final EndpointTestCase SUCCESS_POST = TestCaseHelper
            .buildSuccessCase("test/post?returned_status={code}", new String[] { Integer.toString(Status.OK.getStatusCode()) }, null);
    private static final EndpointTestCase SUCCESS_DELETE = TestCaseHelper
            .buildSuccessCase("test/delete?returned_status={code}", new String[] { Integer.toString(Status.OK.getStatusCode()) }, null);

    private static final EndpointTestCase SUCCESS_PUT_WITH_BODY = TestCaseHelper
            .prepareTestCase("test/put/with-body?returned_status={code}", new String[] { Integer.toString(Status.OK.getStatusCode()) },
                    SchemaNamespaceHelper.TEST_MODEL_SCHEMA_PATH)
            .setBodyValidationParams(Map.of("name", "Sampleson"))
            .build();
    private static final EndpointTestCase SUCCESS_POST_WITH_BODY = TestCaseHelper
            .buildSuccessCase("test/post/with-body?returned_status={code}", new String[] { Integer.toString(Status.OK.getStatusCode()) },
                    SchemaNamespaceHelper.TEST_MODEL_SCHEMA_PATH);
    private static final EndpointTestCase SUCCESS_DELETE_WITH_BODY = TestCaseHelper
            .buildSuccessCase("test/delete/with-body?returned_status={code}", new String[] { Integer.toString(Status.OK.getStatusCode()) },
                    SchemaNamespaceHelper.TEST_MODEL_SCHEMA_PATH);

    private static final EndpointTestCase SUCCESS_GET_XML_WITH_BODY = TestCaseHelper
            .prepareTestCase("test/get/with-body/xml?returned_status={code}", new String[] { Integer.toString(Status.OK.getStatusCode()) },
                    SchemaNamespaceHelper.TEST_MODEL_SCHEMA_PATH)
            .setResponseContentType(ContentType.XML)
            .build();
    private static final EndpointTestCase SUCCESS_GET_XML_WITH_BODY_406_JSON_ACCEPT = TestCaseHelper
            .buildInvalidResponseFormatCase("test/get/with-body/xml?returned_status={code}",
                    new String[] { Integer.toString(Status.OK.getStatusCode()) }, null, ContentType.JSON);

    /*
     * Test cases used to validate bad test case values
     */
    private static final EndpointTestCase FAILURE_INVALID_VALIDATION_PARAMS = TestCaseHelper
            .prepareTestCase("test/get?returned_status={code}", new String[] { Integer.toString(Status.OK.getStatusCode()) }, null)
            .setBodyValidationParams(Map.of("name", "notSampleson"))
            .setHeaderParams(Optional.of(TEST_HEADER))
            .setHeaderValidationParams(Map.of(SampleResource.TEST_HEADER_FIELD, "nope"))
            .build();
    private static final EndpointTestCase FAILURE_INVALID_SCHEMA = TestCaseHelper
            .buildSuccessCase("test/get/with-body?returned_status={code}", new String[] { Integer.toString(Status.OK.getStatusCode()) },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH);
    private static final EndpointTestCase FAILURE_INVALID_STATUS_CODE = TestCaseHelper
            .buildSuccessCase("test/get?returned_status={code}", new String[] { Integer.toString(Status.NOT_FOUND.getStatusCode()) },
                    SchemaNamespaceHelper.TEST_MODEL_SCHEMA_PATH);

    @Inject
    ObjectMapper om;

    @Test
    void run_success() {
        Assertions.assertNotNull(EndpointTestBuilder.from(SUCCESS_GET).run());
    }

    @Test
    void run_success_handlesPut() {
        Assertions.assertNotNull(EndpointTestBuilder.from(SUCCESS_PUT).doPut().run());
    }

    @Test
    void run_success_handlesPost() {
        Assertions.assertNotNull(EndpointTestBuilder.from(SUCCESS_POST).doPost().run());
    }

    @Test
    void run_success_handlesDelete() {
        Assertions.assertNotNull(EndpointTestBuilder.from(SUCCESS_DELETE).doDelete().run());
    }

    @Test
    void run_success_handlesPut_withBody() {
        TestModel m = buildSample();
        // do the test and capture the output
        ValidatableResponse r = EndpointTestBuilder.from(SUCCESS_PUT_WITH_BODY).doPut(m).run();
        // check that the response is as expected
        Assertions.assertEquals(m, r.extract().as(TestModel.class), "Expected body to be properly passed and returned");
    }

    @Test
    void run_success_handlesPost_withBody() {
        TestModel m = buildSample();
        Assertions.assertNotNull(EndpointTestBuilder.from(SUCCESS_POST_WITH_BODY).doPost(m).run());
    }

    @Test
    void run_success_handlesDelete_withBody() {
        TestModel m = buildSample();
        Assertions.assertNotNull(EndpointTestBuilder.from(SUCCESS_DELETE_WITH_BODY).doDelete(m).run());
    }

    @Test
    void run_success_checkFormatInspectsResponseFormat() {
        Assertions
                .assertNotNull(EndpointTestBuilder.from(SUCCESS_GET_XML_WITH_BODY).doGet(buildSample()).andCheckFormat().run(),
                        "There should be a response for a successful test run");

        // When accepting JSON, even if this is unsupported by the resource, our Error will always return as JSON due to our
        // mappers
        Assertions
                .assertNotNull(
                        EndpointTestBuilder.from(SUCCESS_GET_XML_WITH_BODY_406_JSON_ACCEPT).doGet(buildSample()).andCheckFormat().run(),
                        "There should be a response for a successful test run");
    }

    @Test
    void run_success_checkSchemaInspectsSchema() {
        Assertions
                .assertNotNull(EndpointTestBuilder.from(SUCCESS_POST_WITH_BODY).doPost(buildSample()).andCheckSchema().run(),
                        "There should be a response for a successful test run");

        // Ensuring that a test with a mapped error response can also validate the schema
        Assertions
                .assertNotNull(EndpointTestBuilder.from(GET_NOT_FOUND).doGet(buildSample()).andCheckSchema().run(),
                        "There should be a response for a successful test run");
    }

    @Test
    void run_success_checkBodyParamsInspectsBodyParams() {
        Assertions
                .assertNotNull(EndpointTestBuilder.from(SUCCESS_PUT_WITH_BODY).doPut(buildSample()).andCheckBodyParams().run(),
                        "There should be a response for a successful test run");

        // Ensuring that a test with a mapped error response can also validate the body params
        Assertions
                .assertNotNull(EndpointTestBuilder.from(GET_NOT_FOUND).doGet(buildSample()).andCheckBodyParams().run(),
                        "There should be a response for a successful test run");
    }

    @Test
    void run_success_checkHeaderParamsInspectsHeaderParams() {
        Assertions
                .assertNotNull(EndpointTestBuilder.from(SUCCESS_GET).doGet().andCheckHeaderParams().run(),
                        "There should be a response for a successful test run");
    }

    /*
     * Checking a schema that is invalid should throw a {@link JsonSchemaValidationException}
     */
    @Test
    void run_failure_checkSchemaInspectsSchema() {
        // Error response checking against the Status.OK.getStatusCode() entity schema
        EndpointTestBuilder preparedTest = EndpointTestBuilder.from(FAILURE_INVALID_SCHEMA).doGet().andCheckSchema();
        Assertions.assertThrows(JsonSchemaValidationException.class, preparedTest::run);
    }

    /*
     * Checking a status that is invalid should throw a {@link AssertionError}
     */
    @Test
    void run_failure_inspectsStatus() {
        // Status.OK.getStatusCode() test where the return is an error
        EndpointTestBuilder preparedTest = EndpointTestBuilder.from(FAILURE_INVALID_STATUS_CODE).doGet();
        Assertions.assertThrows(AssertionError.class, preparedTest::run);
    }

    /*
     * Using invalid body validation params, or setting 'checkBodyParams' without any provided values should throw a {@link
     * AssertionError}
     */
    @Test
    void run_failure_checkBodyInspectsBody() {
        EndpointTestBuilder preparedTest = EndpointTestBuilder
                .from(FAILURE_INVALID_VALIDATION_PARAMS)
                .doPost(buildSample())
                .andCheckBodyParams();
        Assertions.assertThrows(AssertionError.class, preparedTest::run);

        // body params set to be validated, no params provided
        preparedTest = EndpointTestBuilder.from(SUCCESS_POST).doPost(buildSample()).andCheckBodyParams();
        Assertions.assertThrows(AssertionError.class, preparedTest::run);
    }

    /*
     * Using invalid header params, or setting 'checkHeaderParams' without any provided values should throw a {@link
     * AssertionError}
     */
    @Test
    void run_failure_checkHeaderInspectsHeader() {
        EndpointTestBuilder preparedTest = EndpointTestBuilder.from(FAILURE_INVALID_VALIDATION_PARAMS).doGet().andCheckHeaderParams();
        Assertions.assertThrows(AssertionError.class, preparedTest::run);

        // header params set to be validated, no params provided
        preparedTest = EndpointTestBuilder.from(SUCCESS_POST).doPost().andCheckHeaderParams();
        Assertions.assertThrows(AssertionError.class, preparedTest::run);
    }

    @Test
    void run_failure_detectsNonSuccessCases() {
        Assertions.assertNotNull(EndpointTestBuilder.from(GET_NOT_FOUND).run());
        Assertions.assertNotNull(EndpointTestBuilder.from(GET_BAD_REQUEST).run());
    }

    @Test
    void from_getDefaultHttpMethod() {
        Assertions
                .assertEquals(HttpMethod.GET, EndpointTestBuilder.from(SUCCESS_GET).getMethodName(),
                        "GET HTTP method should be the default for the test builder");
    }

    @Test
    void from_checkSchemaDisabledByDefault() {
        Assertions
                .assertFalse(EndpointTestBuilder.from(SUCCESS_GET).isCheckSchema(),
                        "Schema check should be disabled by default for the test builder");
    }

    @Test
    void from_checkFormatDisabledByDefault() {
        Assertions
                .assertFalse(EndpointTestBuilder.from(SUCCESS_GET).isCheckFormat(),
                        "Format check should be disabled by default for the test builder");
    }

    @Test
    void from_checkBodyDisabledByDefault() {
        Assertions
                .assertFalse(EndpointTestBuilder.from(SUCCESS_GET).isCheckBodyParams(),
                        "Body parameter check should be disabled by default for the test builder");
    }

    @Test
    void from_checkHeaderDisabledByDefault() {
        Assertions
                .assertFalse(EndpointTestBuilder.from(SUCCESS_GET).isCheckHeaderParams(),
                        "Header parameter check should be disabled by default for the test builder");
    }

    @Test
    void doGet_setsMethodToGet() {
        // while not intended usage, different HTTP methods can be called to override
        // previous
        EndpointTestBuilder sample = EndpointTestBuilder.from(SUCCESS_GET).doPut();
        Assertions.assertNotEquals(HttpMethod.GET, sample.getMethodName());
        // do the update to the test builder
        sample = sample.doGet();
        Assertions.assertEquals(HttpMethod.GET, sample.getMethodName());
    }

    @Test
    void doPut_setsMethodToPut() {
        // the endpoint doesn't matter as the test won't actually be running
        EndpointTestBuilder sample = EndpointTestBuilder.from(SUCCESS_GET).doPut();
        Assertions.assertEquals(HttpMethod.PUT, sample.getMethodName());
    }

    @Test
    void doPost_setsMethodToPost() {
        // the endpoint doesn't matter as the test won't actually be running
        EndpointTestBuilder sample = EndpointTestBuilder.from(SUCCESS_GET).doPost();
        Assertions.assertEquals(HttpMethod.POST, sample.getMethodName());
    }

    @Test
    void doDelete_setsMethodToDelete() {
        // the endpoint doesn't matter as the test won't actually be running
        EndpointTestBuilder sample = EndpointTestBuilder.from(SUCCESS_GET).doDelete();
        Assertions.assertEquals(HttpMethod.DELETE, sample.getMethodName());
    }

    @Test
    void andCheckFormat_setsCheckFormatFlagToTrue() {
        Assertions
                .assertTrue(EndpointTestBuilder.from(SUCCESS_GET).andCheckFormat().isCheckFormat(),
                        "The andCheckFormat method should set the checkFormat flag to true");
    }

    @Test
    void andCheckSchema_setsCheckSchemaFlagToTrue() {
        Assertions
                .assertTrue(EndpointTestBuilder.from(SUCCESS_GET).andCheckSchema().isCheckSchema(),
                        "The andCheckSchema method should set the checkSchema flag to true");
    }

    @Test
    void andCheckBodyParams_setsCheckBodyParamsFlagToTrue() {
        Assertions
                .assertTrue(EndpointTestBuilder.from(SUCCESS_GET).andCheckBodyParams().isCheckBodyParams(),
                        "The andCheckBodyParams method should set the checkBodyParams flag to true");
    }

    @Test
    void andCheckHeaderParams_setsCheckHeaderParamsFlagToTrue() {
        Assertions
                .assertTrue(EndpointTestBuilder.from(SUCCESS_GET).andCheckHeaderParams().isCheckHeaderParams(),
                        "The andCheckHeaderParams method should set the checkHeaderParams flag to true");
    }

    private TestModel buildSample() {
        return TestModel.builder().setMail("sample@sample.com").setName("Sampleson").build();
    }
}
