/*********************************************************************
* Copyright (c) 2020 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.testing.test.models;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

import jakarta.annotation.Nullable;

@AutoValue
@JsonDeserialize(builder = AutoValue_TestModel.Builder.class)
public abstract class TestModel implements Serializable {
    @Nullable
    public abstract String getName();

    @Nullable
    public abstract String getMail();

    public static Builder builder() {
        return new AutoValue_TestModel.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setName(@Nullable String name);

        public abstract Builder setMail(@Nullable String mail);

        public abstract TestModel build();
    }
}