/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.testing.test.resources;

import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import org.eclipsefoundation.testing.test.models.TestModel;

/**
 * Sample resource that uses each of the supported HTTP methods for a distinct endpoint to enable basic testing.
 * 
 * @author Martin Lowe
 */
@Path("test")
@Produces(MediaType.APPLICATION_JSON)
public class SampleResource {

    public static final String TEST_HEADER_FIELD = "Test-Header";

    @GET
    @Path("get")
    public Response get(@HeaderParam(TEST_HEADER_FIELD) String testHeader, @QueryParam("returned_status") Integer status) {
        return Response.status(getStatusIf200Series(status)).header(TEST_HEADER_FIELD, testHeader).build();
    }

    @POST
    @Path("post")
    public Response post(@HeaderParam(TEST_HEADER_FIELD) String testHeader, @QueryParam("returned_status") Integer status) {
        return Response.status(getStatusIf200Series(status)).header(TEST_HEADER_FIELD, testHeader).build();
    }

    @PUT
    @Path("put")
    public Response put(@HeaderParam(TEST_HEADER_FIELD) String testHeader, @QueryParam("returned_status") Integer status) {
        return Response.status(getStatusIf200Series(status)).header(TEST_HEADER_FIELD, testHeader).build();
    }

    @DELETE
    @Path("delete")
    public Response delete(@HeaderParam(TEST_HEADER_FIELD) String testHeader, @QueryParam("returned_status") Integer status) {
        return Response.status(getStatusIf200Series(status)).header(TEST_HEADER_FIELD, testHeader).build();
    }

    @GET
    @Path("get/with-body")
    public Response getWithBody(@HeaderParam(TEST_HEADER_FIELD) String testHeader, @QueryParam("returned_status") Integer status,
            TestModel body) {
        return Response.status(getStatusIf200Series(status)).entity(body).header(TEST_HEADER_FIELD, testHeader).build();
    }

    @GET
    @Path("get/with-body/xml")
    @Produces(MediaType.APPLICATION_XML)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getWithBodyAndXmlFormat(@HeaderParam(TEST_HEADER_FIELD) String testHeader,
            @QueryParam("returned_status") Integer status, TestModel body) {
        return Response
                .status(getStatusIf200Series(status))
                .entity(body)
                .header("Content-Type", MediaType.APPLICATION_XML)
                .header(TEST_HEADER_FIELD, testHeader)
                .build();
    }

    @POST
    @Path("post/with-body")
    public Response postWithBody(@HeaderParam(TEST_HEADER_FIELD) String testHeader, @QueryParam("returned_status") Integer status,
            TestModel body) {
        return Response.status(getStatusIf200Series(status)).entity(body).header(TEST_HEADER_FIELD, testHeader).build();
    }

    @PUT
    @Path("put/with-body")
    public Response putWithBody(@HeaderParam(TEST_HEADER_FIELD) String testHeader, @QueryParam("returned_status") Integer status,
            TestModel body) {
        return Response.status(getStatusIf200Series(status)).entity(body).header(TEST_HEADER_FIELD, testHeader).build();
    }

    @DELETE
    @Path("delete/with-body")
    public Response deleteWithBody(@HeaderParam(TEST_HEADER_FIELD) String testHeader, @QueryParam("returned_status") Integer status,
            TestModel body) {
        return Response.status(getStatusIf200Series(status)).entity(body).header(TEST_HEADER_FIELD, testHeader).build();
    }

    // Throw a Status.BAD_REQUEST.getStatusCode() if not a 200-series code. Used to simulate how our errors are returned.
    private Integer getStatusIf200Series(Integer status) {
        if (status.intValue() < Status.OK.getStatusCode() || status.intValue() >= Status.MOVED_PERMANENTLY.getStatusCode()) {
            throw new BadRequestException("Invalid status passed");
        }
        return status;
    }
}
