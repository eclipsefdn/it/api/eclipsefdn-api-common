/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *      Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.testing.models;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.CoreMatchers.is;

import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;

import org.eclipse.microprofile.config.ConfigProvider;
import org.eclipsefoundation.testing.helpers.AuthHelper;
import org.eclipsefoundation.utils.namespace.MicroprofilePropertyNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import jakarta.ws.rs.HttpMethod;

/**
 * <p>
 * Using generic test definitions, generate and run tests based on current test parameters, allowing for HTTP method to be configured, as
 * well as optional flags to check the response format or schema. The instance uses a chaining pattern for more fluent usage in tests.
 * 
 * <p>
 * Example use cases below, given a valid {@link EndpointTestCase} is provided:
 * 
 * <p>
 * <code>
 * EndpointTestBuilder.from(testCase).run()
 * EndpointTestBuilder.from(testCase).doPut(someRequestBody).run()
 * EndpointTestBuilder.from(testCase).doPost().andCheckSchema().run()
 * </code>
 * 
 * @author Martin Lowe
 *
 */
public final class EndpointTestBuilder {
    private static final Logger LOGGER = LoggerFactory.getLogger(EndpointTestBuilder.class);

    private EndpointTestCase testDef;
    private Object body;
    private String methodName;
    private boolean checkSchema;
    private boolean checkFormat;
    private boolean checkBodyParams;
    private boolean checkHeaderParams;

    /**
     * Generate the test base from the definition with generic test defaults.
     * 
     * @param testDef test case definition for the current test
     */
    private EndpointTestBuilder(EndpointTestCase testDef) {
        this.testDef = testDef;
        this.body = null;
        this.methodName = HttpMethod.GET;
        this.checkSchema = false;
        this.checkFormat = false;
        this.checkBodyParams = false;
        this.checkHeaderParams = false;
    }

    /**
     * Construct and return an endpoint test builder for the given test.
     * 
     * @param testDef test case definition for the current test
     * @return the base endpoint test for the given test case with non-intrusive defaults set.
     */
    public static EndpointTestBuilder from(EndpointTestCase testDef) {
        return new EndpointTestBuilder(testDef);
    }

    /**
     * Set the test to perform a GET with the passed test parameters.
     * 
     * @return the current test builder instance for chaining.
     */
    public EndpointTestBuilder doGet() {
        this.methodName = HttpMethod.GET;
        return this;
    }

    /**
     * Set the test to perform a GET with the passed test parameters using the given body.
     * 
     * @param body the content to use as the request body for the request
     * @return the current test builder instance for chaining.
     */
    public EndpointTestBuilder doGet(Object body) {
        this.methodName = HttpMethod.GET;
        return withBody(body);
    }

    /**
     * Set the test to perform a PUT with the passed test parameters.
     * 
     * @return the current test builder instance for chaining.
     */
    public EndpointTestBuilder doPut() {
        this.methodName = HttpMethod.PUT;
        return this;
    }

    /**
     * Set the test to perform a PUT with the passed test parameters using the given body.
     * 
     * @param body the content to use as the request body for the request
     * @return the current test builder instance for chaining.
     */
    public EndpointTestBuilder doPut(Object body) {
        this.methodName = HttpMethod.PUT;
        return withBody(body);
    }

    /**
     * Set the test to perform a POST with the passed test parameters.
     * 
     * @return the current test builder instance for chaining.
     */
    public EndpointTestBuilder doPost() {
        this.methodName = HttpMethod.POST;
        return this;
    }

    /**
     * Set the test to perform a POST with the passed test parameters using the given body.
     * 
     * @param body the content to use as the request body for the request
     * @return the current test builder instance for chaining.
     */
    public EndpointTestBuilder doPost(Object body) {
        this.methodName = HttpMethod.POST;
        return withBody(body);
    }

    /**
     * Set the test to perform a DELETE with the passed test parameters.
     * 
     * @return the current test builder instance for chaining.
     */
    public EndpointTestBuilder doDelete() {
        this.methodName = HttpMethod.DELETE;
        return this;
    }

    /**
     * Set the test to perform a DELETE with the passed test parameters using the given body.
     * 
     * @param body the content to use as the request body for the request
     * @return the current test builder instance for chaining.
     */
    public EndpointTestBuilder doDelete(Object body) {
        this.methodName = HttpMethod.DELETE;
        return withBody(body);
    }

    /**
     * Set the test to perform the schema validation on run.
     * 
     * @return the current test builder instance for chaining.
     */
    public EndpointTestBuilder andCheckSchema() {
        this.checkSchema = true;
        return this;
    }

    /**
     * Set the test to perform the format validation on run.
     * 
     * @return the current test builder instance for chaining.
     */
    public EndpointTestBuilder andCheckFormat() {
        this.checkFormat = true;
        return this;
    }

    /**
     * Set the test to perform the body field/value validation on run.
     * 
     * @return the current test builder instance for chaining.
     */
    public EndpointTestBuilder andCheckBodyParams() {
        this.checkBodyParams = true;
        return this;
    }

    /**
     * Set the test to perform the header field/value validation on run.
     * 
     * @return the current test builder instance for chaining.
     */
    public EndpointTestBuilder andCheckHeaderParams() {
        this.checkHeaderParams = true;
        return this;
    }

    // Handle appending a body to the request
    private EndpointTestBuilder withBody(Object body) {
        this.body = body;
        return this;
    }

    /**
     * Asserts that the given ValidatableResponse body contains the desired key/value pairs. Throwing a {@link AssertionError} on a failed
     * assertion. If all is matching, returns the processed ValidatableResponse for further processing.
     * 
     * @param response The response to process
     * @return The processed ValidatableResponse object
     */
    private ValidatableResponse assertResponseBody(ValidatableResponse response) {
        Map<String, Object> params = testDef.getBodyValidationParams();
        if (params != null) {
            for (Entry<String, Object> entry : params.entrySet()) {
                response = response.body(entry.getKey(), is(entry.getValue()));
            }
        } else {
            throw new AssertionError("checkBodyParams was set, but no validation params were provided");
        }
        return response;
    }

    /**
     * Asserts that the given ValidatableResponse headers contains the desired key/value pairs. Throwing a {@link AssertionError} on a
     * failed assertion. If all is matching, returns the processed ValidatableResponse for further processing.
     * 
     * @param response The response to process
     * @return The processed ValidatableResponse object
     */
    private ValidatableResponse assertHeaderParams(ValidatableResponse response) {
        Map<String, Object> params = testDef.getHeaderValidationParams();
        if (params != null) {
            for (Entry<String, Object> entry : params.entrySet()) {
                response = response.header(entry.getKey(), is(entry.getValue()));
            }
        } else {
            throw new AssertionError("checkHeaderParams was set, but no validation params were provided");
        }
        return response;
    }

    /**
     * Run the current test, using the previously set flags and HTTP method, running assertions to confirm the parameters set into the test
     * case definition. Resulting response from RESTassured is returned for potential further processing if needed.
     * 
     * @return the REST response from the test for further processing if needed
     */
    public ValidatableResponse run() {

        // Get the constructed ValidatableResponse
        ValidatableResponse response = getValidatedResponse(testDef, methodName, body);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("RESPONSE: {}, HEADERS: {}", response.extract().asPrettyString(), response.extract().headers());
        }

        // Assert against the status before allowing other checks
        response = response.assertThat().statusCode(testDef.getStatusCode());

        // check body values when set
        if (checkBodyParams) {
            response = assertResponseBody(response);
        }

        // check header values when set
        if (checkHeaderParams) {
            response = assertHeaderParams(response);
        }

        // check schema when set
        if (checkSchema) {
            response = response.assertThat().body(matchesJsonSchemaInClasspath(testDef.getSchemaLocation()));
        }

        // check format when set
        if (checkFormat) {
            response = response.assertThat().contentType(testDef.getResponseContentType());
        }

        return response;
    }


    private ValidatableResponse getValidatedResponse(EndpointTestCase testCase, String methodName, Object body) {
        Optional<Boolean> isCsrfEnabled = ConfigProvider.getConfig()
                .getOptionalValue(MicroprofilePropertyNames.CSRF_ENABLED, Boolean.class);

        // setup the base request, adding the body if present and setting the CSRF
        RequestSpecification baseRequest;
        if (!testCase.getDisableCsrf() && isCsrfEnabled.isPresent() && Boolean.TRUE.equals(isCsrfEnabled.get())) {
            baseRequest = addBodyIfPresent(AuthHelper.getCSRFDefinedResteasyRequest(), body, testCase);
        } else {
            baseRequest = addBodyIfPresent(given(), body, testCase);
        }

        // check that the response type is set before setting the accept header
        if (testCase.getResponseContentType() != null) {
            baseRequest.accept(testCase.getResponseContentType());
        }

        switch (methodName) {
            case HttpMethod.POST:
                return baseRequest
                        .contentType(testCase.getRequestContentType())
                        .headers(testCase.getHeaderParams().orElse(Collections.emptyMap()))
                        .post(testCase.getPath(), testCase.getParams().orElse(new Object[] {}))
                        .then();
            case HttpMethod.PUT:
                return baseRequest
                        .contentType(testCase.getRequestContentType())
                        .headers(testCase.getHeaderParams().orElse(Collections.emptyMap()))
                        .put(testCase.getPath(), testCase.getParams().orElse(new Object[] {}))
                        .then();
            case HttpMethod.DELETE:
                return baseRequest
                        .contentType(testCase.getRequestContentType())
                        .headers(testCase.getHeaderParams().orElse(Collections.emptyMap()))
                        .delete(testCase.getPath(), testCase.getParams().orElse(new Object[] {}))
                        .then();
            case HttpMethod.GET:
            default:
                return baseRequest
                        .contentType(testCase.getRequestContentType())
                        .headers(testCase.getHeaderParams().orElse(Collections.emptyMap()))
                        .get(testCase.getPath(), testCase.getParams().orElse(new Object[] {}))
                        .then();
        }
    }

    /**
     * We out of the box support JSON and have it mandated in our API best practices. If the code breaks this pattern, then you are either
     * doing something wrong, should write more tailor-made tests, or skip testing in the case of HTML endpoints that are complicated to
     * test.
     * 
     * @param spec     the current request spec in which the body should be added if
     *                 present
     * @param body     the body of the request to include, may be null
     * @param testCase the test case used to set the content type
     * @return the current request, wrapped with the json body content if it is set.
     */
    private RequestSpecification addBodyIfPresent(RequestSpecification spec, Object body,
            EndpointTestCase testCase) {
        return body == null ? spec : spec.body(body).contentType(testCase.getRequestContentType());
    }
    
    /**
     * @return the testDef
     */
    EndpointTestCase getTestDef() {
        return testDef;
    }

    /**
     * @return the body
     */
    Object getBody() {
        return body;
    }

    /**
     * @return the methodName
     */
    String getMethodName() {
        return methodName;
    }

    /**
     * @return the checkSchema
     */
    boolean isCheckSchema() {
        return checkSchema;
    }

    /**
     * @return the checkFormat
     */
    boolean isCheckFormat() {
        return checkFormat;
    }

    /**
     * @return the checkBodyParams
     */
    boolean isCheckBodyParams() {
        return checkBodyParams;
    }

    /**
     * @return the checkHeaderParams
     */
    boolean isCheckHeaderParams() {
        return checkHeaderParams;
    }

    @Override
    public int hashCode() {
        return Objects.hash(body, checkFormat, checkSchema, checkBodyParams, checkHeaderParams, methodName, testDef);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        EndpointTestBuilder other = (EndpointTestBuilder) obj;
        return Objects.equals(body, other.body) && checkFormat == other.checkFormat && checkSchema == other.checkSchema
                && checkBodyParams == other.checkBodyParams && checkHeaderParams == other.checkHeaderParams
                && Objects.equals(methodName, other.methodName) && Objects.equals(testDef, other.testDef);
    }
}
