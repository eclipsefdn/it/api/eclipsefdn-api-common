/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.testing.models;

import java.util.Map;
import java.util.Optional;

import jakarta.annotation.Nullable;
import jakarta.ws.rs.core.Response.Status;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

import io.restassured.http.ContentType;

/**
 * Represents test cases for a given endpoint, including path, parameters, schema location, and some extra test settings
 * for specific cases.
 * 
 * @author Martin Lowe
 *
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_EndpointTestCase.Builder.class)
public abstract class EndpointTestCase {
    public abstract String getPath();

    public abstract Optional<Object[]> getParams();

    @Nullable
    public abstract String getSchemaLocation();

    public abstract boolean getDisableCsrf();

    public abstract ContentType getRequestContentType();

    public abstract ContentType getResponseContentType();

    public abstract Integer getStatusCode();

    @Nullable
    public abstract Map<String, Object> getBodyValidationParams();

    @Nullable
    public abstract Map<String, Object> getHeaderValidationParams();

    public abstract Optional<Map<String, Object>> getHeaderParams();

    public static Builder builder() {
        return new AutoValue_EndpointTestCase.Builder()
                .setDisableCsrf(false)
                .setResponseContentType(ContentType.JSON)
                .setRequestContentType(ContentType.JSON)
                .setStatusCode(Status.OK.getStatusCode());
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setPath(String organizationId);

        public abstract Builder setParams(Optional<Object[]> params);

        public abstract Builder setSchemaLocation(@Nullable String schemaLocation);

        public abstract Builder setDisableCsrf(Boolean disableCsrf);

        public abstract Builder setRequestContentType(ContentType contentType);

        public abstract Builder setResponseContentType(ContentType contentType);

        public abstract Builder setStatusCode(Integer statusCode);

        public abstract Builder setBodyValidationParams(@Nullable Map<String, Object> params);

        public abstract Builder setHeaderValidationParams(@Nullable Map<String, Object> params);

        public abstract Builder setHeaderParams(Optional<Map<String, Object>> params);

        public abstract EndpointTestCase build();
    }
}
