/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.testing.helpers;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.jboss.resteasy.reactive.RestResponse;

import jakarta.ws.rs.core.Link;
import jakarta.ws.rs.core.UriBuilder;

public class MockDataPaginationHandler {
    private static final int DEFAULT_PAGE_SIZE = 100;
    private static final URI PAGINATION_URI = URI.create("http://api.eclipse.org/test?page=1&pagesize=100");

    /**
     * Paginates through a list of type T using the provided pagination parameters from the BaseAPIParemeters argument. Generates a
     * RestResponse containing the paginated items, as well as the appropriate link headers for each page.
     * 
     * @param <T> The raw type returned
     * @param params The incoming pagination parameters.
     * @param data The data to paginate.
     * @return A RestResponse containing a slice of the provided data based on incoming pagination parameters.
     */
    public static <T> RestResponse<List<T>> paginateData(BaseAPIParameters params, List<T> data) {

        int pageSize = params.limit != null ? params.limit : DEFAULT_PAGE_SIZE;
        int lastPage = (int) Math.ceil((double) data.size() / pageSize);

        // Build link headers
        UriBuilder builder = UriBuilder.fromUri(PAGINATION_URI);
        List<Link> links = new ArrayList<>();
        links.add(Link.fromUri(buildHref(builder, 1, pageSize)).rel("first").title("first page of results").build());
        links.add(Link.fromUri(buildHref(builder, params.page, pageSize)).rel("self").title("this page of results").build());
        links.add(Link.fromUri(buildHref(builder, lastPage, pageSize)).rel("last").title("last page of results").build());
        if (params.page > 1) {
            links.add(Link.fromUri(buildHref(builder, params.page - 1, pageSize)).rel("prev").title("previous page of results").build());
        }
        if (params.page < lastPage) {
            links.add(Link.fromUri(buildHref(builder, params.page + 1, pageSize)).rel("next").title("next page of results").build());
        }

        // Paginate items and append link headers
        RestResponse<List<T>> out = RestResponse.ok(paginateItems(params.page, pageSize, data));
        out.getHeaders().put("link", links.stream().map(l -> (Object) l).toList());
        return out;
    }

    private static String buildHref(UriBuilder builder, int page, int pageSize) {
        return builder.replaceQueryParam("page", page).replaceQueryParam("pagesize", pageSize).build().toString();
    }

    /**
     * Paginates through a list of type T using the provided pagination parameters from the BaseAPIParemeters argument.
     * 
     * @param <T> The raw type returned
     * @param page The page to slice.
     * @param pagesize The sliced page size.
     * @param data The data to paginate.
     * @return A slice of the provided data based on incoming pagination parameters.
     */
    private static <T> List<T> paginateItems(int page, int pagesize, List<T> data) {

        // Use default page size if null, not going out of bounds
        int pageSize = pagesize;
        if (pageSize > data.size()) {
            pageSize = data.size();
        }

        // Increment the starting position based on page size
        int from = 0;
        if (page > 1) {
            from += pageSize * (page - 1);
        }

        // Ensure we don't start out of bounds, mimicking API behaviour when a page is set higher than the last available
        if (from >= data.size()) {
            return Collections.emptyList();
        }

        // Increase ending position based on page size. Ensure we don't extend out of bounds
        int to = (pageSize * page);
        if (to >= data.size()) {
            to = data.size();
        }

        return data.subList(from, to);
    }

    private MockDataPaginationHandler() {

    }
}
