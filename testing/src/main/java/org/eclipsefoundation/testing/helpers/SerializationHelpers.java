/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.testing.helpers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.zip.GZIPOutputStream;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Provides serialization helper methods. These method allow encoding values to various types.
 */
@Singleton
public class SerializationHelpers {

    @Inject
    ObjectMapper om;

    /**
     * Required for body of requests in tests, as RESTassured doesn't respect the config set for serializing content used in
     * the standard REST clients.
     * 
     * @param o the object to serialize
     * @return the serialized JSON string
     */
    public String convertToJsonString(Object o) {
        if (o == null) {
            return null;
        }
        try {
            return om.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            //
        }
        return null;
    }

    /**
     * GZIPs the input string and returns a ByteArrayInputStream containing the content.
     * 
     * @param content The GZIP'd content to convert to uncompressed data
     * @return the input stream containing the uncompressed data
     */
    public static ByteArrayInputStream writeAsGZIP(String content) {
        try (ByteArrayOutputStream byteOut = new ByteArrayOutputStream(); GZIPOutputStream gzipOut = new GZIPOutputStream(byteOut)) {
            // write the compressed byte content to the GZIP consumer
            gzipOut.write(content.getBytes());
            // close needs to be called explicitly as it is required to properly flush the content before output
            gzipOut.close();
            // use the GZIP consumer to convert the content into a standard IS for downstream consumption
            return new ByteArrayInputStream(byteOut.toByteArray());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
