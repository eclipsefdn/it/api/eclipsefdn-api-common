/*********************************************************************
* Copyright (c) 2022, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*           Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.deployment;

import io.quarkus.runtime.annotations.ConfigDocSection;
import io.quarkus.runtime.annotations.ConfigGroup;
import io.quarkus.runtime.annotations.ConfigItem;
import io.quarkus.runtime.annotations.ConfigRoot;

@ConfigRoot(name = "eclipse.persistence.deployment")
public class DeploymentPersistenceConfig {

    /**
     * contains data for the multi-source single-tenant settings.
     */
    @ConfigDocSection
    @ConfigItem
    public MultiSourceSingleTenantConfig msst;
    
    @ConfigGroup
    public static class MultiSourceSingleTenantConfig {
        /**
         * Whether the multi-source single tenant code should be enabled.
         */
        @ConfigItem(defaultValue = "false")
        public Boolean enabled;

        /**
         * The datasource name used for the secondary source used for read access.
         */
        @ConfigItem(defaultValue = "secondary")
        public String datasource;
    }
}