/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.deployment;

import java.util.Set;

import org.eclipsefoundation.persistence.config.EclipsePersistenceRuntimeConfig;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.dao.impl.PlaceholderPersistenceDao;
import org.eclipsefoundation.persistence.dao.impl.SecondaryAwareHibernateDao;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DistributedCSRFTokenFilter;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;
import org.eclipsefoundation.persistence.model.SqlBackedPaginationResolver;
import org.eclipsefoundation.persistence.providers.DistributedCSRFProvider;
import org.eclipsefoundation.persistence.providers.ParameterizedSQLStatementBuilderProvider;
import org.eclipsefoundation.persistence.service.impl.DefaultFilterService;
import org.jboss.jandex.DotName;

import io.quarkus.arc.deployment.AdditionalBeanBuildItem;
import io.quarkus.deployment.annotations.BuildProducer;
import io.quarkus.deployment.annotations.BuildStep;
import io.quarkus.deployment.builditem.nativeimage.RuntimeInitializedClassBuildItem;
import io.quarkus.hibernate.orm.deployment.IgnorableNonIndexedClasses;
import io.quarkus.hibernate.orm.deployment.JpaModelBuildItem;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.Dependent;
import jakarta.inject.Singleton;

public class QuarkusPersistenceProcessor {

    @BuildStep
    RuntimeInitializedClassBuildItem parameterizedSQLStatementBuilderConfiguration() {
        return new RuntimeInitializedClassBuildItem(ParameterizedSQLStatementBuilder.class.getCanonicalName());
    }

    /**
     * Register the required CDI beans into the application to be created at runtime. Currently this is only the Hibernate DAO, but this may
     * be extended in the future.
     * 
     * @param beanProducer binding point to add additional CDI beans
     * @param domainObjects JPA model used by hibernate to enhance entity model objects.
     */
    @BuildStep
    public void registerBeans(BuildProducer<AdditionalBeanBuildItem> beanProducer, JpaModelBuildItem domainObjects,
            DeploymentPersistenceConfig config) {

        // check if hibernate has been registered as an extension
        beanProducer
                .produce(AdditionalBeanBuildItem
                        .builder()
                        .setUnremovable()
                        .addBeanClass(ParameterizedSQLStatementBuilderProvider.class)
                        .build());
        beanProducer
                .produce(AdditionalBeanBuildItem
                        .builder()
                        .setUnremovable()
                        .addBeanClass(DefaultFilterService.class)
                        .setDefaultScope(DotName.createSimple(ApplicationScoped.class.getName()))
                        .build());

        if (hasEntities(domainObjects)) {
            // register the Hibernate DAO provider
            if (Boolean.TRUE.equals(config.msst.enabled)) {
                beanProducer
                        .produce(AdditionalBeanBuildItem.builder().setUnremovable().addBeanClass(SecondaryAwareHibernateDao.class).build());
            } else {
                beanProducer.produce(AdditionalBeanBuildItem.builder().setUnremovable().addBeanClass(DefaultHibernateDao.class).build());
            }

            beanProducer
                    .produce(AdditionalBeanBuildItem
                            .builder()
                            .setUnremovable()
                            .addBeanClass(DistributedCSRFProvider.class)
                            .setDefaultScope(DotName.createSimple(Dependent.class.getName()))
                            .build());
            beanProducer
                    .produce(AdditionalBeanBuildItem
                            .builder()
                            .setUnremovable()
                            .addBeanClass(EclipsePersistenceRuntimeConfig.class)
                            .setDefaultScope(DotName.createSimple(Dependent.class.getName()))
                            .build());
            beanProducer
                    .produce(AdditionalBeanBuildItem
                            .builder()
                            .setUnremovable()
                            .addBeanClass(SqlBackedPaginationResolver.class)
                            .setDefaultScope(DotName.createSimple(Dependent.class.getName()))
                            .build());
            beanProducer
                    .produce(AdditionalBeanBuildItem
                            .builder()
                            .setUnremovable()
                            .addBeanClass(DistributedCSRFTokenFilter.class)
                            .setDefaultScope(DotName.createSimple(Singleton.class.getName()))
                            .build());

        } else {
            // register placeholder to unblock downstream dependencies. This can be removed
            // if a search extension is implemented
            beanProducer.produce(AdditionalBeanBuildItem.builder().setUnremovable().addBeanClass(PlaceholderPersistenceDao.class).build());
        }
    }

    /**
     * Marks the 2 node entity base classes as ignorable in the JPA entity scan by Hibernate ORM. Otherwise these entities get detected as
     * JPA entities improperly and cause build issues.
     * 
     * @return class index containing the abstract base entity classes.
     */
    @BuildStep
    IgnorableNonIndexedClasses registerNodeTypes() {
        return new IgnorableNonIndexedClasses(Set.of(BareNode.class.getName()));
    }

    private boolean hasEntities(JpaModelBuildItem jpaEntities) {
        return !jpaEntities.getEntityClassNames().isEmpty();
    }

}
