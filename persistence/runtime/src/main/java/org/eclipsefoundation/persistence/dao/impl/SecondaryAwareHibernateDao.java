/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.dao.impl;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.persistence.namespace.PersistencePropertyNames;
import org.eclipsefoundation.utils.namespace.MicroprofilePropertyNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.arc.Arc;
import io.quarkus.hibernate.orm.PersistenceUnit.PersistenceUnitLiteral;
import jakarta.persistence.EntityManager;

/**
 * <p>
 * Basic multi-connection single-tenant database connection. Used to better
 * manage read and write connections to database replicants more reliably. A
 * second datasource will need to be defined, as well as the setting
 * {@link MicroprofilePropertyNames.MULTI_TENANT_SINGLE_TENANT_ENABLED} should
 * be set to true to enable this DAO client. The second datasource should have a
 * tenant of "default-secondary" to properly bind to this class.
 * 
 * <p>
 * Once enabled, this client should automatically begin working with no extra
 * work as bindings have been taken care of in the core implementation.
 * 
 * @author Martin Lowe
 *
 */
public class SecondaryAwareHibernateDao extends DefaultHibernateDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(SecondaryAwareHibernateDao.class);

    @ConfigProperty(name = PersistencePropertyNames.MULTI_SOURCE_SINGLE_TENANT_DATASOURCE, defaultValue = "secondary")
    String secondaryDatasourceName;

    private EntityManager secondaryConnectionManager;

    @Override
    protected EntityManager getSecondaryEntityManager() {
        if (secondaryConnectionManager == null) {
            this.secondaryConnectionManager = Arc.container()
                    .instance(EntityManager.class, new PersistenceUnitLiteral(secondaryDatasourceName)).get();
            LOGGER.info("Retrieved secondary connection for datasource with name {}: {}", secondaryDatasourceName,
                    secondaryConnectionManager);
        }
        return secondaryConnectionManager;
    }
}
