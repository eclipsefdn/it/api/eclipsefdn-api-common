/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.model;

import java.util.Objects;

/**
 * Represents an object in the database. This should be deprecated to instead
 * read persistence annotations from the jakarta namespace.
 * 
 * @author Martin Lowe
 *
 */
public class DtoTable {

	private Class<?> baseClass;
	private String alias;

	public DtoTable(Class<?> baseClass, String alias) {
		this.baseClass = Objects.requireNonNull(baseClass);
		this.alias = Objects.requireNonNull(alias);
	}

	public Class<?> getType() {
		return this.baseClass;
	}

	public String getAlias() {
		return this.alias;
	}
}
