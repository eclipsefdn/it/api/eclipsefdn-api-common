/**
 * Copyright (c) 2022 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.persistence.model;

import org.eclipsefoundation.persistence.dao.PersistenceDao;

/**
 * Wrapper for DAO and query references to allow for repetition of calls.
 * 
 * @author Martin Lowe
 *
 */
public class QueryPlayback {

    private final RDBMSQuery<?> q;
    private final PersistenceDao dao;

    public QueryPlayback(RDBMSQuery<?> q, PersistenceDao dao) {
        this.q = q;
        this.dao = dao;
    }

    /**
     * @return the q
     */
    public RDBMSQuery<?> getQ() {
        return q;
    }

    /**
     * @return the dao
     */
    public PersistenceDao getDao() {
        return dao;
    }

}
