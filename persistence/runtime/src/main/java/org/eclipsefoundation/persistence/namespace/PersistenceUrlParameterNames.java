/*********************************************************************
* Copyright (c) 2019 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.namespace;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipsefoundation.utils.namespace.UrlParameterNamespace;

import jakarta.inject.Singleton;

/**
 * Namespace containing persistence based URL parameters used throughout the API.
 * 
 * @author Martin Lowe
 */
@Singleton
public final class PersistenceUrlParameterNames implements UrlParameterNamespace {
    public static final String CSRF_CREATED_BEFORE_RAW = "csrf_created_before";

    public static final UrlParameter SORT = new UrlParameter("sort");
    public static final UrlParameter ORDER = new UrlParameter("order");
    public static final UrlParameter MANUAL_OFFSET = new UrlParameter("manual_offset");
    public static final UrlParameter CSRF_CREATED_BEFORE = new UrlParameter(CSRF_CREATED_BEFORE_RAW);

    private static final List<UrlParameter> params = Collections
            .unmodifiableList(Arrays.asList(SORT, MANUAL_OFFSET, CSRF_CREATED_BEFORE, ORDER));

    @Override
    public List<UrlParameter> getParameters() {
        return new ArrayList<>(params);
    }

}
