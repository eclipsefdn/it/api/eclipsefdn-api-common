/*********************************************************************
* Copyright (c) 2019 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.mapper;

import org.eclipsefoundation.http.model.WebError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.persistence.NoResultException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

/**
 * Creates human legible error responses in the case of no result exceptions. By
 * catching the NoResultException, we handle exceptions thrown when there are no
 * results when one is required.
 * 
 * @author Martin Lowe
 */
@Provider
public class NoResultMapper implements ExceptionMapper<NoResultException> {
	private static final Logger LOGGER = LoggerFactory.getLogger(NoResultMapper.class);

	@Override
	public Response toResponse(NoResultException exception) {
		LOGGER.error(exception.getMessage(), exception);
		return new WebError(Status.NOT_FOUND, exception.getMessage()).asResponse();
	}
}
