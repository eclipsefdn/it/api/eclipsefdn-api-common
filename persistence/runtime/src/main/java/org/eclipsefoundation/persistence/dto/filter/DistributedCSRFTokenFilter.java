/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
/**
 * 
 */
package org.eclipsefoundation.persistence.dto.filter;

import java.time.ZonedDateTime;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.persistence.dto.DistributedCSRFToken;
import org.eclipsefoundation.persistence.model.DistributedCSRFGenerator;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;
import org.eclipsefoundation.persistence.namespace.PersistenceUrlParameterNames;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.ws.rs.core.MultivaluedMap;

@Singleton
public class DistributedCSRFTokenFilter implements DtoFilter<DistributedCSRFToken> {
    @Inject
    ParameterizedSQLStatementBuilder builder;

    @Override
    public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
        ParameterizedSQLStatement stmt = builder.build(DistributedCSRFToken.TABLE);
        if (isRoot) {
            // IP check
            String ip = params.getFirst(DistributedCSRFGenerator.IP_PARAM);
            if (ip != null) {
                stmt
                        .addClause(new ParameterizedSQLStatement.Clause(DistributedCSRFToken.TABLE.getAlias() + ".ipAddress = ?",
                                new Object[] { ip }));
            }
            // UA check
            String ua = params.getFirst(DistributedCSRFGenerator.USERAGENT_PARAM);
            if (ua != null) {
                stmt
                        .addClause(new ParameterizedSQLStatement.Clause(DistributedCSRFToken.TABLE.getAlias() + ".userAgent = ?",
                                new Object[] { ua }));
            }
            // user check
            String user = params.getFirst(DistributedCSRFGenerator.USER_PARAM);
            if (user != null) {
                stmt
                        .addClause(new ParameterizedSQLStatement.Clause(DistributedCSRFToken.TABLE.getAlias() + ".user = ?",
                                new Object[] { user }));
            }
            // datestamp filter, expects ISO format of ZonedDateTime.toString()
            String datestamp = params.getFirst(PersistenceUrlParameterNames.CSRF_CREATED_BEFORE_RAW);
            if (StringUtils.isNotBlank(datestamp)) {
                stmt
                        .addClause(new ParameterizedSQLStatement.Clause(DistributedCSRFToken.TABLE.getAlias() + ".timestamp < ?",
                                new Object[] { ZonedDateTime.parse(datestamp) }));
            }
        }

        return stmt;
    }

    @Override
    public Class<DistributedCSRFToken> getType() {
        return DistributedCSRFToken.class;
    }
}