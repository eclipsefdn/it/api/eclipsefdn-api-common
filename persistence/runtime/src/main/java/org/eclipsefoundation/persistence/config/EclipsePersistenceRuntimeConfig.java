/*********************************************************************
* Copyright (c) 2022, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*           Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.config;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;
import io.smallrye.config.WithName;

/**
 * Provides access to persistence related configuration at build/run time.
 * 
 * @author Martin Lowe
 *
 */
@ConfigMapping(prefix = "eclipse.persistence")
public interface EclipsePersistenceRuntimeConfig {

    /**
     * Whether the persistence framework is enabled
     */
    @WithDefault("true")
    public Boolean enabled();

    /**
     * Whether the persistence engine should be in maintenance mode
     */
    @WithDefault("false")
    public Boolean isMaintenanceMode();

    /**
     * Limits associated with requests
     */
    public PersistencePaginationLimits paginationLimit();

    /**
     * Multi-source single tenant default connection settings
     */
    public MultiSourceSingleTenantConfig msst();

    public interface PersistencePaginationLimits {

        /**
         * Default limit associated with pagination
         */
        @WithDefault("100")
        public Integer defaultValue();

        /**
         * The max limit associated with pagination
         */
        @WithName("max")
        @WithDefault("1000")
        public Integer max();
    }

    public interface MultiSourceSingleTenantConfig {
        /**
         * Whether the multi-source single tenant code should be enabled.
         */
        @WithDefault("false")
        public Boolean enabled();

        /**
         * The datasource name used for the secondary source used for read access.
         */
        @WithDefault("secondary")
        public String datasource();
    }
}
