/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.model;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.NotImplementedException;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement.Clause;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement.GroupBy;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement.IJoin;
import org.eclipsefoundation.persistence.namespace.SortOrder;

public class HQLGenerator implements SQLGenerator {
    public static final Pattern ORDINAL_PARAMETER_PATTERN = Pattern.compile("\\?(?!\\d)");
    
    // reduce the need to regenerate string buffers in SQL by setting a sensible starting buffer
    private static final int INITIAL_SQL_BUFFER_SIZE = 64;

    @Override
    public String getSelectSQL(ParameterizedSQLStatement src) {
        DtoTable base = src.getBase();
        StringBuilder sb = new StringBuilder(INITIAL_SQL_BUFFER_SIZE);
        sb.append("SELECT ").append(base.getAlias()).append(getFromClause(src));
        // retrieve the join clauses
        sb.append(getJoinClause(src.getJoins(), base));
        // retrieve the where clauses
        sb.append(getWhereClause(src));
        sb.append(getGroupByClause(src));
        // add sort if set
        if (src.getSortField() != null && !SortOrder.RANDOM.equals(src.getOrder())) {
            sb.append(" ORDER BY ").append(src.getSortField());
            if (SortOrder.ASCENDING.equals(src.getOrder())) {
                sb.append(" asc");
            } else {
                sb.append(" desc");
            }
        } else if (SortOrder.RANDOM.equals(src.getOrder())) {
            sb.append(" order by RAND()");
        }
        return sb.toString();
    }

    @Override
    public String getDeleteSQL(ParameterizedSQLStatement src) {
        throw new NotImplementedException("HQL does not utilize deletion SQL logic");
    }

    @Override
    public String getCountSQL(ParameterizedSQLStatement src) {
        DtoTable base = src.getBase();
        StringBuilder sb = new StringBuilder(INITIAL_SQL_BUFFER_SIZE);
        sb.append("SELECT COUNT(DISTINCT ").append(base.getAlias());
        sb.append(")").append(getFromClause(src));
        // retrieve the join clauses
        sb.append(getJoinClause(src.getJoins(), base));
        // retrieve the where clauses
        sb.append(getWhereClause(src));

        return sb.toString();
    }

    private String getFromClause(ParameterizedSQLStatement src) {
        StringBuilder sb = new StringBuilder();
        sb.append(" FROM");
        // handle selection of table data
        sb.append(' ').append(src.getBase().getType().getSimpleName());
        sb.append(' ').append(src.getBase().getAlias());
        return sb.toString();

    }

    private String getJoinClause(List<IJoin> joins, DtoTable base) {
        StringBuilder sb = new StringBuilder();
        List<DtoTable> selectedTables = new ArrayList<>();
        joins.stream().filter(j -> base != j.getForeignTable() && !selectedTables.contains(j.getForeignTable()))
                .forEach(j -> {
                    selectedTables.add(j.getLocalTable());
                    sb.append(' ').append(j.toSQL());
                });
        return sb.toString();
    }

    private String getWhereClause(ParameterizedSQLStatement src) {
        StringBuilder sb = new StringBuilder();
        List<Clause> clauses = src.getClauses();
        if (!clauses.isEmpty()) {
            sb.append(" WHERE");
        }
        // handle clauses
        int ordinal = 1;
        for (int cIdx = 0; cIdx < clauses.size(); cIdx++) {
            if (cIdx != 0) {
                sb.append(" AND");
            }

            // create matcher on sql clause to replace legacy parameter placeholders with
            // ordinals
            String sql = clauses.get(cIdx).getSql();
            int offset = 0;
            Matcher m = ORDINAL_PARAMETER_PATTERN.matcher(sql);
            while (m.find()) {
                sql = sql.substring(0, m.start() + offset) + '?' + ordinal++ + sql.substring(m.end() + offset);
                // use log to get num of digits in ordinal (handle large queries quickly)
                offset += (int) (Math.log10(ordinal) + 1);
            }
            sb.append(' ').append(sql);
        }
        return sb.toString();
    }

    private String getGroupByClause(ParameterizedSQLStatement src) {
        StringBuilder sb = new StringBuilder();
        if (!src.getGroups().isEmpty()) {
            sb.append(" GROUP BY");
            int count = 0;
            for (GroupBy group : src.getGroups()) {
                // include comma between group clauses
                if (count++ > 0) {
                    sb.append(',');
                }
                sb.append(' ').append(group.getTable().getAlias()).append('.').append(group.getField());
            }
        }
        return sb.toString();
    }

}
