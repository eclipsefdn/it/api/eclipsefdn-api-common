/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.service.impl;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;

import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.mapper.EntityMapper;
import org.eclipsefoundation.persistence.service.MapperService;

import io.quarkus.arc.Unremovable;

/**
 * Indicates an entity mapping between DTOs and models
 *
 * @author Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
 * 
 * @param <T> the DTO object in the mapping pair
 * @param <S> the model object in the mapping pair
 */
@Unremovable
@ApplicationScoped
public class DefaultMapperService implements MapperService {

    @Inject
    Instance<EntityMapper<?, ?>> mappers;

    /**
     * Retrieves an instance of EntityMapper that corresponds to the target model class.
     * 
     * @param target The target class used for EntityMapper retrieval
     * @return A reference to the target EntityMapper if it exists
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T extends BareNode, S> EntityMapper<T, S> get(Class<T> targetDto, Class<S> targetModel) {
        return (EntityMapper<T, S>) mappers
                .stream()
                .filter(map -> map.getModelType().equals(targetModel) && map.getDTOType().equals(targetDto))
                .findFirst()
                .orElse(null);
    }
}
