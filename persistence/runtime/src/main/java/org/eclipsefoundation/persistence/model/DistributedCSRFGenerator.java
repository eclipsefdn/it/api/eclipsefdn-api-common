/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.model;

import java.net.URI;
import java.security.Principal;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.http.model.FlatRequestWrapper;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.dto.DistributedCSRFToken;
import org.eclipsefoundation.persistence.dto.filter.DistributedCSRFTokenFilter;
import org.eclipsefoundation.utils.helper.DateTimeHelper;
import org.eclipsefoundation.utils.model.CSRFGenerator.DefaultCSRFGenerator;
import org.eclipsefoundation.utils.namespace.AdditionalHttpHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.handler.codec.http.HttpHeaderNames;
import io.vertx.core.http.HttpServerRequest;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.SecurityContext;

/**
 * Using IP address and useragent, create a fingerprint for the current user that will be used to access a stored distributed CSRF token. If
 * one does not yet exist, one will be generated using the default method, stored for future reference, and returned.
 * 
 * @author Martin Lowe
 *
 */
public class DistributedCSRFGenerator extends DefaultCSRFGenerator {
    public static final Logger LOGGER = LoggerFactory.getLogger(DistributedCSRFGenerator.class);

    public static final String USER_PARAM = "user";
    public static final String USERAGENT_PARAM = "useragent";
    public static final String IP_PARAM = "ip";

    private static final int USERAGENT_MAX_LENGTH = 255;

    private PersistenceDao manager;
    private DistributedCSRFTokenFilter filter;

    public DistributedCSRFGenerator(PersistenceDao manager, DistributedCSRFTokenFilter filter) {
        LOGGER.info("Distributed CSRF strategy is enabled.");
        this.manager = manager;
        this.filter = filter;
    }

    @Override
    public String getCSRFToken(HttpServerRequest request, SecurityContext context) {
        // generate a non-root query
        MultivaluedMap<String, String> params = getQueryParameters(request, context);
        RDBMSQuery<DistributedCSRFToken> q = new RDBMSQuery<>(new FlatRequestWrapper(URI.create(request.uri())), filter, params);
        q.setRoot(false);
        // attempt to read existing tokens
        List<DistributedCSRFToken> tokens = manager.get(q);
        if (tokens.isEmpty()) {
            // generate a new token to be stored in the distributed persistence table
            String token = super.getCSRFToken(request, context);
            DistributedCSRFToken t = new DistributedCSRFToken();
            t.setIpAddress(params.getFirst(IP_PARAM));
            t.setToken(token);
            t.setUser(params.getFirst(USER_PARAM));
            t.setUserAgent(params.getFirst(USERAGENT_PARAM));
            t.setTimestamp(DateTimeHelper.now());
            List<DistributedCSRFToken> createdTokens = manager.add(q, Arrays.asList(t));
            if (!createdTokens.isEmpty()) {
                LOGGER
                        .trace("Generated distributed CSRF token for current fingerprint; IP: {}, UA: {}, token: {}", t.getIpAddress(),
                                t.getUserAgent(), token);
                return token;
            }
        } else {
            DistributedCSRFToken t = tokens.get(0);
            if (LOGGER.isTraceEnabled()) {
                LOGGER
                        .trace("Found existing distributed CSRF token for current fingerprint; IP: {}, UA: {}, token: {}", t.getIpAddress(),
                                t.getUserAgent(), t.getToken());
            }
            return t.getToken();
        }
        if (LOGGER.isInfoEnabled()) {
            LOGGER
                    .info("Error while generating/retrieving CSRF entry for current user; IP: {}, UA: {}", params.getFirst(IP_PARAM),
                            params.getFirst(USERAGENT_PARAM));
        }
        return null;
    }

    public void destroyCurrentToken(HttpServerRequest request, SecurityContext context) {
        MultivaluedMap<String, String> params = getQueryParameters(request, context);
        LOGGER
                .error("Destroying retrieving CSRF entry for current user; IP: {}, UA: {}", params.getFirst(IP_PARAM),
                        params.getFirst(USERAGENT_PARAM));
        manager.delete(new RDBMSQuery<>(new FlatRequestWrapper(URI.create(request.uri())), filter, params));
    }

    private MultivaluedMap<String, String> getQueryParameters(HttpServerRequest request, SecurityContext context) {
        // get the markers used to identify a user (outside of a unique session ID)
        String ipAddr = getClientIpAddress(request);
        String userAgent = request.getHeader(HttpHeaderNames.USER_AGENT);
        // Iss #109 - Truncate the value if it's too long to keep table entries manageable
        if (userAgent.length() > USERAGENT_MAX_LENGTH) {
            userAgent = userAgent.substring(0, USERAGENT_MAX_LENGTH - 1);
        } else if (StringUtils.isBlank(userAgent)) {
            // Membership Issue #660 - Ensure non-null
            userAgent = "";
        }
        Principal user = context.getUserPrincipal();
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(IP_PARAM, ipAddr);
        params.add(USERAGENT_PARAM, userAgent);
        params.add(USER_PARAM, user != null ? user.getName() : null);
        return params;
    }

    private String getClientIpAddress(HttpServerRequest request) {
        String forwardedFor = request.getHeader(AdditionalHttpHeaders.X_FORWARDED_FOR);
        if (forwardedFor == null) {
            return request.remoteAddress().host();
        } else {
            // get the first most address in forwarded chain
            return new StringTokenizer(forwardedFor, ",").nextToken().trim();
        }
    }
}
