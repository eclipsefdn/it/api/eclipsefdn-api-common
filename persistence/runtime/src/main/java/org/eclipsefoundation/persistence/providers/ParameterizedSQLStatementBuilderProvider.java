/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.providers;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.Dependent;
import jakarta.enterprise.inject.Produces;

import org.eclipsefoundation.persistence.model.HQLGenerator;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;
import org.eclipsefoundation.persistence.model.SQLGenerator;

import io.quarkus.arc.DefaultBean;

@Dependent
public class ParameterizedSQLStatementBuilderProvider {
	
	@Produces
	@ApplicationScoped
	@DefaultBean
	public ParameterizedSQLStatementBuilder parameterizedSQLStatementBuilder(SQLGenerator generator) {
		return new ParameterizedSQLStatementBuilder(generator);
	}
	
	@Produces
	@ApplicationScoped
	@DefaultBean
	public SQLGenerator sqlGenerator() {
		return new HQLGenerator();
	}
}
