/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.dto;

import java.util.Objects;

import jakarta.persistence.MappedSuperclass;

/**
 * Represents a bare node with just ID and title for sake of persistence.
 * 
 * @author Martin Lowe
 */
@MappedSuperclass
public abstract class BareNode {

	/**
	 * @return the id
	 */
	public abstract Object getId();

	/**
	 * Initializes lazy fields through access. This is used to avoid issues with JPA
	 * sessions closing before access.
	 */
	public void initializeLazyFields() {
		// intentionally empty
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), getId());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BareNode other = (BareNode) obj;
		return Objects.equals(getId(), other.getId());
	}
}
