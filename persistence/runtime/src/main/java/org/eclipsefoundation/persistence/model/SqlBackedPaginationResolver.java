/**
 * Copyright (c) 2022 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.persistence.model;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.caching.model.ParameterizedCacheKey;
import org.eclipsefoundation.core.service.PaginationHeaderService;
import org.eclipsefoundation.core.service.PaginationHeaderService.PaginationResolver;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.http.response.PaginatedResultsFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.cache.Cache;
import io.quarkus.cache.CacheName;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

/**
 * @author martin
 *
 */
@ApplicationScoped
public class SqlBackedPaginationResolver implements PaginationResolver<QueryPlayback> {
    private static final Logger LOGGER = LoggerFactory.getLogger(SqlBackedPaginationResolver.class);

    @ConfigProperty(name = "quarkus.cache.caffeine.\"default\".expire-after-write", defaultValue = "P1H")
    Duration cacheTTL;

    @Inject
    PaginationHeaderService headerService;
    @Inject
    @CacheName("default")
    Cache cache;

    @Override
    public Map<String, String> generateEntry(RequestWrapper wrapper, QueryPlayback source) {
        Objects.requireNonNull(source.getDao());
        Objects.requireNonNull(source.getQ());

        // ensure that the source is cached, invalidating old values if they exist
        ParameterizedCacheKey key = headerService.generateKey(wrapper, getContextSource());
        cache.invalidate(key).flatMap(voidResult -> cache.get(key, i -> source)).await().indefinitely();
        // calculate the record
        return calculateRecord(wrapper, source);
    }

    @Override
    public Map<String, String> fallbackEntry(RequestWrapper wrapper, QueryPlayback source) {
        // rebuild query using request params from wrapper. Not always accurate w/ divergent params
        return calculateRecord(wrapper, new QueryPlayback(new RDBMSQuery<>(wrapper, source.getQ().getDTOFilter()), source.getDao()));
    }

    /**
     * Retrieves a fresh copy of total results count on every request to reduce caching errors associated with more complex
     * lookups, and because we have access to the information readily.
     * 
     * @param source the base query playback object to base the pagination data from
     * @return the
     */
    private Map<String, String> calculateRecord(RequestWrapper wrapper, QueryPlayback source) {
        Map<String, String> out = new HashMap<>();
        // perform the counts adhoc, using the current wrapper instead of the cached wrapper
        Long maxLength = source.getDao().count(new RDBMSQuery<>(wrapper, source.getQ().getDTOFilter(), source.getQ().getParams()));
        out.put(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER, Long.toString(maxLength));
        out.put(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER, Integer.toString(source.getDao().getLimit(source.getQ())));
        if (LOGGER.isDebugEnabled()) {
            LOGGER
                    .debug("Storing keys {}({}) and {}({})", PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER,
                            out.get(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER), PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER,
                            out.get(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER));
        }
        return out;
    }

    @Override
    public Class<?> getContextSource() {
        return QueryPlayback.class;
    }

}
