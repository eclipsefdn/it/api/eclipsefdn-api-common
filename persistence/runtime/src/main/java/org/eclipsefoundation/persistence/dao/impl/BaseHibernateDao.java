/*********************************************************************
* Copyright (c) 2019, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*           Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.dao.impl;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.model.ParameterizedCallStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement.Clause;
import org.eclipsefoundation.persistence.model.QueryPlayback;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.model.SqlBackedPaginationResolver;
import org.eclipsefoundation.persistence.namespace.PersistencePropertyNames;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tags;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.ParameterMode;
import jakarta.persistence.Query;
import jakarta.persistence.StoredProcedureQuery;
import jakarta.persistence.TypedQuery;
import jakarta.transaction.Transactional;

/**
 * Default implementation of the DB DAO, persisting via the Hibernate framework.
 * 
 * @author Martin Lowe
 */
public abstract class BaseHibernateDao implements PersistenceDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(BaseHibernateDao.class);

    private static final String METRIC_NAME = "eclipse_sql_time";
    private static final String METRIC_DOCTYPE_TAG_NAME = "type";
    private static final String METRIC_OPERATION_TAG_NAME = "operation";

    @ConfigProperty(name = "quarkus.micrometer.enabled")
    boolean isMetricsEnabled;
    // needs to be a property like this, as there are race conditions in some APIs when using object mapping
    @ConfigProperty(name = PersistencePropertyNames.PERSISTENCE_PAGINATION_LIMIT, defaultValue = "100")
    int defaultPaginationLimit;
    @ConfigProperty(name = PersistencePropertyNames.PERSISTENCE_PAGINATION_LIMIT_MAX, defaultValue = "1000")
    int maxPaginationLimit;

    @Inject
    EntityManager primaryConnectionManager;
    @Inject
    SqlBackedPaginationResolver sqlPaginationResolver;

    @Override
    public <T extends BareNode> List<T> get(RDBMSQuery<T> q) {
        // set up the query, either for stored procedures or standard SQL
        Query query;
        if (q.getFilter() instanceof ParameterizedCallStatement callStmt) {
            query = getSecondaryEntityManager().createStoredProcedureQuery(callStmt.getCallStatement(), q.getDocType());
        } else {
            query = getSecondaryEntityManager().createQuery(q.getFilter().getSelectSql(), q.getDocType());
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Querying DB using the following query: {}", q.getFilter().getSelectSql());
            }

            // handle root request parameters (setting headers to track limit and max count)
            if (q.isRoot()) {
                sqlPaginationResolver.generateEntry(q.getWrapper(), new QueryPlayback(q, this));
            }
            // check if result set should be limited
            if (q.getDTOFilter().useLimit() && q.useLimit()) {
                LOGGER.debug("Querying DB using offset of {} and limit of {}", getOffset(q), getLimit(q));
                query = query.setFirstResult(getOffset(q)).setMaxResults(getLimit(q));
            }
        }

        // used for metrics if enabled, only capture actual SQL query op
        Instant start = Instant.now();
        // update ordinals for the request
        handleOrdinals(q, query);
        // run the query and detach the results (stability w/ hibernate contexts)
        List<T> results = castResultsToType(query.getResultList(), q.getDocType());
        results.forEach(r -> {
            if (r != null)
                getSecondaryEntityManager().detach(r);
        });
        recordMetric(q.getDocType(), "query", start);
        return results;
    }

    @Transactional
    @Override
    public <T extends BareNode> List<T> add(RDBMSQuery<T> q, List<T> documents) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Adding {} documents to DB of type {}", documents.size(), q.getDocType().getSimpleName());
        }
        // fetch both the entity manager and session to action the updates
        EntityManager em = getPrimaryEntityManager();
        Session s = em.unwrap(Session.class);

        // used for metrics if enabled, only capture actual SQL query op
        Instant start = Instant.now();
        // for each doc, check if update or create
        List<T> updatedDocs = new ArrayList<>(documents.size());
        for (T doc : documents) {
            if (doc.getId() != null) {
                // ensure this object exists before merging on it
                if (em.find(q.getDocType(), doc.getId()) != null) {
                    LOGGER.debug("Merging document with existing document with id '{}'", doc.getId());
                    em.merge(doc);
                } else {
                    LOGGER.debug("Persisting new document with id '{}'", doc.getId());
                    s.save(doc);
                }
            } else {
                LOGGER.debug("Persisting new document with generated ID");
                s.save(doc);
            }
            // add the ref to the output list
            updatedDocs.add(doc);
        }
        recordMetric(q.getDocType(), "insert", start);
        return updatedDocs;
    }

    @Transactional
    @Override
    public <T extends BareNode> void delete(RDBMSQuery<T> q) {

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Removing documents from DB using the following query: {}", q);
        }
        // retrieve results for the given deletion query to delete using entity manager
        EntityManager em = getPrimaryEntityManager();
        List<T> results = get(q);
        // used for metrics if enabled, only capture actual SQL query op for deletion
        Instant start = Instant.now();
        if (results != null) {
            // remove all matched documents, merging to ensure we have attached versions
            // (transaction barriers sometimes break)
            results.forEach(r -> em.remove(em.merge(r)));
        }
        recordMetric(q.getDocType(), "delete", start);
    }

    @Transactional
    @Override
    public Long count(RDBMSQuery<?> q) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Counting documents in DB that match the following query: {}", q.getFilter().getCountSql());
        }
        EntityManager em = getSecondaryEntityManager();

        // build base query
        TypedQuery<Long> query = em.createQuery(q.getFilter().getCountSql(), Long.class);
        handleOrdinals(q, query);
        // used for metrics if enabled, only capture actual SQL query op
        Instant start = Instant.now();

        Long result = query.getSingleResult();
        recordMetric(q.getDocType(), "count", start);
        return result;
    }

    @Override
    public <T extends BareNode> T getReference(Object id, Class<T> type) {
        return getSecondaryEntityManager().getReference(type, id);
    }

    /**
     * // tell the recorder to save the headers as set recorderService.recordRequest(q.getWrapper()); Handle adding ordinals from the
     * wrapped query param object into the base query to be sent to db.
     * 
     * @param q the wrapped query params for the current request
     * @param actualQuery the actual DB query to be passed to the entity manager
     */
    private void handleOrdinals(RDBMSQuery<?> q, Query actualQuery) {
        // add ordinal parameters
        int ord = 1;
        for (Clause c : q.getFilter().getClauses()) {
            for (Object param : c.getParams()) {
                // for each of params, if processing call statement, add params as registered
                // params
                if (q.getFilter() instanceof ParameterizedCallStatement) {
                    ((StoredProcedureQuery) actualQuery).registerStoredProcedureParameter(ord, param.getClass(), ParameterMode.IN);
                }
                actualQuery.setParameter(ord++, param);
            }
        }
    }

    /**
     * Get the limit for entities returned in a single bound request. Will use the default limit if no pagination is provided. Will respect
     * the maximum configured limit if the provided one is set higher than allowed.
     * 
     * @param q the query params for the current request to check for set limit in params
     * @return the number of entities allowed
     */
    @Override
    public int getLimit(RDBMSQuery<?> q) {
        // If no pagesize param is provided, this will default to -1
        if (q.getLimit() < 0) {
            return defaultPaginationLimit;
        }
        return Math.min(q.getLimit(), maxPaginationLimit);
    }

    /**
     * Get offset cursor for the current request. Uses paging to enable batch requests that match the current max number of results by URL
     * params.
     * 
     * @param q the current requests query parameters, used to check for offset params
     * @return the offset of the current request, reactive to paging.
     */
    private int getOffset(RDBMSQuery<?> q) {
        // allow for manual offsetting
        int manualOffset = q.getManualOffset();
        if (manualOffset > 0) {
            return manualOffset;
        }
        // if first page, no offset
        if (q.getPage() <= 1) {
            return 0;
        }
        int limit = getLimit(q);
        return (limit * q.getPage()) - limit;
    }

    /**
     * Checked casting of results to properly typed list.
     * 
     * @param <T> the type to cast the list to
     * @param l the list that needs to be cast to a type
     * @param clazz the literal type to cast to
     * @return the original list cast to the passed type
     * @throws ClassCastException if the internal types do not match the passed type.
     */
    private <T> List<T> castResultsToType(List<?> l, Class<T> clazz) {
        return l.stream().map(clazz::cast).filter(out -> {
            if (out == null) {
                LOGGER.debug("Found null reference in result list for type {}", clazz.getSimpleName());
            }
            return out != null;
        }).toList();
    }

    @Override
    public HealthCheckResponse call() {
        return HealthCheckResponse.named("DB readiness").up().build();
    }

    /**
     * Allows for retrieval of multi-tenant persistence sessions. This is needed as sessions are generated based on annotated
     * PersistenceUnit values.
     * 
     * @return entity manager object linked to current transaction
     */
    protected EntityManager getPrimaryEntityManager() {
        return primaryConnectionManager;
    }

    /**
     * Allow for multiple connections to be associated with a datasource for separate read/write strategies. The secondary connection would
     * be a read-only interface to reduce traffic on primary write enable database replicants. Default behaviour returns the the primary
     * entity manager but this can be overriden.
     * 
     * @return the secondary entity manager, defaults to the primary entity manager.
     */
    protected EntityManager getSecondaryEntityManager() {
        return getPrimaryEntityManager();
    }

    private void recordMetric(Class<?> doctype, String operation, Instant startingTime) {
        // if micrometer metrics enabled, capture the metrics for the SQL query
        // required as the synthetic class registered by the deployment doesn't get properly metered via annotation
        if (isMetricsEnabled) {
            // using the global registry, fetch the timer and record the new time
            Metrics.globalRegistry
                    .timer(METRIC_NAME, Tags.of(METRIC_DOCTYPE_TAG_NAME, doctype.getSimpleName(), METRIC_OPERATION_TAG_NAME, operation))
                    .record(Duration.between(startingTime, Instant.now()));
        }
    }
}
