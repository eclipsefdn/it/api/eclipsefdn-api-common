/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.providers;

import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.dto.filter.DistributedCSRFTokenFilter;
import org.eclipsefoundation.persistence.model.DistributedCSRFGenerator;
import org.eclipsefoundation.utils.model.CSRFGenerator;
import org.eclipsefoundation.utils.namespace.MicroprofilePropertyNames;

import io.quarkus.arc.properties.IfBuildProperty;
import io.quarkus.arc.properties.UnlessBuildProperty;
import jakarta.annotation.Priority;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Produces;

public class DistributedCSRFProvider {

    @Produces
    @Priority(5)
    @ApplicationScoped
    @IfBuildProperty(name = MicroprofilePropertyNames.CSRF_DISTRIBUTED_ENABLED, stringValue = "true")
    @UnlessBuildProperty(name = MicroprofilePropertyNames.CSRF_DISTRIBUTED_DEFAULT, stringValue = "false", enableIfMissing = true)
    public CSRFGenerator distributedGenerator(DefaultHibernateDao defaultDao, DistributedCSRFTokenFilter filter) {
        return new DistributedCSRFGenerator(defaultDao, filter);
    }
}
