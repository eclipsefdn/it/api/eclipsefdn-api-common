/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.helper;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.eclipsefoundation.persistence.model.SortableField;
import org.hibernate.boot.model.naming.Identifier;

/**
 * Reflection based helper that reads in a type and reads annotations present on class, drilling down into child types
 * to generate paths to nested types for usage in document queries. Currently max depth is 2.
 * 
 * @author Martin Lowe
 */
public class SortableHelper {
    private static final int MAX_DEPTH = 2;

    /**
     * <p>
     * Recursively parse target classes to a given depth. Each parsed class will have its Fields reflectively read, checking
     * for SortableField annotations marking it as a field to be sortable in MongoDB.
     * </p>
     * <p>
     * Each field will then, if max depth hasn't been reached, checked for further annotations on fields. The parent Fields
     * annotation values will be passed to the recursive call to create compound path keys for queries.
     * </p>
     * 
     * @param tgt the class to reflectively read the fields of
     * @return list of annotated field properties
     */
    public static List<Sortable> getSortableFields(Class<?> tgt) {
        Objects.requireNonNull(tgt);

        return parseClass(0, tgt, null, new LinkedList<>());
    }

    /**
     * Using a list of Sortable field values, streams and retrieves the first Sortable that matches the passed field name if
     * it exists as an optional value.
     * 
     * @param fields the Sortable fields for the current operation
     * @param fieldName the field name to retrieve a Sortable for
     * @return the sortable object if it exists as an Optional value.
     */
    public static Optional<Sortable> getSortableFieldByName(List<Sortable> fields, String fieldName) {
        Objects.requireNonNull(fields);
        Objects.requireNonNull(fieldName);

        return fields.stream().filter(c -> c.getName().equals(fieldName)).findFirst();
    }

    private static List<Sortable> parseClass(int depth, Class<?> tgt, Sortable parent, List<Sortable> coll) {
        for (Field f : tgt.getDeclaredFields()) {
            // create new container for field
            Sortable c = new Sortable();
            c.name = Identifier.toIdentifier(f.getName()).getText();
            c.path = c.name;

            // if annotation exists, get values from it
            SortableField sf = f.getAnnotation(SortableField.class);
            // if not sortable, still generate in case children fields require parent data
            if (sf == null) {
                // if parent exists, concat the paths
                if (parent != null) {
                    c.path = parent.path + '.' + c.path;
                }
            } else {
                coll.add(updateSortableForAnnotation(parent, c, sf));
            }

            // recurse if we haven't reached max depth
            if (depth < MAX_DEPTH) {
                parseClass(depth + 1, f.getType(), c, coll);
            }
        }
        return coll;
    }

    /**
     * Update the base sortable to include annotation fields when present for overrides.
     * 
     * @param parent the parent sortable used in path nesting
     * @param base the base sortable that is being generated
     * @param sf the annotation for the sortable field
     * @return the updated sortable reference.
     */
    private static Sortable updateSortableForAnnotation(Sortable parent, Sortable base, SortableField sf) {
        // if field name isn't empty, update the default name + path name
        if (!"".equals(sf.name())) {
            base.name = sf.name();
            base.path = base.name;
        }
        // if parent exists, concat the paths
        if (parent != null) {
            base.path = parent.path + '.' + base.path;
        }
        // if the sortable annotation has a custom path, use it
        if (!"".equals(sf.path())) {
            base.path = sf.path();
        }
        return base;
    }

    // blank as this is a static helper and should not be instantiated
    private SortableHelper() {
    }

    /**
     * Container for sortable field data. This should only be created by the SortableHelper class on importing field
     * annotation values.
     * 
     * @author Martin Lowe
     *
     * @param <T> the type of data contained in the given field
     */
    public static final class Sortable {
        private String name;
        private String path;

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @return the path
         */
        public String getPath() {
            return path;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("Container[");
            sb.append("name=").append(name);
            sb.append(",path=").append(path);
            sb.append(']');
            return sb.toString();
        }
    }

}
