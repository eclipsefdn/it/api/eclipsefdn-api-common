/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.model;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.eclipsefoundation.persistence.namespace.SortOrder;

import jakarta.persistence.criteria.JoinType;

/**
 * In-context wrapper for prepared statement
 * 
 * @author Martin Lowe
 *
 */
public class ParameterizedSQLStatement {
    private DtoTable base;

    private List<Clause> clauses;
    private List<IJoin> joins;
    private List<GroupBy> groups;

    private String sortField;
    private SortOrder order;
    private SQLGenerator gen;
    private SecureRandom rnd;
    private float seed = 0;

    /**
     * Builds a loaded parameterized statement to be used in querying dataset.
     */
    ParameterizedSQLStatement(DtoTable base, SQLGenerator gen) {
        this.base = Objects.requireNonNull(base);
        this.clauses = new ArrayList<>();
        this.joins = new ArrayList<>();
        this.groups = new ArrayList<>();
        this.gen = Objects.requireNonNull(gen);
    }

    public ParameterizedSQLStatement combine(ParameterizedSQLStatement other) {
        this.clauses.addAll(other.clauses);
        this.joins.addAll(other.joins);
        this.groups.addAll(other.groups);

        return this;
    }

    /**
     * @return the sql
     */
    public String getSelectSql() {
        return gen.getSelectSQL(this);
    }

    /**
     * @return the sql
     */
    public String getCountSql() {
        return gen.getCountSQL(this);
    }

    /**
     * @return the base
     */
    public DtoTable getBase() {
        return base;
    }

    /**
     * @param base the base to set
     */
    public void setBase(DtoTable base) {
        this.base = base;
    }

    /**
     * @return the sortField
     */
    public String getSortField() {
        return sortField;
    }

    /**
     * @param sortField the sortField to set
     */
    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    /**
     * @return the order
     */
    public SortOrder getOrder() {
        return order;
    }

    /**
     * @param order the order to set
     */
    public void setOrder(SortOrder order) {
        this.order = order;
    }

    /**
     * @return the params
     */
    public Object[] getParams() {
        return clauses.stream().map(Clause::getParams).flatMap(Arrays::stream).toList().toArray();
    }

    public void addClause(Clause c) {
        this.clauses.add(c);
    }

    public void addJoin(IJoin j) {
        this.joins.add(j);
    }

    public void addGroup(GroupBy group) {
        this.groups.add(group);
    }

    public List<Clause> getClauses() {
        return new ArrayList<>(clauses);
    }

    public List<IJoin> getJoins() {
        return new ArrayList<>(joins);
    }

    public List<GroupBy> getGroups() {
        return new ArrayList<>(groups);
    }

    private SecureRandom rnd() {
        if (this.rnd == null) {
            this.rnd = new SecureRandom(Long.toString(System.currentTimeMillis()).getBytes());
        }
        return rnd;
    }

    public float getSeed() {
        if (this.seed == 0) {
            this.seed = rnd().nextFloat();
        }
        return this.seed;
    }

    /**
     * Represents a clause for an SQL query
     * 
     * @author Martin Lowe
     *
     */
    public static class Clause {
        private String sql;
        private Object[] params;

        public Clause(String sql, Object[] params) {
            this.sql = sql;
            this.params = params;
        }

        /**
         * @return the sql
         */
        public String getSql() {
            return sql;
        }

        /**
         * @param sql the sql to set
         */
        public void setSql(String sql) {
            this.sql = sql;
        }

        /**
         * @return the params
         */
        public Object[] getParams() {
            return params;
        }

        /**
         * @param params the params to set
         */
        public void setParams(Object[] params) {
            this.params = params;
        }
    }

    public static interface IJoin {

        public DtoTable getLocalTable();

        public DtoTable getForeignTable();

        public JoinType getJoinType();

        /**
         * Generate SQL for the join clause.
         * 
         * @return the rendered SQL for the current clause
         */
        public String toSQL();
    }

    public static class Join implements IJoin {
        private DtoTable localTable;
        private DtoTable foreignTable;
        private String localField;
        private String foreignField;
        private JoinType joinType;

        public Join(DtoTable localTable, DtoTable foreignTable, String localField) {
            this(localTable, foreignTable, localField, null);
        }

        public Join(DtoTable localTable, DtoTable foreignTable, String localField, String foreignField) {
            this(localTable, foreignTable, localField, foreignField, JoinType.LEFT);
        }

        public Join(DtoTable localTable, DtoTable foreignTable, String localField, String foreignField,
                JoinType joinType) {
            this.localTable = localTable;
            this.foreignTable = foreignTable;
            this.localField = localField;
            this.foreignField = foreignField;
            this.joinType = joinType;
        }

        public DtoTable getLocalTable() {
            return localTable;
        }

        public DtoTable getForeignTable() {
            return foreignTable;
        }

        public String getLocalField() {
            return localField;
        }

        public String getForeignField() {
            return foreignField;
        }

        public JoinType getJoinType() {
            return joinType;
        }

        @Override
        public String toSQL() {
            StringBuilder sb = new StringBuilder();
            sb.append(getJoinType() != null ? getJoinType().name() : JoinType.LEFT.name());
            sb.append(" JOIN ").append(getForeignTable().getType().getSimpleName());
            sb.append(" AS ").append(getForeignTable().getAlias());
            sb.append(" ON ").append(getLocalTable().getAlias()).append('.').append(getLocalField());
            sb.append(" = ").append(getForeignTable().getAlias()).append('.').append(getForeignField());
            return sb.toString();
        }
    }

    public static class ComplexJoin implements IJoin {
        private DtoTable localTable;
        private DtoTable foreignTable;
        private List<JoinPart> parts;
        private JoinType joinType;

        public ComplexJoin(DtoTable localTable, DtoTable foreignTable, JoinPart... parts) {
            this(localTable, foreignTable, JoinType.LEFT, parts);
        }

        public ComplexJoin(DtoTable localTable, DtoTable foreignTable, JoinType joinType, JoinPart... parts) {
            this.localTable = localTable;
            this.foreignTable = foreignTable;
            this.parts = Arrays.asList(parts);
            this.joinType = joinType;
        }

        public DtoTable getLocalTable() {
            return localTable;
        }

        public DtoTable getForeignTable() {
            return foreignTable;
        }

        public List<JoinPart> getParts() {
            return new ArrayList<>(parts);
        }

        public JoinType getJoinType() {
            return joinType;
        }

        @Override
        public String toSQL() {
            StringBuilder sb = new StringBuilder();
            sb.append(getJoinType() != null ? getJoinType().name() : JoinType.LEFT.name());
            sb.append(" JOIN ").append(getForeignTable().getType().getSimpleName());
            sb.append(" AS ").append(getForeignTable().getAlias()).append(" ON ");
            sb.append(String.join(" and ",
                    parts.stream().map(p -> p.toJoinClause(localTable, foreignTable)).toList()));
            return sb.toString();
        }

        /**
         * Represents a part of a complex join clause containing the 2 fields that are used for matching join rows.
         * 
         * @author Martin Lowe
         *
         */
        public static class JoinPart {
            private final String localField;
            private final String foreignField;

            public JoinPart(String localField, String foreignField) {
                this.foreignField = foreignField;
                this.localField = localField;
            }

            public String getLocalField() {
                return localField;
            }

            public String getForeignField() {
                return foreignField;
            }

            public String toJoinClause(DtoTable localTable, DtoTable foreignTable) {
                StringBuilder sb = new StringBuilder();
                sb.append(localTable.getAlias()).append('.').append(getLocalField());
                sb.append(" = ").append(foreignTable.getAlias()).append('.').append(getForeignField());
                return sb.toString();
            }
        }
    }

    public static class GroupBy {
        private DtoTable table;
        private String field;

        public GroupBy(DtoTable table, String field) {
            this.table = table;
            this.field = field;
        }

        public String getField() {
            return this.field;
        }

        public DtoTable getTable() {
            return this.table;
        }
    }
}