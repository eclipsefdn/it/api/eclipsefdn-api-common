/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.tasks;

import java.net.URI;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

import org.eclipsefoundation.http.model.FlatRequestWrapper;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.dto.DistributedCSRFToken;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.namespace.PersistenceUrlParameterNames;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.utils.config.CSRFSecurityConfig;
import org.eclipsefoundation.utils.helper.DateTimeHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.scheduler.Scheduled;
import jakarta.enterprise.context.Dependent;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * Scheduled task that will cleanup the CSRF table if distributed mode is enabled. Will remove entries over a day old, as we don't need to
 * keep them any longer than that.
 */
@Dependent
public class CsrfDatabaseCleanupTask {
    public static final Logger LOGGER = LoggerFactory.getLogger(CsrfDatabaseCleanupTask.class);

    private final DefaultHibernateDao dao;
    private final FilterService filters;
    private final CSRFSecurityConfig config;

    /**
     * Default constructor for the cleanup task, importing the DAO bindings + configuration for CSRF to check if enabled.
     * 
     * @param dao default hibernate DAO where the CSRF token should be set
     * @param filters service for looking up DTO filters
     * @param config CSRF configuration mapping object
     */
    public CsrfDatabaseCleanupTask(DefaultHibernateDao dao, FilterService filters, CSRFSecurityConfig config) {
        this.dao = dao;
        this.filters = filters;
        this.config = config;
    }

    @Scheduled(every = "P1D", delay = 5, identity = "csrf-stale-cleanup")
    public void schedule() {
        if (config.distributedMode().enabled()) {
            // set up the max age of the CSRF entries
            ZonedDateTime maxAge = DateTimeHelper.now().minus(1, ChronoUnit.DAYS);
            LOGGER.info("Checking for CSRF database entries created before {}", maxAge);
            // create parameter map for inprogress documents older than the configured period
            MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
            params.add(PersistenceUrlParameterNames.CSRF_CREATED_BEFORE_RAW, maxAge.toString());
            URI baseUri = URI.create("https://api.eclipse.org");
            // generate the query to get expired documents
            RDBMSQuery<DistributedCSRFToken> initialQuery = new RDBMSQuery<>(new FlatRequestWrapper(baseUri),
                    filters.get(DistributedCSRFToken.class), params);
            initialQuery.setRoot(false);

            // delete the forms last
            dao.delete(initialQuery);
        } else {
            LOGGER.warn("CSRF DB clean scheduled task not run as distributed mode has been disabled through configuration");
        }
    }
}
