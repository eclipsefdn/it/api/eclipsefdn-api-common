/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.dao.impl;

/**
 * Allows for proper abstraction of Hibernate DAOs and creation of sub-resources for different tenants
 * 
 * @author Martin Lowe
 *
 */
public class DefaultHibernateDao extends BaseHibernateDao {
}
