/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.dto.mapper;

import org.eclipsefoundation.persistence.dto.BareNode;
import org.mapstruct.InheritInverseConfiguration;

/**
 * Mapper interface to allow mapping of DTOs and Models
 * 
 * @author Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
 */
public interface EntityMapper<T extends BareNode, S> {

    S toModel(T dtoEntity);

    @InheritInverseConfiguration
    T toDTO(S model);

    /**
     * Returns the mapped DTO type.
     * 
     * @return the class of mapped DTO
     */
    Class<T> getDTOType();

    /**
     * Returns the mapped model type.
     * 
     * @return the class of mapped model
     */
    Class<S> getModelType();
}
