package org.eclipsefoundation.persistence.namespace;

public class PersistencePropertyNames {

    public static final String MULTI_SOURCE_SINGLE_TENANT_ENABLED = "eclipse.persistence.deployment.msst.enabled";
    public static final String MULTI_SOURCE_SINGLE_TENANT_DATASOURCE = "eclipse.persistence.deployment.msst.datasource";
    public static final String PERSISTENCE_PAGINATION_LIMIT = "eclipse.persistence.pagination-limit.default-value";
    public static final String PERSISTENCE_PAGINATION_LIMIT_MAX = "eclipse.persistence.pagination-limit.max";

    private PersistencePropertyNames() {
    }
}
