/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.model;

import java.util.Optional;

/**
 * Builder object that is used to construct self-sufficient SQL query statement models that can render their own SQL/HQL
 * on request.
 * 
 * @author Martin Lowe
 *
 */
public class ParameterizedSQLStatementBuilder {

    SQLGenerator generator;

    /**
     * Default constructor for the builder class, takes the SQL generator which does the heavy lifting for converting and
     * passes it to the SQL query statement wrapper.
     * 
     * @param generator the SQL generator for rendering query to use in JPA connections.
     */
    public ParameterizedSQLStatementBuilder(SQLGenerator generator) {
        this.generator = generator;
    }

    /**
     * Builds a basic SQL statement object without any clauses, just using the base table to initialize the wrapper.
     * 
     * @param base the DTO table reference for the class to build an SQL statement wrapper for
     * @return the initial SQL statement.
     */
    public ParameterizedSQLStatement build(DtoTable base) {
        return new ParameterizedSQLStatement(base, generator);
    }

    /**
     * Builds a basic query with the id value used in a clause when present.
     * 
     * @param <T> the type for the ID property
     * @param base the DTO table reference for the class to build an SQL statement wrapper for
     * @param idValue the optional value of the ID to
     * @param idType the type for the ID field, currently supports the following:
     * <ul>
     * <li>String
     * <li>Long
     * </ul>
     * @return the initial SQL statement with the ID clause set if the value is present
     */
    public <T> ParameterizedSQLStatement buildWithId(DtoTable base, Optional<String> idValue, Class<T> idType) {
        // build the base query using the standard build command
        ParameterizedSQLStatement stmt = build(base);
        // check if we should attempt to add an ID clause to query
        if (idValue.isEmpty()) {
            return stmt;
        }
        // add base ID query to output statment
        stmt
                .addClause(new ParameterizedSQLStatement.Clause(base.getAlias() + ".id = ?",
                        new Object[] { idType == Long.class ? Long.parseLong(idValue.get()) : idValue.get() }));
        return stmt;
    }

    /**
     * Builds a call statement, which is a subtype of the base statement to allow for functions and procedures to be called
     * in the persistence framework.
     * 
     * @param base the DTO table reference for the class to build an SQL statement wrapper for
     * @return the basic call statement bound to the given procedure/function table-like structure
     */
    public ParameterizedCallStatement buildCallStatement(DtoTable base) {
        return new ParameterizedCallStatement(base, generator);
    }
}
