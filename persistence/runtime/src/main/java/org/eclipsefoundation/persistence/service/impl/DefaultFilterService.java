/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.service.impl;

import java.util.Optional;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;

import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.service.FilterService;

@ApplicationScoped
public class DefaultFilterService implements FilterService {
  @Inject Instance<DtoFilter<?>> filters;

  @SuppressWarnings("unchecked")
  @Override
  public <T extends BareNode> DtoFilter<T> get(Class<T> target) {
    Optional<DtoFilter<?>> filter =
        filters.stream().filter(f -> f.getType().equals(target)).findFirst();
    if (filter.isPresent()) {
      return (DtoFilter<T>) filter.get();
    }
    return null;
  }
}
