/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.persistence.model;

import java.net.URI;
import java.util.Map;
import java.util.UUID;

import org.eclipsefoundation.caching.model.ParameterizedCacheKey;
import org.eclipsefoundation.core.service.PaginationHeaderService;
import org.eclipsefoundation.http.model.FlatRequestWrapper;
import org.eclipsefoundation.http.response.PaginatedResultsFilter;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.test.dto.PersonDTO;
import org.eclipsefoundation.persistence.test.dto.PersonDTO.PersonDTOFilter;
import org.eclipsefoundation.persistence.test.namespace.PersistenceTestParameterNames;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.cache.Cache;
import io.quarkus.cache.CacheName;
import io.quarkus.cache.CaffeineCache;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * Tests for the SQL-Backed pagination resolver. This test suite should validate both the headers output and the
 * integrity of their values.
 * 
 * @author Martin Lowe
 *
 */
@QuarkusTest
class SqlBackedPaginationResolverTest {

    public static final int PAGINATION_HEADER_COUNT = 2;
    public static final String PAGINATION_HEADER_COUNT_MESSAGE = "Pagination headers should only contain 2 headers in current state.";

    @Inject
    SqlBackedPaginationResolver resolver;

    // used to validate results and provide context
    @Inject
    PersistenceDao dao;
    @Inject
    PersonDTOFilter filter;

    // used to generate keys used for caching
    @Inject
    PaginationHeaderService headerService;

    // used to validate caching of context on predictable keys
    @Inject
    @CacheName("default")
    Cache cache;

    @Test
    void canResolve_success() {
        Assertions.assertFalse(resolver.canResolve(Object.class), "Should not be able to resolve plain Objects");
        Assertions.assertTrue(resolver.canResolve(QueryPlayback.class), "Cannot resolve query playback wrapper");
    }

    @Test
    void generateEntry_success_returnsPaginationHeaders() {
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org/sample/endpoint/" + UUID.randomUUID().toString()));
        RDBMSQuery<PersonDTO> q = new RDBMSQuery<>(wrap, filter);

        // run the actual test and validate the outputs
        Map<String, String> results = resolver.generateEntry(wrap, new QueryPlayback(q, dao));
        Assertions.assertEquals(PAGINATION_HEADER_COUNT, results.keySet().size(), PAGINATION_HEADER_COUNT_MESSAGE);
        Assertions.assertTrue(results.keySet().contains(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER));
        Assertions.assertTrue(results.keySet().contains(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER));
    }

    @Test
    void generateEntry_success_headersReflectResultSetSize() {
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org/sample/endpoint/" + UUID.randomUUID().toString()));
        RDBMSQuery<PersonDTO> q = new RDBMSQuery<>(wrap, filter);
        // these fields should equal the headers in the result map
        long count = dao.count(q);
        int limit = dao.getLimit(q);

        // run the actual test and validate the outputs
        Map<String, String> results = resolver.generateEntry(wrap, new QueryPlayback(q, dao));
        Assertions.assertEquals(PAGINATION_HEADER_COUNT, results.keySet().size(), PAGINATION_HEADER_COUNT_MESSAGE);
        Assertions.assertEquals(Long.toString(count), results.get(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER));
        Assertions.assertEquals(Integer.toString(limit), results.get(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER));
    }

    @Test
    void generateEntry_success_respectsFilters() {
        // set up the test with a filter
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org/sample/endpoint/" + UUID.randomUUID().toString()));
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(PersistenceTestParameterNames.AT_LEAST_OF_AGE.getName(), "40");
        RDBMSQuery<PersonDTO> q = new RDBMSQuery<>(wrap, filter, params);
        // these fields should equal the headers in the result map
        long count = dao.count(q);
        int limit = dao.getLimit(q);

        // run the actual test and validate the outputs
        Map<String, String> results = resolver.generateEntry(wrap, new QueryPlayback(q, dao));
        Assertions.assertEquals(PAGINATION_HEADER_COUNT, results.keySet().size(), PAGINATION_HEADER_COUNT_MESSAGE);
        Assertions.assertEquals(Long.toString(count), results.get(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER));
        Assertions.assertEquals(Integer.toString(limit), results.get(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER));
    }

    @Test
    void generateEntry_success_addsCacheEntryForContext() {
        // set up the test with a filter
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org/sample/endpoint/" + UUID.randomUUID().toString()));
        RDBMSQuery<PersonDTO> q = new RDBMSQuery<>(wrap, filter);
        QueryPlayback contextSource = new QueryPlayback(q, dao);

        // run request that should cache context QueryPlayback object
        resolver.generateEntry(wrap, contextSource);

        // check that the resolver caches the context source for reuse in fallback/playback scenarios
        ParameterizedCacheKey cacheKey = headerService.generateKey(wrap, QueryPlayback.class);
        Assertions
                .assertTrue(cache.as(CaffeineCache.class).keySet().contains(cacheKey),
                        "Cache should contain a context source for requested pagination headers");
        Assertions
                .assertEquals(contextSource, cache.get(cacheKey, k -> null).await().indefinitely(),
                        "Cache should hold a context object matching the passed value for the given key");
    }

    @Test
    void generateEntry_success_regeneratesCacheOnRequest() {
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org/sample/endpoint/" + UUID.randomUUID().toString()));
        RDBMSQuery<PersonDTO> q = new RDBMSQuery<>(wrap, filter);
        // these fields should equal the headers in the result map
        long count = dao.count(q);
        int limit = dao.getLimit(q);

        // run the first half of the test with 1 set of params
        Map<String, String> results = resolver.generateEntry(wrap, new QueryPlayback(q, dao));
        Assertions.assertEquals(PAGINATION_HEADER_COUNT, results.keySet().size(), PAGINATION_HEADER_COUNT_MESSAGE);
        Assertions.assertEquals(Long.toString(count), results.get(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER));
        Assertions.assertEquals(Integer.toString(limit), results.get(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER));

        // rerun with different params
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(PersistenceTestParameterNames.AT_LEAST_OF_AGE.getName(), "40");
        q = new RDBMSQuery<>(wrap, filter, params);
        long newCount = dao.count(q);
        int newLimit = dao.getLimit(q);

        results = resolver.generateEntry(wrap, new QueryPlayback(q, dao));
        Assertions.assertEquals(PAGINATION_HEADER_COUNT, results.keySet().size(), PAGINATION_HEADER_COUNT_MESSAGE);
        Assertions.assertNotEquals(count, newCount, "Count should change with updates to filters");
        Assertions.assertEquals(limit, newLimit, "Limit should not change between calls");
        Assertions
                .assertEquals(Long.toString(newCount), results.get(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER),
                        "Results size from SQL backed headers should be equal to the count");
        Assertions
                .assertEquals(Integer.toString(newLimit), results.get(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER),
                        "The limit should be set to the limit provided by the persistence layer");
    }

    @Test
    void fallbackEntry_success_returnsPaginationHeaders() {
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org/sample/endpoint/" + UUID.randomUUID().toString()));
        RDBMSQuery<PersonDTO> q = new RDBMSQuery<>(wrap, filter);

        // run the actual test and validate the outputs
        Map<String, String> results = resolver.fallbackEntry(wrap, new QueryPlayback(q, dao));
        Assertions.assertEquals(PAGINATION_HEADER_COUNT, results.keySet().size(), PAGINATION_HEADER_COUNT_MESSAGE);
        Assertions.assertTrue(results.keySet().contains(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER));
        Assertions.assertTrue(results.keySet().contains(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER));
    }

    @Test
    void fallbackEntry_success_headersReflectResultSetSize() {
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org/sample/endpoint/" + UUID.randomUUID().toString()));
        RDBMSQuery<PersonDTO> q = new RDBMSQuery<>(wrap, filter);
        // these fields should equal the headers in the result map
        long count = dao.count(q);
        int limit = dao.getLimit(q);

        // run the actual test and validate the outputs
        Map<String, String> results = resolver.fallbackEntry(wrap, new QueryPlayback(q, dao));
        Assertions.assertEquals(PAGINATION_HEADER_COUNT, results.keySet().size(), PAGINATION_HEADER_COUNT_MESSAGE);
        Assertions.assertEquals(Long.toString(count), results.get(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER));
        Assertions.assertEquals(Integer.toString(limit), results.get(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER));
    }

    @Test
    void fallbackEntry_success_noGeneratedContextCache() {
        // set up the test with a filter
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org/sample/endpoint/" + UUID.randomUUID().toString()));
        RDBMSQuery<PersonDTO> q = new RDBMSQuery<>(wrap, filter);
        QueryPlayback contextSource = new QueryPlayback(q, dao);

        // run request that should not cache context object. Fallback is an error case when we can't be sure of results.
        resolver.fallbackEntry(wrap, contextSource);

        // check that the resolver caches the context source for reuse in fallback/playback scenarios
        ParameterizedCacheKey cacheKey = headerService.generateKey(wrap, QueryPlayback.class);
        Assertions
                .assertFalse(cache.as(CaffeineCache.class).keySet().contains(cacheKey),
                        "Cache should not contain a context source for requested fallback pagination headers");
    }

    @Test
    void fallbackEntry_success_recalculatesQueryBasedOnContext() {
        // set up context source to be used in fallback scenario. No filters are available initially
        FlatRequestWrapper primaryWrap = new FlatRequestWrapper(
                URI.create("http://eclipse.org/sample/endpoint/" + UUID.randomUUID().toString()));
        RDBMSQuery<PersonDTO> q = new RDBMSQuery<>(primaryWrap, filter);
        QueryPlayback contextSource = new QueryPlayback(q, dao);

        // set up best case scenario test, where the required params are set in the URL and available for filter
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org/sample/endpoint/" + UUID.randomUUID().toString()));
        wrap.setParam(PersistenceTestParameterNames.AT_LEAST_OF_AGE, "40");
        RDBMSQuery<PersonDTO> expectedQuery = new RDBMSQuery<>(wrap, filter, wrap.asMap());
        // these fields should equal the headers in the result map
        long count = dao.count(expectedQuery);
        int limit = dao.getLimit(expectedQuery);

        // run the test
        Map<String, String> results = resolver.fallbackEntry(wrap, contextSource);
        Assertions.assertEquals(PAGINATION_HEADER_COUNT, results.keySet().size(), PAGINATION_HEADER_COUNT_MESSAGE);
        Assertions.assertEquals(Long.toString(count), results.get(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER));
        Assertions.assertEquals(Integer.toString(limit), results.get(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER));
    }

    @Test
    void fallbackEntry_success_overwritesExistingFilterParams() {
        // set up context source to be used in fallback scenario. No filters are available initially
        FlatRequestWrapper primaryWrap = new FlatRequestWrapper(
                URI.create("http://eclipse.org/sample/endpoint/" + UUID.randomUUID().toString()));
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(PersistenceTestParameterNames.AT_LEAST_OF_AGE.getName(), "100");
        RDBMSQuery<PersonDTO> q = new RDBMSQuery<>(primaryWrap, filter, params);
        QueryPlayback contextSource = new QueryPlayback(q, dao);

        // set up best case scenario test, where the required params are set in the URL and available for filter
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org/sample/endpoint/" + UUID.randomUUID().toString()));
        wrap.setParam(PersistenceTestParameterNames.AT_LEAST_OF_AGE, "40");
        RDBMSQuery<PersonDTO> expectedQuery = new RDBMSQuery<>(wrap, filter, wrap.asMap());
        // these fields should equal the headers in the result map
        long count = dao.count(expectedQuery);
        int limit = dao.getLimit(expectedQuery);

        // run the test
        Map<String, String> results = resolver.fallbackEntry(wrap, contextSource);
        Assertions.assertEquals(PAGINATION_HEADER_COUNT, results.keySet().size(), PAGINATION_HEADER_COUNT_MESSAGE);
        Assertions.assertEquals(Long.toString(count), results.get(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER));
        Assertions.assertEquals(Integer.toString(limit), results.get(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER));
    }
}
