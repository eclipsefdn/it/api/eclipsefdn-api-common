/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.persistence.test.dto;

import java.io.Serializable;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * @author Martin Lowe
 *
 */
@Entity
@Table
public class PersonAddress extends BareNode implements Serializable {
    private static final long serialVersionUID = 1L;

    public static final DtoTable TABLE = new DtoTable(PersonAddress.class, "pa");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "person_id")
    private PersonDTO person;
    private String address1;
    private String address2;

    /**
     * @return the id
     */
    @Override
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the person
     */
    public PersonDTO getPerson() {
        return person;
    }

    /**
     * @param person the person to set
     */
    public void setPerson(PersonDTO person) {
        this.person = person;
    }

    /**
     * @return the address1
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * @param address1 the address1 to set
     */
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    /**
     * @return the address2
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * @param address2 the address2 to set
     */
    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(address1, address2, id, person);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        PersonAddress other = (PersonAddress) obj;
        return Objects.equals(address1, other.address1) && Objects.equals(address2, other.address2) && Objects.equals(id, other.id)
                && Objects.equals(person, other.person);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("PersonAddress [id=");
        builder.append(id);
        builder.append(", person=");
        builder.append(person);
        builder.append(", address1=");
        builder.append(address1);
        builder.append(", address2=");
        builder.append(address2);
        builder.append("]");
        return builder.toString();
    }

    @Singleton
    public static class PersonAddressFilter implements DtoFilter<PersonAddress> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement stmt = builder.build(TABLE);
            // id check
            String id = params.getFirst(DefaultUrlParameterNames.ID.getName());
            if (StringUtils.isNumeric(id)) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".id = ?", new Object[] { Integer.valueOf(id) }));
            }
            // person id check
            String personId = params.getFirst("person_id");
            if (StringUtils.isNumeric(id)) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".person.id = ?", new Object[] { personId }));
            }
            return stmt;
        }

        @Override
        public Class<PersonAddress> getType() {
            return PersonAddress.class;
        }

    }
}
