package org.eclipsefoundation.persistence.test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import io.quarkus.test.junit.QuarkusTestProfile;

/**
 * Enabled Distributed CSRF mode for testing. 
 * 
 * @author Martin Lowe
 */
public class DistributedCsrfEnabledTestProfile implements QuarkusTestProfile {

    private static final Map<String, String> CONFIG_OVERRIDES;
    static {
        Map<String, String> tmp = new HashMap<>();
        tmp.put("eclipse.security.csrf.distributed-mode.enabled", "true");
        CONFIG_OVERRIDES = Collections.unmodifiableMap(tmp);
    }

    @Override
    public Map<String, String> getConfigOverrides() {
        return CONFIG_OVERRIDES;
    }
}
