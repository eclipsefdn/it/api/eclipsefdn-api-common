/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.persistence.model;

import java.util.Optional;

import jakarta.inject.Inject;

import org.eclipsefoundation.persistence.test.dto.PersonDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

/**
 * Tests for the parameterized SQL statement builder. Should ensure that the logic to build the base statements set the
 * internal fields as intended.
 * 
 * @author Martin Lowe
 *
 */
@QuarkusTest
class ParameterizedSQLStatementBuilderTest {

    @Inject
    ParameterizedSQLStatementBuilder builder;

    @Test
    void build_success() {
        ParameterizedSQLStatement stmt = builder.build(PersonDTO.TABLE);
        Assertions.assertEquals(PersonDTO.TABLE, stmt.getBase(), "Base of new SQL statement should be the passed table reference");
    }

    @Test
    void buildCallStatement_success() {
        ParameterizedSQLStatement stmt = builder.buildCallStatement(PersonDTO.TABLE);
        Assertions.assertEquals(PersonDTO.TABLE, stmt.getBase(), "Base of new SQL call statement should be the passed table reference");
        Assertions
                .assertEquals(ParameterizedCallStatement.class, stmt.getClass(),
                        "Returned SQL statement should be the call statement subtype");
    }

    @Test
    void buildWithId_success_addsIdClause() {
        String sampleId = "sample-id";
        ParameterizedSQLStatement stmt = builder.buildWithId(PersonDTO.TABLE, Optional.of(sampleId), String.class);
        Assertions.assertFalse(stmt.getClauses().isEmpty());
        Assertions
                .assertEquals(sampleId, stmt.getClauses().get(0).getParams()[0],
                        "Expected the sample id to be passed without modification");
    }

    @Test
    void buildWithId_success_supportsLong() {
        Long expectedId = 100l;
        String sampleId = Long.toString(expectedId);
        ParameterizedSQLStatement stmt = builder.buildWithId(PersonDTO.TABLE, Optional.of(sampleId), Long.class);
        Assertions.assertFalse(stmt.getClauses().isEmpty());
        Assertions
                .assertEquals(expectedId, stmt.getClauses().get(0).getParams()[0],
                        "Expected the sample id to be passed as a Long of the same value");
    }

    @Test
    void buildWithId_success_skipsIdClauseWhenMissing() {
        ParameterizedSQLStatement stmt = builder.buildWithId(PersonDTO.TABLE, Optional.empty(), Long.class);
        Assertions.assertTrue(stmt.getClauses().isEmpty());
    }

    @Test
    void buildWithId_success_defaultsToStringConversionWhenMissingType() {
        String sampleId = "sample-id";
        ParameterizedSQLStatement stmt = builder.buildWithId(PersonDTO.TABLE, Optional.of(sampleId), null);
        Assertions.assertFalse(stmt.getClauses().isEmpty());
        Assertions
                .assertEquals(sampleId, stmt.getClauses().get(0).getParams()[0],
                        "Expected the sample id to be passed without modification");
    }

    @Test
    void buildWithId_success_defaultsToStringConversionWhenUnknownType() {
        String sampleId = "sample-id";
        // byte was used as it isn't handled for ID, not because it should be a string
        ParameterizedSQLStatement stmt = builder.buildWithId(PersonDTO.TABLE, Optional.of(sampleId), Byte.class);
        Assertions.assertFalse(stmt.getClauses().isEmpty());
        Assertions
                .assertEquals(sampleId, stmt.getClauses().get(0).getParams()[0],
                        "Expected the sample id to be passed without modification");
    }
}
