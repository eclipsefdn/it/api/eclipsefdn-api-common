/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.service.impl;

import jakarta.inject.Inject;

import org.eclipsefoundation.persistence.dto.mapper.EntityMapper;
import org.eclipsefoundation.persistence.service.MapperService;
import org.eclipsefoundation.persistence.test.dto.PersonAddress;
import org.eclipsefoundation.persistence.test.dto.PersonDTO;
import org.eclipsefoundation.persistence.test.model.PersonModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

/**
 * Tests the MapperService by attempting to retrieve a Mapper by model and DTO. Tests that the retrieved Mapper converts
 * properly in both directions.
 * 
 * @author Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
 */
@QuarkusTest
class MapperServiceTest {

    @Inject
    MapperService mapperService;

    @Test
    void get_success() {
        EntityMapper<?, ?> mapper = mapperService.get(PersonDTO.class, PersonModel.class);
        Assertions.assertNotNull(mapper);
        Assertions.assertEquals(PersonDTO.class, mapper.getDTOType());
        Assertions.assertEquals(PersonModel.class, mapper.getModelType());
    }

    @Test
    void get_failure_mismatchDto() {
        Assertions.assertNull(mapperService.get(PersonAddress.class, PersonModel.class));
    }

    @Test
    void get_failure_mismatchModel() {
        Assertions.assertNull(mapperService.get(PersonAddress.class, getClass()));
    }

}
