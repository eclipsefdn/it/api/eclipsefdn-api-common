/**
 * Copyright (c) 2022 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.persistence.test.dao;

import jakarta.enterprise.context.ApplicationScoped;

import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;

/**
 * Enable DAO tests as deployment phase injections aren't available in this package.
 * 
 * @author Martin Lowe
 *
 */
@ApplicationScoped
public class DefaultTestDao extends DefaultHibernateDao {
}
