/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.persistence.service.impl;

import jakarta.inject.Inject;

import org.eclipsefoundation.persistence.test.dto.EntityWithoutFilter;
import org.eclipsefoundation.persistence.test.dto.PersonDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

/**
 * 
 */
@QuarkusTest
class DefaultFilterServiceTest {

    @Inject
    DefaultFilterService svc;

    @Test
    void get_success() {
        Assertions.assertNotNull(svc.get(PersonDTO.class));
    }

    @Test
    void get_failure_missingFilter() {
        Assertions.assertNull(svc.get(EntityWithoutFilter.class));
    }
}
