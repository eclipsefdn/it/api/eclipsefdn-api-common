package org.eclipsefoundation.persistence.model;

import java.net.URI;
import java.security.Principal;
import java.util.List;
import java.util.UUID;

import org.eclipsefoundation.http.model.FlatRequestWrapper;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.dto.DistributedCSRFToken;
import org.eclipsefoundation.persistence.dto.filter.DistributedCSRFTokenFilter;
import org.eclipsefoundation.utils.namespace.AdditionalHttpHeaders;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import io.netty.handler.codec.http.HttpHeaderNames;
import io.quarkus.test.junit.QuarkusTest;
import io.vertx.core.http.HttpServerRequest;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.SecurityContext;

@QuarkusTest
class DistributedCSRFGeneratorTest {

    @Inject
    PersistenceDao dao;
    @Inject
    DistributedCSRFTokenFilter filter;

    private DistributedCSRFGenerator generator;

    @BeforeEach
    void init() {
        this.generator = new DistributedCSRFGenerator(dao, filter);
    }

    @Test
    void getCSRFToken_success_anon() {
        String ip = "127.0.0.1";
        String userAgent = "eclipse/test:" + UUID.randomUUID().toString();
        Principal p = null;
        String url = "https://eclipse.org/test";
        RequestWrapper wrap = new FlatRequestWrapper(URI.create(url));

        // set up parameters to check for token before and after test
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(DistributedCSRFGenerator.IP_PARAM, ip);
        params.add(DistributedCSRFGenerator.USERAGENT_PARAM, userAgent);

        Assertions.assertTrue(dao.get(new RDBMSQuery<>(wrap, filter, params)).isEmpty(), "Expected no tokens for unique useragent in test");

        // set up mocked request to forward to the generator
        HttpServerRequest mockedRequest = Mockito.mock(HttpServerRequest.class);
        Mockito.when(mockedRequest.uri()).thenReturn(url);
        Mockito.when(mockedRequest.getHeader(AdditionalHttpHeaders.X_FORWARDED_FOR)).thenReturn(ip);
        Mockito.when(mockedRequest.getHeader(HttpHeaderNames.USER_AGENT)).thenReturn(userAgent);
        
        SecurityContext mockedSecurity = Mockito.mock(SecurityContext.class);
        Mockito.when(mockedSecurity.getUserPrincipal()).thenReturn(p);

        // generate the token, which should persist the token
        String token = this.generator.getCSRFToken(mockedRequest, mockedSecurity);
        // get the tokens in the database w/ the same params
        List<DistributedCSRFToken> tokens = dao.get(new RDBMSQuery<>(wrap, filter, params));
        // check that we have entries
        Assertions.assertEquals(1, tokens.size());
        // retrieve the token for testing
        DistributedCSRFToken storedToken = tokens.get(0);

        // do assertions to validate test state
        Assertions.assertEquals(ip, storedToken.getIpAddress(), "Expected matching IP with string passed from request");
        Assertions.assertNull(storedToken.getUser(), "Expected null user for tokens with anonymous user");
        Assertions.assertEquals(userAgent, storedToken.getUserAgent(), "Stored user agent should match the value passed on the request");
        Assertions.assertEquals(storedToken.getToken(), token, "Expected database token value to match the output of the get token call.");
    }
}
