/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.test.dto.mapper;

import org.eclipsefoundation.persistence.config.QuarkusMappingConfig;
import org.eclipsefoundation.persistence.dto.mapper.EntityMapper;
import org.eclipsefoundation.persistence.test.dto.PersonDTO;
import org.eclipsefoundation.persistence.test.model.PersonModel;
import org.mapstruct.Mapper;

/**
 * A Mapper used for testing. It maps between 2 test entity classes, PersonDTO and PersonModel.
 */
@Mapper(config = QuarkusMappingConfig.class)
public interface PersonMapper extends EntityMapper<PersonDTO, PersonModel> {

    @Override
    default Class<PersonDTO> getDTOType() {
        return PersonDTO.class;
    }

    @Override
    default Class<PersonModel> getModelType() {
        return PersonModel.class;
    }
}