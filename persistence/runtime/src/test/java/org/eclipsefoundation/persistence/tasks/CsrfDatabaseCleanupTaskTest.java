/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.tasks;

import java.net.URI;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import org.eclipsefoundation.http.model.FlatRequestWrapper;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.dto.DistributedCSRFToken;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.persistence.test.DistributedCsrfEnabledTestProfile;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import jakarta.inject.Inject;

/**
 * Test the db cleanup task by running it manually and checking the outputs
 */
@QuarkusTest
@TestInstance(Lifecycle.PER_CLASS)
@TestProfile(DistributedCsrfEnabledTestProfile.class)
class CsrfDatabaseCleanupTaskTest {

    @Inject
    CsrfDatabaseCleanupTask task;
    @Inject
    DefaultHibernateDao dao;
    @Inject
    FilterService filters;

    @Test
    void schedule_success_deletesOnlyRecordsInPast() {
        // initial results to compare post-test
        RDBMSQuery<DistributedCSRFToken> initialQuery = new RDBMSQuery<>(
                new FlatRequestWrapper(URI.create("http://api.eclipse.org/task/test")), filters.get(DistributedCSRFToken.class), null);
        List<DistributedCSRFToken> initial = dao.get(initialQuery);

        // get the records that should be retained after deletion scheduled task
        ZonedDateTime before = ZonedDateTime.now().minus(1, ChronoUnit.DAYS);
        List<DistributedCSRFToken> expected = initial.stream().filter(t -> t.getTimestamp().isAfter(before)).toList();
        // test is invalid, as we should always be deleting at least 1 record
        long count = initial.size() - expected.size();
        Assertions.assertTrue(count > 0);

        // run the task to clean out records
        task.schedule();

        // get the results after the run and compare to previous list size minus the expected removal count
        List<DistributedCSRFToken> actual = dao.get(initialQuery);
        Assertions.assertEquals(expected, actual);
    }
}
