/**
 * Copyright (c) 2022 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.persistence.dao;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.http.model.FlatRequestWrapper;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.namespace.PersistencePropertyNames;
import org.eclipsefoundation.persistence.test.dto.PersonAddress;
import org.eclipsefoundation.persistence.test.dto.PersonAddress.PersonAddressFilter;
import org.eclipsefoundation.persistence.test.dto.PersonDTO;
import org.eclipsefoundation.persistence.test.dto.PersonDTO.PersonDTOFilter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * Tests the default hibernate DAO to ensure basic functionality.
 * 
 * @author Martin Lowe
 *
 */
@QuarkusTest
class DefaultHibernateDaoTest {

    // Set to 10 in test profile
    @ConfigProperty(name = PersistencePropertyNames.PERSISTENCE_PAGINATION_LIMIT)
    int defaultPaginationLimit;
    // Set to 20 in test profile
    @ConfigProperty(name = PersistencePropertyNames.PERSISTENCE_PAGINATION_LIMIT_MAX)
    int maxPaginationLimit;

    @Inject
    PersistenceDao dao;
    @Inject
    PersonDTOFilter filter;
    @Inject
    PersonAddressFilter paFilter;

    @Test
    void testGet_existingRecord() {
        // run get to test results
        List<PersonDTO> results = dao.get(getBasicQuery("12345"));
        // assert we have the proper result
        Assertions.assertNotNull(results);
        Assertions.assertFalse(results.isEmpty());

        // check the fields to ensure same
        PersonDTO dto = results.get(0);
        Assertions.assertEquals("12345", dto.getPersonID());
        Assertions.assertEquals("opearson", dto.getName());
        Assertions.assertEquals(99, dto.getAge());
    }

    @Test
    void testGet_noExistingRecord() {
        // run get to test results
        List<PersonDTO> results = dao.get(getBasicQuery("no-record-here"));
        // assert we have the proper result
        Assertions.assertNotNull(results);
        Assertions.assertTrue(results.isEmpty());
    }

    @Test
    void testGet_paginated() {

        // Generate initial list larger than max limit
        dao.add(getBasicQuery(""), generateDTOList(30));

        // No pagesize provided, return default size
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("https://eclipse.org"));
        RDBMSQuery<PersonDTO> query = new RDBMSQuery<>(wrap, filter);
        List<PersonDTO> paginatedResults = dao.get(query);
        Assertions.assertEquals(defaultPaginationLimit, paginatedResults.size());

        // Test with pagesize within max limit
        wrap = new FlatRequestWrapper(URI.create("https://eclipse.org"));
        wrap.addParam(DefaultUrlParameterNames.PAGESIZE, "5");
        query = new RDBMSQuery<>(wrap, filter);
        paginatedResults = dao.get(query);
        Assertions.assertEquals(5, paginatedResults.size());

        // Test with pagesize larger than max
        wrap = new FlatRequestWrapper(URI.create("https://eclipse.org"));
        wrap.addParam(DefaultUrlParameterNames.PAGESIZE, "42");
        query = new RDBMSQuery<>(wrap, filter);
        paginatedResults = dao.get(query);
        Assertions.assertEquals(maxPaginationLimit, paginatedResults.size());

        // Test with negative pagesize
        wrap = new FlatRequestWrapper(URI.create("https://eclipse.org"));
        wrap.addParam(DefaultUrlParameterNames.PAGESIZE, "-42");
        query = new RDBMSQuery<>(wrap, filter);
        paginatedResults = dao.get(query);
        Assertions.assertEquals(defaultPaginationLimit, paginatedResults.size());
    }

    @Test
    void testAdd_newRecord() {
        String id = UUID.randomUUID().toString();
        PersonDTO personRecord = new PersonDTO(id, "test", 11);
        List<PersonDTO> results = dao.add(getBasicQuery(""), Arrays.asList(personRecord));
        // assert we have the proper result and it is persisted
        Assertions.assertNotNull(results);
        Assertions.assertFalse(results.isEmpty());
        Assertions.assertEquals(personRecord, results.get(0));

        // retrieve a fresh instance to ensure data has been saved properly
        List<PersonDTO> persistedResults = dao.get(getBasicQuery(personRecord.getPersonID()));
        Assertions.assertNotNull(persistedResults);
        Assertions.assertFalse(persistedResults.isEmpty());
        Assertions.assertEquals(personRecord, persistedResults.get(0));
    }

    @Test
    void testAdd_newRecord_returnsAutoIncrementIds() {
        String personId = "67890";
        // retrieve a fresh instance to ensure data has been saved properly
        List<PersonDTO> results = dao.get(getBasicQuery(personId));
        PersonAddress pa = new PersonAddress();
        pa.setAddress1("");
        pa.setAddress2("");
        pa.setPerson(results.get(0));

        List<PersonAddress> actualResults = dao.add(getPersonAddressQuery(personId), Arrays.asList(pa));
        Assertions.assertNotNull(actualResults);
        Assertions.assertFalse(actualResults.isEmpty());
        Assertions.assertNotNull(actualResults.get(0).getId());
        Assertions.assertEquals(pa, actualResults.get(0));
    }

    /**
     * Relies on get(...) being functional. If this and the get call fails, there's a good chance the problem is there.
     */
    @Test
    void testAdd_updateRecord() {
        // check that the record for this test exists
        String targetId = "update-record-test-1";
        List<PersonDTO> initialRecords = dao.get(getBasicQuery(targetId));
        Assertions.assertNotNull(initialRecords);
        Assertions.assertFalse(initialRecords.isEmpty());

        // generate new record using same ID, random name
        PersonDTO insertedRecord = new PersonDTO(targetId, UUID.randomUUID().toString(), 100);
        List<PersonDTO> results = dao.add(getBasicQuery(targetId), Arrays.asList(insertedRecord));
        Assertions.assertNotNull(results);
        Assertions.assertEquals(1, results.size());
        Assertions.assertEquals(insertedRecord, results.get(0));
        Assertions.assertNotEquals(initialRecords.get(0), results.get(0));

        // check that the DB record has been truly updated
        List<PersonDTO> updatedRecords = dao.get(getBasicQuery(targetId));
        Assertions.assertNotNull(updatedRecords);
        Assertions.assertEquals(1, updatedRecords.size());
        Assertions.assertEquals(results.get(0), updatedRecords.get(0));
    }

    @Test
    void testAdd_addOrUpdateMultiple() {
        // trigger update with multiple records
        List<PersonDTO> results = dao
                .add(getBasicQuery(""),
                        Arrays
                                .asList(new PersonDTO(UUID.randomUUID().toString(), UUID.randomUUID().toString(), 101),
                                        new PersonDTO(UUID.randomUUID().toString(), UUID.randomUUID().toString(), 102)));
        // as test table, there won't ever be more than maybe a dozen entries here
        List<PersonDTO> actualResults = dao.get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://eclipse.org")), filter));
        // check that each of the results in the return of the add call exist in the dataset
        Assertions.assertTrue(results.stream().allMatch(r -> actualResults.stream().anyMatch(ar -> ar.equals(r))));
    }

    @Test
    void testDelete_noMatchingRecords() {
        // run delete, and have no exception throw
        Assertions.assertDoesNotThrow(() -> dao.delete(getBasicQuery("no-record-here")));
    }

    @Test
    void getLimit_respectsConfigs() {

        // No pagesize provided in request, use default size
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("https://eclipse.org"));
        RDBMSQuery<PersonDTO> query = new RDBMSQuery<>(wrap, filter);
        Assertions.assertEquals(defaultPaginationLimit, dao.getLimit(query));

        // Test with pagesize within max limit
        wrap = new FlatRequestWrapper(URI.create("https://eclipse.org"));
        wrap.addParam(DefaultUrlParameterNames.PAGESIZE, "5");
        query = new RDBMSQuery<>(wrap, filter);
        Assertions.assertEquals(5, dao.getLimit(query));

        // Test with pagesize larger than max
        wrap = new FlatRequestWrapper(URI.create("https://eclipse.org"));
        wrap.addParam(DefaultUrlParameterNames.PAGESIZE, "42");
        query = new RDBMSQuery<>(wrap, filter);
        Assertions.assertEquals(maxPaginationLimit, dao.getLimit(query));

        // Test with negative pagesize
        wrap = new FlatRequestWrapper(URI.create("https://eclipse.org"));
        wrap.addParam(DefaultUrlParameterNames.PAGESIZE, "-42");
        query = new RDBMSQuery<>(wrap, filter);
        Assertions.assertEquals(defaultPaginationLimit, dao.getLimit(query));
    }

    @Test
    void testDelete_matchingRecord() {
        // check that the record for this test exists
        String targetId = "delete-record-test-1";
        List<PersonDTO> initialRecords = dao.get(getBasicQuery(targetId));
        Assertions.assertNotNull(initialRecords);
        Assertions.assertFalse(initialRecords.isEmpty());

        // run delete, and have no exception throw
        Assertions.assertDoesNotThrow(() -> dao.delete(getBasicQuery(targetId)));

        // check that the record for this test has been removed
        List<PersonDTO> updatedRecords = dao.get(getBasicQuery(targetId));
        Assertions.assertNotNull(updatedRecords);
        Assertions.assertTrue(updatedRecords.isEmpty());
    }

    /**
     * Create basic query based on ID to simplify tests.
     * 
     * @param id person ID of entry to attempt to retrieve
     * @return a prepared RDBMS query, ready to be used
     */
    private RDBMSQuery<PersonDTO> getBasicQuery(String id) {
        // set up query for existing data
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(DefaultUrlParameterNames.ID.getName(), id);
        return new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://eclipse.org")), filter, params);
    }

    /**
     * Create basic query based on ID to simplify tests.
     * 
     * @param id person ID of entry to attempt to retrieve
     * @return a prepared RDBMS query, ready to be used
     */
    private RDBMSQuery<PersonAddress> getPersonAddressQuery(String personId) {
        // set up query for existing data
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add("person_id", personId);
        return new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://eclipse.org")), paFilter, params);
    }

    /**
     * Create a sized list of PersonDTO entities using the given size.
     * @param size The desired list size.
     * @return A sized list of PersonDTO entities
     */
    private List<PersonDTO> generateDTOList(int size) {
        List<PersonDTO> out = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            out.add(new PersonDTO(UUID.randomUUID().toString(), UUID.randomUUID().toString(), i));
        }
        return out;
    }
}
