/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.persistence.namespace;

import java.util.UUID;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;

import io.quarkus.test.junit.QuarkusTest;

/**
 * Basic tests to assure sort order code works as expected.
 */
@QuarkusTest
class SortOrderTest {

    @ParameterizedTest
    @ArgumentsSource(value = NotNoneSortOrderValues.class)
    void getOrderByName_success(SortOrder order) {
        Assertions
                .assertEquals(order, SortOrder.getOrderByName(order.name()), "Name of an order should directly map to the same sort order");
    }

    @ParameterizedTest
    @ArgumentsSource(value = NotNoneSortOrderValues.class)
    void getOrderByName_success_shortName(SortOrder order) {
        Assertions
                .assertEquals(order, SortOrder.getOrderByName(order.getShortName()),
                        "Short name of an order should directly map to the same sort order");
    }

    @ParameterizedTest
    @ArgumentsSource(value = NotNoneSortOrderValues.class)
    void getOrderByValue_success(SortOrder order) {
        Assertions
                .assertEquals(order, SortOrder.getOrderFromValue(UUID.randomUUID().toString() + ' ' + order.name()),
                        "Short name of an order should directly map to the same sort order");
    }

    @ParameterizedTest
    @ArgumentsSource(value = NotNoneSortOrderValues.class)
    void getOrderByValue_success_shortName(SortOrder order) {
        Assertions
                .assertEquals(order, SortOrder.getOrderFromValue(UUID.randomUUID().toString() + ' ' + order.getShortName()),
                        "Short name of an order should directly map to the same sort order");
    }

    @Test
    void getOrderByValue_success_supportsRandom() {
        Assertions
                .assertEquals(SortOrder.RANDOM, SortOrder.getOrderFromValue("random"),
                        "Using random as sort value should return the random sort order");
        Assertions
                .assertEquals(SortOrder.RANDOM, SortOrder.getOrderFromValue("RANDOM"),
                        "Using RANDOM as sort value should return the random sort order");
        Assertions
                .assertEquals(SortOrder.RANDOM, SortOrder.getOrderFromValue("rand"),
                        "Using rand as sort value should return the random sort order");
        Assertions
                .assertEquals(SortOrder.RANDOM, SortOrder.getOrderFromValue("RAND"),
                        "Using rand as sort value should return the random sort order");
    }

    /**
     * Arg provider that returns Hellosign Event Types that are recorded by the database on submission.
     * 
     * @author Martin Lowe
     *
     */
    public static class NotNoneSortOrderValues implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
            return Stream.of(SortOrder.values()).filter(so -> so != SortOrder.NONE).map(Arguments::of);
        }
    }
}
