/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.test.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * A basic Model class with a few simple fields. Used for testing.
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_PersonModel.Builder.class)
public abstract class PersonModel {
    public abstract String getPersonID();

    public abstract String getName();

    public abstract int getAge();

    public static Builder builder() {
        return new AutoValue_PersonModel.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setPersonID(String personID);

        public abstract Builder setName(String name);

        public abstract Builder setAge(int age);

        public abstract PersonModel build();
    }
}