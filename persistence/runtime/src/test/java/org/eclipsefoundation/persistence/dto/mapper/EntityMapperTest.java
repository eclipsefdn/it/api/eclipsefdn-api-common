/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.persistence.dto.mapper;

import jakarta.inject.Inject;

import org.eclipsefoundation.persistence.test.dto.PersonDTO;
import org.eclipsefoundation.persistence.test.dto.mapper.PersonMapper;
import org.eclipsefoundation.persistence.test.model.PersonModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

/**
 * Tests entity mapper defaults through simple case with no complex mappings.
 * 
 * @author Martin Lowe
 *
 */
@QuarkusTest
class EntityMapperTest {

    @Inject
    PersonMapper mapper;

    @Test
    void convertToDTO_mapperByModel() {
        PersonModel personModel = PersonModel.builder().setAge(25).setName("JimBob").setPersonID("TM-50").build();
        PersonDTO personDTO = mapper.toDTO(personModel);

        Assertions.assertEquals(personDTO.getId(), personModel.getPersonID());
        Assertions.assertEquals(personDTO.getName(), personModel.getName());
        Assertions.assertEquals(personDTO.getAge(), personModel.getAge());
    }

    @Test
    void convertToModel_mapperbyDTO() {

        PersonDTO personDTO = new PersonDTO();
        personDTO.setPersonID("TM-50");
        personDTO.setName("JimBob");
        personDTO.setAge(25);

        PersonModel personModel = mapper.toModel(personDTO);

        Assertions.assertEquals(personDTO.getId(), personModel.getPersonID());
        Assertions.assertEquals(personDTO.getName(), personModel.getName());
        Assertions.assertEquals(personDTO.getAge(), personModel.getAge());
    }
}
