/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.persistence.test.dto;

import java.io.Serializable;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;
import org.eclipsefoundation.persistence.test.namespace.PersistenceTestParameterNames;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * A basic DTO class with a few simple fields. Used for testing.
 */
@Entity
@Table(name = "Person")
public class PersonDTO extends BareNode implements Serializable {
    private static final long serialVersionUID = 1L;

    public static final DtoTable TABLE = new DtoTable(PersonDTO.class, "p");

    @Id
    @Column(unique = true, nullable = false)
    private String personID;
    private String name;
    private int age;

    /**
     * Default constructor, empty
     */
    public PersonDTO() {
    }

    /**
     * Generate a populated Person record.
     * 
     * @param personID the ID of the person
     * @param name the name for the record
     * @param age the age of the person
     */
    public PersonDTO(String personID, String name, int age) {
        this.personID = personID;
        this.name = name;
        this.age = age;
    }

    @Override
    public String getId() {
        return getPersonID();
    }

    public String getPersonID() {
        return this.personID;
    }

    public void setPersonID(String id) {
        this.personID = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return this.age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(age, name, personID);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        PersonDTO other = (PersonDTO) obj;
        return age == other.age && Objects.equals(name, other.name) && Objects.equals(personID, other.personID);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("PersonDTO [personID=");
        builder.append(personID);
        builder.append(", name=");
        builder.append(name);
        builder.append(", age=");
        builder.append(age);
        builder.append("]");
        return builder.toString();
    }

    @Singleton
    public static class PersonDTOFilter implements DtoFilter<PersonDTO> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement stmt = builder.build(TABLE);
            // id check
            String id = params.getFirst(DefaultUrlParameterNames.ID.getName());
            if (id != null) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".personID = ?", new Object[] { id }));
            }
            // minimum age
            String minAge = params.getFirst(PersistenceTestParameterNames.AT_LEAST_OF_AGE.getName());
            if (StringUtils.isNumeric(minAge)) {
                stmt
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".age > ?",
                                new Object[] { Integer.parseInt(minAge) }));
            }
            return stmt;
        }

        @Override
        public Class<PersonDTO> getType() {
            return PersonDTO.class;
        }

    }
}