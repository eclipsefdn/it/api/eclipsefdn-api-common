/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.persistence.test.namespace;

import java.util.Arrays;
import java.util.List;

import org.eclipsefoundation.utils.namespace.UrlParameterNamespace;

/**
 * A couple of parameters to be used in creating realistic test cases for this package.
 * 
 * @author Martin Lowe
 *
 */
public class PersistenceTestParameterNames implements UrlParameterNamespace {
    public static final UrlParameter AT_LEAST_OF_AGE = new UrlParameter("at_least_of_age");

    @Override
    public List<UrlParameter> getParameters() {
        return Arrays.asList(AT_LEAST_OF_AGE);
    }

}
