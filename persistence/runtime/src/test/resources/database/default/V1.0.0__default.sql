CREATE TABLE Person (
  personID varchar(128) NOT NULL,
  name varchar(128) NOT NULL,
  age int NOT NULL,
  PRIMARY KEY(personID)
);
INSERT INTO Person(personID,name,age)
  VALUES('12345', 'opearson', 99);
INSERT INTO Person(personID,name,age)
  VALUES('update-record-test-1', 'someone else', 111);
INSERT INTO Person(personID,name,age)
  VALUES('delete-record-test-1', 'someone else', 111);
INSERT INTO Person(personID,name,age)
  VALUES('67890', 'aporpoise', 15);
INSERT INTO Person(personID,name,age)
  VALUES('13467', 'Dan Jackson', 32);
INSERT INTO Person(personID,name,age)
  VALUES('25852', 'John Sheppard', 35);
INSERT INTO Person(personID,name,age)
  VALUES('19735', 'Ronon Dex', 30);

CREATE TABLE PersonAddress (
  id integer auto_increment,
  person_id varchar(128) NOT NULL,
  address1 varchar(128) NOT NULL,
  address2 varchar(128) NOT NULL,
  PRIMARY KEY(id)
);

CREATE TABLE `DistributedCSRFToken` (
  `token` varchar(100) NOT NULL,
  `ipAddress` varchar(64) NOT NULL,
  `userAgent` varchar(255) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `user` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`token`)
);

INSERT INTO DistributedCSRFToken(token, ipAddress, userAgent, user)
  VALUES ('1','0.0.0.0','some/useragent', 'tester' );
INSERT INTO DistributedCSRFToken(token, ipAddress, userAgent, user)
  VALUES ('2','0.0.0.0','some/useragent', 'opearson' );
INSERT INTO DistributedCSRFToken(token, ipAddress, userAgent, user)
  VALUES ('3','0.0.0.0','some/useragent', 'slom' );
INSERT INTO DistributedCSRFToken(token, ipAddress, userAgent, user)
  VALUES ('4','0.0.0.0','some/useragent', 'slom' );
INSERT INTO DistributedCSRFToken(token, ipAddress, userAgent, timestamp, user)
  VALUES ('123','0.0.0.0','some/useragent', '2023-01-01', 'opearson' );
INSERT INTO DistributedCSRFToken(token, ipAddress, userAgent, timestamp, user)
  VALUES ('456','0.0.0.0','some/useragent', '2023-01-02', 'opearson' );
INSERT INTO DistributedCSRFToken(token, ipAddress, userAgent, timestamp, user)
  VALUES ('789','0.0.0.0','some/useragent', '2023-01-03', 'opearson' );
