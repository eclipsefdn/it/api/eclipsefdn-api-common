/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.core.tasks;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.eclipsefoundation.core.config.LogRotationConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.scheduler.Scheduled;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

/**
 * <p>
 * Cleanup task for log files with a given format. This is created as the JBoss logging framework for periodic rotated log files will not
 * delete log files from previous days, only items that surpass the max backups when there are multiple log files for a given day.
 * 
 * <p>
 * This solution will take the configured base directory and base file name along with a hardcoded date suffix format and look for files
 * that should be removed. Take the below example assuming the date is March 1st, 2024:
 * </p>
 * 
 * <p>
 * Properties:<br>
 * <code>
 * eclipse.log.file-rotation.max-age=P365D<br>
 * eclipse.log.file-rotation.base-dir=/deployments/logs<br>
 * eclipse.log.file-rotation.base-file-name=app.log  
 * </code>
 * </p>
 * 
 * <p>
 * Files:
 * <ol>
 * <li>/deployments/logs/app.log.machine-name-1.2024-03-01
 * <li>/deployments/logs/app.log.machine-name-1.2024-01-01
 * <li>/deployments/logs/app.log.machine-name-2.2024-01-01
 * <li>/deployments/logs/app.log.machine-name-2.2023-01-01
 * <li>/deployments/logs/app.log.machine-name-2.2023-01-01.2
 * <li>/deployments/logs/app.log.machine-name-2
 * <li>/deployments/logs/test.machine-name-2.2024-01-01
 * <li>/deployments/logs/nested/app.log.machine-name-2.2023-01-01
 * </ol>
 * </p>
 * 
 * <p>
 * In this example, entries 1 through 5 are detected as log files for the application. Of those, #4 has a timestamp that would indicate
 * expiration based on the max-age, and #5 is the rotated secondary file for the same day and would also be deleted. Those log files would
 * be deleted on run of this task. The remaining items logic for not processing or retaining are as such:
 * <ul>
 * <li>#6 has no datestamp, so it would not be processed by this task
 * <li>#7 has a different base file name, and would not be further processed
 * <li>#8 is in a nested directory and would not be managed by this task.
 * </ul>
 * </p>
 */
@ApplicationScoped
public class LogFileRotationCleanupTask {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogFileRotationCleanupTask.class);

    private static final Pattern FILE_SUFFIX_PATTERN = Pattern.compile("\\.(\\d{4}-\\d{2}-\\d{2})(?:\\.\\d+)?$");
    public static final DateTimeFormatter SIMPLE_DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Inject
    LogRotationConfig config;

    @Scheduled(delay = 5L, every = "${eclipse.log.file.rotation.frequency:P1D}")
    public void cleanup() {
        if (config.enabled()) {
            // check that the dir for logs is valid before starting
            Path baseDir = Path.of(config.baseDir());
            if (!Files.exists(baseDir) || !Files.isDirectory(baseDir)) {
                LOGGER.error("Log folder {} cannot be read or verified as a directory, skipping clean", baseDir);
                return;
            }
            // generate a localdate to hold that maximum stated age of a log file
            LocalDate ld = LocalDate.now().minus(config.maxAge());

            // filter out any of the paths that are within the max age of the configs
            List<Path> olderFilePaths = getLogFilePaths(baseDir).filter(p -> isFileOlderThanMaxAge(p, ld)).toList();
            LOGGER.info("Found {} log files for cleaning", olderFilePaths.size());

            // for each path that is older than max age, attempt to delete the log file
            olderFilePaths.forEach(p -> {
                try {
                    Files.delete(p);
                } catch (IOException e) {
                    LOGGER.error("Error while removing log file '{}'", p, e);
                }
            });
        }
    }

    /**
     * Checks if the file indicated by the path exists and is accepted as a log file by name format. This check assumes that the file is in
     * the correct directory and has already been checked to have the right file prefix.
     * 
     * The check performed for age assumes that the file ends with a datestamp in the format year-month-day like `2020-01-15`, or the
     * previous option with a rotated log file index like `2020-01-15.1`. This is then checked against the max-age as set in the
     * configuration.
     * 
     * @param p path to the potential log file to be removed.
     * @param maxAge the local date containing the max age of log files to keep.
     * @return true if the file exists and matches the above conditions for deletion, otherwise false
     */
    private boolean isFileOlderThanMaxAge(Path p, LocalDate maxAge) {
        // check if the path is a valid file and not a directory or missing.
        if (!Files.exists(p) || Files.isDirectory(p)) {
            LOGGER.debug("System resource w/ path {} is not a valid file and will not be processed", p);
            return false;
        }

        // attempt to extract the date string from the path
        Matcher m = FILE_SUFFIX_PATTERN.matcher(p.toString());
        if (!m.find()) {
            LOGGER.debug("File w/ path {} does not match log suffix pattern and will not be evaluated for cleaning", p);
            return false;
        }

        // extract the date string and parse it into a date object
        String dateString = m.group(1);
        try {
            return LocalDate.parse(dateString, SIMPLE_DATE_FORMAT).isBefore(maxAge);
        } catch (DateTimeParseException e) {
            LOGGER.warn("Log file has bad date string in file path {}", p, e);
            // return false as we don't want to delete files we aren't sure of
            return false;
        }
    }

    /**
     * Using the validated base directory for log files, do a file walk of files in the direct folder that have a file name that start with
     * the stated file prefix.
     * 
     * @param baseDir path that represents a valid directory that contains log files
     * @return stream of filtered file paths that match the prefix set in `eclipse.log.file-rotation.base-file-name`.
     */
    private Stream<Path> getLogFilePaths(Path baseDir) {
        try {
            return Files.find(baseDir, 1, (p, attrs) -> p.getFileName().toString().startsWith(config.baseFileName()));
        } catch (IOException e) {
            LOGGER.error("Error while loading log files for cleaning", e);
        }
        return Stream.empty();
    }
}
