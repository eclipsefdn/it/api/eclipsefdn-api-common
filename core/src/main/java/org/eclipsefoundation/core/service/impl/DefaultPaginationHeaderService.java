/**
 * Copyright (c) 2022 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.core.service.impl;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipsefoundation.caching.model.ParameterizedCacheKey;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.core.service.PaginationHeaderService;
import org.eclipsefoundation.http.config.PaginationConfig;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.utils.helper.ParameterHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.arc.All;
import io.quarkus.cache.Cache;
import io.quarkus.cache.CacheName;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

/**
 * Default resolver that uses the Quarkus base cache to store context objects for later lookup and usage. Enables usage
 * of fallbacks by using fuzzy lookups of keys in cache.
 * 
 * @author Martin Lowe
 *
 */
@ApplicationScoped
public class DefaultPaginationHeaderService implements PaginationHeaderService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultPaginationHeaderService.class);

    @Inject
    PaginationConfig config;

    @All
    List<PaginationResolver<?>> resolvers;

    @Inject
    ParameterHelper paramHelper;
    @Inject
    CachingService cacheService;
    // inject cache to get better low-level access
    @Inject
    @CacheName("default")
    Cache cache;

    @Override
    public Map<String, String> resolveHeadersForRequest(RequestWrapper wrap) {
        // lookup similar key, matching on id + params
        ParameterizedCacheKey fuzzyKey = generateKey(wrap, Map.class);
        LOGGER.debug("Checking for headers associated with request {}", fuzzyKey);
        Optional<ParameterizedCacheKey> key = getKeyIgnoreInner(fuzzyKey);
        if (key.isPresent()) {
            LOGGER.debug("Found cached source for headers associated with request {}", key.get());
            // if value exists, the value loader should never get called
            return cache
                    .get(key.get(), k -> null)
                    .onItem()
                    .transform(context -> handle(wrap, context))
                    .onFailure()
                    .recoverWithUni(err -> attemptFallbackRecovery(wrap))
                    .await()
                    .atMost(config.headerResolver().timeout());
        }
        LOGGER.debug("Checking for fallback similar to {}", fuzzyKey);
        // if the key isn't recorded, check if there is a similar key in the cache to use
        return attemptFallbackRecovery(wrap).await().atMost(config.headerResolver().timeout());
    }

    /**
     * Returns an async flow that checks the cache for similar keys and attempts to get an upstream context to do
     * best-efforts caching for similar entries
     * 
     * @param wrap
     * @return
     */
    private Uni<Map<String, String>> attemptFallbackRecovery(RequestWrapper wrap) {
        // get the most generic similar key
        Optional<ParameterizedCacheKey> similarKey = getFuzzyKeys(generateKey(wrap, Map.class)).stream().findFirst();
        // if we have a similar key, start processing
        if (similarKey.isPresent()) {
            LOGGER.debug("Falling back to headers associated with request {}", similarKey.get());
            return cache
                    .get(similarKey.get(), k -> null)
                    .onItem()
                    .transform(context -> handle(wrap, context))
                    .onFailure()
                    .recoverWithItem(Collections::emptyMap);
        }
        LOGGER.debug("Found no similar cached sources for current request, passing no pagination headers");
        return Uni.createFrom().item(Collections.emptyMap());
    }

    /**
     * Consumes a cached context object and the current request, looks up up a resolver to transform the context object, and
     * resolves the pagination headers with the context data.
     * 
     * @param <E> the type of the pagination primer to match on for resolvers
     * @param wrap the wrapper for the current http request
     * @param context the object to prime the fetch of the pagination headers
     * @return the transformed pagination headers, or an empty map.
     */
    private <E> Map<String, String> handle(RequestWrapper wrap, E context) {
        if (context == null) {
            return Collections.emptyMap();
        }
        Optional<PaginationResolver<E>> match = getResolver(context);
        if (match.isEmpty()) {
            LOGGER.warn("Could not find a matching resolver for type '{}'", context.getClass().getSimpleName());
        }
        return match.isPresent() ? match.get().generateEntry(wrap, context) : Collections.emptyMap();
    }

    /**
     * Retrieves an optional resolver for the current context data in cache.
     * 
     * Unchecked is suppressed as the canResolve call should always match what the resolver can consume.
     * 
     * @param <E>
     * @param target
     * @return
     */
    @SuppressWarnings("unchecked")
    private <E> Optional<PaginationResolver<E>> getResolver(E target) {
        return Optional
                .ofNullable(
                        (PaginationResolver<E>) resolvers.stream().filter(r -> r.canResolve(target.getClass())).findFirst().orElse(null));
    }

    /**
     * Retrieves cache keys, and does a fuzzy lookup of similar keys using the ID and inner type to match similar result
     * keys. This result is then ordered from least to most exact by number of additional parameters associated with the
     * key.
     * 
     * @param key the generated key to retrieve similar keys for
     * @return a list of similar keys, or an empty list.
     */
    public List<ParameterizedCacheKey> getFuzzyKeys(ParameterizedCacheKey key) {
        return cacheService
                .getCacheKeys()
                .stream()
                .filter(ck -> resolvers
                        .stream()
                        .map(PaginationResolver::getContextSource)
                        .toList()
                        .contains(ck.clazz()) && ck.id().equals(key.id()))
                .sorted((o1, o2) -> Integer.compare(o1.params().size(), o2.params().size()))
                .toList();
    }

    /**
     * Retrieves a key that matches on key parameters and ID, skipping the inner class compare so that we can generically
     * lookup keys.
     * 
     * @param key the generic key to find a more specific key for
     * @return an optional containing the matching key if it exists, or an empty optional if no match found.
     */
    private Optional<ParameterizedCacheKey> getKeyIgnoreInner(ParameterizedCacheKey key) {
        return cacheService
                .getCacheKeys()
                .stream()
                .filter(ck -> ck.params().equals(key.params()) && ck.id().equals(key.id()))
                .findFirst();
    }

    @Override
    public ParameterHelper getParamHelper() {
        return this.paramHelper;
    }
}
