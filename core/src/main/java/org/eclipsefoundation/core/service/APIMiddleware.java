/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.core.service;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;
import org.jboss.resteasy.reactive.RestResponse;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import jakarta.annotation.Nullable;
import jakarta.ws.rs.QueryParam;

public interface APIMiddleware {

    <T> List<T> paginationPassThrough(Function<BaseAPIParameters, RestResponse<List<T>>> supplier, RequestWrapper request);

    /**
     * Returns the full list of data for the given API call, using a function that accepts the page number to iterate over the pages of
     * data. The Link header is scraped off of the first request and is used to determine how many calls need to be made to get the full
     * data set.
     * 
     * @param <T> the type of data that is retrieved
     * @param supplier function that accepts a page number and makes an API call for the given page.
     * @param type class for the entity type returned for the response.
     * @return the full list of results for the given endpoint.
     */
    <T> List<T> getAll(Function<BaseAPIParameters, RestResponse<List<T>>> supplier);

    /**
     * Returns a stream containing all items for the given API call, using a function that accepts the page number to asynchronously get all
     * pages of data. The "next" Link header is scraped off of each request and is used to determine when to stop requesting further data.
     * 
     * @param <T> the type of data that is retrieved
     * @param supplier function that accepts a page number and makes an API call for the given page.
     * @return A stream containing all results for the given endpoint.
     */
    <T> Multi<T> getAllAsync(Function<BaseAPIParameters, Uni<RestResponse<List<T>>>> supplier);

    public static class BaseAPIParameters {
        @Nullable
        @QueryParam("page")
        public Integer page;
        @Nullable
        @QueryParam("pagesize")
        public Integer limit;
        public final RequestWrapper requestWrapper;

        public BaseAPIParameters() {
            this.requestWrapper = null;
        }

        public BaseAPIParameters(Integer page, Integer limit, RequestWrapper requestWrapper) {
            this.page = page;
            this.limit = limit;
            this.requestWrapper = requestWrapper;
        }

        public static BaseAPIParameters buildFromWrapper(RequestWrapper request) {
            // set the params in the new wrap to be used
            Optional<String> page = request.getFirstParam(DefaultUrlParameterNames.PAGE);
            int pageActual = 1;
            if (page.isPresent() && StringUtils.isNumeric(page.get())) {
                pageActual = Integer.valueOf(page.get());
            }
            return new BaseAPIParameters(pageActual, request.getPageSize(), request);
        }

        @Override
        public String toString() {
            return "BaseAPIParameters [page=" + page + ", limit=" + limit + "]";
        }
    }
}