/*********************************************************************
* Copyright (c) 2019. 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.core.service.impl;

import java.net.URI;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

import org.eclipse.microprofile.context.ManagedExecutor;
import org.eclipsefoundation.core.model.StandardPaginationResolver;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.http.response.PaginatedResultsFilter;
import org.eclipsefoundation.utils.helper.ParameterHelper;
import org.jboss.resteasy.reactive.RestResponse;
import org.jboss.resteasy.reactive.common.util.MultivaluedTreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.arc.Unremovable;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.groups.MultiOnItem;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Link;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * Serves as a middleware for external calls. This generalizes retrieving multiple pages of data using the Link header rather than manually
 * iterating over the data or using less robust checks (such as no data in response), or for passing that pagination data on to the current
 * request.
 * 
 * @author Martin Lowe, Zachary Sabourin
 *
 */
@ApplicationScoped
@Unremovable
public class DefaultAPIMiddleware implements APIMiddleware {
    public static final Logger LOGGER = LoggerFactory.getLogger(DefaultAPIMiddleware.class);

    private final StandardPaginationResolver headerResolver;
    private final ManagedExecutor executor;

    public DefaultAPIMiddleware(StandardPaginationResolver headerResolver, ManagedExecutor executor) {
        this.headerResolver = headerResolver;
        this.executor = executor;
    }

    @Override
    public <T> List<T> paginationPassThrough(Function<BaseAPIParameters, RestResponse<List<T>>> supplier, RequestWrapper request) {
        // get the response for the current request
        LOGGER.trace("Making request with: {}", BaseAPIParameters.buildFromWrapper(request));
        RestResponse<List<T>> r = supplier.apply(BaseAPIParameters.buildFromWrapper(request));
        // apply response headers forward (pass through)
        request
                .setHeader(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER,
                        r.getHeaderString(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER));
        request.setHeader(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER, r.getHeaderString(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER));
        if (LOGGER.isTraceEnabled()) {
            LOGGER
                    .trace("Found pagination data max_size={} and page_size={} for request: {}",
                            r.getHeaderString(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER),
                            r.getHeaderString(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER), getURISafely(request.getURI()));
        }

        // record the data to be used for this request. Map is cloned as inheritance isn't reliable in this case
        headerResolver.generateEntry(request, MultivaluedTreeMap.clone(r.getStringHeaders()));
        return r.getEntity();
    }

    @Override
    public <T> List<T> getAll(Function<BaseAPIParameters, RestResponse<List<T>>> supplier) {
        // get initial response
        int count = 1;
        RestResponse<List<T>> r = doCall(supplier, count);
        // get the Link response header values
        int lastPage = getLastPageIndex(r);
        List<T> out = new LinkedList<>();
        out.addAll(r.getEntity());

        // iterate through all pages before returning results
        while (++count <= lastPage) {
            r = doCall(supplier, count);
            out.addAll(r.getEntity());
        }
        return out;
    }

    /**
     * Order of operations:
     * <ol>
     * <li>Create a stream by repeating the given provider until we have exhausted all pages. This creates a stream of {@link RestResponse}
     * items, each wrapped around a {@link List} of type T.</li>
     * <li>Transform into stream of {@link List} of type T by invoking {@link RestResponse#getEntity()} on each {@link RestResponse} item.
     * </li>
     * <li>Transform into stream of type T by invoking {@link MultiOnItem#disjoint()} on each {@link List} item.</li>
     * </ol>
     */
    @Override
    public <T> Multi<T> getAllAsync(Function<BaseAPIParameters, Uni<RestResponse<List<T>>>> provider) {
        return Multi
                .createBy()
                .repeating()
                .uni(AtomicInteger::new, pageNum -> provider.apply(new BaseAPIParameters(pageNum.incrementAndGet(), null, null)))
                .whilst(res -> res.getLink("next") != null)
                .invoke(this::logResponse)
                .emitOn(executor) // Pass next transformations to executor thread
                .onItem()
                .transform(RestResponse::getEntity) // This seems to handle GZIP without any extra work :)
                .invoke(this::logItemSize)
                .onItem()
                .disjoint();
    }

    private <T> void logResponse(RestResponse<List<T>> response) {
        if (LOGGER.isDebugEnabled()) {
            MultivaluedMap<String, Object> headers = response.getHeaders();
            LOGGER.debug("STATUS: {}", response.getStatus());
            LOGGER.debug("ENCODING: {}", headers.get(HttpHeaders.CONTENT_ENCODING));
            LOGGER.debug("HEADERS: {}", headers);
        }
    }

    private <T> void logItemSize(List<T> items) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("PAGINATED ITEMS: {}", items.size());
        }
    }

    /**
     * Wrap the paginated call in a try-catch to better log errors in paginated calls, reporting on parameters to better enable
     * investigation of errors.
     * 
     * @param supplier function to call the paginated results
     * @param count current page number
     * @return the response from upstream
     */
    private <T> RestResponse<List<T>> doCall(Function<BaseAPIParameters, RestResponse<List<T>>> supplier, int count) {
        try {
            return supplier.apply(new BaseAPIParameters(count, null, null));
        } catch (WebApplicationException e) {
            LOGGER.error("Error retrieving a paginated request with params: {}", new BaseAPIParameters(count, null, null));
            // continue throwing after logging details about the problematic request
            throw e;
        }
    }

    private String getURISafely(URI uri) {
        if (uri == null) {
            return "";
        }
        return uri.toString();
    }

    private <T> int getLastPageIndex(RestResponse<List<T>> r) {
        Link lastPageLink = r.getLink("last");
        if (lastPageLink == null) {
            return 0;
        }
        // check what the last page link has as its 'page' param val
        MultivaluedMap<String, String> params = ParameterHelper.parseQueryString(lastPageLink.getUri().getQuery());
        if (params.keySet().contains("page")) {
            return Integer.parseInt(params.getFirst("page"));
        }
        return 0;
    }
}
