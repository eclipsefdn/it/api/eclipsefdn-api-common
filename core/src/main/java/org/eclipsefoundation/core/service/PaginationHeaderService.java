/**
 * Copyright (c) 2022 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.core.service;

import java.util.Map;

import org.eclipsefoundation.caching.model.ParameterizedCacheKey;
import org.eclipsefoundation.caching.model.ParameterizedCacheKeyBuilder;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.utils.helper.ParameterHelper;

import io.quarkus.cache.CacheKey;
import io.quarkus.cache.CacheResult;

/**
 * Using pagination resolvers, this service centralizes caching and resolution of pagination headers for a request. This
 * will handle fuzzy lookups as well as fallback scenarios when no exact match is available.
 * 
 * @author Martin Lowe
 *
 */
public interface PaginationHeaderService {

    /**
     * Accepting a wrapper for the current request, a check the cache for any exact matching resolution instructions for a
     * requests pagination headers. If no exact match is found, a lookup will be done for fallback logic/results, and fetch
     * if available. If no results can be found for the current request, an empty map is returned.
     * 
     * @param wrap wrapper for the current potentially paginated request
     * @return a map containing the pagination headers if they exist, otherwise an empty map.
     */
    Map<String, String> resolveHeadersForRequest(RequestWrapper wrap);

    default ParameterizedCacheKey generateKey(RequestWrapper context, Class<?> innerType) {
        // when available, prepend with the endpoint URL. This should always exist, but just in case
        String path = "/";
        if (context.getURI() != null) {
            path = context.getURI().getPath();
            // normalize the path so that there aren't weird mismatches between the same endpoint w/ + w/o trailing /
            if (path.endsWith("/")) {
                path = path.substring(0, path.length() - 1);
            }
        }
        return ParameterizedCacheKeyBuilder.builder().clazz(innerType).id(path).params(context.asMap()).build();
    }

    /**
     * Returns the parameter helper bean instance
     * 
     * @return the parameter helper bean
     */
    ParameterHelper getParamHelper();

    /**
     * Resolver that takes a contextual source object of the generic type and transforms it into pagination base headers.
     * 
     * @author Martin Lowe
     *
     * @param <T> the type of context that the resolver can accept and process.
     */
    public static interface PaginationResolver<T> {

        /**
         * Generates a map of pagination base headers using the source object.
         * 
         * @param wrapper the current requests context
         * @param source the source of data for the headers
         * @return the base pagination headers given the source and wrapper context.
         */
        Map<String, String> generateEntry(RequestWrapper wrapper, T source);

        /**
         * Using a similar stashed context based on the key and extend where possible or make best efforts to pass close-enough
         * data back when there is no other choice.
         * 
         * @param wrapper the wrapper for the current request
         * @param source the source for the fallback data
         * @return the base pagination headers given the source and wrapper context.
         */
        Map<String, String> fallbackEntry(RequestWrapper wrapper, T source);

        /**
         * Caches the source for later resolution when reapplying the pagination headers to the requests.
         * 
         * @param key the key to match for the pagination headers source
         * @param source the data used to build the actual pagination data
         * @return the source for the pagination header calculation
         */
        @CacheResult(cacheName = "default")
        default T cacheSource(@CacheKey ParameterizedCacheKey key, T source) {
            return source;
        }

        /**
         * Indicates whether the current resolver can resolve the type of class passed. This should only match on the type
         * indicated by the generic of the current resolver.
         * 
         * @param <E> the type of context that needs to be handled
         * @param clazz the raw type of the context object
         * @return true if the current resolver can handle the type, false otherwise.
         */
        default <E> boolean canResolve(Class<E> clazz) {
            return getContextSource().isAssignableFrom(clazz);
        }

        /**
         * Returns the raw class type for the context source.
         * 
         * @return type for context source type.
         */
        Class<?> getContextSource();
    }
}
