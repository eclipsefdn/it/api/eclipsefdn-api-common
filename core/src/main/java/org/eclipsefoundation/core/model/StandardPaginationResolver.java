/**
 * Copyright (c) 2022 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.core.model;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipsefoundation.caching.model.ParameterizedCacheKey;
import org.eclipsefoundation.core.service.PaginationHeaderService;
import org.eclipsefoundation.core.service.PaginationHeaderService.PaginationResolver;
import org.eclipsefoundation.http.config.PaginationConfig;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.http.response.PaginatedResultsFilter;
import org.jboss.resteasy.reactive.common.util.MultivaluedTreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.reflect.TypeToken;

import io.quarkus.cache.Cache;
import io.quarkus.cache.CacheName;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * Caches pagination headers passed in from a header map.
 * 
 * @author Martin Lowe
 *
 */
@Singleton
public class StandardPaginationResolver implements PaginationResolver<MultivaluedTreeMap<String, String>> {
    private static final Logger LOGGER = LoggerFactory.getLogger(StandardPaginationResolver.class);

    @Inject
    PaginationConfig config;

    @Inject
    PaginationHeaderService headerService;
    @Inject
    @CacheName("default")
    Cache cache;

    @Override
    public Map<String, String> generateEntry(RequestWrapper wrapper, MultivaluedTreeMap<String, String> source) {
        // ensure that the source is cached
        ParameterizedCacheKey key = headerService.generateKey(wrapper, getContextSource());
        cache.invalidate(key).flatMap(k -> cache.get(key, i -> source)).await().atMost(config.headerResolver().timeout());

        return calculateRecord(source);
    }

    @Override
    public Map<String, String> fallbackEntry(RequestWrapper wrapper, MultivaluedTreeMap<String, String> source) {
        // use the standard method of calculation as we can't extend it any better
        return generateEntry(wrapper, source);
    }

    private Map<String, String> calculateRecord(MultivaluedMap<String, String> source) {
        if (source == null) {
            return Collections.emptyMap();
        }
        // build the output map
        Map<String, String> out = new HashMap<>();
        String maxResults = source.getFirst(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER);
        String maxPageSize = source.getFirst(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER);
        out.put(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER, maxResults);
        out.put(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER, maxPageSize);
        LOGGER
                .debug("Storing keys {}({}) and {}({})", PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER, maxPageSize,
                        PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER, maxResults);
        return out;
    }

    @Override
    @SuppressWarnings("serial") // this shouldn't be serialized, so this is safe to ignore
    public Class<?> getContextSource() {
        return new TypeToken<MultivaluedTreeMap<String, String>>() {
        }.getRawType();
    }
}
