/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.core.config;

import java.time.Period;

import io.smallrye.config.ConfigMapping;

/**
 * Configurations containing details on how the application should handle file rotation of logs.
 */
@ConfigMapping(prefix = "eclipse.log.file-rotation")
public interface LogRotationConfig {

    /**
     * Whether cleanup is enabled for the file logs
     * 
     * @return true if enabled, false otherwise
     */
    boolean enabled();

    /**
     * Frequency at which the cleanup script should be run (typically daily, though can be set to longer intervals if wanted).
     * 
     * @return the period for the frequency of the cleanup task runs.
     */
    Period frequency();

    /**
     * Max age of log files measured in days, months, and/or years.
     * 
     * @return max age of log files to retain
     */
    Period maxAge();

    /**
     * Location of the log files, kept separate from the log file format for ease of use.
     * 
     * @return path to the base directory for log files for this application
     */
    String baseDir();

    /**
     * File prefix of logs managed by this application. This should not include dynamic values, and only be the static portion of the file
     * name.
     * 
     * @return the static file prefix for managed log files.
     */
    String baseFileName();
}
