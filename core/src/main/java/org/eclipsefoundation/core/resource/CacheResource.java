package org.eclipsefoundation.core.resource;

import java.io.IOException;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.collections.CollectionUtils;
import org.eclipsefoundation.caching.model.ParameterizedCacheKey;
import org.eclipsefoundation.caching.model.WrappedCacheKey;
import org.eclipsefoundation.caching.namespaces.CachingPropertyNames;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.caching.service.LoadingCacheManager;
import org.eclipsefoundation.caching.service.impl.DefaultLoadingCacheManager.LoadingCacheWrapper;
import org.eclipsefoundation.core.model.CacheRequest;
import org.eclipsefoundation.http.annotations.KeySecured;
import org.eclipsefoundation.http.annotations.OptionalPath;
import org.eclipsefoundation.utils.helper.TransformationHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.cache.CaffeineCache;
import io.quarkus.qute.Location;
import io.quarkus.qute.Template;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;

@Path("/caches")
@Produces(MediaType.APPLICATION_JSON)
public class CacheResource {
    static final Logger LOGGER = LoggerFactory.getLogger(CacheResource.class);

    private static final String STANDARD_CACHE_TYPE = "standard";
    private static final String LOADING_CACHE_TYPE = "loading";
    private static final int CACHE_GET_TIMEOUT = 5;

    Template cacheKeysView;
    CachingService service;
    LoadingCacheManager lcm;
    ObjectMapper mapper;

    public CacheResource(@Location("cache_keys.html") Template cacheKeysView, CachingService service, LoadingCacheManager lcm,
            ObjectMapper mapper) {
        this.cacheKeysView = cacheKeysView;
        this.service = service;
        this.lcm = lcm;
        this.mapper = mapper;
    }

    @GET
    @Path("keys")
    @KeySecured
    @Produces(MediaType.TEXT_HTML)
    @OptionalPath(CachingPropertyNames.CACHE_RESOURCE_ENABLED)
    public Response getCaches(@QueryParam("key") String passedKey) {
        return Response
                .ok()
                .entity(cacheKeysView
                        .data("cacheKeys", service.getCacheKeys().stream().map(this::buildWrappedKeys).toList())
                        .data("loadingCacheKeys",
                                lcm
                                        .getManagedCaches()
                                        .stream()
                                        .flatMap(wrapper -> wrapper.getCacheKeys().stream())
                                        .toList())
                        .data("key", passedKey)
                        .data("title", "Application Cache Keys")
                        .render())
                .build();
    }

    @GET
    @Path("{cacheKey}/value")
    @KeySecured
    @Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
    @OptionalPath(CachingPropertyNames.CACHE_RESOURCE_ENABLED)
    public Response getCacheValue(@PathParam("cacheKey") String cacheKey, @QueryParam("cache_request") String compressedCacheRequest)
            throws InterruptedException, ExecutionException, TimeoutException, IOException {
        // decode the compressed cache request
        CacheRequest request = mapper.readValue(Base64.getDecoder().decode(compressedCacheRequest), CacheRequest.class);
        // retrieve the matching key
        ParameterizedCacheKey matchingKey = getMatchingKey(cacheKey, request);
        if (STANDARD_CACHE_TYPE.equals(request.cacheType())) {
            return buildContentAwareReturn(service.getCache().as(CaffeineCache.class).getIfPresent(matchingKey).get(CACHE_GET_TIMEOUT, TimeUnit.SECONDS));
        } else if (LOADING_CACHE_TYPE.equals(request.cacheType())) {
            return buildContentAwareReturn(
                    getLoadingCacheForRequest(request).getCache().getIfPresent(matchingKey).get(CACHE_GET_TIMEOUT, TimeUnit.SECONDS));
        }
        throw handleBadCacheType(request);
    }

    @POST
    @Path("{cacheKey}/clear")
    @KeySecured
    @OptionalPath(CachingPropertyNames.CACHE_RESOURCE_ENABLED)
    public Response clearForKey(@PathParam("cacheKey") String cacheKey, @HeaderParam("x-cache-key") String passedKey,
            CacheRequest request) {
        ParameterizedCacheKey matchingKey = getMatchingKey(cacheKey, request);
        if (STANDARD_CACHE_TYPE.equals(request.cacheType())) {
            service.remove(matchingKey);
            return Response.ok().build();
        } else if (LOADING_CACHE_TYPE.equals(request.cacheType())) {
            getLoadingCacheForRequest(request).getCache().synchronous().refresh(matchingKey);
            return Response.ok().build();
        }
        throw handleBadCacheType(request);
    }

    /**
     * Using the cache ID key and the incoming request, retrieve the matching active cache keys given the filters and return them.
     * 
     * @param cacheKey the ID key for the cache entry
     * @param request the cache request containing additional cache info
     * @return
     */
    private ParameterizedCacheKey getMatchingKey(String cacheKey, CacheRequest request) {
        if (STANDARD_CACHE_TYPE.equals(request.cacheType())) {
            return service
                    .getCacheKeys()
                    .stream()
                    .filter(k -> k.id().equals(cacheKey) && checkParams(request.params(), k.params())
                            && k.clazz().getName().equals(request.clazz()))
                    .findFirst()
                    .orElseThrow(() -> new NotFoundException("No cache entry found with given parameters"));
        } else if (LOADING_CACHE_TYPE.equals(request.cacheType())) {
            return getLoadingCacheForRequest(request)
                    .getCacheKeys()
                    .stream()
                    .filter(k -> k.id().equals(cacheKey) && checkParams(request.params(), k.params())
                            && k.clazz().getName().equals(request.clazz()))
                    .findFirst()
                    .orElseThrow(() -> new NotFoundException("No cache entry found with given parameters"));
        }
        throw handleBadCacheType(request);
    }

    /**
     * Using the cache request, load and return the corresponding cache wrapper for use in looking up keys or minor cache modification.
     * 
     * @param request the current cache request
     * @return a loading cache wrapper for the given loading cache if it exists, or throws a bad request exception if none found.
     */
    private LoadingCacheWrapper<?> getLoadingCacheForRequest(CacheRequest request) {
        // iterate over the available loading caches to find one with a matching class
        Optional<LoadingCacheWrapper<?>> matchingLoadingCache = lcm
                .getManagedCaches()
                .stream()
                .filter(c -> c.getInnerType().getName().equals(request.clazz()))
                .findFirst();
        if (matchingLoadingCache.isEmpty()) {
            throw handleBadCacheType(request);
        }

        // for each of the matching keys, request that cache entry to refresh
        return matchingLoadingCache.get();
    }

    /**
     * Converts cache keys to wrapped cache keys with their expiration time.
     * 
     * @param k the key to wrap
     * @return the wrapped key, containing the passed key and its time to live.
     */
    private WrappedCacheKey buildWrappedKeys(ParameterizedCacheKey k) {
        Date d = null;
        try {
            d = new Date(service.getExpiration(k.id(), k.params(), k.clazz()).orElse(service.getMaxAge()));
        } catch (RuntimeException e) {
            // intentionally empty
        }
        return new WrappedCacheKey(k, d);
    }

    /**
     * Compare 2 parameter collections to check if they match.
     * 
     * @param passedParams the passed params we need to match
     * @param entryParams the current entry to checks params
     * @return true if they match (insensitive to order), false otherwise
     */
    private boolean checkParams(Map<String, List<String>> passedParams, MultivaluedMap<String, String> entryParams) {
        if (passedParams.size() != entryParams.size()) {
            return false;
        }
        return passedParams.entrySet().stream().allMatch(e -> CollectionUtils.isEqualCollection(e.getValue(), entryParams.get(e.getKey())));
    }

    /**
     * Centralized to reduce repetition when handling bad cache calls, as the message is same across the board.
     * 
     * @param r the bad cache request.
     * @return the error to be thrown
     */
    private BadRequestException handleBadCacheType(CacheRequest r) {
        // if we don't have a match, throw as we can't handle the cache type
        return new BadRequestException("Passed cache parameters are not valid: " + TransformationHelper.formatLog(r.toString()));
    }

    /**
     * Builds the response object for the given content, using a plain text response for non-structured data.
     *
     * @param data the cache data to return.
     * @return response to return to the user with proper Content-Type.
     */
    private Response buildContentAwareReturn(Object data) {
        Response.ResponseBuilder r = Response.ok(data);
        if (data instanceof String) {
            return r.header("Content-Type", MediaType.TEXT_PLAIN).build();
        }
        return r.build();
    }
}
