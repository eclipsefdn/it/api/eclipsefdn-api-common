/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.core.response;

import java.io.IOException;

import org.eclipsefoundation.core.service.PaginationHeaderService;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.utils.helper.TransformationHelper;
import org.jboss.resteasy.reactive.server.ServerResponseFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.vertx.core.http.HttpMethod;
import jakarta.inject.Inject;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ContainerResponseFilter;
import jakarta.ws.rs.core.Response.Status;

/**
 * Either applies recorded actions to the response or to record the response if nothing has been recorded yet. Priority
 * on this should be high, if not highest in the stack to ensure that any values required for calculations such as
 * pagination are included.
 *
 * @author Martin Lowe
 */
public class CachedResponseFilter implements ContainerResponseFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(CachedResponseFilter.class);

    @Inject
    PaginationHeaderService headerService;
    @Inject
    RequestWrapper wrap;

	@ServerResponseFilter(priority = 5050)
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Checking request {}", TransformationHelper.formatLog(wrap.getEndpoint()));
        }
        // check if response status is in OK range and was a GET method
        if (requestContext.getMethod().equalsIgnoreCase(HttpMethod.GET.name()) && responseContext.getStatus() >= Status.OK.getStatusCode()
                && responseContext.getStatus() < Status.MOVED_PERMANENTLY.getStatusCode()) {
            // using mutable getHeaders, update headers with updated values
            headerService
                    .resolveHeadersForRequest(wrap)
                    .entrySet()
                    .forEach(e -> responseContext.getHeaders().add(e.getKey(), e.getValue()));
        }
    }
}
