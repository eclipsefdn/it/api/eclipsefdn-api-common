/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.core.test.resources;

import java.util.List;
import java.util.Optional;

import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.http.response.PaginatedResultsFilter;
import org.jboss.resteasy.reactive.RestResponse;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;

/**
 * Test endpoint for different mechanisms
 * 
 * @author Martin Lowe
 *
 */
@Path("test")
public class TestResource {
    public static final String PAGINATION_HEADER_RESULT_SIZE = "30";
    public static final String PAGINATION_HEADER_PAGE_SIZE_DEFAULT = "10";

    @Inject
    APIMiddleware middleware;
    @Inject
    CachingService cache;

    @Inject
    RequestWrapper wrap;

    /**
     * Requires header to be set on initial call, will allow for custom result size to be set on requests to spoof the results size.
     */
    @GET
    @Path("pagination/cached/{key}")
    public List<String> cachedPaginationTest(@HeaderParam("result-size") String resultSize, @PathParam("key") String key) {
        RestResponse<List<String>> r = RestResponse.ok(List.of("1", "2", "3"));
        r
                .getHeaders()
                .add(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER, Optional.ofNullable(resultSize).orElse(PAGINATION_HEADER_RESULT_SIZE));
        r.getHeaders().add(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER, PAGINATION_HEADER_PAGE_SIZE_DEFAULT);
        return cache.get(key, null, String.class, () -> middleware.paginationPassThrough(i -> r, wrap)).data().get();
    }
}
