package org.eclipsefoundation.core.test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipsefoundation.http.namespace.RequestPropertyNames;

import io.quarkus.test.junit.QuarkusTestProfile;

/**
 * 
 * @author Martin Lowe
 */
public class OptionalResourceEnabledTestProfile implements QuarkusTestProfile {

    // private immutable copy of the configs for auth state
    private static final Map<String, String> CONFIG_OVERRIDES;
    static {
        Map<String, String> tmp = new HashMap<>();
        tmp.put(RequestPropertyNames.OPTIONAL_RESOURCES_ENABLED, "true");
        tmp.put(RequestPropertyNames.CACHE_RESOURCE_ENABLED, "true");
        CONFIG_OVERRIDES = Collections.unmodifiableMap(tmp);
    }

    @Override
    public Map<String, String> getConfigOverrides() {
        return CONFIG_OVERRIDES;
    }
}
