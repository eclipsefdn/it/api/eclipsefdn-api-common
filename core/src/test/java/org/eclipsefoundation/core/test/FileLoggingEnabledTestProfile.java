/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.core.test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import io.quarkus.test.junit.QuarkusTestProfile;

public class FileLoggingEnabledTestProfile implements QuarkusTestProfile {

    // private immutable copy of the configs for auth state
    private static Map<String, String> configOverrides;

    @Override
    public Map<String, String> getConfigOverrides() {
        if (FileLoggingEnabledTestProfile.configOverrides == null) {
            Map<String, String> tmp = new HashMap<>();
            tmp.put("quarkus.log.file.enable", "true");
            try {
                // generate a random folder in test to reduce chance of collision
                Path logdir = Files.createTempDirectory(UUID.randomUUID().toString());
                tmp.put("eclipse.log.file-rotation.base-dir", logdir.toAbsolutePath().toString());
            } catch (IOException e) {
                throw new RuntimeException("Cannot complete test setup for temporary files", e);
            }
            FileLoggingEnabledTestProfile.configOverrides = Collections.unmodifiableMap(tmp);
        }
        return configOverrides;
    }
}
