/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.core.tasks;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Stream;

import org.eclipsefoundation.core.config.LogRotationConfig;
import org.eclipsefoundation.core.test.FileLoggingEnabledTestProfile;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import jakarta.inject.Inject;

@QuarkusTest
@TestInstance(Lifecycle.PER_CLASS)
@TestProfile(FileLoggingEnabledTestProfile.class)
class LogFileRotationCleanupTaskTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogFileRotationCleanupTaskTest.class);

    @Inject
    LogRotationConfig config;

    @Inject
    LogFileRotationCleanupTask task;

    List<Path> sampleFiles;
    List<Path> sampleOutdatedFiles;

    @BeforeAll
    public void initTestLog() throws IOException {
        // set up a few sample dates
        // TEST 1: Today, should be kept
        LocalDate ld1 = LocalDate.now();
        // TEST 2: Recent log case
        LocalDate ld2 = ld1.minus(5, ChronoUnit.DAYS);
        // TEST 3: About to expire case
        LocalDate ld3 = ld1.minus(config.maxAge().minusDays(1));
        // TEST 4: Recently expire case
        LocalDate ld4 = ld1.minus(config.maxAge().plusDays(1));
        // convert the tests to actual files with sample contents
        this.sampleFiles = Stream
                .of(ld1.format(LogFileRotationCleanupTask.SIMPLE_DATE_FORMAT), ld2.format(LogFileRotationCleanupTask.SIMPLE_DATE_FORMAT),
                        ld3.format(LogFileRotationCleanupTask.SIMPLE_DATE_FORMAT))
                .map(this::generateTempLogFile)
                .toList();
        this.sampleOutdatedFiles = Stream
                .of("2020-01-01", ld4.format(LogFileRotationCleanupTask.SIMPLE_DATE_FORMAT))
                .map(this::generateTempLogFile)
                .toList();
    }

    @AfterAll
    public void cleanup() {
        // cleanup good + outdated files if they are still there
        this.sampleFiles.forEach(p -> {
            if (Files.exists(p)) {
                try {
                    Files.delete(p);
                } catch (IOException e) {
                    LOGGER.error("Error while cleaning up test resources for file at {}", p);
                }
            }
        });
        this.sampleOutdatedFiles.forEach(p -> {
            if (Files.exists(p)) {
                try {
                    Files.delete(p);
                } catch (IOException e) {
                    LOGGER.error("Error while cleaning up test resources for file at {}", p);
                }
            }
        });
    }

    @Test
    void cleanup_success() {
        // all files should exist before cleanup
        this.sampleFiles.forEach(f -> Assertions.assertTrue(Files.exists(f)));
        this.sampleOutdatedFiles.forEach(f -> Assertions.assertTrue(Files.exists(f)));
        
        // manually call the cleanup
        task.cleanup();

        // normal files should still exist, but files with older than max age date stamp should be deleted
        this.sampleFiles.forEach(f -> Assertions.assertTrue(Files.exists(f)));
        this.sampleOutdatedFiles.forEach(f -> Assertions.assertFalse(Files.exists(f)));
    }

    private Path generateTempLogFile(String path) {
        try {
            return Files
                    .write(Path.of(config.baseDir(), String.format("%s.%s.%s", config.baseFileName(), "sample-host-name", path)),
                            "sample".getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
