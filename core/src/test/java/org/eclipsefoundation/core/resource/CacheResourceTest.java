package org.eclipsefoundation.core.resource;

import static io.restassured.RestAssured.given;

import java.util.UUID;

import org.eclipsefoundation.core.test.OptionalResourceEnabledTestProfile;
import org.eclipsefoundation.http.helper.SecureResourceKey;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response.Status;

@QuarkusTest
@TestProfile(OptionalResourceEnabledTestProfile.class)
class CacheResourceTest {

    @Inject
    SecureResourceKey key;

    @Test
    void cacheUI_protected() {
        given().when().get("/caches/keys").then().statusCode(Status.FORBIDDEN.getStatusCode());
    }

    @Test
    void cacheUI_usesInstanceKey() {
        given().when().get("/caches/keys?key={key}", UUID.randomUUID().toString()).then().statusCode(Status.FORBIDDEN.getStatusCode());
        given().when().get("/caches/keys?key={key}", key.getKey()).then().statusCode(Status.OK.getStatusCode());
    }
}
