package org.eclipsefoundation.core.resource;

import static io.restassured.RestAssured.given;

import java.util.UUID;

import org.eclipsefoundation.http.helper.SecureResourceKey;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response.Status;

@QuarkusTest
class CacheResourceDisabledTest {

    @Inject
    SecureResourceKey key;

    @Test
    void cacheUI_protected() {
        given().when().get("/caches/keys").then().statusCode(Status.NOT_FOUND.getStatusCode());
    }

    @Test
    void cacheUI_returnsMissingWithKeyValues() {
        given().when().get("/caches/keys?key={key}", UUID.randomUUID().toString()).then().statusCode(Status.NOT_FOUND.getStatusCode());
        given().when().get("/caches/keys?key={key}", key.getKey()).then().statusCode(Status.NOT_FOUND.getStatusCode());
    }
}
