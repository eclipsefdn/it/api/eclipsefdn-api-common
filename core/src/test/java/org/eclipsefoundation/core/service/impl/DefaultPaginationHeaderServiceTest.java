/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.core.service.impl;

import java.net.URI;
import java.util.Map;

import org.eclipsefoundation.caching.model.ParameterizedCacheKey;
import org.eclipsefoundation.core.model.StandardPaginationResolver;
import org.eclipsefoundation.core.service.PaginationHeaderService;
import org.eclipsefoundation.core.test.models.TestModel;
import org.eclipsefoundation.http.model.FlatRequestWrapper;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.http.response.PaginatedResultsFilter;
import org.jboss.resteasy.reactive.common.util.MultivaluedTreeMap;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;

/**
 * @author Martin Lowe
 *
 */
@QuarkusTest
class DefaultPaginationHeaderServiceTest {

    @Inject
    PaginationHeaderService headerService;

    @Inject
    StandardPaginationResolver resolver;

    @Test
    void generateKey_success() {
        // create flat/basic wrapper to test
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org/sample/svc/endpoint"));
        // run test key generation
        ParameterizedCacheKey k = headerService.generateKey(wrap, TestModel.class);
        Assertions.assertEquals(TestModel.class, k.clazz());
        Assertions.assertEquals(wrap.getEndpoint(), k.id());
        Assertions.assertTrue(k.params().isEmpty());
    }

    @Test
    void generateKey_success_withParams() {
        // create flat/basic wrapper to test
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org/sample/svc/endpoint"));
        wrap.addParam(DefaultUrlParameterNames.ID, "sample");
        // run test key generation
        ParameterizedCacheKey k = headerService.generateKey(wrap, TestModel.class);
        Assertions.assertEquals(1, k.params().size());
        Assertions.assertEquals("sample", k.params().getFirst(DefaultUrlParameterNames.ID.getName()));
    }

    @Test
    void generateKey_success_removesTrailingSlashesInPath() {
        String expectedPath = "/sample/svc/endpoint";
        // create flat/basic wrapper to test
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org" + expectedPath + "/"));
        wrap.addParam(DefaultUrlParameterNames.ID, "sample");
        // run test key generation
        ParameterizedCacheKey k = headerService.generateKey(wrap, TestModel.class);
        Assertions.assertEquals(TestModel.class, k.clazz());
        Assertions.assertEquals(expectedPath, k.id());
    }

    @Test
    void resolveHeadersForRequest_success() {
        // test set up
        String resultsSize = "25";
        String pageSize = "10";
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org/sample/svc/endpoint"));
        MultivaluedTreeMap<String, String> values = new MultivaluedTreeMap<>();
        values.add(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER, resultsSize);
        values.add(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER, pageSize);
        // set up the cached context for request
        Map<String, String> expectedResults = resolver.generateEntry(wrap, values);

        // run the test from the pagination header service
        Map<String, String> actualResults = headerService.resolveHeadersForRequest(wrap);
        Assertions.assertEquals(2, actualResults.keySet().size());
        Assertions.assertEquals(resultsSize, actualResults.get(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER));
        Assertions.assertEquals(pageSize, actualResults.get(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER));
        Assertions.assertEquals(expectedResults, actualResults, "Output pagination header map should match resolver results");
    }

    @Test
    void resolveHeadersForRequest_success_endpointWithMixedTrailingSlashes() {
        // test set up
        String resultsSize = "25";
        String pageSize = "10";
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org/sample/svc/endpoint"));
        MultivaluedTreeMap<String, String> values = new MultivaluedTreeMap<>();
        values.add(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER, resultsSize);
        values.add(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER, pageSize);
        // set up the cached context for request
        Map<String, String> expectedResults = resolver.generateEntry(wrap, values);

        // run the test from the pagination header service
        Map<String, String> actualResults = headerService
                .resolveHeadersForRequest(new FlatRequestWrapper(URI.create("http://eclipse.org/sample/svc/endpoint/")));
        Assertions.assertEquals(expectedResults, actualResults, "Output pagination header map should match resolver results");
    }

    @Test
    void resolveHeadersForRequest_success_utilizesFallbacks() {
        // test set up
        String resultsSize = "25";
        String pageSize = "10";
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org/sample/svc/endpoint"));
        wrap.addParam(DefaultUrlParameterNames.ID, "sample");
        MultivaluedTreeMap<String, String> values = new MultivaluedTreeMap<>();
        values.add(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER, resultsSize);
        values.add(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER, pageSize);
        // set up the cached context for request
        Map<String, String> expectedResults = resolver.generateEntry(wrap, values);

        // run the test from the pagination header service
        Map<String, String> actualResults = headerService
                .resolveHeadersForRequest(new FlatRequestWrapper(URI.create("http://eclipse.org/sample/svc/endpoint/")));
        Assertions.assertEquals(expectedResults, actualResults, "Output pagination header map should match resolver results");
    }
}
