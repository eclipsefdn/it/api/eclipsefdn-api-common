/*********************************************************************
* Copyright (c) 2019 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.core.service.impl;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.core.service.PaginationHeaderService;
import org.eclipsefoundation.core.test.models.TestModel;
import org.eclipsefoundation.http.model.FlatRequestWrapper;
import org.eclipsefoundation.http.response.PaginatedResultsFilter;
import org.jboss.resteasy.reactive.RestResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectSpy;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.helpers.spies.MultiOnCompletionSpy;
import io.smallrye.mutiny.helpers.spies.MultiOnFailureSpy;
import io.smallrye.mutiny.helpers.spies.MultiOnRequestSpy;
import io.smallrye.mutiny.helpers.spies.Spy;
import io.smallrye.mutiny.helpers.test.AssertSubscriber;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.core.Link;

@QuarkusTest
class DefaultAPIMiddlewareTest {

    @InjectSpy
    TestEndpoint endpoint;

    @Inject
    APIMiddleware middleware;
    @Inject
    ObjectMapper objectMapper;

    @Inject
    PaginationHeaderService headerService;

    @ParameterizedTest
    @ValueSource(strings = { "", "sample=2&", "sample=2&otherpage=10&" })
    void getAll_success_pagination(String leadingQueryParams) {
        int pages = 5;
        // call the sample endpoint
        middleware.getAll(params -> endpoint.sample(params, pages, leadingQueryParams));
        // assert that the endpoint sample was called the number of times expected
        Mockito
                .verify(endpoint, Mockito.times(pages))
                .sample(Mockito.any(BaseAPIParameters.class), Mockito.any(Integer.class), Mockito.any(String.class));
    }

    @ParameterizedTest
    @ValueSource(strings = { "", "sample=2&", "sample=2&otherpage=10&" })
    void getAll_success_paginationAsync(String leadingQueryParams) {
        int pages = 5;
        // call the sample endpoint
        Multi<TestModel> requests = middleware.getAllAsync(params -> endpoint.sampleAsync(params, pages, leadingQueryParams));

        // Create a spy for the multi
        MultiOnRequestSpy<TestModel> requestSpy = Spy.onRequest(requests);
        MultiOnCompletionSpy<TestModel> completionSpy = Spy.onCompletion(requestSpy);
        AssertSubscriber<TestModel> assertSub = completionSpy.subscribe().withSubscriber(AssertSubscriber.create(pages));

        // Verify subscription and that the correct number of items have been received
        assertSub.assertSubscribed();
        assertSub.awaitItems(pages).assertCompleted();
        Assertions.assertEquals(pages, assertSub.getItems().size());
    }

    @ParameterizedTest
    @ValueSource(strings = { "", "sample=2&", "sample=2&otherpage=10&" })
    void getAll_failure_paginationAsync(String leadingQueryParams) {
        int pages = 5;
        // call the sample endpoint
        Multi<TestModel> requests = middleware.getAllAsync(params -> endpoint.errorAsync(params, pages, leadingQueryParams));

        // Create a spy for the multi
        MultiOnRequestSpy<TestModel> requestSpy = Spy.onRequest(requests);
        MultiOnFailureSpy<TestModel> completionSpy = Spy.onFailure(requestSpy);
        AssertSubscriber<TestModel> assertSub = completionSpy.subscribe().withSubscriber(AssertSubscriber.create());

        // Verify subscription and that the correct error is received when a failure occurs
        assertSub.assertSubscribed();
        assertSub.awaitFailure().assertFailedWith(BadRequestException.class);
    }

    @Test
    void paginationPassThrough_success_paginationHeadersGenerated() {
        int pages = 5;
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org/sample/endpoint"));
        middleware.paginationPassThrough(params -> endpoint.sample(params, pages, ""), wrap);

        // test raw headers on response
        Assertions.assertEquals(10, Integer.parseInt(wrap.getHeader(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER)));
        Assertions.assertEquals(getMaxResultsSize(pages), Integer.parseInt(wrap.getHeader(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER)));
    }

    @Test
    void paginationPassThrough_success_paginationResolverUsed() {
        int pages = 5;
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org/sample/endpoint"));
        middleware.paginationPassThrough(params -> endpoint.sample(params, pages, ""), wrap);

        // retrieve the pagination data for current request and test it
        Map<String, String> results = headerService.resolveHeadersForRequest(wrap);
        Assertions.assertEquals(10, Integer.parseInt(results.get(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER)));
        Assertions.assertEquals(getMaxResultsSize(pages), Integer.parseInt(results.get(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER)));
    }

    public static int getMaxResultsSize(int pageCount) {
        return (pageCount - 1) * 10 + 3;
    }

    @ApplicationScoped
    public static class TestEndpoint {
        private static final int PAGE_SIZE = 1;

        private final List<TestModel> internal;

        public TestEndpoint() {
            this.internal = new ArrayList<>();
            this.generateTestModels(5);
        }

        public RestResponse<List<TestModel>> sample(BaseAPIParameters base, int pageCount, String leadingParams) {
            return generatePaginatedSample(base, pageCount, leadingParams);
        }

        public Uni<RestResponse<List<TestModel>>> sampleAsync(BaseAPIParameters base, int pageCount, String leadingParams) {
            return Uni.createFrom().item(generatePaginatedSample(base, pageCount, leadingParams));
        }

        public Uni<RestResponse<List<TestModel>>> errorAsync(BaseAPIParameters base, int pageCount, String leadingParams) {
            throw new BadRequestException();
        }

        private RestResponse<List<TestModel>> generatePaginatedSample(BaseAPIParameters base, int pageCount, String leadingParams) {
            // get current page safely
            Integer currentPage = 1;
            // null check the designated page in request
            Integer designatedPage = base.page;
            if (designatedPage != null) {
                currentPage = designatedPage;
            }
            // generate link headers

            List<Link> links = new ArrayList<>();
            // add first + last page link headers
            links
                    .add(Link
                            .fromUri(String.format("https://sample.com/api?%spage=%d", leadingParams, 1))
                            .rel("first")
                            .title("first page of results")
                            .build());
            links
                    .add(Link
                            .fromUri(String.format("https://sample.com/api?%spage=%d", leadingParams, currentPage))
                            .rel("self")
                            .title("this page of results")
                            .build());
            links
                    .add(Link
                            .fromUri(String.format("https://sample.com/api?%spage=%d", leadingParams, pageCount))
                            .rel("last")
                            .title("last page of results")
                            .build());

            if (currentPage < pageCount) {
                links
                        .add(Link
                                .fromUri(String.format("https://sample.com/api?%spage=%d", leadingParams, currentPage + 1))
                                .rel("next")
                                .title("next page of results")
                                .build());
            }

            int from = 0;
            if (currentPage > 1) {
                from += PAGE_SIZE * (currentPage - 1);
            }

            int to = (PAGE_SIZE * currentPage);
            if (to >= internal.size()) {
                to = internal.size();
            }

            // return basic return with link headers to test, w/ consistent fake data set size
            RestResponse<List<TestModel>> out = RestResponse.ok(internal.subList(from, to));
            out.getHeaders().addAll("link", links.stream().map(l -> (Object) l).toList());
            out.getHeaders().add(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER, getMaxResultsSize(pageCount));
            out.getHeaders().add(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER, 10);
            return out;
        }

        private void generateTestModels(int numItems) {
            for (int i = 0; i < numItems; i++) {
                internal.add(new TestModel("test-" + i, i + "@email.com"));
            }
        }
    }
}
