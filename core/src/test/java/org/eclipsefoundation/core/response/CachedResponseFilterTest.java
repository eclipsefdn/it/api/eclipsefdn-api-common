package org.eclipsefoundation.core.response;

import static io.restassured.RestAssured.given;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.eclipsefoundation.core.test.resources.TestResource;
import org.eclipsefoundation.http.response.PaginatedResultsFilter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.logging.Log;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.response.Response;
import jakarta.ws.rs.core.Link;

@QuarkusTest
class CachedResponseFilterTest {

    @Test
    void filter_success_headerCachingAppliesBeforeFilter() {
        String uniqueKey = UUID.randomUUID().toString();
        Map<String, String> expectedHeaders = new HashMap<>();
        expectedHeaders.put(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER,
                TestResource.PAGINATION_HEADER_RESULT_SIZE);
        expectedHeaders.put(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER,
                TestResource.PAGINATION_HEADER_PAGE_SIZE_DEFAULT);
        // endpoint has stored pagination header sources
        Response vr = given().header("result-size", 120).get("test/pagination/cached/" + uniqueKey);
        Log.error(vr.headers().getValues("Link"));
        Optional<Link> expectedLastRel = vr.headers().getValues("Link").stream().map(l -> Link.valueOf(l))
                .filter(l -> "last".equals(l.getRel())).findFirst();

        // response that should have the cached results which would apply to the link
        // header
        Response cachedResponse = given().get("test/pagination/cached/" + uniqueKey);
        Optional<Link> actualLastRel = cachedResponse.headers().getValues("Link").stream().map(l -> Link.valueOf(l))
                .filter(l -> "last".equals(l.getRel())).findFirst();

        // check that our last link is the same as in the initial request which had the
        // headers set
        Assertions.assertTrue(expectedLastRel.isPresent());
        Assertions.assertTrue(actualLastRel.isPresent());
        Assertions.assertEquals(expectedLastRel.get(), actualLastRel.get());
        // check that the intended value of 12 is used for the resultant query string
        Assertions.assertEquals("page=12", actualLastRel.get().getUri().getQuery(),
                "Expected 12 pages (10 items per page)");
    }
}
