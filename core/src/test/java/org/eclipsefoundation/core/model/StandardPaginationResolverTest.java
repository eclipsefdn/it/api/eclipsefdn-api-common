/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.core.model;

import java.net.URI;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.http.model.FlatRequestWrapper;
import org.eclipsefoundation.http.response.PaginatedResultsFilter;
import org.jboss.resteasy.reactive.common.util.CaseInsensitiveMap;
import org.jboss.resteasy.reactive.common.util.MultivaluedTreeMap;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.cache.Cache;
import io.quarkus.cache.CacheName;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;

/**
 * @author martin
 *
 */
@QuarkusTest
class StandardPaginationResolverTest {

    @Inject
    StandardPaginationResolver resolver;

    @Inject
    @CacheName("default")
    Cache cache;

    @Test
    void canResolve_standardHeaderMaps() {
        Assertions.assertTrue(resolver.canResolve(MultivaluedTreeMap.class), "Cannot resolve multivalued tree map (common header map)");
        Assertions.assertTrue(resolver.canResolve(CaseInsensitiveMap.class), "Cannot resolve case insensitive map (common header map)");
    }

    @Test
    void generateEntry_success_returnsPaginationHeaders() {
        // prepare the test
        String resultsSize = "25";
        String pageSize = "10";
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org/sample/endpoint"));
        // create a header map with some junk values
        MultivaluedTreeMap<String, String> values = new MultivaluedTreeMap<>();
        values.add(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER, resultsSize);
        values.add(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER, pageSize);

        // run the call and verify the outputs
        Map<String, String> results = resolver.generateEntry(wrap, values);
        Assertions.assertEquals(2, results.keySet().size());
        Assertions.assertEquals(resultsSize, results.get(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER));
        Assertions.assertEquals(pageSize, results.get(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER));
    }

    @Test
    void generateEntry_success_ignoresJunkHeaders() {
        // prepare the test
        String resultsSize = "25";
        String pageSize = "10";
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org/sample/endpoint"));
        // create a header map with some junk values
        MultivaluedTreeMap<String, String> values = new MultivaluedTreeMap<>();
        values.add(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER, resultsSize);
        values.add(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER, pageSize);
        values.add(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        values.add("X-Sample-Header", UUID.randomUUID().toString());

        // run the call and verify the outputs
        Map<String, String> results = resolver.generateEntry(wrap, values);
        Assertions.assertEquals(2, results.keySet().size());
        Assertions.assertEquals(resultsSize, results.get(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER));
        Assertions.assertEquals(pageSize, results.get(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER));
    }

    @Test
    void generateEntry_success_invalidatesCacheOnRerun() {
        // prepare the test
        String resultsSize = "25";
        String pageSize = "10";
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org/sample/endpoint"));
        // create a header map with some junk values
        MultivaluedTreeMap<String, String> values = new MultivaluedTreeMap<>();
        values.add(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER, "15");
        values.add(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER, "5");
        // first run to set initial cache
        resolver.generateEntry(wrap, values);

        values = new MultivaluedTreeMap<>();
        values.add(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER, resultsSize);
        values.add(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER, pageSize);

        // run again and validate the results
        Map<String, String> results = resolver.generateEntry(wrap, values);
        Assertions.assertEquals(2, results.keySet().size());
        Assertions.assertEquals(resultsSize, results.get(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER));
        Assertions.assertEquals(pageSize, results.get(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER));
    }

    @Test
    void generateEntry_handlesNullSource() {
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org/sample/endpoint"));
        Map<String, String> results = resolver.generateEntry(wrap, null);
        Assertions.assertTrue(results.isEmpty());
    }

    @Test
    void generateEntry_handlesEmptySource() {
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org/sample/endpoint"));
        Map<String, String> results = resolver.generateEntry(wrap, new MultivaluedTreeMap<>());
        Assertions.assertEquals(2, results.keySet().size());
        Assertions.assertTrue(StringUtils.isBlank(results.get(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER)));
        Assertions.assertTrue(StringUtils.isBlank(results.get(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER)));
    }

    @Test
    void fallbackEntry_success_returnsPaginationHeaders() {
        // prepare the test
        String resultsSize = "25";
        String pageSize = "10";
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org/sample/endpoint"));
        // create a header map with some junk values
        MultivaluedTreeMap<String, String> values = new MultivaluedTreeMap<>();
        values.add(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER, resultsSize);
        values.add(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER, pageSize);

        // run the call and verify the outputs
        Map<String, String> results = resolver.fallbackEntry(wrap, values);
        Assertions.assertEquals(2, results.keySet().size());
        Assertions.assertEquals(resultsSize, results.get(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER));
        Assertions.assertEquals(pageSize, results.get(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER));
    }

    @Test
    void fallbackEntry_success_ignoresJunkHeaders() {
        // prepare the test
        String resultsSize = "25";
        String pageSize = "10";
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org/sample/endpoint"));
        // create a header map with some junk values
        MultivaluedTreeMap<String, String> values = new MultivaluedTreeMap<>();
        values.add(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER, resultsSize);
        values.add(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER, pageSize);
        values.add(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        values.add("X-Sample-Header", UUID.randomUUID().toString());

        // run the call and verify the outputs
        Map<String, String> results = resolver.fallbackEntry(wrap, values);
        Assertions.assertEquals(2, results.keySet().size());
        Assertions.assertEquals(resultsSize, results.get(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER));
        Assertions.assertEquals(pageSize, results.get(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER));
    }

    @Test
    void fallbackEntry_handlesNullSource() {
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org/sample/endpoint"));
        Map<String, String> results = resolver.fallbackEntry(wrap, null);
        Assertions.assertTrue(results.isEmpty());
    }

    @Test
    void fallbackEntry_handlesEmptySource() {
        FlatRequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org/sample/endpoint"));
        Map<String, String> results = resolver.fallbackEntry(wrap, new MultivaluedTreeMap<>());
        Assertions.assertEquals(2, results.keySet().size());
        Assertions.assertTrue(StringUtils.isBlank(results.get(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER)));
        Assertions.assertTrue(StringUtils.isBlank(results.get(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER)));
    }
}
