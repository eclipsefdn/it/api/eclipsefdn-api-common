/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.namespace;

import java.util.Arrays;
import java.util.List;

import org.eclipsefoundation.utils.namespace.UrlParameterNamespace;

import jakarta.inject.Singleton;

@Singleton
public class EfServicesParameterNames implements UrlParameterNamespace {
    public static final String UID_RAW = "uid";
    public static final String NAME_RAW = "name";
    public static final String MAIL_RAW = "mail";
    public static final String SPEC_PROJECT_RAW = "spec_project";
    public static final String STRATEGY_RAW = "strategy";
    public static final String VISIBILITY_RAW = "visibility";
    public static final String PARENT_ORG_RAW = "parent_organization";
    public static final String STATUS_RAW = "status";
    public static final UrlParameter UID = new UrlParameter(UID_RAW);
    public static final UrlParameter NAME = new UrlParameter(NAME_RAW);
    public static final UrlParameter MAIL = new UrlParameter(MAIL_RAW);
    public static final UrlParameter SPEC_PROJECT = new UrlParameter(SPEC_PROJECT_RAW);
    public static final UrlParameter STRATEGY = new UrlParameter(STRATEGY_RAW);
    public static final UrlParameter VISIBILITY = new UrlParameter(VISIBILITY_RAW);
    public static final UrlParameter PARENT_ORG = new UrlParameter(PARENT_ORG_RAW);
    public static final UrlParameter STATUS = new UrlParameter(STATUS_RAW);


    @Override
    public List<UrlParameter> getParameters() {
        return Arrays.asList(UID, NAME, MAIL, SPEC_PROJECT, STRATEGY, VISIBILITY, PARENT_ORG, STATUS);
    }
}
