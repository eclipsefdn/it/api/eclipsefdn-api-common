/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.api.models;

/**
 * Contains basic user data associated with a valid OAuth token. Used to access
 * basic information about the currently logged in user.
 */
public record DrupalUserInfo(String sub, String name, String githubHandle) {

    /**
     * Retrieves the uid of the user associated with the current token.
     * 
     * @return The uid of the user associated with the current token.
     */
    public String getCurrentUserUid() {
        return this.sub();
    }
}