/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.api.models;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.soabase.recordbuilder.core.RecordBuilder;

/**
 * Model representing the return data from the projects-api at '/api/interest-groups'
 */
@RecordBuilder
public record InterestGroup(String id, String title, Descriptor description, String logo, Descriptor scope,
        List<InterestGroupParticipant> leads, List<InterestGroupParticipant> participants, String projectId, String shortProjectId,
        String state, GitlabProject gitlab, Resource resources, String mailingList) {

    public InterestGroup(String id, String title, Descriptor description, String logo, Descriptor scope,
            List<InterestGroupParticipant> leads, List<InterestGroupParticipant> participants, String projectId, String shortProjectId,
            String state, GitlabProject gitlab, Resource resources, String mailingList) {
        this.id = Objects.requireNonNull(id);
        this.title = Objects.requireNonNull(title);
        this.description = Objects.requireNonNull(description);
        this.logo = Objects.requireNonNull(logo);
        this.scope = Objects.requireNonNull(scope);
        this.leads = Objects.requireNonNull(leads);
        this.participants = Objects.requireNonNull(participants);
        this.projectId = Objects.requireNonNull(projectId);
        this.shortProjectId = Objects.requireNonNull(shortProjectId);
        this.state = Objects.requireNonNull(state);
        this.gitlab = Objects.requireNonNull(gitlab);
        this.resources = Objects.requireNonNull(resources);
        this.mailingList = Objects.requireNonNull(mailingList);
    }

    @RecordBuilder
    public record Descriptor(String summary, String full) {

        public Descriptor(String summary, String full) {
            this.summary = Objects.requireNonNull(summary);
            this.full = Objects.requireNonNull(full);
        }
    }

    @RecordBuilder
    public record InterestGroupParticipant(String username, String fullName, String url, Organization organization) {

        public InterestGroupParticipant(String username, String fullName, String url, Organization organization) {
            this.username = Objects.requireNonNull(username);
            this.fullName = Objects.requireNonNull(fullName);
            this.url = Objects.requireNonNull(url);
            this.organization = Objects.requireNonNull(organization);

        }
    }

    @RecordBuilder
    public record Resource(String website, String members) {

        public Resource(String website, String members) {
            this.website = Objects.requireNonNull(website);
            this.members = Objects.requireNonNull(members);
        }
    }

    @RecordBuilder
    public record Organization(String id, String name, Map<String, Boolean> documents) {

        public Organization(String id, String name, Map<String, Boolean> documents) {
            this.id = Objects.requireNonNull(id);
            this.name = Objects.requireNonNull(name);
            this.documents = Objects.requireNonNull(documents);
        }
    }
}
