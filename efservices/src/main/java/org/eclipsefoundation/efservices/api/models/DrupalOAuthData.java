/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.api.models;

import java.util.Objects;

import io.soabase.recordbuilder.core.RecordBuilder;

/**
 * Contains information about a Drupal OAuth token. Used to validate incoming tokens.
 */
@RecordBuilder
public record DrupalOAuthData(String clientId, String userId, String accessToken, long expires, String scope) {

    public DrupalOAuthData(String clientId, String userId, String accessToken, long expires, String scope) {
        this.clientId = Objects.requireNonNull(clientId);
        this.userId = userId;
        this.accessToken = Objects.requireNonNull(accessToken);
        this.expires = expires;
        this.scope = Objects.requireNonNull(scope);
    }
}