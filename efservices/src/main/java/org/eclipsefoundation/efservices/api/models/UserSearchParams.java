/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.api.models;

import jakarta.annotation.Nullable;
import jakarta.ws.rs.QueryParam;

/**
 * Contains a set of query parameters used to perform authenticated user lookups with the ProfileService.
 */
public class UserSearchParams {

    @Nullable
    @QueryParam("uid")
    public String uid;

    @Nullable
    @QueryParam("name")
    public String name;

    @Nullable
    @QueryParam("mail")
    public String mail;

    public UserSearchParams() {

    }

    public UserSearchParams(String uid, String name, String mail) {
        this.uid = uid;
        this.name = name;
        this.mail = mail;
    }

    @Override
    public String toString() {
        return "UserSearchParams [uid=" + uid + ", name=" + name + ", mail=" + mail + "]";
    }
}