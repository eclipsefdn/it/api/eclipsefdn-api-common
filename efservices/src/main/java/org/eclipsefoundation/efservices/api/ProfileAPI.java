/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.api;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.UserSearchParams;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.HttpHeaders;

/**
 * Profile-api binding.
 */
@ApplicationScoped
@Produces("application/json")
@RegisterRestClient(configKey = "eclipse-api")
public interface ProfileAPI {

    /**
     * Searches for users that match the given search params.
     * 
     * @param token  The bearer token
     * @param params The given search params
     * @return A List of EfUser entities if they exist
     */
    @GET
    @Path("/account/profile")
    List<EfUser> getUsers(@HeaderParam(HttpHeaders.AUTHORIZATION) String token, @BeanParam UserSearchParams params);

    /**
     * Fetches a profile that matches the given Ef username. An anauthenticated call
     * will return public profile data that does not include the user email or their
     * location data.
     * 
     * @param token    The bearer oauth2 token.
     * @param username The desired user's username.
     * @return An efUser entity if it exists.
     */
    @GET
    @Path("/account/profile/{username}")
    EfUser getUserByEfUsername(@HeaderParam(HttpHeaders.AUTHORIZATION) String token,
            @PathParam("username") String username);

    /**
     * Fetches a profile that matches the given GH handle. An anauthenticated call
     * will return public profile data that does not include the user email or their
     * location data.
     * 
     * @param token  The bearer oauth2 token.
     * @param handle The desired user's GH handle.
     * @return An efUser entity if it exists.
     */
    @GET
    @Path("/github/profile/{handle}")
    EfUser getUserByGithubHandle(@HeaderParam(HttpHeaders.AUTHORIZATION) String token,
            @PathParam("handle") String handle);
}