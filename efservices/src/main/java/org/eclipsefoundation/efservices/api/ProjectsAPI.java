/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.api;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.efservices.api.models.InterestGroup;
import org.eclipsefoundation.efservices.api.models.Project;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.vertx.http.Compressed;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;

/**
 * Project-API binding. Used to fetch Project and InterestGroup entities.
 */
@ApplicationScoped
@Path("api")
@RegisterRestClient(configKey = "projects-api")
public interface ProjectsAPI {

    /**
     * Fetches all projects from the projects-api with a flag to return only spec projects.
     * 
     * @param params The BaseAPIParameters
     * @param isSpecProject Flag for matching against spec projects.
     * @return A Response containing the project entities if they exist.
     */
    @GET
    @Compressed
    @Path("projects")
    Uni<RestResponse<List<Project>>> getProjects(@BeanParam BaseAPIParameters params, @QueryParam("spec_project") int isSpecProject);

    /**
     * Fetches all interest groups from the projects-api.
     * 
     * @param params The BaseAPIParameters
     * @return A Response containing the InterestGroup entities if they exist.
     */
    @GET
    @Path("interest-groups")
    Uni<RestResponse<List<InterestGroup>>> getInterestGroups(@BeanParam BaseAPIParameters params);
}
