/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.api.models;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import io.soabase.recordbuilder.core.RecordBuilder;

/**
 * Model representing the return data from the projects-api at '/api/projects'
 */
@RecordBuilder
public record Project(String projectId, String shortProjectId, String name, String summary, String url, String websiteUrl, List<WebsiteRepo> websiteRepo,
        String logo, List<String> tags, List<Repo> githubRepos, List<Repo> gitlabRepos, GithubProject github, GitlabProject gitlab,
        List<Repo> gerritRepos, List<ProjectParticipant> contributors, List<ProjectParticipant> committers,
        List<ProjectParticipant> projectLeads, List<Collaboration> industryCollaborations, Object specProjectWorkingGroup,
        List<Release> releases, String topLevelProject) {

    public Project(String projectId, String shortProjectId, String name, String summary, String url, String websiteUrl, List<WebsiteRepo> websiteRepo,
            String logo, List<String> tags, List<Repo> githubRepos, List<Repo> gitlabRepos, GithubProject github, GitlabProject gitlab,
            List<Repo> gerritRepos, List<ProjectParticipant> contributors, List<ProjectParticipant> committers,
            List<ProjectParticipant> projectLeads, List<Collaboration> industryCollaborations, Object specProjectWorkingGroup,
            List<Release> releases, String topLevelProject) {
        this.projectId = Objects.requireNonNull(projectId);
        this.shortProjectId = Objects.requireNonNull(shortProjectId);
        this.name = Objects.requireNonNull(name);
        this.summary = Objects.requireNonNull(summary);
        this.url = Objects.requireNonNull(url);
        this.websiteUrl = Objects.requireNonNull(websiteUrl);
        this.websiteRepo = Objects.requireNonNull(websiteRepo);
        this.logo = Objects.requireNonNull(logo);
        this.tags = Objects.requireNonNull(tags);
        this.githubRepos = Objects.requireNonNull(githubRepos);
        this.gitlabRepos = Objects.requireNonNull(gitlabRepos);
        this.github = Objects.requireNonNull(github);
        this.gitlab = Objects.requireNonNull(gitlab);
        this.gerritRepos = Objects.requireNonNull(gerritRepos);
        this.contributors = Objects.requireNonNull(contributors);
        this.committers = Objects.requireNonNull(committers);
        this.projectLeads = Objects.requireNonNull(projectLeads);
        this.industryCollaborations = Objects.requireNonNull(industryCollaborations);
        this.specProjectWorkingGroup = Objects.requireNonNull(specProjectWorkingGroup);
        this.releases = Objects.requireNonNull(releases);
        this.topLevelProject = Objects.requireNonNull(topLevelProject);
    }

    public Optional<Collaboration> getSpecWorkingGroup() {
        // stored as map as empty returns an array instead of a map
        Object specProjectWorkingGroup = specProjectWorkingGroup();
        if (specProjectWorkingGroup instanceof Map) {
            // we checked in line above that the map exists, so we can safely cast it
            @SuppressWarnings("unchecked")
            Map<Object, Object> map = (Map<Object, Object>) specProjectWorkingGroup;

            Object name = map.get("name");
            Object id = map.get("id");

            if (name instanceof String nameStr && id instanceof String idStr) {
                return Optional.of(ProjectCollaborationBuilder.builder().id(idStr).name(nameStr).build());
            }
        }

        return Optional.empty();
    }

    @RecordBuilder
    public record WebsiteRepo(String checkoutDir, String sourceBranch, String url) {

        public WebsiteRepo(String checkoutDir, String sourceBranch, String url) {
            this.checkoutDir = Objects.requireNonNull(checkoutDir);
            this.sourceBranch = Objects.requireNonNull(sourceBranch);
            this.url = Objects.requireNonNull(url);
        }
    }

    @RecordBuilder
    public record Repo(String url) {

        public Repo(String url) {
            this.url = Objects.requireNonNull(url);
        }
    }

    @RecordBuilder
    public record GithubProject(String org, List<String> ignoredRepos) {

        public GithubProject(String org, List<String> ignoredRepos) {
            this.org = Objects.requireNonNull(org);
            this.ignoredRepos = Objects.requireNonNull(ignoredRepos);
        }

    }

    @RecordBuilder
    public record ProjectParticipant(String username, String fullName, String url) {

        public ProjectParticipant(String username, String fullName, String url) {
            this.username = Objects.requireNonNull(username);
            this.fullName = Objects.requireNonNull(fullName);
            this.url = Objects.requireNonNull(url);
        }
    }

    @RecordBuilder
    public record Collaboration(String name, String id) {

        public Collaboration(String name, String id) {
            this.name = name;
            this.id = Objects.requireNonNull(id);
        }
    }

    @RecordBuilder
    public record Release(String name, String url) {

        public Release(String name, String url) {
            this.name = Objects.requireNonNull(name);
            this.url = Objects.requireNonNull(url);
        }
    }
}
