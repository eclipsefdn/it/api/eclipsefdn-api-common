/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.api.models;

import java.util.List;
import java.util.Objects;

import io.soabase.recordbuilder.core.RecordBuilder;

@RecordBuilder
public record GitlabProject(String projectGroup, List<String> ignoredSubGroups) {

    public GitlabProject(String projectGroup, List<String> ignoredSubGroups) {
        this.projectGroup = Objects.requireNonNull(projectGroup);
        this.ignoredSubGroups = Objects.requireNonNull(ignoredSubGroups);
    }
}
