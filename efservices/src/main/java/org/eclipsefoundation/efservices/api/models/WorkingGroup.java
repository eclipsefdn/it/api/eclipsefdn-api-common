/**
 * Copyright (c) 2021 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.efservices.api.models;

import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.soabase.recordbuilder.core.RecordBuilder;

/**
 * JSON object for importing Working group JSON assets.
 * 
 * @author Martin Lowe <martin.lowe@eclipse-foundation.org>
 */
@RecordBuilder
public record WorkingGroup(String alias, String title, String status, String logo, String description, String parentOrganization,
        WorkingGroupResources resources, List<WorkingGroupParticipationLevel> levels) {

    public WorkingGroup(String alias, String title, String status, String logo, String description, String parentOrganization,
            WorkingGroupResources resources, List<WorkingGroupParticipationLevel> levels) {
        this.alias = Objects.requireNonNull(alias);
        this.title = Objects.requireNonNull(title);
        this.status = Objects.requireNonNull(status);
        this.logo = Objects.requireNonNull(logo);
        this.description = Objects.requireNonNull(description);
        this.parentOrganization = Objects.requireNonNull(parentOrganization);
        this.resources = Objects.requireNonNull(resources);
        this.levels = Objects.requireNonNull(levels);
    }

    @RecordBuilder
    public record WorkingGroupResources(String charter,
            @JsonProperty("participation_agreements") WorkingGroupParticipationAgreements participationAgreements, String website,
            String members, String sponsorship, String contactForm) {

        public WorkingGroupResources(String charter, WorkingGroupParticipationAgreements participationAgreements, String website,
                String members, String sponsorship, String contactForm) {
            this.charter = Objects.requireNonNull(charter);
            this.participationAgreements = Objects.requireNonNull(participationAgreements);
            this.website = Objects.requireNonNull(website);
            this.members = Objects.requireNonNull(members);
            this.sponsorship = Objects.requireNonNull(sponsorship);
            this.contactForm = Objects.requireNonNull(contactForm);
        }
    }

    @RecordBuilder
    public record WorkingGroupParticipationLevel(String relation, String description) {

        public WorkingGroupParticipationLevel(String relation, String description) {
            this.relation = Objects.requireNonNull(relation);
            this.description = Objects.requireNonNull(description);
        }
    }

    @RecordBuilder
    public record WorkingGroupParticipationAgreement(@JsonProperty("document_id") String documentId, String pdf) {

        public WorkingGroupParticipationAgreement(String documentId, String pdf) {
            this.documentId = Objects.requireNonNull(documentId);
            this.pdf = Objects.requireNonNull(pdf);
        }
    }

    @RecordBuilder
    public record WorkingGroupParticipationAgreements(WorkingGroupParticipationAgreement individual,
            WorkingGroupParticipationAgreement organization) {

    }
}
