/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.api;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.efservices.api.models.DrupalOAuthData;
import org.eclipsefoundation.efservices.api.models.DrupalUserInfo;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MediaType;

/**
 * Drupal OAuth2 token validation API binding
 */
@Path("oauth2")
@Produces(MediaType.APPLICATION_JSON)
@ApplicationScoped
@RegisterRestClient(configKey = "accounts-api")
public interface DrupalOAuthAPI {

    /**
     * Fetches the data tied to the given token if it is valid. Returns a
     * DrupaOAuthData entity.
     * 
     * @param token The given token.
     * @return A DrupalOAuthData if the token is valid.
     */
    @GET
    @Path("/tokens/{token}")
    DrupalOAuthData getTokenInfo(@PathParam("token") String token);

    /**
     * Fetches basic user information using the given token. Returns a
     * DrupalUserInfo entity if the token is valid.
     * 
     * @param token The given token.
     * @return A DrupalUserInfo entity if the token is valid.
     */
    @POST
    @Path("/userinfo")
    DrupalUserInfo getUserInfoFromToken(@HeaderParam(HttpHeaders.AUTHORIZATION) String token);
}