/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.api.models;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.soabase.recordbuilder.core.RecordBuilder;

/**
 * This model contains all user profile data. It is the entity served by the profile-api. Used to allow access to current user profile
 * information. Provides a method for creating a stubbed bot user.
 */
@RecordBuilder
public record EfUser(String uid, String name, String picture, String mail, Eca eca, Boolean isCommitter, String firstName, String lastName,
        String fullName, Map<String, PublisherAgreement> publisherAgreements, String githubHandle, String twitterHandle, String org,
        Integer orgId, String jobTitle, String website, String mxid, Country country, String bio, List<String> interests,
        List<Email> mailHistory, List<String> mailAlternate, String ecaUrl, String projectsUrl, String gerritUrl, String mailinglistUrl,
        String mpcFavoritesUrl, @JsonIgnore Boolean isBot) {

    /**
     * 
     */
    public EfUser(String uid, String name, String picture, String mail, Eca eca, Boolean isCommitter, String firstName, String lastName,
            String fullName, Map<String, PublisherAgreement> publisherAgreements, String githubHandle, String twitterHandle, String org,
            Integer orgId, String jobTitle, String website, String mxid, Country country, String bio, List<String> interests,
            List<Email> mailHistory, List<String> mailAlternate, String ecaUrl, String projectsUrl, String gerritUrl, String mailinglistUrl,
            String mpcFavoritesUrl, Boolean isBot) {
        this.uid = Objects.requireNonNull(uid);
        this.name = Objects.requireNonNull(name);
        this.picture = Objects.requireNonNull(picture);
        this.mail = mail;
        this.eca = eca;
        this.isCommitter = isCommitter;
        this.firstName = Objects.requireNonNull(firstName);
        this.lastName = Objects.requireNonNull(lastName);
        this.fullName = Objects.requireNonNull(fullName);
        this.publisherAgreements = Objects.requireNonNull(publisherAgreements);
        this.githubHandle = githubHandle;
        this.twitterHandle = Objects.requireNonNull(twitterHandle);
        this.org = org;
        this.orgId = orgId;
        this.jobTitle = Objects.requireNonNull(jobTitle);
        this.website = Objects.requireNonNull(website);
        this.mxid = mxid;
        this.country = country;
        this.bio = bio;
        this.interests = Objects.requireNonNull(interests);
        this.mailHistory = mailHistory;
        this.mailAlternate = mailAlternate;
        this.ecaUrl = ecaUrl;
        this.projectsUrl = projectsUrl;
        this.gerritUrl = gerritUrl;
        this.mailinglistUrl = mailinglistUrl;
        this.mpcFavoritesUrl = mpcFavoritesUrl;
        this.isBot = isBot;
    }

    /**
     * Converts the user profile to the restricted public view. Json Ignore is super important, as otherwise this will recursively add
     * itself to json and crash.
     * 
     * @return current user profile, with the private data blanked out.
     */
    @JsonIgnore
    public EfUser getPublicProfile() {
        return EfUserBuilder
                .builder(this)
                .mail("")
                .mxid("")
                .country(EfUserCountryBuilder.builder().code(null).name(null).build())
                .mailAlternate(Collections.emptyList())
                .mailHistory(Collections.emptyList())
                .build();
    }

    public static EfUser createBotStub(String name, String mail) {
        return EfUserBuilder
                .builder()
                .uid("0")
                .name(name)
                .mail(mail)
                .eca(new Eca(false, false))
                .isBot(Boolean.TRUE)
                .picture("")
                .firstName("")
                .lastName("")
                .fullName("")
                .mxid("")
                .publisherAgreements(Collections.emptyMap())
                .twitterHandle("")
                .org(null)
                .jobTitle("bot")
                .website("")
                .country(new Country(null, null))
                .interests(Collections.emptyList())
                .mailHistory(Collections.emptyList())
                .mailAlternate(Collections.emptyList())
                .build();
    }

    @RecordBuilder
    public record Eca(boolean signed, boolean canContributeSpecProject) {

        public Eca(boolean signed, boolean canContributeSpecProject) {
            this.signed = Objects.requireNonNull(signed);
            this.canContributeSpecProject = Objects.requireNonNull(canContributeSpecProject);
        }
    }

    @RecordBuilder
    public record Country(String code, String name) {

    }

    @RecordBuilder
    public record PublisherAgreement(String version) {

        public PublisherAgreement(String version) {
            this.version = Objects.requireNonNull(version);
        }
    }

    @RecordBuilder
    public record Email(String id, String mail) {

        public Email(String id, String mail) {
            this.id = Objects.requireNonNull(id);
            this.mail = Objects.requireNonNull(mail);
        }
    }
}
