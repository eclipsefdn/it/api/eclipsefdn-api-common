/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.api;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.efservices.api.models.WorkingGroup;
import org.jboss.resteasy.reactive.RestResponse;

import io.smallrye.mutiny.Uni;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

@Path("working-groups/")
@Produces(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey = "wg-api")
public interface WorkingGroupsAPI {

    /**
     * Retrieves all working groups available from the API, passing along pagination information.
     * 
     * @param baseParams base pagination parameters
     * @return response containing working group results.
     */
    @GET
    public Uni<RestResponse<List<WorkingGroup>>> get(@BeanParam BaseAPIParameters baseParams, @QueryParam("status") List<String> statuses);
}
