/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
/**
 * 
 */
package org.eclipsefoundation.efservices.config;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Optional;

import org.eclipsefoundation.efservices.api.models.DrupalOAuthData;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.UserSearchParams;
import org.eclipsefoundation.efservices.helpers.DrupalAuthHelper;
import org.eclipsefoundation.efservices.models.AuthenticatedRequestWrapper;
import org.eclipsefoundation.efservices.namespace.RequestContextPropertyNames;
import org.eclipsefoundation.efservices.services.DrupalOAuthService;
import org.eclipsefoundation.efservices.services.ProfileService;
import org.eclipsefoundation.http.annotations.AuthenticatedAlternate;
import org.eclipsefoundation.http.config.OAuth2SecurityConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.RequestScoped;
import jakarta.enterprise.inject.Produces;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.HttpHeaders;

/**
 * Provider for alternate auth states for OAuth2 servers that aren't fully compliant with required specs/conventions.
 */
public class AuthenticatedRequestWrapperProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticatedRequestWrapperProvider.class);

    /**
     * Using the passed context + services, retrieve the current user for the request. This does not use standard OIDC flow as the usual alt
     * server does not properly implement the OAuth2 spec required for OIDC binding.
     * 
     * @param requestContext current HTTP request context
     * @param info information about the endpoint called, used to check if alt authentication is enabled
     * @param oauthService service binding to the OAuth2 service backing the alternate authentication
     * @param profile instance for the EF user profile service, used in helper methods in the wrapper
     * @param config values about the alternate authentication server to be used in the request
     * @return the current authenticated request token and user, wrapped in a helper for ease of access
     */
    @Produces
    @RequestScoped
    public AuthenticatedRequestWrapper requestTokenWrapper(ContainerRequestContext requestContext, ResourceInfo info,
            DrupalOAuthService oauthService, ProfileService profile, OAuth2SecurityConfig config) {
        Method m = info.getResourceMethod();
        AuthenticatedAlternate authenticated = m.getAnnotation(AuthenticatedAlternate.class);

        if (authenticated != null || config.filter().alwaysOn().enabled()) {
            try {
                String token = DrupalAuthHelper.stripBearerToken(requestContext.getHeaderString(HttpHeaders.AUTHORIZATION));
                DrupalOAuthData tokenStatus = oauthService
                        .validateTokenStatus(token, config.filter().validScopes().orElse(Collections.emptyList()),
                                config.filter().validClientIds());
                // check if we were able to get a token state for the passed header value
                if (tokenStatus != null) {
                    requestContext.setProperty(RequestContextPropertyNames.TOKEN_STATUS, tokenStatus);
                    if (tokenStatus.userId() != null && !"0".equals(tokenStatus.userId())) {
                        // Fetch user data from token and set in context
                        LOGGER.trace("Fetching user info for token with uid: {}", tokenStatus.userId());
                        // fetch profile directly, as we can't rely on tokens having openid scope
                        Optional<EfUser> currentUserProfile = profile
                                .performUserSearch(new UserSearchParams(tokenStatus.userId(), null, null));
                        return new AuthenticatedRequestWrapper(tokenStatus, currentUserProfile.orElse(null));
                    }
                    LOGGER.trace("Anonymous client request with no associated user detected, continuing");
                } else {
                    LOGGER.trace("No valid authentication for current request found, returning anonymous state");
                }
                // fallback to return the found token as the requests auth state (token can be null)
                return new AuthenticatedRequestWrapper(tokenStatus, null);
            } catch (Exception e) {
                // We want to prevent this from reaching user on profile queries.
                LOGGER.debug("Invalid authentication", e);

                // If the endpoint is authenticated and doesn't have a valid token, we deny the request
                if (!allowPartialResponse(authenticated, config)) {
                    throw e;
                }
            }
        }
        LOGGER.trace("No valid authentication for current request found, returning anonymous state");
        return new AuthenticatedRequestWrapper(null, null);
    }

    /**
     * Whether partial responses should be allowed for the current request, using the generic configurations and the specific endpoint
     * 
     * @param authenticated annotation on current matched request endpoint definition
     * @param config global configs for authentication.
     * @return true if partial response is allowed, or else false
     */
    private boolean allowPartialResponse(AuthenticatedAlternate authenticated, OAuth2SecurityConfig config) {
        // check if the partial response has been disabled as an override/explicit setting
        boolean perMethodPartialResponseDisabled = (authenticated != null && !authenticated.allowPartialResponse());
        if (perMethodPartialResponseDisabled) {
            return false;
        }

        // check if global filter has been enabled or if annotation is present. We've already checked for explicit disable above
        return (config.filter().alwaysOn().enabled() && config.filter().alwaysOn().allowPartialResponse()) || authenticated != null;
    }
}
