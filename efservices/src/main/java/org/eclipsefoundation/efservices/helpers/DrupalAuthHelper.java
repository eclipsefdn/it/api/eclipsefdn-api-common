/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.helpers;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipsefoundation.utils.exception.FinalForbiddenException;

/**
 * Defines helper methods for Drupal OAuth2 token validation.
 */
public final class DrupalAuthHelper {

    // should be 2 as bearer tokens follow pattern of "Bearer the-token"
    private static final int BEARER_TOKEN_PART_COUNT = 2;

    /**
     * Validates whether a given expiry date has passed.
     * 
     * @param expiryDate The token expiry date in seconds since epoch.
     * @return True if expiry date is older than current time. False if not.
     */
    public static boolean isExpired(long expiryDate) {
        return expiryDate <= Instant.now().getEpochSecond();
    }

    /**
     * Validates whether the token scopes and valid scopes are the same.
     * 
     * @param tokenScope The space-separated token scopes.
     * @param validScopes The list of valid scopes.
     * @return Returns false if any token scopes are not in the list of valid scopes. True if all match.
     */
    public static boolean hasScopes(String tokenScope, List<String> validScopes) {

        List<String> tokenScopes = Arrays.asList(tokenScope.split(" "));

        Collections.sort(tokenScopes);
        Collections.sort(validScopes);

        return tokenScopes.containsAll(validScopes);
    }

    /**
     * Validates whether the the token's client id matches a desired client id.
     * 
     * @param clientId The client id associated with the token
     * @param validClientIds The valid client ids allowed in the service.
     * @return True if ids match. False if not.
     */
    public static boolean hasValidclientId(String clientId, List<String> validClientIds) {
        return validClientIds.stream().anyMatch(id -> id.equalsIgnoreCase(clientId));
    }

    /**
     * Strips the bearer token down to just the token. Throws a FinalForbiddenException if the token is in an invalid format.
     * 
     * @param header the bearer token suthorization header
     * @return The raw token string.
     */
    public static String stripBearerToken(String header) {
        if (header == null || header.equals("")) {
            throw new FinalForbiddenException("No auth token present");
        }

        String[] splits = header.split(" ");

        // Invalid token if no space or more than one space
        if (splits.length != BEARER_TOKEN_PART_COUNT || !splits[0].equals("Bearer")) {
            throw new FinalForbiddenException("Invalid auth header");
        }

        return splits[1];
    }

    private DrupalAuthHelper() {
    }
}