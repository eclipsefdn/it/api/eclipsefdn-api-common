/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.oauth;

import org.eclipse.microprofile.config.ConfigProvider;

import com.github.scribejava.core.builder.api.DefaultApi20;
import com.github.scribejava.core.oauth2.clientauthentication.ClientAuthentication;
import com.github.scribejava.core.oauth2.clientauthentication.RequestBodyAuthenticationScheme;

/**
 * Wrapper around the OAuth API for Scribejava. Enables OAuth2.0 binding to the Eclipse Foundation OAuth server.
 */
public class OAuthAPI extends DefaultApi20 {

    private final String accessTokenUrl;

    public OAuthAPI(String accessTokenUrl) {
        this.accessTokenUrl = accessTokenUrl;
    }

    @Override
    public String getAccessTokenEndpoint() {
        return accessTokenUrl;
    }

    @Override
    protected String getAuthorizationBaseUrl() {
        return null;
    }

    @Override
    public ClientAuthentication getClientAuthentication() {
        return RequestBodyAuthenticationScheme.instance();
    }

    private static class InstanceHolder {
        private static final OAuthAPI INSTANCE = new OAuthAPI(ConfigProvider
                .getConfig()
                .getOptionalValue("eclipse.security.oauth2.token-generation.access-token-url", String.class)
                .orElse("https://accounts.eclipse.org/oauth2/token"));
    }

    public static OAuthAPI instance() {
        return InstanceHolder.INSTANCE;
    }
}