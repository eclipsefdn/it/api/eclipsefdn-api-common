/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.models;

import org.eclipsefoundation.efservices.api.models.DrupalOAuthData;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.utils.exception.FinalForbiddenException;

/**
 * A RequestScoped bean that wraps the DrupalOAuthData value set in the request chain. This bean is used to retrieve various information
 * about the current token. To access this data, a DrupalOAuthData object must be set as the 'token_status' property in the
 * HttpServletRequest bound to the current request chain.
 */
public class AuthenticatedRequestWrapper {
    private static final String NO_USER_ERR_MSG = "No user associated with this token";

    private final DrupalOAuthData tokenStatus;
    private final EfUser currentUser;

    public AuthenticatedRequestWrapper(DrupalOAuthData tokenStatus, EfUser currentUser) {
        this.tokenStatus = tokenStatus;
        this.currentUser = currentUser;
    }

    /**
     * Retrieves the DrupalOAuthData bound to the current request chain.
     * 
     * @return The DrupalOAuthData bound to the current request.
     */
    public DrupalOAuthData getTokenStatus() {
        return this.tokenStatus;
    }

    /**
     * Retrieves the DrupalUserInfo object associated with the current token.
     * 
     * @return The DrupalUserInfo associated with the current token.
     */
    public EfUser getCurrentUser() {
        if (currentUser == null) {
            throw new FinalForbiddenException(NO_USER_ERR_MSG);
        }
        return this.currentUser;
    }

    /**
     * A simple check to determine if the current request has a valid token associated with it.
     * 
     * @return True if valid token, false if not.
     */
    public boolean isAuthenticated() {
        return tokenStatus != null;
    }

}
