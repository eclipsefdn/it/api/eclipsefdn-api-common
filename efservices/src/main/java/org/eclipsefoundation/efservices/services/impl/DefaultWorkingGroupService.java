/**
 * Copyright (c) 2021 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.efservices.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipsefoundation.caching.model.ParameterizedCacheKeyBuilder;
import org.eclipsefoundation.caching.service.LoadingCacheManager;
import org.eclipsefoundation.efservices.api.models.WorkingGroup;
import org.eclipsefoundation.efservices.api.models.WorkingGroup.WorkingGroupParticipationAgreement;
import org.eclipsefoundation.efservices.services.WorkingGroupService;

import jakarta.enterprise.context.ApplicationScoped;

/**
 * Retrieves working groups related to the Eclipse Foundation, as well as provides some basic filters built-in for ease of use.
 * 
 * @author Martin Lowe
 */
@ApplicationScoped
public class DefaultWorkingGroupService implements WorkingGroupService {

	private final LoadingCacheManager cache;

	public DefaultWorkingGroupService(LoadingCacheManager cache) {
		this.cache = cache;
	}

	@Override
	public List<WorkingGroup> get() {
		return new ArrayList<>(cache.getList(ParameterizedCacheKeyBuilder.builder().id("all").clazz(WorkingGroup.class).build()));
	}

	@Override
	public List<WorkingGroup> get(List<String> parentOrgs, List<String> statuses) {
		return get()
				.stream()
				.filter(wg -> filterByParentOrganizations(parentOrgs, wg))
				.filter(wg -> filterByProjectStatuses(statuses, wg))
				.toList();
	}

	@Override
	public Optional<WorkingGroup> getByName(String name) {
		return get().stream().filter(wg -> wg.alias().equalsIgnoreCase(name)).findFirst();
	}

	@Override
	public Optional<WorkingGroup> getByAgreementId(String docId) {
		Optional<Entry<String, List<String>>> entry = getWGPADocumentIDs()
				.entrySet()
				.stream()
				.filter(e -> e.getValue().contains(docId))
				.findFirst();
		return entry.isEmpty() ? Optional.empty() : getByName(entry.get().getKey());
	}

	@Override
	public Map<String, List<String>> getWGPADocumentIDs() {
		return get().stream().collect(Collectors.toMap(WorkingGroup::alias, this::extractWGPADocumentIDs));
	}

	private List<String> extractWGPADocumentIDs(WorkingGroup wg) {
		List<String> ids = new ArrayList<>();
		WorkingGroupParticipationAgreement iwgpa = wg.resources().participationAgreements().individual();
		if (iwgpa != null) {
			ids.add(iwgpa.documentId());
		}
		WorkingGroupParticipationAgreement wgpa = wg.resources().participationAgreements().organization();
		if (wgpa != null) {
			ids.add(wgpa.documentId());
		}
		return ids;
	}

	private boolean filterByParentOrganizations(List<String> parentOrganizations, WorkingGroup wg) {
		return (parentOrganizations == null || parentOrganizations.isEmpty()) || parentOrganizations.contains(wg.parentOrganization());
	}

	private boolean filterByProjectStatuses(List<String> statuses, WorkingGroup wg) {
		return (statuses == null || statuses.isEmpty()) || statuses.contains(wg.status());
	}
}
