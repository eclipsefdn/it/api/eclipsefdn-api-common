/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.services;

import java.util.List;

import org.eclipsefoundation.efservices.api.models.InterestGroup;
import org.eclipsefoundation.efservices.api.models.Project;

/**
 * Intermediate layer beween the resource and rest client layers. Used to access
 * pre-cached Project and InterestGroup data.
 */
public interface ProjectService {

    /**
     * Retrieves all currently available projects from cache, otherwise
     * retrieves a fresh copy of the data from the API.
     * 
     * @return A list of Project entities available from API.
     */
    List<Project> getAllProjects();

    /**
     * Retrieves all currently available spec projects from cache, otherwise
     * retrieves a fresh copy of the data from the API.
     * 
     * @return A list of spec Project entities available from API.
     */
    List<Project> getAllSpecProjects();

    /**
     * Retrieves all currently available interest groups from cache, otherwise
     * retrieves a fresh copy of the data from the API.
     * 
     * @return A list of InterestGroup entities available from API.
     */
    List<InterestGroup> getAllInterestGroups();
}
