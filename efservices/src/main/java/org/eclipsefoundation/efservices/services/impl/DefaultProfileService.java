/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.services.impl;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.caching.model.CacheWrapper;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.efservices.api.ProfileAPI;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.UserSearchParams;
import org.eclipsefoundation.efservices.namespace.EfServicesParameterNames;
import org.eclipsefoundation.efservices.services.DrupalTokenService;
import org.eclipsefoundation.efservices.services.ProfileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * Default implementation of the ProfileService. Provides a mechanism to perform user lookups via various fields such as EfUsername,
 * GhHandle, uid, and email.
 */
@ApplicationScoped
public class DefaultProfileService implements ProfileService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultProfileService.class);

    private final ProfileAPI profileAPI;
    private final DrupalTokenService tokenService;
    private final CachingService cache;

    public DefaultProfileService(@RestClient ProfileAPI profileAPI, DrupalTokenService tokenService, CachingService cache) {
        this.profileAPI = profileAPI;
        this.tokenService = tokenService;
        this.cache = cache;
    }

    @Override
    public Optional<EfUser> fetchUserByUsername(String username, boolean enablePrivateData) {
        String visibility = enablePrivateData ? "private" : "public";
        LOGGER.debug("Fetching {} profile for username: {}", visibility, username);

        // Fetch from API with "username" cache strategy
        CacheWrapper<EfUser> result = cache
                .get(username, buildCacheStrategy("username", visibility), EfUser.class,
                        () -> profileAPI.getUserByEfUsername(enablePrivateData ? getBearerToken() : "", username));
        if (result.data().isEmpty()) {
            LOGGER.warn("Could not find user: {}", username);
        }
        return result.data();
    }

    @Override
    public Optional<EfUser> fetchUserByGhHandle(String handle, boolean enablePrivateData) {
        String visibility = enablePrivateData ? "private" : "public";
        LOGGER.debug("Fetching {} profile for handle: {}", visibility, handle);

        // Fetch from API with "handle" cache strategy
        CacheWrapper<EfUser> result = cache
                .get(handle, buildCacheStrategy("handle", visibility), EfUser.class,
                        () -> profileAPI.getUserByGithubHandle(enablePrivateData ? getBearerToken() : "", handle));
        if (result.data().isEmpty()) {
            LOGGER.warn("Could not find user: {}", handle);
        }
        return result.data();
    }

    @Override
    public Optional<EfUser> performUserSearch(UserSearchParams params) {
        LOGGER.debug("Fetching profile with params: {}", params);

        CacheWrapper<List<EfUser>> results = cache
                .get("search", buildSearchCacheParams(params), EfUser.class, () -> profileAPI.getUsers(getBearerToken(), params));
        Optional<List<EfUser>> opt = results.data();
        if (opt.isEmpty() || opt.get().isEmpty()) {
            LOGGER.warn("Could not find user with params: {}", params);
            return Optional.empty();
        }

        LOGGER.debug("Found {} profile results", opt.get().size());
        return Optional.of(opt.get().get(0));
    }

    /**
     * Builds a set of cache key parameters for use when performing a user search.
     * 
     * @param params The given params for performing the user search
     * @return A MultivaluedMap containing the given cache key params
     */
    private MultivaluedMap<String, String> buildSearchCacheParams(UserSearchParams params) {
        MultivaluedMap<String, String> cacheParams = new MultivaluedHashMap<>();
        if (params.uid != null) {
            cacheParams.add(EfServicesParameterNames.UID_RAW, params.uid);
        }
        if (StringUtils.isNotBlank(params.name)) {
            cacheParams.add(EfServicesParameterNames.NAME_RAW, params.name);
        }
        if (StringUtils.isNotBlank(params.mail)) {
            cacheParams.add(EfServicesParameterNames.MAIL_RAW, params.mail);
        }
        return cacheParams;
    }

    /**
     * Builds a MultivaluedMap containing the desired cache strategy and profile visibility. This is to avoid collision between two entities
     * of the same type that map to the same cache id but are meant to be separate cache entries.
     * 
     * Ex: A Gh handle and an EfUsername that are the same but belong to different people.
     * 
     * @param strategy The caching strategy. Ex: username | handle
     * @param visibility The profile visibility
     * @return A MultivaluedMap containing the desired cache strategy.
     */
    private MultivaluedMap<String, String> buildCacheStrategy(String strategy, String visibility) {
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(EfServicesParameterNames.STRATEGY.getName(), strategy);
        params.add(EfServicesParameterNames.VISIBILITY.getName(), visibility);
        return params;
    }

    /**
     * Builds a bearer token using the token generated by the Oauth server.
     * 
     * @return A fully formed Bearer token
     */
    private String getBearerToken() {
        return "Bearer " + tokenService.getToken();
    }
}
