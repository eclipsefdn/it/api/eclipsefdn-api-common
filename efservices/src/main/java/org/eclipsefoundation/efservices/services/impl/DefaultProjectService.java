/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.services.impl;

import java.util.List;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.caching.model.ParameterizedCacheKeyBuilder;
import org.eclipsefoundation.caching.service.LoadingCacheManager;
import org.eclipsefoundation.efservices.api.models.InterestGroup;
import org.eclipsefoundation.efservices.api.models.Project;
import org.eclipsefoundation.efservices.namespace.EfServicesParameterNames;
import org.eclipsefoundation.efservices.services.ProjectService;

import io.quarkus.runtime.Startup;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Instance;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * Default implementation of the ProjectService. Fetches all entities from a corresponding loading-cache. Populates the spec_project cache
 * entry on application startup.
 */
@Startup
@ApplicationScoped
public class DefaultProjectService implements ProjectService {

    private static final String DEFAULT_CACHE_ID = "all";

    private final Instance<Boolean> isStartup;
    private final LoadingCacheManager cacheManager;

    public DefaultProjectService(
            @ConfigProperty(name = "eclipse.services.projects.cache-on-startup", defaultValue = "false") Instance<Boolean> isStartup,
            LoadingCacheManager cacheManager) {
        this.isStartup = isStartup;
        this.cacheManager = cacheManager;
    }

    @PostConstruct
    void init() {
        if (Boolean.TRUE.equals(isStartup.get())) {
            getAllProjects();
            getAllSpecProjects();
            getAllInterestGroups();
        }
    }

    @Override
    public List<Project> getAllProjects() {
        return cacheManager
                .getList(ParameterizedCacheKeyBuilder
                        .builder()
                        .id(DEFAULT_CACHE_ID)
                        .clazz(Project.class)
                        .params(new MultivaluedHashMap<>())
                        .build());
    }

    @Override
    public List<Project> getAllSpecProjects() {
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(EfServicesParameterNames.SPEC_PROJECT_RAW, "1");
        return cacheManager
                .getList(ParameterizedCacheKeyBuilder.builder().id(DEFAULT_CACHE_ID).clazz(Project.class).params(params).build());
    }

    @Override
    public List<InterestGroup> getAllInterestGroups() {
        return cacheManager
                .getList(ParameterizedCacheKeyBuilder
                        .builder()
                        .id(DEFAULT_CACHE_ID)
                        .clazz(InterestGroup.class)
                        .params(new MultivaluedHashMap<>())
                        .build());
    }
}
