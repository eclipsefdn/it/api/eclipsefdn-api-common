/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.services.impl;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.efservices.api.DrupalOAuthAPI;
import org.eclipsefoundation.efservices.api.models.DrupalOAuthData;
import org.eclipsefoundation.efservices.api.models.DrupalUserInfo;
import org.eclipsefoundation.efservices.helpers.DrupalAuthHelper;
import org.eclipsefoundation.efservices.services.DrupalOAuthService;
import org.eclipsefoundation.utils.exception.FinalForbiddenException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.arc.Unremovable;
import jakarta.enterprise.context.ApplicationScoped;

/**
 * Default implementaion of the DrupaOAuthSErvice. Provides a mechanism for validating tokens and fetching basic user info about the token
 * user.
 */
@ApplicationScoped
@Unremovable
public class DefaultDrupalOAuthService implements DrupalOAuthService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultDrupalOAuthService.class);

    private final DrupalOAuthAPI oauthAPI;

    public DefaultDrupalOAuthService(@RestClient DrupalOAuthAPI oauthAPI) {
        this.oauthAPI = oauthAPI;
    }

    @Override
    public DrupalOAuthData validateTokenStatus(String token, List<String> validScopes, List<String> validClientIds) {
        try {
            DrupalOAuthData tokenData = oauthAPI.getTokenInfo(token);

            if (DrupalAuthHelper.isExpired(tokenData.expires())) {
                throw new FinalForbiddenException("This token is expired");
            }
            if (!DrupalAuthHelper.hasScopes(tokenData.scope(), validScopes)) {
                throw new FinalForbiddenException("This token has invalid scope(s)");
            }
            if (!DrupalAuthHelper.hasValidclientId(tokenData.clientId(), validClientIds)) {
                throw new FinalForbiddenException("Invalid client_id associated with token");
            }

            return tokenData;
        } catch (Exception e) {
            LOGGER.error("Error validating token", e);
            throw new FinalForbiddenException("Error validating token");
        }
    }

    @Override
    public DrupalUserInfo getTokenUserInfo(String token) {
        try {
            return oauthAPI.getUserInfoFromToken("Bearer " + token);
        } catch (Exception e) {
            LOGGER.error("Error getting user from token", e);
            throw new FinalForbiddenException("Error getting user from token");
        }
    }
}