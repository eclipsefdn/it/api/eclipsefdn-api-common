/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.services;

import java.util.Optional;

import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.UserSearchParams;

/**
 * Defines a service to allow fetching of Eclipse User profile data.
 */
public interface ProfileService {

    /**
     * Fetches profile data for a user with the given username. Has a flag to enable/disable authenticated calls to the profile-api. Provate
     * profile information does not contain fields such as email and location.
     * 
     * @param username The given ef username.
     * @param enablePrivateData Flag to reveal/hide private profile data.
     * @return An Optional containg the desired EfUser if it exists.
     */
    Optional<EfUser> fetchUserByUsername(String username, boolean enablePrivateData);

    /**
     * Fetches profile data for a user with the given gh handle. Has a flag to enable/disable authenticated calls to the profile-api.
     * Provate profile information does not contain fields such as email and location.
     * 
     * @param handle The given GH handle.
     * @param enablePrivateData Flag to reveal/hide private profile data.
     * @return An Optional containg the desired EfUser if it exists.
     */
    Optional<EfUser> fetchUserByGhHandle(String handle, boolean enablePrivateData);

    /**
     * Performs a user search using the given search params.
     * 
     * @param params The given searhc params
     * @return An Optional containing an EfUser entity if it exists
     */
    Optional<EfUser> performUserSearch(UserSearchParams params);
}
