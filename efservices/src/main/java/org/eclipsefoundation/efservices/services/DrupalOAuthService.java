/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.services;

import java.util.List;

import org.eclipsefoundation.efservices.api.models.DrupalOAuthData;
import org.eclipsefoundation.efservices.api.models.DrupalUserInfo;

/**
 * Defines a Drupal Oauth validation service.
 */
public interface DrupalOAuthService {

    /**
     * Validates an OAuth2 token using a list of valid scopes. Also validates token
     * expiry and client id. Returns an DrupalOAuthData entity containing token
     * validity information as well as the basis for denial if token deemed invalid.
     * 
     * @param token         The token string.
     * @param validScopes   A list of valid scopes for this token.
     * @param validClientId A valid client_id for this token.
     * @return A DrupalOAuthData entity containing status info.
     */
    DrupalOAuthData validateTokenStatus(String token, List<String> validScopes, List<String> validClientIds);

    /**
     * Fetches the user data associated with the current token. Throws a
     * FinalForbiddenException if no user data is found or if there is a connection
     * error. Returns a DrupalUserInfo entity containing the user's uid, username,
     * and gh handle.
     * 
     * @param token The current request bearer token.
     * @return A DrupalUserInfo entity containing logged in user data.
     */
    DrupalUserInfo getTokenUserInfo(String token);
}