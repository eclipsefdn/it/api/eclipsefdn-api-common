/**
 * Copyright (c) 2021 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.efservices.services;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipsefoundation.efservices.api.models.WorkingGroup;

/**
 * Defines a service that will return available working group data, as well as provide some basic filters.
 * 
 * @author Martin Lowe
 */
public interface WorkingGroupService {

    /**
     * Returns all available working groups as a list.
     * 
     * @return list of all working group definitions from the working group API
     */
    public List<WorkingGroup> get();

    /**
     * Returns all available working groups as a list, filtered by the given statuses and parent organizations.
     * 
     * @param statuses The list of statuses used to filter the WGs
     * @param parentOrgs The list of parent organizations used to filter the WGs
     * @return list of all working group definitions from the working group API
     */
    public List<WorkingGroup> get(List<String> parentOrgs, List<String> statuses);

    /**
     * Retrieves a single working group by its alias if available.
     * 
     * @param name the alias of the working group to retrieve
     * @return the working group with the given alias if available in a nullable optional
     */
    public Optional<WorkingGroup> getByName(String name);

    /**
     * Retrieves a single working group by one of its WGPA ids if available.
     * 
     * @param docId the id of WGPA
     * @return the working group with the given WGPA if available in a nullable optional
     */
    public Optional<WorkingGroup> getByAgreementId(String docId);

    /**
     * Retrieves all WG Participation Agreements mapped to each WG alias.
     * 
     * @return A map conataining all WGPA ids, mapped by WG alias.
     */
    public Map<String, List<String>> getWGPADocumentIDs();
}
