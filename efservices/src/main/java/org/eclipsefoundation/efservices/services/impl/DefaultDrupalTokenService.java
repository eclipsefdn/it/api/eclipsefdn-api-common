/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.services.impl;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.eclipsefoundation.efservices.oauth.OAuthAPI;
import org.eclipsefoundation.efservices.services.DrupalTokenService;
import org.eclipsefoundation.http.config.OAuth2SecurityConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.oauth.OAuth20Service;

import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;

/**
 * Default implementation for requesting an OAuth request token. The reason that we can't used baked-in OIDC mechanisms is that Drupal uses
 * non-standard/non-compliant endpoints and flows that aren't easy to bind.
 */
@ApplicationScoped
public class DefaultDrupalTokenService implements DrupalTokenService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultDrupalTokenService.class);

    private static final long MINUTE_IN_MILLIS = 60000;

    private final OAuth2SecurityConfig config;

    // service reference (as we only need one)
    private OAuth20Service service;

    // token state vars
    private long expirationTime;
    private String accessToken;

    public DefaultDrupalTokenService(OAuth2SecurityConfig config) {
        this.config = config;
    }

    /**
     * Create an OAuth service reference.
     */
    @PostConstruct
    void createServiceRef() {
        this.service = new ServiceBuilder(config.tokenGeneration().clientId())
                .apiSecret(config.tokenGeneration().clientSecret())
                .scope(config.tokenGeneration().scope())
                .build(OAuthAPI.instance());
    }

    @Override
    public String getToken() {
        // lock on the class instance to stop multiple threads from requesting new
        // tokens at the same time
        synchronized (this) {
            if (accessToken == null || isExpiredOrExpiresSoon(expirationTime)) {
                // clear access token
                this.accessToken = null;
                try {
                    OAuth2AccessToken requestToken = service.getAccessTokenClientCredentialsGrant();
                    if (requestToken != null) {
                        this.accessToken = requestToken.getAccessToken();
                        this.expirationTime = System.currentTimeMillis()
                                + TimeUnit.SECONDS.toMillis(requestToken.getExpiresIn().longValue());
                    }
                } catch (IOException e) {
                    LOGGER.error("Issue communicating with OAuth server for authentication", e);
                } catch (InterruptedException e) {
                    LOGGER.error("Authentication communication was interrupted before completion", e);
                    Thread.currentThread().interrupt();
                } catch (ExecutionException e) {
                    LOGGER.error("Error while retrieving access token for request", e);
                }
            }
        }
        return accessToken;
    }

    private boolean isExpiredOrExpiresSoon(long time) {
        return System.currentTimeMillis() >= time || (time - System.currentTimeMillis() <= MINUTE_IN_MILLIS);
    }
}
