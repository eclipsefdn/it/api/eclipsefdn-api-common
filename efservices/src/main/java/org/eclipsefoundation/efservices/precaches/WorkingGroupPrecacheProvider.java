/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.precaches;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.caching.config.CacheKeyClassTagResolver;
import org.eclipsefoundation.caching.model.ParameterizedCacheKey;
import org.eclipsefoundation.caching.service.LoadingCacheManager.LoadingCacheProvider;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.efservices.api.WorkingGroupsAPI;
import org.eclipsefoundation.efservices.api.models.WorkingGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.micrometer.core.annotation.Timed;
import io.micrometer.core.aop.MeterTag;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Named;

@Named("working-groups")
@ApplicationScoped
public class WorkingGroupPrecacheProvider implements LoadingCacheProvider<WorkingGroup> {
    private static final Logger LOGGER = LoggerFactory.getLogger(WorkingGroupPrecacheProvider.class);

    private WorkingGroupsAPI api;
    private APIMiddleware middleware;

    public WorkingGroupPrecacheProvider(@RestClient WorkingGroupsAPI api, APIMiddleware middleware) {
        this.api = api;
        this.middleware = middleware;
    }

    @Override
    @Timed(value = METRICS_REGION_NAME)
    public List<WorkingGroup> fetchData(
            @MeterTag(resolver = CacheKeyClassTagResolver.class, key = METRICS_KEY_TAG_NAME) ParameterizedCacheKey k) {
        LOGGER.debug("LOADING WORKING GROUPS WITH KEY: {}", k);
        List<WorkingGroup> results = middleware
                .getAllAsync(params -> api.get(params, null))
                .collect()
                .asList()
                .subscribeAsCompletionStage()
                .join();
        LOGGER.debug("LOADED {} WORKING GROUPS", results.size());
        return results;
    }

    @Override
    public Class<WorkingGroup> getType() {
        return WorkingGroup.class;
    }
}
