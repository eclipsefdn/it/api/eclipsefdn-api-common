/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.precaches;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.caching.config.CacheKeyClassTagResolver;
import org.eclipsefoundation.caching.model.ParameterizedCacheKey;
import org.eclipsefoundation.caching.service.LoadingCacheManager.LoadingCacheProvider;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.efservices.api.ProjectsAPI;
import org.eclipsefoundation.efservices.api.models.InterestGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.micrometer.core.annotation.Timed;
import io.micrometer.core.aop.MeterTag;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Named;

@Named("interest-groups")
@ApplicationScoped
public class InterestGroupPrecacheProvider implements LoadingCacheProvider<InterestGroup> {
    private static final Logger LOGGER = LoggerFactory.getLogger(InterestGroupPrecacheProvider.class);

    private ProjectsAPI projectAPI;
    private APIMiddleware middleware;

    public InterestGroupPrecacheProvider(@RestClient ProjectsAPI projectAPI, APIMiddleware middleware) {
        this.projectAPI = projectAPI;
        this.middleware = middleware;
    }

    @Override
    @Timed(value = METRICS_REGION_NAME)
    public List<InterestGroup> fetchData(
            @MeterTag(resolver = CacheKeyClassTagResolver.class, key = METRICS_KEY_TAG_NAME) ParameterizedCacheKey k) {
        LOGGER.debug("LOADING ALL IGS");
        List<InterestGroup> results = middleware
                .getAllAsync(projectAPI::getInterestGroups)
                .collect()
                .asList()
                .subscribeAsCompletionStage()
                .join();
        LOGGER.debug("LOADED {} INTEREST GROUPS", results.size());
        return results;
    }

    @Override
    public Class<InterestGroup> getType() {
        return InterestGroup.class;
    }
}
