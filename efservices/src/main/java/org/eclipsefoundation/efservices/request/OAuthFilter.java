/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.request;

import org.eclipsefoundation.efservices.models.AuthenticatedRequestWrapper;
import org.eclipsefoundation.http.config.OAuth2SecurityConfig;
import org.eclipsefoundation.utils.exception.FinalForbiddenException;
import org.eclipsefoundation.utils.helper.TransformationHelper;
import org.jboss.resteasy.reactive.server.ServerRequestFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.UriInfo;

/**
 * This filter is used to validate the incoming Bearer tokens. Sets the current token user in the request context for use downstream.
 */
public class OAuthFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(OAuthFilter.class);

    private final OAuth2SecurityConfig config;
    private final AuthenticatedRequestWrapper wrappedToken;

    /**
     * Default constructor for oauth filter, accepts required configuration for checking if filter is enabled, as well as the current
     * requests wrapped token instance.
     * 
     * @param config oauth2 configuration for non-standard server binding
     * @param wrappedToken oauth token wrapper associated with the current request
     */
    public OAuthFilter(OAuth2SecurityConfig config, AuthenticatedRequestWrapper wrappedToken) {
        this.config = config;
        this.wrappedToken = wrappedToken;
    }

    @ServerRequestFilter
    public void filter(ContainerRequestContext requestContext, UriInfo uri, ResourceInfo info) {
        if (Boolean.TRUE.equals(config.filter().enabled())) {
            if (wrappedToken.isAuthenticated()) {
                try {
                    LOGGER.trace("User authenticated - {}", wrappedToken.getCurrentUser().name());
                } catch (FinalForbiddenException e) {
                    // current user will throw if this is a no-user client, which is a valid state
                    LOGGER.trace("Token authenticated, no user associated with token");
                }
            } else {
                LOGGER.trace("User not authenticated for current request to {}", TransformationHelper.formatLog(uri.getPath()));
            }
        }
    }
}
