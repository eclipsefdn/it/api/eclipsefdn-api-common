/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
/**
 * 
 */
package org.eclipsefoundation.efservices.alternate;

import java.util.Map;
import java.util.Optional;

import org.eclipsefoundation.efservices.test.SecondaryTestProfile;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Response.Status;

/**
 * 
 */
@QuarkusTest
@TestInstance(Lifecycle.PER_CLASS)
@TestProfile(SecondaryTestProfile.class)
public class OAuthFilterTest {
    public static final String STANDARD_AUTH_URL = "authenticated";
    public static final String PARTIAL_AUTH_URL = STANDARD_AUTH_URL + "/partial";

    public static final Optional<Map<String, Object>> VALID_USER_AUTH_HEADER = Optional
            .of(Map.of(HttpHeaders.AUTHORIZATION, "Bearer token5"));
    public static final Optional<Map<String, Object>> INVALID_ANON_AUTH_HEADER = Optional
            .of(Map.of(HttpHeaders.AUTHORIZATION, "Bearer othertoken"));

    public static final EndpointTestCase STANDARD_AUTH_WITH_USER_SUCCESS = TestCaseHelper
            .prepareTestCase(STANDARD_AUTH_URL, new String[] {}, null)
            .setHeaderParams(VALID_USER_AUTH_HEADER)
            .build();
    public static final EndpointTestCase PARTIAL_AUTH_USER_SUCCESS = TestCaseHelper
            .prepareTestCase(PARTIAL_AUTH_URL, new String[] {}, null)
            .setHeaderParams(VALID_USER_AUTH_HEADER)
            .build();

    @Test
    void oauthFilter_success() {
        EndpointTestBuilder.from(STANDARD_AUTH_WITH_USER_SUCCESS).run();
    }

    @Test
    void oauthFilter_failure_invalidAuth() {
        // Invalid token, we want this to fail
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(STANDARD_AUTH_URL, new String[] {}, null)
                        .setHeaderParams(INVALID_ANON_AUTH_HEADER)
                        .setStatusCode(403)
                        .build())
                .run();
    }

    @Test
    void oauthFilter_failure_noAuth() {
        // No auth token, we want this to fail
        EndpointTestBuilder.from(TestCaseHelper.prepareTestCase(STANDARD_AUTH_URL, new String[] {}, null).setStatusCode(403).build()).run();
    }

    @Test
    void oauthFilterPartialAuth_success() {
        EndpointTestBuilder.from(PARTIAL_AUTH_USER_SUCCESS).run();
    }

    @Test
    void oauthFilterPartialAuth_failure_invalidAuth() {
        // Invalid token, we want this to fail
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(PARTIAL_AUTH_URL, new String[] {}, null)
                        .setHeaderParams(INVALID_ANON_AUTH_HEADER)
                        .setStatusCode(Status.NO_CONTENT.getStatusCode())
                        .build())
                .run();
    }

    @Test
    void oauthFilterPartialAuth_failure_noAuth() {
        // No auth token, we want this to fail
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(PARTIAL_AUTH_URL, new String[] {}, null)
                        .setStatusCode(Status.NO_CONTENT.getStatusCode())
                        .build())
                .run();
    }
}
