/*********************************************************************
* Copyright (c) 2025 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.alternate;

import org.eclipsefoundation.efservices.oauth.OAuthAPI;
import org.eclipsefoundation.efservices.test.SecondaryTestProfile;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;

/**
 * 
 */
@QuarkusTest
@TestInstance(Lifecycle.PER_CLASS)
@TestProfile(SecondaryTestProfile.class)
class OAuthAPITest {

    @Test
    void getAccessTokenEndpoint_success_canBeOverridden() {
        Assertions
                .assertEquals(SecondaryTestProfile.ACCESS_TOKEN_URL, OAuthAPI.instance().getAccessTokenEndpoint(),
                        "Value for oauth instance wasn't properly overridden");
    }
}
