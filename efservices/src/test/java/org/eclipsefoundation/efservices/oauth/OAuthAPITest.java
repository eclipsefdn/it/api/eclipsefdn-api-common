/*********************************************************************
* Copyright (c) 2025 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.oauth;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

/**
 * 
 */
@QuarkusTest
class OAuthAPITest {

    @Test
    void getAccessTokenEndpoint_success_defaultValue() {
        Assertions
                .assertEquals("https://accounts.eclipse.org/oauth2/token", OAuthAPI.instance().getAccessTokenEndpoint(),
                        "Value for oauth instance wasn't properly overridden");
    }
}
