/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.helpers;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class DrupalAuthHelperTest {

    @Test
    void testExpiry_notExpired() {
        Assertions.assertFalse(DrupalAuthHelper.isExpired(Instant.now().getEpochSecond() + 20000));
        Assertions.assertFalse(DrupalAuthHelper.isExpired(Instant.now().getEpochSecond() + 3000));
        Assertions.assertFalse(DrupalAuthHelper.isExpired(Instant.now().getEpochSecond() + 100));
    }

    @Test
    void testExpiry_expired() {
        Assertions.assertTrue(DrupalAuthHelper.isExpired(Instant.now().getEpochSecond() - 20000));
        Assertions.assertTrue(DrupalAuthHelper.isExpired(Instant.now().getEpochSecond() - 3000));
        Assertions.assertTrue(DrupalAuthHelper.isExpired(Instant.now().getEpochSecond() - 100));
    }

    @Test
    void testScope_success() {
        List<String> validScopes = new ArrayList<>();
        validScopes.add("read");
        validScopes.add("write");
        validScopes.add("admin");

        Assertions.assertTrue(DrupalAuthHelper.hasScopes("read write admin", validScopes));

        validScopes = new ArrayList<>();
        validScopes.add("read");
        validScopes.add("write");

        Assertions.assertTrue(DrupalAuthHelper.hasScopes("read write", validScopes));

        validScopes = new ArrayList<>();
        validScopes.add("read");
        validScopes.add("admin");

        Assertions.assertTrue(DrupalAuthHelper.hasScopes("read admin", validScopes));

        validScopes = new ArrayList<>();
        validScopes.add("read");

        Assertions.assertTrue(DrupalAuthHelper.hasScopes("read", validScopes));
    }

    @Test
    void testScope_failure() {
        List<String> validScopes = new ArrayList<>();
        validScopes.add("read");
        validScopes.add("write");
        validScopes.add("admin");

        Assertions.assertFalse(DrupalAuthHelper.hasScopes("read write guy", validScopes));

        validScopes = new ArrayList<>();
        validScopes.add("read");
        validScopes.add("write");

        Assertions.assertFalse(DrupalAuthHelper.hasScopes("read", validScopes));

        validScopes = new ArrayList<>();
        validScopes.add("read");
        validScopes.add("admin");

        Assertions.assertFalse(DrupalAuthHelper.hasScopes("admin,read", validScopes));

        validScopes = new ArrayList<>();
        validScopes.add("read");

        Assertions.assertFalse(DrupalAuthHelper.hasScopes("admin", validScopes));
    }

    @Test
    void testClientId_success() {
        List<String> validIds = new ArrayList<>();
        validIds.add("client-id");
        validIds.add("test-id");
        validIds.add("valid-id");

        Assertions.assertTrue(DrupalAuthHelper.hasValidclientId("client-id", validIds));
        Assertions.assertTrue(DrupalAuthHelper.hasValidclientId("test-id", validIds));
        Assertions.assertTrue(DrupalAuthHelper.hasValidclientId("valid-id", validIds));
    }

    @Test
    void testClientId_failure() {
        List<String> validIds = new ArrayList<>();
        validIds.add("client-id");
        validIds.add("test-id");
        validIds.add("valid-id");

        Assertions.assertFalse(DrupalAuthHelper.hasValidclientId("invalid-id", validIds));
        Assertions.assertFalse(DrupalAuthHelper.hasValidclientId("not-id", validIds));
        Assertions.assertFalse(DrupalAuthHelper.hasValidclientId("no-valid", validIds));
    }
}