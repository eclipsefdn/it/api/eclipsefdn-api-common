/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.test.api;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.efservices.api.DrupalOAuthAPI;
import org.eclipsefoundation.efservices.api.models.DrupalOAuthData;
import org.eclipsefoundation.efservices.api.models.DrupalOAuthDataBuilder;
import org.eclipsefoundation.efservices.api.models.DrupalUserInfo;
import org.eclipsefoundation.efservices.helpers.DrupalAuthHelper;
import org.eclipsefoundation.utils.exception.FinalForbiddenException;

import io.quarkus.test.Mock;
import jakarta.enterprise.context.ApplicationScoped;

@Mock
@RestClient
@ApplicationScoped
public class MockDrupalOAuthAPI implements DrupalOAuthAPI {

    List<DrupalOAuthData> tokens;
    List<DrupalUserInfo> users;

    public MockDrupalOAuthAPI() {
        tokens = new ArrayList<>();
        tokens
                .addAll(Arrays
                        .asList(DrupalOAuthDataBuilder
                                .builder()
                                .accessToken("token1")
                                .clientId("client-id")
                                .expires(1674111182)
                                .scope("read write")
                                .build(),
                                DrupalOAuthDataBuilder
                                        .builder()
                                        .accessToken("token2")
                                        .clientId("test-id")
                                        .userId("42")
                                        .expires(Instant.now().getEpochSecond() + 20000)
                                        .scope("read write admin")
                                        .build(),
                                DrupalOAuthDataBuilder
                                        .builder()
                                        .accessToken("token3")
                                        .clientId("test-id")
                                        .expires(1234567890)
                                        .scope("read admin")
                                        .build(),
                                DrupalOAuthDataBuilder
                                        .builder()
                                        .accessToken("token4")
                                        .clientId("client-id")
                                        .expires(Instant.now().getEpochSecond() + 20000)
                                        .scope("read write")
                                        .build(),
                                DrupalOAuthDataBuilder
                                        .builder()
                                        .accessToken("token5")
                                        .clientId("test-id")
                                        .userId("333")
                                        .expires(Instant.now().getEpochSecond() + 20000)
                                        .scope("admin")
                                        .build(),
                                DrupalOAuthDataBuilder
                                        .builder()
                                        .accessToken("token6")
                                        .clientId("test-id")
                                        .expires(Instant.now().getEpochSecond() + 20000)
                                        .scope("read write admin")
                                        .build(),
                                DrupalOAuthDataBuilder
                                        .builder()
                                        .accessToken("token7")
                                        .clientId("test-id")
                                        .userId("444")
                                        .expires(Instant.now().getEpochSecond() + 20000)
                                        .scope("read write admin")
                                        .build()));

        users = new ArrayList<>();
        users.addAll(Arrays.asList(new DrupalUserInfo("42", "fakeuser", "fakeuser"), new DrupalUserInfo("333", "otheruser", "other")));
    }

    @Override
    public DrupalOAuthData getTokenInfo(String token) {
        return tokens.stream().filter(t -> t.accessToken().equalsIgnoreCase(token)).findFirst().orElse(null);
    }

    @Override
    public DrupalUserInfo getUserInfoFromToken(String token) {
        DrupalOAuthData tokenInfo = getTokenInfo(DrupalAuthHelper.stripBearerToken(token));
        if (tokenInfo == null || tokenInfo.userId() == null) {
            throw new FinalForbiddenException("The access token provided is invalid");
        }

        return users.stream().filter(u -> u.sub().equalsIgnoreCase(tokenInfo.userId())).findFirst().orElse(null);
    }
}