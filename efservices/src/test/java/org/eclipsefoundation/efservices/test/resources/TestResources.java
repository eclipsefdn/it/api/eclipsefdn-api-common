/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
/**
 * 
 */
package org.eclipsefoundation.efservices.test.resources;

import org.eclipsefoundation.efservices.models.AuthenticatedRequestWrapper;
import org.eclipsefoundation.http.annotations.AuthenticatedAlternate;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

/**
 * Test resource for alternate authentication
 */
@Path("authenticated")
public class TestResources {
    
    @Inject
    AuthenticatedRequestWrapper altAuthUser;

    @GET
    @AuthenticatedAlternate
    public Response getAuthenticated() {
        return Response.ok().build();
    }

    @GET
    @Path("partial")
    @AuthenticatedAlternate(allowPartialResponse = true)
    public Response getAuthenticatedOptional() {
        if (altAuthUser.isAuthenticated()) {
            return Response.ok().build();
        }
        // use slightly different response for validation
        return Response.status(Status.NO_CONTENT).build();
    }

}
