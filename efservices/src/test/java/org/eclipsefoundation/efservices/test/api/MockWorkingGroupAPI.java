/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.efservices.test.api;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.efservices.api.WorkingGroupsAPI;
import org.eclipsefoundation.efservices.api.models.WorkingGroup;
import org.eclipsefoundation.efservices.api.models.WorkingGroupBuilder;
import org.eclipsefoundation.efservices.api.models.WorkingGroupWorkingGroupParticipationAgreementBuilder;
import org.eclipsefoundation.efservices.api.models.WorkingGroupWorkingGroupParticipationAgreementsBuilder;
import org.eclipsefoundation.efservices.api.models.WorkingGroupWorkingGroupParticipationLevelBuilder;
import org.eclipsefoundation.efservices.api.models.WorkingGroupWorkingGroupResourcesBuilder;
import org.eclipsefoundation.testing.helpers.MockDataPaginationHandler;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.test.Mock;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;

/**
 * Mocked working groups API for basic testing.
 */
@Mock
@RestClient
@ApplicationScoped
public class MockWorkingGroupAPI implements WorkingGroupsAPI {

    private static final int INITIAL_SIZE = 150;
    private static final int CHANCE_BOUND = 100;
    private static final int CHANCE_FOR_ALT_STATUS = 10;

    public List<WorkingGroup> wgs;

    public MockWorkingGroupAPI() {
        // used to help generate random enough entries to avoid too many hard coded fallacies
        Random r = new Random();

        this.wgs = IntStream.range(0, INITIAL_SIZE).mapToObj(i -> {
            WorkingGroupBuilder wg = buildBasic("sample-" + Integer.toString(i));
            // do a random chance twice in a row to get a small chance of alternate statuses
            if (r.nextInt(CHANCE_BOUND) < CHANCE_FOR_ALT_STATUS) {
                wg = wg.status("archived");
            } else if (r.nextInt(CHANCE_BOUND) < CHANCE_FOR_ALT_STATUS) {
                wg = wg.status("incubating");
            }
            return wg.build();
        }).toList();
    }

    @Override
    public Uni<RestResponse<List<WorkingGroup>>> get(BaseAPIParameters baseParams, List<String> statuses) {
        return Uni
                .createFrom()
                .item(MockDataPaginationHandler
                        .paginateData(baseParams,
                                wgs.stream().filter(wg -> statuses == null || statuses.contains(wg.status())).toList()));
    }

    private WorkingGroupBuilder buildBasic(String alias) {
        return WorkingGroupBuilder
                .builder()
                .alias(alias)
                .description("")
                .levels(Arrays
                        .asList(WorkingGroupWorkingGroupParticipationLevelBuilder
                                .builder()
                                .description("sample")
                                .relation("WGSAMP")
                                .build()))
                .logo("")
                .parentOrganization("eclipse")
                .resources(WorkingGroupWorkingGroupResourcesBuilder
                        .builder()
                        .charter("")
                        .contactForm("")
                        .members("")
                        .sponsorship("")
                        .website("")
                        .participationAgreements(WorkingGroupWorkingGroupParticipationAgreementsBuilder
                                .builder()
                                .individual(WorkingGroupWorkingGroupParticipationAgreementBuilder
                                        .builder()
                                        .documentId(alias + "-iwgpa")
                                        .pdf(alias + "-iwgpa.pdf")
                                        .build())
                                .organization(WorkingGroupWorkingGroupParticipationAgreementBuilder
                                        .builder()
                                        .documentId(alias + "-owgpa")
                                        .pdf(alias + "-owgpa.pdf")
                                        .build())
                                .build())
                        .build())
                .status("active")
                .title("Sample WG");
    }
}
