/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import io.quarkus.test.junit.QuarkusTestProfile;

/**
 * Secondary config state for tests, used to ensure that certain values get properly overridden and read.
 */
public class SecondaryTestProfile implements QuarkusTestProfile {
    
    public static final String ACCESS_TOKEN_URL = "https://eclipse.org/test";

    // private immutable copy of the configs for auth state
    private static Map<String, String> configOverrides;

    @Override
    public Map<String, String> getConfigOverrides() {
        if (SecondaryTestProfile.configOverrides == null) {
            Map<String, String> tmp = new HashMap<>();
            tmp.put("eclipse.security.oauth2.token-generation.access-token-url", ACCESS_TOKEN_URL);
            tmp.put("eclipse.security.oauth2.filter.enabled", "true");
            tmp.put("eclipse.security.oauth2.filter.valid-scopes", "admin");
            tmp.put("eclipse.security.oauth2.filter.valid-client-ids", "test-id");
            SecondaryTestProfile.configOverrides = Collections.unmodifiableMap(tmp);
        }
        return configOverrides;
    }
}
