/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.test.services.impl;

import jakarta.enterprise.context.ApplicationScoped;

import org.eclipsefoundation.efservices.services.DrupalTokenService;

import io.quarkus.test.Mock;

@Mock
@ApplicationScoped
public class MockDrupalTokenService implements DrupalTokenService {

    @Override
    public String getToken() {
        return "token5";
    }
}
