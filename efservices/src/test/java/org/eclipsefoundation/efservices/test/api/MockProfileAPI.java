/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.efservices.api.ProfileAPI;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.EfUserBuilder;
import org.eclipsefoundation.efservices.api.models.EfUserCountryBuilder;
import org.eclipsefoundation.efservices.api.models.UserSearchParams;
import org.eclipsefoundation.efservices.helpers.DrupalAuthHelper;
import org.eclipsefoundation.efservices.services.DrupalOAuthService;

import io.quarkus.test.Mock;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.NotFoundException;

@Mock
@RestClient
@ApplicationScoped
public class MockProfileAPI implements ProfileAPI {

    private List<EfUser> users;

    @ConfigProperty(name = "eclipse.profile-api-test.scopes", defaultValue = "admin")
    List<String> validScopes;
    @ConfigProperty(name = "eclipse.profile-api-test.clients", defaultValue = "test-id")
    List<String> validClientIds;

    @Inject
    DrupalOAuthService oauthService;

    public MockProfileAPI() {
        this.users = new ArrayList<>();
        this.users
                .addAll(Arrays
                        .asList(EfUserBuilder
                                .builder()
                                .uid("666")
                                .name("firstlast")
                                .githubHandle("handle")
                                .mail("firstlast@test.com")
                                .picture("pic url")
                                .firstName("fake")
                                .lastName("user")
                                .fullName("fake user")
                                .publisherAgreements(new HashMap<>())
                                .twitterHandle("")
                                .org("null")
                                .jobTitle("employee")
                                .website("site url")
                                .mxid("")
                                .country(EfUserCountryBuilder.builder().code("CA").name("Canada").build())
                                .interests(Arrays.asList())
                                .build(),
                                EfUserBuilder
                                        .builder()
                                        .uid("42")
                                        .name("fakeuser")
                                        .picture("pic url")
                                        .firstName("fake")
                                        .lastName("user")
                                        .fullName("fake user")
                                        .mail("fakeuser@test.com")
                                        .publisherAgreements(new HashMap<>())
                                        .githubHandle("fakeuser")
                                        .twitterHandle("")
                                        .org("null")
                                        .jobTitle("employee")
                                        .website("site url")
                                        .mxid("")
                                        .country(EfUserCountryBuilder.builder().code("CA").name("Canada").build())
                                        .interests(Arrays.asList())
                                        .build(),
                                EfUserBuilder
                                        .builder()
                                        .uid("333")
                                        .name("name")
                                        .githubHandle("name")
                                        .mail("name@test.com")
                                        .picture("pic url")
                                        .firstName("fake")
                                        .lastName("user")
                                        .fullName("fake user")
                                        .publisherAgreements(new HashMap<>())
                                        .twitterHandle("")
                                        .org("null")
                                        .jobTitle("employee")
                                        .website("site url")
                                        .mxid("")
                                        .country(EfUserCountryBuilder.builder().code("CA").name("Canada").build())
                                        .interests(Arrays.asList())
                                        .build(),
                                EfUserBuilder
                                        .builder()
                                        .uid("11")
                                        .name("testtesterson")
                                        .githubHandle("mctesty")
                                        .mail("testtesterson@test.com")
                                        .picture("pic url")
                                        .firstName("fake")
                                        .lastName("user")
                                        .fullName("fake user")
                                        .publisherAgreements(new HashMap<>())
                                        .twitterHandle("")
                                        .org("null")
                                        .jobTitle("employee")
                                        .website("site url")
                                        .mxid("")
                                        .country(EfUserCountryBuilder.builder().code("CA").name("Canada").build())
                                        .interests(Arrays.asList())
                                        .build(),
                                EfUserBuilder
                                        .builder()
                                        .uid("444")
                                        .name("nodoc")
                                        .githubHandle("nodoc")
                                        .mail("nodoc@test.com")
                                        .picture("pic url")
                                        .firstName("fake")
                                        .lastName("user")
                                        .fullName("fake user")
                                        .publisherAgreements(new HashMap<>())
                                        .twitterHandle("")
                                        .org("null")
                                        .jobTitle("employee")
                                        .website("site url")
                                        .mxid("")
                                        .country(EfUserCountryBuilder.builder().code("CA").name("Canada").build())
                                        .interests(Collections.emptyList())
                                        .build()));
    }

    @Override
    public List<EfUser> getUsers(String token, UserSearchParams params) {
        // Ensure request is authenticated
        oauthService.validateTokenStatus(DrupalAuthHelper.stripBearerToken(token), validScopes, validClientIds);

        if (params.uid == null && StringUtils.isBlank(params.mail) && StringUtils.isBlank(params.name)) {
            return Collections.emptyList();
        }

        List<EfUser> results = Collections.emptyList();

        // Only filter via additional fields if it can't find with previous ones
        if (params.uid != null) {
            results = users.stream().filter(u -> u.uid().equalsIgnoreCase(params.uid)).toList();
        }
        if (StringUtils.isNotBlank(params.name) && results.isEmpty()) {
            results = users.stream().filter(u -> u.name().equalsIgnoreCase(params.name)).toList();
        }
        if (StringUtils.isNotBlank(params.mail) && results.isEmpty()) {
            results = users.stream().filter(u -> u.mail().equalsIgnoreCase(params.mail)).toList();
        }

        return results;
    }

    @Override
    public EfUser getUserByEfUsername(String token, String username) {
        Optional<EfUser> result = users.stream().filter(u -> u.name().equalsIgnoreCase(username)).findFirst();
        if (result.isEmpty()) {
            throw new NotFoundException(String.format("User '%s' not found", username));
        }

        return privateProfileFilter(token, result.get());
    }

    @Override
    public EfUser getUserByGithubHandle(String token, String handle) {
        Optional<EfUser> result = users.stream().filter(u -> u.githubHandle().equalsIgnoreCase(handle)).findFirst();
        if (result.isEmpty()) {
            throw new NotFoundException(String.format("User '%s' not found", handle));
        }

        return privateProfileFilter(token, result.get());
    }

    /*
     * Strips the public fields from the incoming EfUser entity if the request if not properly authenticated.
     */
    private EfUser privateProfileFilter(String token, EfUser user) {
        try {
            // Ensure request is authenticated
            oauthService.validateTokenStatus(DrupalAuthHelper.stripBearerToken(token), validScopes, validClientIds);
            return user;
        } catch (Exception e) {
            return EfUserBuilder.builder(user).mail("").country(EfUserCountryBuilder.builder().code(null).name(null).build()).build();
        }
    }
}
