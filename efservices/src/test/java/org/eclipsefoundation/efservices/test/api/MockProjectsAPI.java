/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.efservices.api.ProjectsAPI;
import org.eclipsefoundation.efservices.api.models.GitlabProjectBuilder;
import org.eclipsefoundation.efservices.api.models.InterestGroup;
import org.eclipsefoundation.efservices.api.models.InterestGroupBuilder;
import org.eclipsefoundation.efservices.api.models.InterestGroupDescriptorBuilder;
import org.eclipsefoundation.efservices.api.models.InterestGroupInterestGroupParticipantBuilder;
import org.eclipsefoundation.efservices.api.models.InterestGroupOrganizationBuilder;
import org.eclipsefoundation.efservices.api.models.InterestGroupResourceBuilder;
import org.eclipsefoundation.efservices.api.models.Project;
import org.eclipsefoundation.efservices.api.models.Project.ProjectParticipant;
import org.eclipsefoundation.efservices.api.models.Project.Repo;
import org.eclipsefoundation.efservices.api.models.ProjectBuilder;
import org.eclipsefoundation.efservices.api.models.ProjectGithubProjectBuilder;
import org.eclipsefoundation.efservices.api.models.ProjectProjectParticipantBuilder;
import org.eclipsefoundation.efservices.api.models.ProjectRepoBuilder;
import org.eclipsefoundation.testing.helpers.MockDataPaginationHandler;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.test.Mock;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;

@Mock
@RestClient
@ApplicationScoped
public class MockProjectsAPI implements ProjectsAPI {

    private List<Project> projects;
    private List<InterestGroup> igs;

    public MockProjectsAPI() {
        this.projects = new ArrayList<>();

        // sample repos
        Repo r1 = ProjectRepoBuilder.builder().url("http://www.github.com/eclipsefdn/sample").build();
        Repo r2 = ProjectRepoBuilder.builder().url("http://www.github.com/eclipsefdn/test").build();
        Repo r3 = ProjectRepoBuilder.builder().url("http://www.github.com/eclipsefdn/prototype.git").build();
        Repo r4 = ProjectRepoBuilder.builder().url("http://www.github.com/eclipsefdn/tck-proto").build();
        Repo r5 = ProjectRepoBuilder.builder().url("/gitroot/sample/gerrit.project.git").build();
        Repo r6 = ProjectRepoBuilder.builder().url("/gitroot/sample/gerrit.other-project").build();

        // sample users
        ProjectParticipant u1 = ProjectProjectParticipantBuilder.builder().url("").username("da_wizz").fullName("da_wizz").build();
        ProjectParticipant u2 = ProjectProjectParticipantBuilder.builder().url("").username("grunter").fullName("grunter").build();

        this.projects
                .add(ProjectBuilder
                        .builder()
                        .name("Sample project")
                        .projectId("sample.proj")
                        .shortProjectId("sample.proj")
                        .summary("summary")
                        .url("project.url.com")
                        .websiteUrl("someproject.com")
                        .websiteRepo(Collections.emptyList())
                        .logo("logoUrl.com")
                        .tags(Collections.emptyList())
                        .githubRepos(Arrays.asList(r1, r2))
                        .gitlabRepos(Collections.emptyList())
                        .gitlab(GitlabProjectBuilder.builder().ignoredSubGroups(Collections.emptyList()).projectGroup("").build())
                        .github(ProjectGithubProjectBuilder.builder().org("org name").ignoredRepos(Collections.emptyList()).build())
                        .gerritRepos(Arrays.asList(r5))
                        .contributors(Collections.emptyList())
                        .committers(Arrays.asList(u1, u2))
                        .projectLeads(Collections.emptyList())
                        .industryCollaborations(Collections.emptyList())
                        .specProjectWorkingGroup(Collections.emptyMap())
                        .releases(Collections.emptyList())
                        .topLevelProject("eclipse")
                        .build());

        this.projects
                .add(ProjectBuilder
                        .builder()
                        .name("Prototype thing")
                        .projectId("sample.proto")
                        .specProjectWorkingGroup(Collections.emptyMap())
                        .githubRepos(Arrays.asList(r3))
                        .gitlabRepos(Collections.emptyList())
                        .gerritRepos(Arrays.asList(r6))
                        .committers(Arrays.asList(u2))
                        .projectLeads(Collections.emptyList())
                        .gitlab(GitlabProjectBuilder
                                .builder()
                                .ignoredSubGroups(Collections.emptyList())
                                .projectGroup("eclipse/dash-second")
                                .build())
                        .shortProjectId("sample.proto")
                        .websiteUrl("someproject.com")
                        .summary("summary")
                        .url("project.url.com")
                        .websiteRepo(Collections.emptyList())
                        .logo("logoUrl.com")
                        .tags(Collections.emptyList())
                        .github(ProjectGithubProjectBuilder.builder().org("org name").ignoredRepos(Collections.emptyList()).build())
                        .contributors(Collections.emptyList())
                        .industryCollaborations(Collections.emptyList())
                        .releases(Collections.emptyList())
                        .topLevelProject("eclipse")
                        .build());

        this.projects
                .add(ProjectBuilder
                        .builder()
                        .name("Spec project")
                        .projectId("spec.proj")
                        .specProjectWorkingGroup(Map.of("id", "proj1", "name", "proj1"))
                        .githubRepos(Arrays.asList(r4))
                        .gitlabRepos(Collections.emptyList())
                        .gerritRepos(Collections.emptyList())
                        .gitlab(GitlabProjectBuilder
                                .builder()
                                .ignoredSubGroups(Arrays.asList("eclipse/dash/mirror"))
                                .projectGroup("eclipse/dash")
                                .build())
                        .committers(Arrays.asList(u1, u2))
                        .projectLeads(Collections.emptyList())
                        .shortProjectId("spec.proj")
                        .summary("summary")
                        .url("project.url.com")
                        .websiteUrl("someproject.com")
                        .websiteRepo(Collections.emptyList())
                        .logo("logoUrl.com")
                        .tags(Collections.emptyList())
                        .github(ProjectGithubProjectBuilder.builder().org("org name").ignoredRepos(Collections.emptyList()).build())
                        .contributors(Collections.emptyList())
                        .industryCollaborations(Collections.emptyList())
                        .releases(Collections.emptyList())
                        .topLevelProject("eclipse")
                        .build());

        igs = Arrays
                .asList(InterestGroupBuilder
                        .builder()
                        .projectId("foundation-internal.ig.mittens")
                        .id("1")
                        .logo("")
                        .state("active")
                        .title("Magical IG Tributed To Eclipse News Sources")
                        .description(InterestGroupDescriptorBuilder.builder().full("Sample").summary("Sample").build())
                        .scope(InterestGroupDescriptorBuilder.builder().full("Sample").summary("Sample").build())
                        .gitlab(GitlabProjectBuilder
                                .builder()
                                .ignoredSubGroups(Collections.emptyList())
                                .projectGroup("eclipse-ig/mittens")
                                .build())
                        .leads(Arrays
                                .asList(InterestGroupInterestGroupParticipantBuilder
                                        .builder()
                                        .url("https://api.eclipse.org/account/profile/zacharysabourin")
                                        .username("zacharysabourin")
                                        .fullName("zachary sabourin")
                                        .organization(InterestGroupOrganizationBuilder
                                                .builder()
                                                .documents(Collections.emptyMap())
                                                .id("id")
                                                .name("org")
                                                .build())
                                        .build()))
                        .participants(Arrays
                                .asList(InterestGroupInterestGroupParticipantBuilder
                                        .builder()
                                        .url("https://api.eclipse.org/account/profile/skilpatrick")
                                        .username("skilpatrick")
                                        .fullName("Skil Patrick")
                                        .organization(InterestGroupOrganizationBuilder
                                                .builder()
                                                .documents(Collections.emptyMap())
                                                .id("id")
                                                .name("org")
                                                .build())
                                        .build()))
                        .shortProjectId("mittens")
                        .resources(InterestGroupResourceBuilder.builder().members("members").website("google.com").build())
                        .mailingList("mailinglist.com")
                        .build());
    }

    @Override
    public Uni<RestResponse<List<Project>>> getProjects(BaseAPIParameters params, int isSpecProject) {
        List<Project> out = projects;
        if (isSpecProject == 1) {
            out = out.stream().filter(p -> p.getSpecWorkingGroup().isPresent()).toList();
        }

        return Uni.createFrom().item(MockDataPaginationHandler.paginateData(params, out));
    }

    @Override
    public Uni<RestResponse<List<InterestGroup>>> getInterestGroups(BaseAPIParameters params) {
        return Uni.createFrom().item(MockDataPaginationHandler.paginateData(params, igs));
    }
}
