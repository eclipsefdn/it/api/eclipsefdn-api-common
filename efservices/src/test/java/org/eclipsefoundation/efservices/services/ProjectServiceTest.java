/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.services;

import java.util.List;

import jakarta.inject.Inject;

import org.eclipsefoundation.efservices.api.models.InterestGroup;
import org.eclipsefoundation.efservices.api.models.Project;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class ProjectServiceTest {

    @Inject
    ProjectService projectService;

    @Test
    void testGetProjects_success() {
        List<Project> results = projectService.getAllProjects();
        Assertions.assertTrue(!results.isEmpty(), "The expected projects were not found");
        Assertions.assertEquals(3, results.size(), "3 Projects were expected");
    }

    @Test
    void testGetSpecProjects_success() {
        List<Project> results = projectService.getAllSpecProjects();
        Assertions.assertTrue(!results.isEmpty(), "The expected projects were not found");
        Assertions.assertEquals(1, results.size(), "1 spec Project was expected");
    }

    @Test
    void testGetInterestGroups_success() {
        List<InterestGroup> results = projectService.getAllInterestGroups();
        Assertions.assertTrue(!results.isEmpty(), "The expected Interest groups were not found");
        Assertions.assertEquals(1, results.size(), "1 interest group was expected");
    }
}
