/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.efservices.services;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import jakarta.inject.Inject;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.efservices.api.models.WorkingGroup;
import org.eclipsefoundation.efservices.test.api.MockWorkingGroupAPI;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

/**
 * 
 */
@QuarkusTest
class WorkingGroupServiceTest {

    @Inject
    WorkingGroupService svc;
    @RestClient
    MockWorkingGroupAPI mockApi;

    @Test
    void get_success() {
        List<WorkingGroup> wgs = svc.get();
        Assertions.assertNotNull(wgs, "Working group return should always have a return object");
        Assertions.assertEquals(mockApi.wgs.size(), wgs.size(), "get() call should contain all entries from API");
    }

    @Test
    void getByName_success() {
        Optional<WorkingGroup> result = svc.getByName("sample-1");
        Assertions.assertNotNull(result, "Working group name call should have returned a nonnull optional");
        Assertions.assertTrue(result.isPresent(), "The passed name should have returned a result");
    }

    @Test
    void getByName_success_caseInsensitive() {
        Optional<WorkingGroup> result = svc.getByName("SAMPLE-1");
        Assertions.assertNotNull(result, "Working group name call should have returned a nonnull optional");
        Assertions.assertTrue(result.isPresent(), "The passed name should have returned a result");
    }

    @Test
    void getByName_failure_notPresent() {
        Optional<WorkingGroup> result = svc.getByName("not-present");
        Assertions.assertNotNull(result, "Working group name call should have returned a nonnull optional");
        Assertions.assertFalse(result.isPresent(), "The passed name should have returned no result");
    }

    @Test
    void get_success_filterStatus() {
        List<String> archivedIncubatingStatus = Arrays.asList("archived", "incubating");
        List<WorkingGroup> results = svc.get(null, archivedIncubatingStatus);
        Assertions.assertNotNull(results, "Working group return should always have a return object");
        results
                .forEach(wg -> Assertions
                        .assertTrue(archivedIncubatingStatus.contains(wg.status()),
                                String.format("Found unexpected status: %s", wg.status())));

        // Perform same test with different status list
        List<String> archived = Arrays.asList("archived");
        results = svc.get(null, archived);
        Assertions.assertNotNull(results, "Working group return should always have a return object");
        results
                .forEach(wg -> Assertions
                        .assertTrue(archived.contains(wg.status()), String.format("Found unexpected status: %s", wg.status())));

        // Perform same test with invalid status list
        List<String> invalidStatus = Arrays.asList("invalid", "nope");
        results = svc.get(null, invalidStatus);
        Assertions.assertNotNull(results, "Working group return should always have a return object");
        Assertions.assertEquals(0, results.size(), "there should be no results using these statuses");

        // Perform same test with no statuses
        results = svc.get(null, null);
        Assertions.assertNotNull(results, "Working group return should always have a return object");
        Assertions.assertEquals(mockApi.wgs.size(), results.size(), "get(null, null) call should contain all entries from API");
    }

    @Test
    void get_success_filterParentOrg() {
        List<String> parentOrgs = Arrays.asList("eclipse");
        List<WorkingGroup> results = svc.get(parentOrgs, null);
        Assertions.assertNotNull(results, "Working group return should always have a return object");
        results
                .forEach(wg -> Assertions
                        .assertTrue(parentOrgs.contains(wg.parentOrganization()),
                                String.format("Found unexpected parent org: %s", wg.parentOrganization())));

        // Perform same test with invalid parent org list
        List<String> invalidOrg = Arrays.asList("invalid", "nope");
        results = svc.get(invalidOrg, null);
        Assertions.assertNotNull(results, "Working group return should always have a return object");
        Assertions.assertEquals(0, results.size(), "There should be no results using these parent orgs");
    }

    @Test
    void getWGPAS_success() {
        Map<String, List<String>> wgpas = svc.getWGPADocumentIDs();
        Assertions.assertNotNull(wgpas, "Working group name call should have returned a nonnull map");
        Assertions.assertEquals(mockApi.wgs.size(), wgpas.size(), "getWGPADocumentIDs() call should contain same number of entries as API");
    }

    @Test
    void getByWgpaId_success() {
        Optional<WorkingGroup> result = svc.getByAgreementId("sample-1-iwgpa");
        Assertions.assertNotNull(result, "Working group doc id call should have returned a nonnull optional");
        Assertions.assertTrue(result.isPresent(), "The passed doc id should have returned a result");

        result = svc.getByAgreementId("sample-1-owgpa");
        Assertions.assertNotNull(result, "Working group doc id call should have returned a nonnull optional");
        Assertions.assertTrue(result.isPresent(), "The passed doc id should have returned a result");
    }
}
