/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.services;

import java.util.ArrayList;
import java.util.List;

import org.eclipsefoundation.efservices.api.models.DrupalOAuthData;
import org.eclipsefoundation.utils.exception.FinalForbiddenException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;

@QuarkusTest
class DrupalOAuthServiceTest {

    @Inject
    DrupalOAuthService oauthService;

    @Test
    void testToken_success() {
        List<String> validScopes = new ArrayList<>();
        validScopes.add("read");
        validScopes.add("write");
        validScopes.add("admin");

        List<String> validIds = new ArrayList<>();
        validIds.add("client-id");
        validIds.add("test-id");
        validIds.add("valid-id");

        DrupalOAuthData tokenData = oauthService.validateTokenStatus("token2", validScopes, validIds);
        Assertions.assertEquals("token2", tokenData.accessToken());
        Assertions.assertEquals("test-id", tokenData.clientId());

        validScopes = new ArrayList<>();
        validScopes.add("read");
        validScopes.add("write");

        tokenData = oauthService.validateTokenStatus("token4", validScopes, validIds);
        Assertions.assertEquals("token4", tokenData.accessToken());
        Assertions.assertEquals("client-id", tokenData.clientId());
    }

    @Test
    void testToken_failure_expired() {
        List<String> validScopes = new ArrayList<>();
        validScopes.add("read");
        validScopes.add("write");

        List<String> validIds = new ArrayList<>();
        validIds.add("client-id");
        validIds.add("test-id");
        validIds.add("valid-id");

        Assertions.assertThrows(FinalForbiddenException.class,
                () -> oauthService.validateTokenStatus("token3", validScopes, validIds));

        Assertions.assertThrows(FinalForbiddenException.class,
                () -> oauthService.validateTokenStatus("token1", validScopes, validIds));
    }

    @Test
    void testToken_failure_invalidScope() {
        List<String> validScopes = new ArrayList<>();
        validScopes.add("admin");

        List<String> validIds = new ArrayList<>();
        validIds.add("client-id");
        validIds.add("test-id");
        validIds.add("valid-id");

        Assertions.assertThrows(FinalForbiddenException.class,
                () -> oauthService.validateTokenStatus("token1", validScopes, validIds));

        Assertions.assertThrows(FinalForbiddenException.class,
                () -> oauthService.validateTokenStatus("token4", validScopes, validIds));
    }

    @Test
    void testToken_failure_invalidClientId() {
        List<String> scopes1 = new ArrayList<>();
        scopes1.add("read");
        scopes1.add("write");
        scopes1.add("admin");

        List<String> validIds = new ArrayList<>();
        validIds.add("valid-id");

        Assertions.assertThrows(FinalForbiddenException.class,
                () -> oauthService.validateTokenStatus("token2", scopes1, validIds));

        List<String> scopes2 = new ArrayList<>();
        scopes2.add("read");
        scopes2.add("write");

        Assertions.assertThrows(FinalForbiddenException.class,
                () -> oauthService.validateTokenStatus("token4", scopes2, validIds));
    }
}