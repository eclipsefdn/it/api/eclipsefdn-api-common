/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.efservices.services;

import java.util.Optional;

import jakarta.inject.Inject;

import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.UserSearchParams;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class ProfileServiceTest {
    private static final String NOT_FOUND_MSG_FORMAT = "User %s should exist";
    private static final String SEARCH_NOT_FOUND_MSG_FORMAT = "User with params %s should exist";

    @Inject
    ProfileService profileService;

    @Test
    void testGetByUsername_success() {
        boolean containsPrivate = true;
        String username = "firstlast";
        Optional<EfUser> result = profileService.fetchUserByUsername(username, containsPrivate);
        Assertions.assertTrue(result.isPresent(), String.format(NOT_FOUND_MSG_FORMAT, username));
        Assertions.assertEquals(username, result.get().name());
        Assertions.assertEquals("firstlast@test.com", result.get().mail());

        // Same call with no auth should also work
        containsPrivate = false;
        result = profileService.fetchUserByUsername(username, containsPrivate);
        Assertions.assertTrue(result.isPresent(), String.format(NOT_FOUND_MSG_FORMAT, username));
        Assertions.assertEquals(username, result.get().name());
        Assertions.assertEquals("", result.get().mail());
    }

    @Test
    void testGetByUsername_failure_notFound() {
        boolean containsPrivate = true;
        String username = "wrongname";
        Optional<EfUser> result = profileService.fetchUserByUsername(username, containsPrivate);
        Assertions.assertTrue(result.isEmpty());

        // Same call with no auth should also be empty
        containsPrivate = false;
        result = profileService.fetchUserByUsername(username, containsPrivate);
        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    void testGetByHandle_success() {
        boolean containsPrivate = true;
        String handle = "mctesty";
        Optional<EfUser> result = profileService.fetchUserByGhHandle(handle, containsPrivate);
        Assertions.assertTrue(result.isPresent(), String.format(NOT_FOUND_MSG_FORMAT, handle));
        Assertions.assertEquals(handle, result.get().githubHandle());
        Assertions.assertEquals("testtesterson@test.com", result.get().mail());

        // Same call with no auth should also work
        containsPrivate = false;
        result = profileService.fetchUserByGhHandle(handle, containsPrivate);
        Assertions.assertTrue(result.isPresent(), String.format(NOT_FOUND_MSG_FORMAT, handle));
        Assertions.assertEquals(handle, result.get().githubHandle());
        Assertions.assertEquals("", result.get().mail());
    }

    @Test
    void testGetByHandle_failure_notFound() {
        boolean containsPrivate = true;
        String handle = "badhandle";
        Optional<EfUser> result = profileService.fetchUserByGhHandle(handle, containsPrivate);
        Assertions.assertTrue(result.isEmpty());

        // Same call with no auth should also be empty
        containsPrivate = false;
        result = profileService.fetchUserByGhHandle(handle, containsPrivate);
        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    void testSearchUser_success() {
        // Valid uid
        UserSearchParams params = new UserSearchParams("666", null, null);
        Optional<EfUser> result = profileService.performUserSearch(params);
        Assertions.assertTrue(result.isPresent(), String.format(SEARCH_NOT_FOUND_MSG_FORMAT, params.toString()));
        Assertions.assertEquals("666", result.get().uid());

        // Invalid uid. Valid name
        params = new UserSearchParams("12", "firstlast", null);
        result = profileService.performUserSearch(params);
        Assertions.assertTrue(result.isPresent(), String.format(SEARCH_NOT_FOUND_MSG_FORMAT, params.toString()));
        Assertions.assertEquals("666", result.get().uid());
        Assertions.assertEquals("firstlast", result.get().name());

        // Invalid uid and name. Valid email
        params = new UserSearchParams("12", "wrongname", "firstlast@test.com");
        result = profileService.performUserSearch(params);
        Assertions.assertTrue(result.isPresent(), String.format(SEARCH_NOT_FOUND_MSG_FORMAT, params.toString()));
        Assertions.assertEquals("666", result.get().uid());
        Assertions.assertEquals("firstlast", result.get().name());
        Assertions.assertEquals("firstlast@test.com", result.get().mail());
    }

    @Test
    void testSearchUser_failure_notfound() {
        // Invalid uid
        UserSearchParams params = new UserSearchParams("12", null, null);
        Optional<EfUser> result = profileService.performUserSearch(params);
        Assertions.assertTrue(result.isEmpty());

        // Invalid uid and name
        params = new UserSearchParams("12", "wrongname", null);
        result = profileService.performUserSearch(params);
        Assertions.assertTrue(result.isEmpty());

        // Invalid uid, name, and email
        params = new UserSearchParams("12", "wrongname", "wrongemail@test.co");
        result = profileService.performUserSearch(params);
        Assertions.assertTrue(result.isEmpty());
    }
}
