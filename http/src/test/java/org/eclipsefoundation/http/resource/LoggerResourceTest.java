/*******************************************************************************
 * Copyright (c) 2024 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipsefoundation.http.resource;

import static io.restassured.RestAssured.given;

import java.util.UUID;
import java.util.stream.Stream;

import org.eclipsefoundation.http.helper.SecureResourceKey;
import org.eclipsefoundation.http.model.LoggerWrapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response.Status;

@QuarkusTest
class LoggerResourceTest {

    private static final String VALID_LOG_LEVEL = "TRACE";
    private static final String VALID_CLASS_NAME = "org.eclipsefoundation.http.helper.SecureResourceKey";

    @Inject
    SecureResourceKey key;

    @Test
    void updateLogger_protected() {
        given().when().get("/loggers").then().statusCode(Status.FORBIDDEN.getStatusCode());
    }

    @Test
    void updateLogger_usesInstanceKey() {
        given().when().get("/loggers?key={key}", UUID.randomUUID().toString()).then()
                .statusCode(Status.FORBIDDEN.getStatusCode());
        given()
                .when()
                .get("/loggers?key={key}&clazz={clazz}&level={level}", key.getKey(), VALID_CLASS_NAME, VALID_LOG_LEVEL)
                .then()
                .statusCode(Status.OK.getStatusCode());
    }

    @Test
    void updateLogger_error_failsMissingLevelOrClass() {
        given()
                .when()
                .get("/loggers?key={key}&level={level}", key.getKey(), VALID_LOG_LEVEL)
                .then()
                .statusCode(Status.BAD_REQUEST.getStatusCode());
        given()
                .when()
                .get("/loggers?key={key}&clazz={clazz}", key.getKey(), VALID_CLASS_NAME)
                .then()
                .statusCode(Status.BAD_REQUEST.getStatusCode());
    }

    @Test
    void updateLogger_error_failsInvalidClass() {
        given()
                .when()
                .get("/loggers?key={key}&clazz={clazz}&level={level}", key.getKey(),
                        "org.eclipsefoundation.core.resource.NotARealClass",
                        VALID_LOG_LEVEL)
                .then()
                .statusCode(Status.BAD_REQUEST.getStatusCode());
    }

    @Test
    void updateLogger_error_failsInvalidLogLevel() {
        given()
                .when()
                .get("/loggers?key={key}&clazz={clazz}&level={level}", key.getKey(), VALID_CLASS_NAME, "TOASTER_OVEN")
                .then()
                .statusCode(Status.BAD_REQUEST.getStatusCode());
    }

    @Test
    void getCurrentLoggers_usesInstanceKey() {
        given().when().get("/loggers/all?key={key}", UUID.randomUUID().toString()).then()
                .statusCode(Status.FORBIDDEN.getStatusCode());
        given().when().get("/loggers/all?key={key}", key.getKey()).then().statusCode(Status.OK.getStatusCode());
    }

    @Test
    void getCurrentLoggers_containsEfAndBaseAppLoggers() {
        // do the call to the loggers endpoint, and convert the response to a list of
        // returned loggers
        LoggerWrapper[] loggers = given()
                .when()
                .get("/loggers/all?key={key}", key.getKey())
                .then()
                .extract()
                .as(LoggerWrapper[].class);
        // do 2 assertions to check we have both application and base app loggers
        // present
        Assertions.assertTrue(
                Stream.of(loggers).anyMatch(loggerName -> loggerName.name().startsWith("org.eclipsefoundation")));
        Assertions.assertTrue(
                Stream.of(loggers).anyMatch(loggerName -> !loggerName.name().startsWith("org.eclipsefoundation")));
    }

    @Test
    void getApplicationLoggers_usesInstanceKey() {
        given()
                .when()
                .get("/loggers/application?key={key}", UUID.randomUUID().toString())
                .then()
                .statusCode(Status.FORBIDDEN.getStatusCode());
        given().when().get("/loggers/application?key={key}", key.getKey()).then().statusCode(Status.OK.getStatusCode());
    }

    @Test
    void getApplicationLoggers_onlyContainsEclipseNamespace() {
        // do the call to the loggers endpoint, and convert the response to a list of
        // returned loggers
        LoggerWrapper[] loggers = given()
                .when()
                .get("/loggers/application?key={key}", key.getKey())
                .then()
                .extract()
                .as(LoggerWrapper[].class);
        // do assertion to check we have only application loggers present
        Assertions.assertTrue(
                Stream.of(loggers).anyMatch(loggerName -> loggerName.name().startsWith("org.eclipsefoundation")));
    }
}
