/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.request;

import static io.restassured.RestAssured.given;

import org.eclipsefoundation.http.request.CacheBypassFilter.BypassCondition;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.enterprise.context.Dependent;
import jakarta.ws.rs.container.ContainerRequestContext;

/**
 * Basic tests to ensure that the bypass condition filters can activate as intended on requests.
 */
@QuarkusTest
class CacheBypassFilterTest {

    public static final String BYPASS_HEADER_NAME = "Use-Test-Bypass";

    @Test
    void filter_success_noBypass() {
        given().when().get("/test/unguarded").then().header("Cache-Control", Matchers.blankOrNullString());
    }

    @Test
    void filter_success_bypassConditionMet() {
        given()
                .when()
                .header(BYPASS_HEADER_NAME, "true")
                .get("/test/unguarded")
                .then()
                .header("Cache-Control", Matchers.equalTo("no-store"));
    }

    @Dependent
    public static class TestBypassCondition implements BypassCondition {
        @Override
        public boolean matches(ContainerRequestContext requestContext) {
            return "true".equalsIgnoreCase(requestContext.getHeaderString("Use-Test-Bypass"));
        }
    }
}
