/*******************************************************************************
 * Copyright (c) 2024 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipsefoundation.http.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

@QuarkusTest
class ErrorTest {

    @Test
    void constructor_acceptsHttpStatus() {
        WebError e = new WebError(Status.OK, "Is nice");
        Assertions
                .assertEquals(Status.OK.getStatusCode(), e.getStatusCode(),
                        "Expected error class status code to be same as passed HTTP status enum.");
    }

    @Test
    void constructor_acceptsRawHttpStatusCode() {
        int statusCode = 500;
        WebError e = new WebError(statusCode, "Is bad");
        Assertions
                .assertEquals(statusCode, e.getStatusCode(),
                        "Expected error class status code to be same as passed raw status code.");

    }

    @Test
    void asResponse_success() {
        WebError e = new WebError(Status.INTERNAL_SERVER_ERROR, "Is bad");
        Response actual = e.asResponse();
        Assertions.assertEquals(e, actual.getEntity(), "Expected entity of response to be error object");
        Assertions.assertEquals(e.getStatusCode(), actual.getStatus(), "Expected status code of response to be same as error");
    }
}
