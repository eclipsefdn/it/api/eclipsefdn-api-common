/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
/**
 * 
 */
package org.eclipsefoundation.http.annotations;

import static io.restassured.RestAssured.given;

import org.eclipsefoundation.http.namespace.CacheControlCommonValues;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.ws.rs.core.Response.Status;

/**
 * 
 */
@QuarkusTest
class CachingAnnotationTest {

    @Test
    void test_safe() {
        given()
                .when()
                .get("/test/safe-caching")
                .then()
                .statusCode(Status.OK.getStatusCode())
                .header("Cache-Control", "max-age=" + CacheControlCommonValues.SAFE_CACHE_MAX_AGE);
    }

    @Test
    void test_noCache() {
        given().when().get("/test/no-cache").then().statusCode(Status.OK.getStatusCode()).header("Cache-Control", "no-store");
    }
}
