/**
 * Copyright (c) 2023, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.http.response;

import static io.restassured.RestAssured.given;

import java.util.HashMap;
import java.util.Map;

import org.eclipsefoundation.http.test.resources.TestResource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.response.ValidatableResponse;

/**
 * Basic tests to make sure that pagination headers are properly passed to the response object from the cache.
 * 
 * @author Martin Lowe
 *
 */
@QuarkusTest
class PaginatedResultsFilterTest {

    @Test
    void filter_success_noAvailableHeaders() {
        // endpoint has no stored pagination header sources
        ValidatableResponse vr = given().get("test/no-pagination").then();
        Assertions.assertNull(vr.extract().header(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER));
        Assertions.assertNull(vr.extract().header(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER));
        Assertions.assertNotNull(vr.extract().header("Link"));
    }

    @Test
    void filter_success_endpointWithPaginationHeaders() {
        Map<String, String> expectedHeaders = new HashMap<>();
        expectedHeaders.put(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER, TestResource.PAGINATION_HEADER_RESULT_SIZE);
        expectedHeaders.put(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER, TestResource.PAGINATION_HEADER_PAGE_SIZE_DEFAULT);
        // endpoint has stored pagination header sources
        ValidatableResponse vr = given().get("test/pagination").then();
        vr.headers(expectedHeaders);
        Assertions.assertNotNull(vr.extract().header("Link"));
    }
}
