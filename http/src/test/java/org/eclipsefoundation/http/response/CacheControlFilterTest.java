/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.response;

import static io.restassured.RestAssured.given;

import org.eclipsefoundation.http.test.CacheControlEnabledTestProfile;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import io.restassured.response.ValidatableResponse;
import jakarta.ws.rs.core.HttpHeaders;

@QuarkusTest
@TestProfile(CacheControlEnabledTestProfile.class)
class CacheControlFilterTest {

    @Test
    void headersAdded() {
        // This URL is listed in the config paths
        ValidatableResponse response = given().get("test/unguarded").then();
        String cacheControlHeader = response.extract().header(HttpHeaders.CACHE_CONTROL);
        String expiresHeader = response.extract().header(HttpHeaders.EXPIRES);
        String pragmaHeader = response.extract().header("Pragma");

        Assertions.assertNotNull(cacheControlHeader, "The Cache-Control header should be present");
        Assertions.assertEquals("no-cache, no-store, must-revalidate", cacheControlHeader, "Cache-control header is invalid");
        Assertions.assertNotNull(expiresHeader, "The Expires header should be present");
        Assertions.assertEquals("0", expiresHeader, "Expires header is invalid");
        Assertions.assertNotNull(pragmaHeader, "The Pragma header should be present");
        Assertions.assertEquals("no-cache", pragmaHeader, "Pragma header is invalid");
    }

    @Test
    void headersNotAdded() {
        // This URL is not listed in the config paths
        ValidatableResponse response = given().post("test").then();
        String cacheControlHeader = response.extract().header(HttpHeaders.CACHE_CONTROL);
        String expiresHeader = response.extract().header(HttpHeaders.EXPIRES);
        String pragmaHeader = response.extract().header("Pragma");

        Assertions.assertNull(cacheControlHeader, "The Cache-Control header should not be present");
        Assertions.assertNull(expiresHeader, "The Expires header should not be present");
        Assertions.assertNull(pragmaHeader, "The Pragma header should not be present");
    }
}
