/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.response;

import static io.restassured.RestAssured.given;

import java.util.Optional;

import org.eclipsefoundation.http.namespace.ResponseHeaderNames;
import org.eclipsefoundation.http.test.HostnameEnabledTestProfile;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import io.restassured.response.ValidatableResponse;

@QuarkusTest
@TestProfile(HostnameEnabledTestProfile.class)
class HostnameResponseFilterTest {

    @Test
    void filter_success() {
        // Get system name for comparison
        String hostname = Optional.ofNullable(HostnameResponseFilter.getInternalNodeName()).orElse(System.getenv("HOSTNAME"));

        // Send request and extract the node name header
        ValidatableResponse response = given().get("test").then();
        String nodeNameHeader = response.extract().header(ResponseHeaderNames.X_NODE_NAME);

        Assertions.assertNotNull(nodeNameHeader, "The hostname should be present");
        Assertions.assertEquals(hostname, nodeNameHeader, "The node name header and hostname should be the same");
    }
}
