/**
 * Copyright (c) 2023, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.http.test.resources;

import java.util.Collections;
import java.util.Optional;

import org.eclipsefoundation.http.annotations.Csrf;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.http.namespace.CacheControlCommonValues;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.http.response.PaginatedResultsFilter;
import org.jboss.resteasy.reactive.Cache;
import org.jboss.resteasy.reactive.RestResponse;

import jakarta.inject.Inject;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;

/**
 * Test endpoint for different mechanisms
 * 
 * @author Martin Lowe
 *
 */
@Path("test")
public class TestResource {
    public static final String PAGINATION_HEADER_RESULT_SIZE = "30";
    public static final String PAGINATION_HEADER_PAGE_SIZE_DEFAULT = "10";

    @Inject
    RequestWrapper wrap;

    /**
     * Basic sample GET call that is not gated through CSRF. This could represent a user data endpoint, or a straight CSRF endpoint to
     * trigger the header to be returned if enabled.
     * 
     * @return empty ok response
     */
    @GET
    @Path("unguarded")
    public Response getUnguarded() {
        return Response.ok().build();
    }

    /**
     * Basic POST call that can be used to assist in validating filters.
     * 
     * @return empty ok response if CSRF is disabled or properly passed, Status.FORBIDDEN.getStatusCode() response otherwise.
     */
    @POST
    public Response postGuarded() {
        return Response.ok().build();
    }

    /**
     * Basic POST call that can be used to assist in validating filters. CSRF is disabled.
     * 
     * @return empty ok response
     */
    @POST
    @Path("unguarded")
    @Csrf(enabled = false)
    public Response postUnguarded() {
        return Response.ok().build();
    }

    /**
     * Basic PUT call that can be used to assist in validating filters.
     * 
     * @return empty ok response if CSRF is disabled or properly passed, Status.FORBIDDEN.getStatusCode() response otherwise.
     */
    @PUT
    public Response put() {
        return Response.ok().build();
    }

    /**
     * Basic DELETE call that can be used to assist in validating filters.
     * 
     * @return empty ok response if CSRF is disabled or properly passed, Status.FORBIDDEN.getStatusCode() response otherwise.
     */
    @DELETE
    public Response delete() {
        return Response.ok().build();
    }

    @GET
    @Path("pagination")
    public Response basicPaginationTest(@QueryParam(DefaultUrlParameterNames.PAGESIZE_PARAMETER_NAME) String pageSize) {
        return Response
                .ok(Collections.emptyList())
                .header(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER, PAGINATION_HEADER_RESULT_SIZE)
                .header(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER,
                        Optional.ofNullable(pageSize).orElse(PAGINATION_HEADER_PAGE_SIZE_DEFAULT))
                .build();
    }

    @GET
    @Path("no-pagination")
    public Response noPaginationData() {
        return Response.ok(Collections.emptyList()).build();
    }

    @GET
    @Path("safe-caching")
    @Cache(maxAge=CacheControlCommonValues.SAFE_CACHE_MAX_AGE)
    public RestResponse<String> safeCachingLevel() {
        return RestResponse.ok("Safe!");
    }
    @GET
    @Path("no-cache")
    @Cache(noStore = true)
    public RestResponse<String> noCache() {
        return RestResponse.ok("NADDA!");
    }

}
