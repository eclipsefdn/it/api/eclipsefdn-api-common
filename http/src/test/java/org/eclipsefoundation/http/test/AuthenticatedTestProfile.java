/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipsefoundation.http.namespace.RequestPropertyNames;

import io.quarkus.test.junit.QuarkusTestProfile;

/**
 * Used to enable authentication profile in testing for tests that use this profile. Note that tests that use this
 * profile should be grouped in a single package to ensure back to back runs to not increase test run time. More
 * available on https://quarkus.io/blog/quarkus-test-profiles/.
 * 
 * @author Martin Lowe
 */
public class AuthenticatedTestProfile implements QuarkusTestProfile {

    // private immutable copy of the configs for auth state
    private static final Map<String, String> CONFIG_OVERRIDES;
    static {
        Map<String, String> tmp = new HashMap<>();
        tmp.put("quarkus.oauth2.enabled", "true");
        tmp.put(RequestPropertyNames.CSRF_ENABLED, "true");
        tmp.put(RequestPropertyNames.CSRF_TOKEN_SALT, "sample-salt-value-64^%$6DG54$DG46%Eas6egf54s%1#g5");
        CONFIG_OVERRIDES = Collections.unmodifiableMap(tmp);
    }

    @Override
    public Map<String, String> getConfigOverrides() {
        return CONFIG_OVERRIDES;
    }
}
