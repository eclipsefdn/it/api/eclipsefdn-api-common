/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.exception;

import java.util.Collections;
import java.util.Map;

/**
 * A custom exception used to indicate a conflict with the resource accessed by
 * the client. Allows setting of header values upon creation.
 */
public class ConflictException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private final Map<String, String> headers;

    public ConflictException(String message) {
        this(message, Collections.emptyMap());
    }

    public ConflictException(String message, Map<String, String> headers) {
        super(message);
        this.headers = headers;
    }

    public Map<String, String> getHeaders() {
        return this.headers;
    }
}
