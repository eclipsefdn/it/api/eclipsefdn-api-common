/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.exception;

import jakarta.ws.rs.core.Response.Status;

/**
 * A custom exception used as a wrapper to handle basic runtime exceptions.
 */
public class ApplicationException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private final Integer statusCode;

    /**
     * Instantiate an ApplicationException with a message. HttpStatus.SC_INTERNAL_SERVER_ERROR Status is assumed.
     * 
     * @param message the message to display
     */
    public ApplicationException(String message) {
        this(message, Status.INTERNAL_SERVER_ERROR.getStatusCode());
    }

    /**
     * Instantiate an ApplicationException with a message and http response status code.
     * 
     * @param message the message to display
     * @param statusCode The status code to return to the client
     */
    public ApplicationException(String message, Integer statusCode) {
        super(message);
        this.statusCode = statusCode;
    }

    /**
     * Instantiate an ApplicationException with a message and a cause. Status HttpStatus.SC_INTERNAL_SERVER_ERROR is
     * assumed.
     * 
     * @param message the message to display
     * @param cause The cause of the error
     */
    public ApplicationException(String message, Throwable cause) {
        this(message, cause, Status.INTERNAL_SERVER_ERROR.getStatusCode());
    }

    /**
     * Instantiate an ApplicationException with a message, http response code, and a cause;
     * 
     * @param message the message to display
     * @param cause The cause of the error
     * @param statusCode The status code to return to the client
     */
    public ApplicationException(String message, Throwable cause, Integer statusCode) {
        super(message, cause);
        this.statusCode = statusCode;
    }

    /**
     * Retrieve the current status code.
     * 
     * @return The exception status code.
     */
    public Integer getStatusCode() {
        return this.statusCode;
    }
}
