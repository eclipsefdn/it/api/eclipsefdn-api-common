/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.exception;

/**
 * A Custom exception used to indicate an unauthorized request.
 * Supports being thrown as a standalone exception as well as a wrapper
 * for previously bubbled up exceptions.
 * 
 * Used in place of jakarta.rs.NotAuthorizedException as it does not properly
 * log the cause of the exception when used with an ExceptionMapper.
 */
public class UnauthorizedException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public UnauthorizedException(String message) {
        super(message);
    }

    public UnauthorizedException(String message, Throwable cause) {
        super(message, cause);
    }
}
