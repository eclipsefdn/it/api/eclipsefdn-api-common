/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.request;

import java.io.IOException;
import java.lang.reflect.Method;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.http.annotations.KeySecured;
import org.eclipsefoundation.http.helper.SecureResourceKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.runtime.configuration.ConfigUtils;
import jakarta.annotation.Priority;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.Provider;

/**
 * Checks if the current resource is protected by the shared {@link SecureResourceKey} and evaluates the key to make
 * sure it matches.
 * 
 * @author Martin Lowe
 */
@Provider
@Priority(50)
public class SecuredResourceFilter implements ContainerRequestFilter {
    public static final Logger LOGGER = LoggerFactory.getLogger(SecuredResourceFilter.class);

    @ConfigProperty(name = "eclipse.core.secure-key.override-key", defaultValue = "")
    Instance<String> bypassKey;

    @Inject
    SecureResourceKey key;

    @Context
    ResourceInfo info;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        // ensure the key exists by blindly fetching it (generated on first fetch)
        key.getKey();
        // check annotation on target endpoint to be sure that endpoint is enabled
        Method m = info.getResourceMethod();
        KeySecured opt = m.getAnnotation(KeySecured.class);
        if (opt != null) {
            // retrieve the passed key from the parameters and abort request if it doesn't match
            String passedKey = requestContext.getUriInfo().getQueryParameters().getFirst("key");
            if (StringUtils.isBlank(passedKey)) {
                if (LOGGER.isTraceEnabled()) {
                    LOGGER
                            .trace("Request to '{}' aborted as the passed secure key was blank but was required",
                                    requestContext.getUriInfo().getAbsolutePath());
                }
                requestContext.abortWith(Response.status(Status.FORBIDDEN).build());
            } else if (!checkCurrentKey(passedKey)) {
                if (LOGGER.isTraceEnabled()) {
                    LOGGER
                            .trace("Request to '{}' aborted as the passed secure key was incorrect",
                                    requestContext.getUriInfo().getAbsolutePath());
                }
                requestContext.abortWith(Response.status(Status.FORBIDDEN).build());
            }
        }
    }

    /**
     * Checks whether the passed key matches the secure key, or if in dev if the bypass key is set and matches.
     * 
     * @return true if the passed key matches a valid key, false otherwise.
     */
    private boolean checkCurrentKey(String passedKey) {
        return passedKey.equals(key.getKey()) || (ConfigUtils.getProfiles().contains("dev") && passedKey.equals(bypassKey.get()));
    }
}
