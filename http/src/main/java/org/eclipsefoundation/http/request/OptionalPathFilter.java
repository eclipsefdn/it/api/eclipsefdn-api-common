package org.eclipsefoundation.http.request;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Optional;

import org.eclipse.microprofile.config.ConfigProvider;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.http.annotations.OptionalPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.annotation.Priority;
import jakarta.enterprise.inject.Instance;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.Provider;

/**
 * Does lookups on the current path to see if the resource is disabled.
 * 
 * @author Martin Lowe
 *
 */
@Provider
@Priority(25)
public class OptionalPathFilter implements ContainerRequestFilter {
    public static final Logger LOGGER = LoggerFactory.getLogger(OptionalPathFilter.class);

    @ConfigProperty(name = "eclipse.optional-resources.enabled", defaultValue = "false")
    Instance<Boolean> optionalResourcesEnabled;

    @Context
    ResourceInfo info;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        // check annotation on target endpoint to be sure that endpoint is enabled
        Method m = info.getResourceMethod();
        OptionalPath opt = m.getAnnotation(OptionalPath.class);
        if (opt != null) {
            if (Boolean.FALSE.equals(optionalResourcesEnabled.get())) {
                LOGGER.trace("Request to '{}' rejected as optional resources are not enabled",
                        requestContext.getUriInfo().getAbsolutePath());
                // abort with 404 as we should hide that this exists
                requestContext.abortWith(Response.status(Status.NOT_FOUND.getStatusCode()).build());
            }
            // get the specified config value fresh and check that it is enabled, or enabled by default if missing
            Optional<Boolean> configValue = ConfigProvider.getConfig().getOptionalValue(opt.value(), Boolean.class);
            if (configValue.isPresent() && Boolean.TRUE.equals(configValue.get())) {
                LOGGER.trace("Request to '{}' enabled by config, allowing call",
                        requestContext.getUriInfo().getAbsolutePath());
            } else if (configValue.isEmpty() && opt.enabledByDefault()) {
                LOGGER.trace("Request to '{}' enabled by default, allowing call",
                        requestContext.getUriInfo().getAbsolutePath());
            } else {
                LOGGER.trace("Request to '{}' rejected as endpoint is not enabled",
                        requestContext.getUriInfo().getAbsolutePath());
                // abort with 404 as we should hide that this exists
                requestContext.abortWith(Response.status(Status.NOT_FOUND.getStatusCode()).build());
            }
        }

    }
}
