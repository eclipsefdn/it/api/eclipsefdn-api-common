/*********************************************************************
* Copyright (c) 2019, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.request;

import java.io.IOException;

import org.jboss.resteasy.reactive.server.ServerRequestFilter;

import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.core.Response;

/**
 * Checks passed parameters and if any match one of the criteria for bypassing
 * caching, an attribute will be set to the request to skip cache requests and
 * instead directly return results.
 * 
 * @author Martin Lowe
 *
 */
public class CacheBypassFilter {
	public static final String ATTRIBUTE_NAME = "bypass-cache";

	@Inject
	Instance<BypassCondition> conditions;

	@ServerRequestFilter
	public Response filter(ContainerRequestContext requestContext, HttpServerRequest request, HttpServerResponse response) throws IOException {
		// check for random sort order, which always bypasses cache
		for (BypassCondition cond : conditions) {
			if (cond.matches(requestContext)) {
				setBypass(requestContext, response);
				return null;
			}
		}
		requestContext.setProperty(ATTRIBUTE_NAME, Boolean.FALSE);
		return null;
	}

	private void setBypass(ContainerRequestContext requestContext, HttpServerResponse response) {
		requestContext.setProperty(ATTRIBUTE_NAME, Boolean.TRUE);
		// no-store should be used as cache bypass should not return
		response.putHeader("Cache-Control", "no-store");
	}

	/**
	 * Interface for adding a bypass condition to requests made against a given
	 * server.
	 * 
	 * @author Martin Lowe
	 *
	 */
	public interface BypassCondition {
		/**
		 * Tests the request context to check whether any data fetched for this request
		 * should bypass the cache layer.
		 * 
		 * @param requestContext the current requests container context
		 * @return true if the request should bypass the cache, false otherwise.
		 */
		boolean matches(ContainerRequestContext requestContext);
	}
}
