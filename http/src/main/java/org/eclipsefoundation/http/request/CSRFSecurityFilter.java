/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*           Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.request;

import java.lang.reflect.Method;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.http.annotations.Csrf;
import org.eclipsefoundation.http.namespace.RequestHeaderNames;
import org.eclipsefoundation.utils.config.CSRFSecurityConfig;
import org.eclipsefoundation.utils.exception.FinalForbiddenException;
import org.eclipsefoundation.utils.helper.CSRFHelper;
import org.eclipsefoundation.utils.model.AdditionalUserData;
import org.jboss.resteasy.reactive.server.ServerRequestFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.smallrye.mutiny.Uni;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerRequest;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.SecurityContext;

/**
 * Creates a security layer in front of mutation requests to require CSRF tokens (if enabled). This layer does not perform the check of the
 * token in-case there are other conditions that would rebuff the request.
 * 
 * @author Martin Lowe
 */
public class CSRFSecurityFilter {
    public static final Logger LOGGER = LoggerFactory.getLogger(CSRFSecurityFilter.class);

    private final CSRFSecurityConfig config;
    private final CSRFHelper csrf;
    private final AdditionalUserData aud;

    public CSRFSecurityFilter(CSRFHelper csrf, AdditionalUserData aud, CSRFSecurityConfig config) {
        this.config = config;
        this.csrf = csrf;
        this.aud = aud;
    }

    @ServerRequestFilter(priority = 4999)
    public Uni<Void> filter(ContainerRequestContext requestContext, ResourceInfo info, HttpServerRequest request) {
        if (config.enabled()) {
            // Reflect method and get annotation
            Method m = info.getResourceMethod();
            Csrf annotation = m.getAnnotation(Csrf.class);

            if ((annotation != null && annotation.enabled()) || (annotation == null && isMutationAction(requestContext.getMethod()))) {
                validateCsrfToken(requestContext.getHeaderString(RequestHeaderNames.CSRF_TOKEN), request,
                        requestContext.getSecurityContext());
            }
        }

        return Uni.createFrom().nullItem();
    }

    /**
     * Check if the HTTP method indicates a mutation action such as DELETE, POST, or PUT.
     * 
     * @param method The given HTTP method
     * @return True if DELETE, POST, or PUT
     */
    private boolean isMutationAction(String method) {
        return HttpMethod.DELETE.name().equals(method) || HttpMethod.POST.name().equals(method) || HttpMethod.PUT.name().equals(method);
    }

    /**
     * Validates the incoming token by checking if it exists and is valid
     * 
     * @param token The request CSRF token
     */
    private void validateCsrfToken(String token, HttpServerRequest request, SecurityContext context) {
        // Validate presence
        if (StringUtils.isBlank(token)) {
            throw new FinalForbiddenException("No CSRF token passed for mutation call, refusing connection");
        } else if (config.distributedMode().enabled()) {
            // distributed mode should return the stored token if it exists
            csrf.compareCSRF(csrf.getNewCSRFToken(request, context), token);
        } else {
            // run comparison. If error, exception will be thrown
            csrf.compareCSRF(aud.getCsrf(), token);
        }
    }
}
