package org.eclipsefoundation.http.request;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import jakarta.ws.rs.client.ClientRequestContext;
import jakarta.ws.rs.client.ClientRequestFilter;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.ext.Provider;

/**
 * Adds 'Accept-Encoding: gzip' header to all outbound RestClient requests. Can be enabled/disabled via configuration.
 */
@Provider
public class GzipClientRequestFilter implements ClientRequestFilter {

    private final Boolean isEnabled;

    public GzipClientRequestFilter(@ConfigProperty(name = "eclipse.rest-client.gzip-enabled", defaultValue = "true") Boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    @Override
    public void filter(ClientRequestContext requestContext) {
        if (Boolean.TRUE.equals(isEnabled)) {
            requestContext.getHeaders().add(HttpHeaders.ACCEPT_ENCODING, "gzip");
        }
    }
}