/*********************************************************************
* Copyright (c) 2019, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.namespace;

/**
 * Namespace for Eclipse-specific header names used in application.
 * 
 * @author Martin Lowe
 */
public class RequestHeaderNames {

	public static final String ACCESS_TOKEN = "Eclipse-Access-Token";
	public static final String ACCESS_VERSION = "Access-Version";
    public static final String CSRF_TOKEN = "x-csrf-token";
	
	private RequestHeaderNames() {}
}
