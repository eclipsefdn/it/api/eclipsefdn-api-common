/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*           Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.namespace;

/**
 * Contains Microprofile property names used by this application.
 * 
 * @author Martin Lowe
 *
 */
public class RequestPropertyNames {
    public static final String MULTI_SOURCE_SINGLE_TENANT_ENABLED = "eclipse.persistence.deployment.msst.enabled";
    public static final String MULTI_SOURCE_SINGLE_TENANT_DATASOURCE = "eclipse.persistence.deployment.msst.datasource";
    public static final String PERSISTENCE_PAGINATION_LIMIT = "eclipse.persistence.pagination-limit";
    public static final String PERSISTENCE_PAGINATION_LIMIT_MAX = "eclipse.persistence.pagination-limit.max";

    public static final String CSRF_ENABLED = "eclipse.security.csrf.enabled";
    public static final String CSRF_DISTRIBUTED_ENABLED = "eclipse.security.csrf.distributed-mode.enabled";
    public static final String CSRF_DISTRIBUTED_DEFAULT= "eclipse.security.csrf.distributed-mode.is-default-provider";
    public static final String CSRF_TOKEN_SALT = "eclipse.security.csrf.token.salt";

    public static final String DEFAULT_PAGE_SIZE = "eclipse.pagination.filter.default-page-size";

    public static final String OPTIONAL_RESOURCES_ENABLED = "eclipse.optional-resources.enabled";

    public static final String CACHE_TTL_MAX_SECONDS = "eclipse.cache.ttl-max-seconds";
    public static final String CACHE_RESOURCE_ENABLED = "eclipse.cache.resource.enabled";

    private RequestPropertyNames() {
    }
}
