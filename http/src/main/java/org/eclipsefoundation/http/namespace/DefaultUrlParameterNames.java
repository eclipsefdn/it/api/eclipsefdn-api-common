/*********************************************************************
* Copyright (c) 2019, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.namespace;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipsefoundation.utils.namespace.UrlParameterNamespace;

import jakarta.inject.Singleton;

/**
 * Namespace containing common URL parameters used throughout the API.
 * 
 * @author Martin Lowe
 */
@Singleton
public final class DefaultUrlParameterNames implements UrlParameterNamespace {
    public static final String ID_PARAMETER_NAME = "id";
    public static final String IDS_PARAMETER_NAME = "ids";
    public static final String PAGE_PARAMETER_NAME = "page";
    public static final String PAGESIZE_PARAMETER_NAME = "pagesize";
    public static final String QUERY_PARAMETER_NAME = "q";
    public static final UrlParameter QUERY_STRING = new UrlParameter(QUERY_PARAMETER_NAME);
    public static final UrlParameter PAGE = new UrlParameter(PAGE_PARAMETER_NAME);
    public static final UrlParameter PAGESIZE = new UrlParameter(PAGESIZE_PARAMETER_NAME);
    public static final UrlParameter IDS = new UrlParameter(IDS_PARAMETER_NAME);
    public static final UrlParameter ID = new UrlParameter(ID_PARAMETER_NAME);

    private static final List<UrlParameter> params = Collections
            .unmodifiableList(Arrays.asList(QUERY_STRING, PAGE, IDS, ID, PAGESIZE));

    @Override
    public List<UrlParameter> getParameters() {
        return new ArrayList<>(params);
    }

}
