/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
/**
 * 
 */
package org.eclipsefoundation.http.namespace;

/**
 * Common values used for Cache-Control headers in ehte Eclipse Foundation space. As annotations cannot be decomposed, this is the best we
 * can do for standardized values for caching.
 * 
 * <p>
 * For "safe" caching level, use the following:
 * <p>
 * &commat;Cache(maxAge=CacheControlCommonValues.SAFE_CACHE_MAX_AGE)
 * 
 * <p>
 * For "aggressive" caching level, use the following:
 * <p>
 * &commat;Cache(maxAge=CacheControlCommonValues.AGGRESSIVE_CACHE_MAX_AGE)
 * 
 * <p>
 * For no HTTP level caching, use the following:
 * <p>
 * &commat;Cache(noStore=true)
 * 
 */
public final class CacheControlCommonValues {

    public static final int SAFE_CACHE_MAX_AGE = 300;
    public static final int AGGRESSIVE_CACHE_MAX_AGE = 600;

    private CacheControlCommonValues() {
    }
}
