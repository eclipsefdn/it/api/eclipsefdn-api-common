/*******************************************************************************
 * Copyright (c) 2024 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipsefoundation.http.template;

import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.json.JsonObjectBuilder;
import jakarta.ws.rs.core.MultivaluedMap;

import io.quarkus.qute.TemplateExtension;

@TemplateExtension
public class MapToJSONExtension {
    public static JsonObject toJson(MultivaluedMap<String, String> params) {
        JsonObjectBuilder out = Json.createObjectBuilder();
        params.forEach((k, v) -> out.add(k, Json.createArrayBuilder(v)));
        return out.build();
    }

    private MapToJSONExtension() {
        // private constructor for Qute template helper
    }
}
