/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.config;

import org.eclipsefoundation.utils.model.AdditionalUserData;

import io.quarkus.arc.DefaultBean;
import io.quarkus.arc.Unremovable;
import jakarta.enterprise.context.Dependent;
import jakarta.enterprise.context.RequestScoped;
import jakarta.enterprise.inject.Produces;

/**
 * Allows for the implementation of other mechanisms to provide CSRF tokens while enabling a default mechanism which uses random values at
 * runtime to create sufficiently hardened values.
 * 
 * @author Martin Lowe
 *
 */
@Dependent
@Unremovable
public class AdditionalUserDataProvider {

    @Produces
    @DefaultBean
    @RequestScoped
    public AdditionalUserData generator() {
        return new AdditionalUserData();
    }
}
