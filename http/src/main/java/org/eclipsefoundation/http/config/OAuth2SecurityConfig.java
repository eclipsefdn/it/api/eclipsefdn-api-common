/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.config;

import java.util.List;
import java.util.Optional;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;

/**
 * Configuration mapping for all OAuth2 related values. Includes token
 * generation, oauth2 filtering, and role augmentation.
 */
@ConfigMapping(prefix = "eclipse.security.oauth2")
public interface OAuth2SecurityConfig {

    /**
     * @return OAuth2 token generation configs.
     */
    OAuth2TokenGenerationDefinition tokenGeneration();

    /**
     * @return OAuth2 filter configs.
     */
    OAuth2FilterDefinition filter();

    /**
     * Config mapping for all OAuth2 token generation values. These will be managed
     * in the consumer project's 'secrets.properties' file.
     */
    interface OAuth2TokenGenerationDefinition {

        /**
         * @return Client id used to generate OAuth2 tokens.
         */
        @WithDefault("sample")
        String clientId();

        /**
         * @return Client secret used to generate OAuth2 tokens.
         */
        @WithDefault("sample")
        String clientSecret();

        /**
         * @return The comma-separated scopes for the requested token
         */
        @WithDefault("sample")
        String scope();
        
        /**
         * @return the URL for the access token endpoint
         */
        @WithDefault("https://accounts.eclipse.org/oauth2/token")
        String accessTokenUrl();
    }

    /**
     * Config mapping for all OAuth2 filter values. These will be managed
     * in the consumer project's 'secrets.properties' file.
     */
    interface OAuth2FilterDefinition {

        /**
         * @return A flag to enable/disable the OAuth2 filter.
         */
        @WithDefault("false")
        boolean enabled();

        /**
         * @return A flag to enable/disable the OAuth2 filter for all endpoints, regardless of annotations
         */
        @WithDefault("false")
        AlwaysOnSettings alwaysOn();

        /**
         * @return The list of valid scopes allowed by client requests. Incoming tokens
         *         must contain all of these scopes to be considered valid.
         */
        Optional<List<String>> validScopes();

        /**
         * @return The list of valid client-ids to match against the incoming OAuth2
         *         bearer token. Incoming client id must match with one of these to be
         *         considered valid.
         */
        @WithDefault("sample")
        List<String> validClientIds();
    }

    /**
     * Config mapping for all OAuth2 role augmentation values. These will be managed
     * in the consumer project's 'secrets.properties' file.
     */
    interface OAuth2OverrideDefinition {

        /**
         * @return A flag to enable/disable the OAuth2 role augmentation.
         */
        @WithDefault("false")
        boolean enabled();

        /**
         * @return The role with which to augment a user.
         */
        @WithDefault("marketplace_admin_access")
        String role();
    }
    
    interface AlwaysOnSettings {

        /**
         * @return A flag to enable/disable the OAuth2 filter for all endpoints, regardless of annotations
         */
        @WithDefault("false")
        boolean enabled();
        
        @WithDefault("false")
        boolean allowPartialResponse();
        
    }
}
