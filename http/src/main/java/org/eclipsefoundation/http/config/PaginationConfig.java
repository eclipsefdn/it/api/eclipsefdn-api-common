/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.config;

import java.time.Duration;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;

@ConfigMapping(prefix = "eclipse.pagination")
public interface PaginationConfig {

    PaginationFilterDefinition filter();

    PaginationResolverDefinition headerResolver();

    interface PaginationFilterDefinition {

        @WithDefault("true")
        boolean enabled();

        @WithDefault("1000")
        int defaultPageSize();

        PaginationSchemeDefinition scheme();
    }

    interface PaginationResolverDefinition {

        @WithDefault("1S")
        Duration timeout();
    }

    interface PaginationSchemeDefinition {

        @WithDefault("true")
        boolean enforce();

        @WithDefault("https")
        String value();
    }
}
