/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.helper;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;

@RequestScoped
public class IPParser {
    private static final Logger LOGGER = LoggerFactory.getLogger(IPParser.class);

    private static final String X_REAL_IP = "X-Real-Ip";
    private static final String X_FORWARDED_FOR = "X-Forwarded-For";
    private static final String LOCALHOST_IP = "0.0.0.0";

    @Inject
    RequestWrapper wrap;

    /**
     * Retrieves the best IP for the current request, using the X-Real-Ip and X-Forwarded-For headers to pull the information from the
     * current request. Defaults to localhost(0.0.0.0) if none can be found.
     * 
     * @return the best matching IP for current request, or localhost if none can be found
     */
    public String getBestMatchingIP() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Looking up best IP address");
        }

        // Use real IP, followed by forwarded for to get the requests IP
        String requestIP = wrap.getHeader(X_REAL_IP);
        if (StringUtils.isBlank(requestIP)) {
            String[] forwardedIps = StringUtils.split(wrap.getHeader(X_FORWARDED_FOR), ",\\s*");
            if (forwardedIps != null && forwardedIps.length > 1) {
                requestIP = forwardedIps[1];
            }
        }

        // Default to localhost if we can't find request information
        if (StringUtils.isBlank(requestIP)) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Cannot parse IP from headers. Defaulting to localhost(0.0.0.0)");
            }
            requestIP = LOCALHOST_IP;
        }
        return requestIP;
    }
}
