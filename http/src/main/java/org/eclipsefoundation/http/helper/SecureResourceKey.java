/**
 * Copyright (c) 2023, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.http.helper;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;

/**
 * Contains the random key generated on system start. Creates a random secure key that will be used to gate access to
 * secure resource endpoints.
 * 
 * @author Martin Lowe
 */
@ApplicationScoped
public class SecureResourceKey {
    private static final Logger LOGGER = LoggerFactory.getLogger(SecureResourceKey.class);

    private String key;

    /**
     * Returns the secure key used for the current applications lifespan.
     * 
     * @return current secure key
     */
    public String getKey() {
        // generate post-start on first usage to make sure that this key isn't embedded in native applications
        if (this.key == null) {
            this.key = UUID.randomUUID().toString();
            LOGGER.info("Generated secure resource key: {}", key);
        }
        return this.key;
    }
}
