/*********************************************************************
* Copyright (c) 2019, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.resource.mapper;

import org.eclipsefoundation.utils.exception.FinalForbiddenException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

/**
 * Exception mapper to allow Status.FORBIDDEN.getStatusCode() to be thrown past auth barrier. Typical unauthorized exceptions
 * cause redirects through OIDC layers which isn't always wanted
 *
 * @author Martin Lowe
 */
@Provider
public class FinalForbiddenMapper implements ExceptionMapper<FinalForbiddenException> {
  private static final Logger LOGGER = LoggerFactory.getLogger(FinalForbiddenMapper.class);

  @Override
  public Response toResponse(FinalForbiddenException exception) {
    LOGGER.error(exception.getMessage(), exception);
    // return an empty response with a server error response
    return Response.status(Status.FORBIDDEN.getStatusCode()).build();
  }
}
