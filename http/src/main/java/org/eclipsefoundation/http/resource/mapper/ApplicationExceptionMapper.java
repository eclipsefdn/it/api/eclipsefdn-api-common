/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.resource.mapper;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import org.eclipsefoundation.http.exception.ApplicationException;
import org.eclipsefoundation.http.model.WebError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Mapper for the {@link ApplicationException}. Converts the exception into a
 * custom {@link WebError} class and sends it as a response with the ApplicationException status code.
 */
@Provider
public class ApplicationExceptionMapper implements ExceptionMapper<ApplicationException> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationExceptionMapper.class);

    public Response toResponse(ApplicationException exception) {
        LOGGER.error(exception.getMessage(), exception);
        return new WebError(exception.getStatusCode(),
                "Internal server error while processing request: " + exception.getMessage())
                .asResponse();
    }
}
