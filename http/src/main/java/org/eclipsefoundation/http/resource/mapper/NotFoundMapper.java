/*********************************************************************
* Copyright (c) 2019, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.resource.mapper;

import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import org.eclipsefoundation.http.model.WebError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Creates human legible error responses in the case of not found exceptions. By
 * catching the NotFoundException, we handle exceptions caused by bad path
 * casting.
 * 
 * @author Martin Lowe
 */
@Provider
public class NotFoundMapper implements ExceptionMapper<NotFoundException> {
	private static final Logger LOGGER = LoggerFactory.getLogger(NotFoundMapper.class);

	@Override
	public Response toResponse(NotFoundException exception) {
		LOGGER.error(exception.getMessage(), exception);
		return new WebError(Status.NOT_FOUND, exception.getMessage()).asResponse();
	}
}
