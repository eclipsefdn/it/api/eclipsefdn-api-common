/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.resource.mapper;

import jakarta.ws.rs.NotAcceptableException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import org.eclipsefoundation.http.model.WebError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class NotAcceptableMapper implements ExceptionMapper<NotAcceptableException> {
    private static final Logger LOGGER = LoggerFactory.getLogger(NotAcceptableMapper.class);

    @Override
    public Response toResponse(NotAcceptableException exception) {
        LOGGER.error(exception.getMessage(), exception);
        return new WebError(Status.NOT_ACCEPTABLE, "Could not process the given request: " + exception.getMessage())
                .asResponse();
    }
}