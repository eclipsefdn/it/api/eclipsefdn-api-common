/*********************************************************************
* Copyright (c) 2019, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.resource.mapper;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import org.eclipsefoundation.http.model.WebError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Creates human legible error responses in the case of NumberFormat exceptions.
 * 
 * @author Martin Lowe
 *
 */
@Provider
public class NumberFormatMapper implements ExceptionMapper<NumberFormatException> {
	private static final Logger LOGGER = LoggerFactory.getLogger(NumberFormatMapper.class);

	@Override
	public Response toResponse(NumberFormatException exception) {
		LOGGER.error(exception.getMessage(), exception);
		return new WebError(Status.BAD_REQUEST, "Passed parameter was not a number").asResponse();
	}

}
