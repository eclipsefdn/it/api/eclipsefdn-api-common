/**
 * Copyright (c) 2023, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.http.resource;

import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.http.annotations.KeySecured;
import org.eclipsefoundation.http.model.LoggerWrapperBuilder;
import org.eclipsefoundation.utils.helper.TransformationHelper;

import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * Resource endpoints for managing application logging, all secured by the random key generated at runtime.
 * 
 * @author Martin Lowe
 */
@Path("/loggers")
@Produces(MediaType.APPLICATION_JSON)
public class LoggerResource {

    /**
     * Simple update call that allows the user to pass a logger name along with a level to update the logging level on the fly from the
     * browser.
     * 
     * @param clazzName the name of the logger to update, typically just the full name of the class, e.g.
     * org.eclipsefoundation.core.helpers.SecureResourceKey
     * @param level the raw name of the logging level, or the number representing the level to set
     * @return the updated logger if it can be updated.
     */
    @GET
    @KeySecured
    public Response updateLogger(@QueryParam("clazz") String clazzName, @QueryParam("level") String level) {
        if (StringUtils.isBlank(clazzName) || StringUtils.isBlank(level)) {
            throw new BadRequestException("Both the 'clazz' and 'level' parameters need to be set to update logging levels");
        }

        // check if a logger exists before fetching, as fetching will create loggers if missing
        if (Collections.list(LogManager.getLogManager().getLoggerNames()).stream().noneMatch(clazzName::equals)) {
            throw new BadRequestException("Passed class reference string did not have an associated logger");
        }

        // actually fetch the logger once it is confirmed it exists
        Logger l = LogManager.getLogManager().getLogger(clazzName);
        try {
            // attempt to set the indicated level to the logger
            l.setLevel(Level.parse(level));
            // return a wrapper containing the updated logger for clarity
            return Response.ok(LoggerWrapperBuilder.builder().level(level).name(clazzName).build()).build();
        } catch (IllegalArgumentException e) {
            // format the level as it will end up in the log
            throw new BadRequestException("Could not set passed logger to level: " + TransformationHelper.formatLog(level));
        }
    }

    /**
     * Returns all loggers currently available within the application code, both from custom code and base libraries
     * 
     * @return list of all current loggers
     */
    @GET
    @KeySecured
    @Path("all")
    public Response getCurrentLoggers() {
        return Response
                .ok(Collections
                        .list(LogManager.getLogManager().getLoggerNames())
                        .stream()
                        .map(name -> LoggerWrapperBuilder
                                .builder()
                                .name(name)
                                .level(getLoggingLevel(LogManager.getLogManager().getLogger(name)))
                                .build()))
                .build();
    }

    /**
     * Returns all loggers associated with the custom application code under the EclipseFdn namespace
     * 
     * @return list of loggers under the prefix org.eclipsefoundation
     */
    @GET
    @KeySecured
    @Path("application")
    public Response getApplicationLoggers() {
        return Response
                .ok(Collections
                        .list(LogManager.getLogManager().getLoggerNames())
                        .stream()
                        .filter(name -> name.startsWith("org.eclipsefoundation"))
                        .map(name -> LoggerWrapperBuilder
                                .builder()
                                .name(name)
                                .level(getLoggingLevel(LogManager.getLogManager().getLogger(name)))
                                .build()))
                .build();
    }

    /**
     * Using recursions, digs out the most relevant logging level for the given logger. Will recurse through parents until there is a base
     * logging level set. If it somehow gets to the root logger without a set level, it will return empty.
     * 
     * @param l logger to retrieve the logging level of.
     * @return the unlocalized string name of the logging level for the given logger.
     */
    private String getLoggingLevel(Logger l) {
        // if we reach the root or bad data gets passed, we return blank as there is no valid data here
        if (l == null) {
            return "";
        }
        // if the current logger doesn't have a direct level, check the parent
        if (l.getLevel() == null) {
            return getLoggingLevel(l.getParent());
        }
        // return the name of the logging level associated with the current logger.
        return l.getLevel().getName();
    }

    
}
