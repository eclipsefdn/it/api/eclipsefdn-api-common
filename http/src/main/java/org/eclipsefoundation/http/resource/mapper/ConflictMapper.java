/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.resource.mapper;

import java.util.Map;
import java.util.Map.Entry;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import org.eclipsefoundation.http.exception.ConflictException;
import org.eclipsefoundation.http.model.WebError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ExceptionMapper class used to map the custom ConflictException to a Response
 * object returned to the client. Adds all headers from the ConflictException to
 * the outgoing response.
 */
@Provider
public class ConflictMapper implements ExceptionMapper<ConflictException> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConflictMapper.class);

    @Override
    public Response toResponse(ConflictException exception) {
        LOGGER.error(exception.getMessage(), exception);
        ResponseBuilder builder = Response
                .fromResponse(new WebError(Status.CONFLICT, exception.getMessage()).asResponse());

        // Add any headers to the response if they exist
        Map<String, String> headers = exception.getHeaders();
        if (headers != null && !headers.isEmpty()) {
            for (Entry<String, String> entry : headers.entrySet()) {
                builder.header(entry.getKey(), entry.getValue());
            }
        }

        return builder.build();
    }
}
