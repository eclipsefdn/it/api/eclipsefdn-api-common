/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.utils.helper.TransformationHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.annotation.PostConstruct;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ContainerResponseFilter;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.ext.Provider;

/**
 * A response filter used to append cache-control headers to an outgoing request. Toggled with configs.
 */
@Provider
public class CacheControlFilter implements ContainerResponseFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(CacheControlFilter.class);

    /**
     * Individual properties needed in filter as Mapping objects not supported in JAX-RS filters.
     * 
     * Src: https://github.com/quarkusio/quarkus/issues/19990#issuecomment-915330195
     */
    @ConfigProperty(name = "eclipse.core.cache-control.enabled", defaultValue = "false")
    Boolean enabled;
    @ConfigProperty(name = "eclipse.core.cache-control.paths", defaultValue = ".*")
    List<String> paths;

    private List<Pattern> patterns;

    @PostConstruct
    public void init() {
        this.patterns = new ArrayList<>();
        paths.stream().forEach(pattern -> patterns.add(Pattern.compile(pattern)));
    }

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        if (Boolean.TRUE.equals(enabled)) {
            String currentPath = requestContext.getUriInfo().getPath();

            // Check if the current path matches any URL patterns and add the cache-control headers
            if (patterns.stream().anyMatch(pattern -> pattern.matcher(currentPath).matches())) {
                LOGGER.debug("Adding Cache-Control headers for path: {}", TransformationHelper.formatLog(currentPath));
                responseContext.getHeaders().add(HttpHeaders.CACHE_CONTROL, "no-cache, no-store, must-revalidate");
                responseContext.getHeaders().add(HttpHeaders.EXPIRES, "0");
                responseContext.getHeaders().add("Pragma", "no-cache");
            }
        }
    }
}
