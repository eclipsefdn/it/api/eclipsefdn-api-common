/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
        Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.response;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.http.annotations.Pagination;
import org.eclipsefoundation.http.config.PaginationConfig;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;
import org.jboss.resteasy.reactive.server.ServerResponseFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.inject.Instance;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.Link;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.UriBuilder;
import jakarta.ws.rs.core.UriInfo;

/**
 * Adds pagination and Link headers to the response by slicing the response entity if its a list entity. This will not dig into complex
 * entities to avoid false positives.
 *
 * @author Martin Lowe
 */
public class PaginatedResultsFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(PaginatedResultsFilter.class);
    // should be set whenever we pass a limited subset to the response to be
    // paginated of a larger subset
    public static final String MAX_RESULTS_SIZE_HEADER = "X-Max-Result-Size";
    // should be set whenever we request data that has a maximum page size that is
    // different than the default here
    public static final String MAX_PAGE_SIZE_HEADER = "X-Max-Page-Size";

    Instance<PaginationConfig> config;

    public PaginatedResultsFilter(Instance<PaginationConfig> config) {
        this.config = config;
    }

    @ServerResponseFilter
    public void filter(ResourceInfo resourceInfo, UriInfo uriInfo, ContainerResponseContext responseContext) {

        if (checkPaginationAnnotation(resourceInfo) && config.get().filter().enabled()) {
            Object entity = responseContext.getEntity();

            // only try and paginate if there are multiple entities
            if (entity instanceof Set) {
                paginateResults(responseContext, uriInfo, (new TreeSet<>((Set<?>) entity)).stream().toList());
            } else if (entity instanceof List) {
                paginateResults(responseContext, uriInfo, (List<?>) entity);
            }
        }
    }

    private void paginateResults(ContainerResponseContext responseContext, UriInfo uriInfo, List<?> listEntity) {

        MultivaluedMap<String, String> responseHeaders = responseContext.getStringHeaders();
        MultivaluedMap<String, String> requestQueryParams = uriInfo.getQueryParameters();
        int pageSize = getCurrentLimit(requestQueryParams, getInternalMaxPageSize(responseHeaders));

        // if available, use max results header value
        String rawMaxSize = responseHeaders.getFirst(MAX_RESULTS_SIZE_HEADER);
        int maxSize = listEntity.size();
        if (StringUtils.isNumeric(rawMaxSize)) {
            maxSize = Integer.valueOf(rawMaxSize);
        }

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Using max results of {} with page size {}", maxSize, pageSize);
        }

        int page = getRequestedPage(requestQueryParams);
        int lastPage = (int) Math.ceil((double) maxSize / pageSize);

        // Return an empty list if requested page is beyond max
        if (page > lastPage) {
            responseContext.setEntity(Collections.emptyList());
        } else if (listEntity.size() > pageSize) {
            // slice if the results set is larger than page size
            int start = getArrayLimitedNumber(listEntity, Math.max(0, page - 1) * pageSize);
            int end = getArrayLimitedNumber(listEntity, pageSize * page);
            responseContext.setEntity(listEntity.subList(start, end));
        }

        // set the link header to the response
        List<Object> links = createLinkHeader(uriInfo, page, lastPage).stream().map(l -> (Object) l).toList();
        responseContext.getHeaders().put("Link", links);
    }

    /**
     * Checks the current method for pagination annotations and if present, checks to make sure pagination is enabled. Defaults to using
     * pagination if the pagination annotation is missing or cannot be read.
     * 
     * @return true if pagination should be enabled, false otherwise
     */
    private boolean checkPaginationAnnotation(ResourceInfo resourceInfo) {
        // method can be null sometimes for quarkus native endpoints
        Method method = resourceInfo.getResourceMethod();
        if (method != null) {
            Pagination annotation = method.getAnnotation(Pagination.class);
            return annotation == null || annotation.value();
        }
        return true;
    }

    private List<Link> createLinkHeader(UriInfo uriInfo, int page, int lastPage) {
        // add link headers for paginated page hints

        UriBuilder builder = uriInfo.getRequestUriBuilder();
        List<Link> links = new ArrayList<>();
        // add first + last page link headers
        links.add(Link.fromUri(buildHref(builder, 1)).rel("first").title("first page of results").build());
        links.add(Link.fromUri(buildHref(builder, page)).rel("self").title("this page of results").build());
        links.add(Link.fromUri(buildHref(builder, lastPage)).rel("last").title("last page of results").build());
        // add next/prev if needed
        if (page > 1) {
            links.add(Link.fromUri(buildHref(builder, page - 1)).rel("prev").title("previous page of results").build());
        }
        if (page < lastPage) {
            links.add(Link.fromUri(buildHref(builder, page + 1)).rel("next").title("next page of results").build());
        }
        return links;
    }

    /**
     * Gets the current requested page, rounding up if below 1.
     *
     * @param requestQueryParams Query parameters within the current request context.
     * @return The requested page number if set, defaulting to 1 if lower or not set.
     */
    private int getRequestedPage(MultivaluedMap<String, String> requestQueryParams) {
        String pageParam = requestQueryParams.getFirst(DefaultUrlParameterNames.PAGE.getName());
        if (StringUtils.isNumeric(pageParam)) {
            int page = Integer.parseInt(pageParam);
            return Math.max(1, page);
        }
        return 1;
    }

    /**
     * Allows for external bindings to affect the current page size, defaulting to the internal set configuration.
     * 
     * @param responseContext
     * @return
     */
    private int getCurrentLimit(MultivaluedMap<String, String> requestQueryParams, int maxPageSize) {
        String pageSizeParam = requestQueryParams.getFirst(DefaultUrlParameterNames.PAGESIZE.getName());
        if (StringUtils.isNumeric(pageSizeParam)) {
            int pageSize = Integer.parseInt(pageSizeParam);
            return Math.min(pageSize, maxPageSize);
        }
        return maxPageSize;
    }

    /**
     * Returns the max page size as defined by internal metrics (ignoring pagesize params).
     * 
     * @return the internal max page size.
     */
    private int getInternalMaxPageSize(MultivaluedMap<String, String> responseHeaders) {
        String rawPageSize = responseHeaders.getFirst(MAX_PAGE_SIZE_HEADER);
        if (StringUtils.isNumeric(rawPageSize)) {
            return Integer.parseInt(rawPageSize);
        }
        return config.get().filter().defaultPageSize();
    }

    /**
     * Builds an href for a paginated link using the BaseUri UriBuilder from the UriInfo object, replacing just the page query parameter.
     *
     * @param builder base URI builder from the UriInfo object.
     * @param page the page to link to in the returned link
     * @return fully qualified HREF for the paginated results
     */
    private String buildHref(UriBuilder builder, int page) {
        // Force scheme of header links to be a given value, useful for proxied requests
        if (config.get().filter().scheme().enforce()) {
            return builder.scheme(config.get().filter().scheme().value()).replaceQueryParam("page", page).build().toString();
        }
        return builder.replaceQueryParam("page", page).build().toString();
    }

    /**
     * Gets an int bound by the size of a list.
     *
     * @param list the list to bind the number by
     * @param num the number to check for exceeding bounds.
     * @return the passed number if its within the size of the given array, 0 if the number is negative, and the array size if greater than
     * the maximum bounds.
     */
    private int getArrayLimitedNumber(List<?> list, int num) {
        return Math.min(list.size(), Math.max(0, num));
    }
}
