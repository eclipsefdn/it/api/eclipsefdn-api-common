/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.response;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.eclipse.microprofile.context.ManagedExecutor;
import org.eclipsefoundation.http.namespace.RequestHeaderNames;
import org.eclipsefoundation.utils.config.CSRFSecurityConfig;
import org.eclipsefoundation.utils.helper.CSRFHelper;
import org.eclipsefoundation.utils.model.AdditionalUserData;
import org.jboss.resteasy.reactive.server.ServerResponseFilter;

import io.smallrye.mutiny.Uni;
import io.vertx.core.http.HttpServerRequest;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;

/**
 * Injects the CSRF header token into the response when enabled for a server.
 *
 * @author Martin Lowe
 */
public class CSRFHeaderFilter {

    private final CSRFSecurityConfig config;
    private final CSRFHelper csrf;
    private final AdditionalUserData aud;
    private final ManagedExecutor executor;

    public CSRFHeaderFilter(CSRFHelper csrf, AdditionalUserData aud, CSRFSecurityConfig config, ManagedExecutor executor) {
        this.config = config;
        this.csrf = csrf;
        this.aud = aud;
        this.executor = executor;
    }

    @ServerResponseFilter
    public Uni<Void> filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext, HttpServerRequest request)
            throws InterruptedException, ExecutionException, TimeoutException {
        // only attach if CSRF is enabled for the current runtime
        if (config.enabled()) {
            // generate the token
            CompletableFuture<String> tokenTask = executor
                    .supplyAsync(() -> csrf.getNewCSRFToken(request, requestContext.getSecurityContext()));
            String token = tokenTask.get(5, TimeUnit.SECONDS);
            // store token in session if not distributed mode
            if (aud.getCsrf() == null && !config.distributedMode().enabled()) {
                aud.setCsrf(token);
            }
            // attach the current CSRF token as a header on the request
            responseContext.getHeaders().add(RequestHeaderNames.CSRF_TOKEN, token);
        }

        return Uni.createFrom().nullItem();
    }
}
