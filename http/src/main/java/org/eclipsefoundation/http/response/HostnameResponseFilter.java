/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.response;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.http.namespace.ResponseHeaderNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.inject.Instance;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ContainerResponseFilter;
import jakarta.ws.rs.ext.Provider;

@Provider
public class HostnameResponseFilter implements ContainerResponseFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(HostnameResponseFilter.class);

    @ConfigProperty(name = "eclipse.core.hostname.enabled", defaultValue = "false")
    Instance<Boolean> enabled;

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
        if (Boolean.TRUE.equals(enabled.get())) {
            String hostname = Optional.ofNullable(getInternalNodeName()).orElse(System.getenv("HOSTNAME"));
            if (StringUtils.isBlank(hostname)) {
                LOGGER.warn("There was an error fetching the hostname");
            } else {
                responseContext.getHeaders().add(ResponseHeaderNames.X_NODE_NAME, hostname);
            }
        }
    }

    /**
     * Runs the 'uname -n' command on the current system. Used to get the internal name of the machine
     * 
     * @return The internal machine name or null
     */
    static String getInternalNodeName() {
        try {
            // Create a process using the uname command and run it
            ProcessBuilder builder = new ProcessBuilder("uname", "-n");
            Process process = builder.start();

            // Read the ouput and set the hostname. Only 1 line should output from this command
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String hostname = reader.readLine();

            // Wait for the process to finish if not already done
            process.waitFor();
            return hostname;
        } catch (InterruptedException e) {
            LOGGER.error("Error while using uname command", e);
            Thread.currentThread().interrupt();
            return null;
        } catch (Exception e) {
            LOGGER.error("Error while using uname command", e);
            return null;
        }
    }
}
