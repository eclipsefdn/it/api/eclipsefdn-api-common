/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.model;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.utils.helper.TransformationHelper;
import org.eclipsefoundation.utils.namespace.UrlParameterNamespace.UrlParameter;

import io.quarkus.security.identity.SecurityIdentity;
import io.quarkus.security.runtime.QuarkusSecurityIdentity;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * Scopeless and simple request wrapper that can live outside of a request scope.
 * 
 * @author Martin Lowe
 *
 */
public class FlatRequestWrapper implements RequestWrapper {
    private MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
    private MultivaluedMap<String, String> headers = new MultivaluedHashMap<>();
    private URI endpoint;

    private static final int DEFAULT_FLAT_PAGE_SIZE = 1000;

    public FlatRequestWrapper(URI endpoint) {
        this.endpoint = endpoint;
    }

    @Override
    public Optional<String> getFirstParam(UrlParameter key) {
        List<String> vals = getParams().get(key.getName());
        if (vals == null || vals.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(vals.get(0));
    }

    @Override
    public List<String> getParams(UrlParameter key) {
        List<String> vals = getParams().get(key.getName());
        if (vals == null || vals.isEmpty()) {
            return Collections.emptyList();
        }
        return vals;
    }

    @Override
    public void setParam(UrlParameter key, String value) {
        Objects.requireNonNull(value);
        // remove current value, and add new value in its place
        getParams().remove(key.getName());
        addParam(key, value);
    }

    @Override
    public void addParam(UrlParameter key, String value) {
        Objects.requireNonNull(value);
        getParams().computeIfAbsent(key.getName(), k -> new ArrayList<>()).add(value);
    }

    @Override
    public void setParam(String key, List<String> value) {
        getParams().put(key, Objects.requireNonNull(value));
    }

    @Override
    public MultivaluedMap<String, String> asMap() {
        MultivaluedMap<String, String> out = new MultivaluedHashMap<>();
        getParams().forEach(out::addAll);
        return out;
    }

    private Map<String, List<String>> getParams() {
        return this.params;
    }

    @Override
    public String getEndpoint() {
        return endpoint.getPath();
    }

    @Override
    public Optional<Object> getAttribute(String key) {
        return Optional.empty();
    }

    @Override
    public boolean isCacheBypass() {
        return false;
    }

    @Override
    public String getHeader(String key) {
        return headers.getFirst(key);
    }

    @Override
    public String getResponseHeader(String key) {
        return getHeader(key);
    }

    @Override
    public void setHeader(String name, String value) {
        headers.add(name, value);
    }

    @Override
    public String getRequestVersion() {
        return null;
    }

    @Override
    public URI getURI() {
        return endpoint;
    }

    @Override
    public SecurityIdentity getCurrentUser() {
        return QuarkusSecurityIdentity.builder().setAnonymous(true).build();
    }

    @Override
    public int getPageSize() {
        Optional<String> pagesizeRaw = getFirstParam(DefaultUrlParameterNames.PAGESIZE);
        if (pagesizeRaw.isPresent() && StringUtils.isNumeric(pagesizeRaw.get())) {
            return Integer.parseInt(pagesizeRaw.get());
        }
        return DEFAULT_FLAT_PAGE_SIZE;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder
                .append("FlatRequestWrapper [params=")
                .append(params)
                .append(", headers=")
                .append(headers)
                .append(", endpoint=")
                .append(TransformationHelper.formatLog(endpoint.toString()))
                .append("]");
        return builder.toString();
    }

}
