/*********************************************************************
* Copyright (c) 2019, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.model;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.http.config.PaginationConfig;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.http.namespace.RequestHeaderNames;
import org.eclipsefoundation.http.request.CacheBypassFilter;
import org.eclipsefoundation.utils.namespace.UrlParameterNamespace.UrlParameter;

import io.quarkus.arc.Unremovable;
import io.quarkus.security.identity.SecurityIdentity;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.UriInfo;

/**
 * Wrapper class for query parameter functionality, wrapping a Map of String to collection of String values. This class
 * should be used anywhere where query parameters are used to control access and prevent bad data from being entered.
 *
 * @author Martin Lowe
 */
@Unremovable
@RequestScoped
public class DefaultRequestWrapper implements RequestWrapper {
    private static final String EMPTY_KEY_MESSAGE = "Key must not be null or blank";

    private Map<String, List<String>> params;
    private MultivaluedMap<String, String> headers = new MultivaluedHashMap<>();

    @Inject
    SecurityIdentity ident;

    @Inject
    PaginationConfig config;

    @Inject
    UriInfo uriInfo;
    @Inject
    HttpServerRequest request;
    @Inject
    ContainerRequestContext requestContext;
    @Inject
    HttpServerResponse response;

    /**
     * Retrieves the first value set in a list from the map for a given key.
     *
     * @param wrapper the parameter map containing the value
     * @param key the key to retrieve the value for
     * @return the first value set in the parameter map for the given key, or null if absent.
     */
    @Override
    public Optional<String> getFirstParam(UrlParameter key) {
        if (key == null) {
            throw new IllegalArgumentException(EMPTY_KEY_MESSAGE);
        }

        List<String> vals = getParams().get(key.getName());
        if (vals == null || vals.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(vals.get(0));
    }

    /**
     * Retrieves the value list from the map for a given key.
     *
     * @param wrapper the parameter map containing the values
     * @param key the key to retrieve the values for
     * @return the value list for the given key if it exists, or an empty collection if none exists.
     */
    @Override
    public List<String> getParams(UrlParameter key) {
        if (key == null) {
            throw new IllegalArgumentException(EMPTY_KEY_MESSAGE);
        }

        List<String> vals = getParams().get(key.getName());
        if (vals == null || vals.isEmpty()) {
            return Collections.emptyList();
        }
        return vals;
    }

    /**
     * Adds the given value for the given key, preserving previous values if they exist.
     *
     * @param key string key to add the value to, must not be null
     * @param value the value to add to the key
     */
    @Override
    public void addParam(UrlParameter key, String value) {
        if (key == null) {
            throw new IllegalArgumentException(EMPTY_KEY_MESSAGE);
        }
        Objects.requireNonNull(value);
        getParams().computeIfAbsent(key.getName(), k -> new ArrayList<>()).add(value);
    }

    /**
     * Sets the value as the value for the given key, removing previous values if they exist.
     *
     * @param key string key to add the value to, must not be null
     * @param value the value to add to the key
     */
    @Override
    public void setParam(UrlParameter key, String value) {
        if (key == null) {
            throw new IllegalArgumentException(EMPTY_KEY_MESSAGE);
        }
        Objects.requireNonNull(value);
        // remove current value, and add new value in its place
        getParams().remove(key.getName());
        addParam(key, value);
    }

    /**
     * Sets the value as the value for the given key.
     *
     * @param key string key to add the value to, must not be null
     * @param value the values to add to the key
     */
    @Override
    public void setParam(String key, List<String> value) {
        if (key == null) {
            throw new IllegalArgumentException(EMPTY_KEY_MESSAGE);
        }
        getParams().put(key, Objects.requireNonNull(value));
    }

    /**
     * Returns this QueryParams object as a Map of param values indexed by the param name.
     *
     * @return a copy of the internal param map
     */
    @Override
    public MultivaluedMap<String, String> asMap() {
        MultivaluedMap<String, String> out = new MultivaluedHashMap<>();
        getParams().forEach((key, values) -> out.addAll(key, new ArrayList<>(values)));
        return out;
    }

    private Map<String, List<String>> getParams() {
        if (params == null) {
            params = new HashMap<>();
            if (uriInfo != null) {
                params.putAll(uriInfo.getQueryParameters());
            }
        }
        return this.params;
    }

    /**
     * Returns the endpoint for the current call
     *
     * @return
     */
    @Override
    public String getEndpoint() {
        String endpoint = "";
        if (uriInfo != null) {
            endpoint = uriInfo.getPath();
        }
        return endpoint;
    }

    /**
     * Retrieve a request attribute
     *
     * @param key attribute key
     * @return the attribute value, or an empty optional if missing.
     */
    @Override
    public Optional<Object> getAttribute(String key) {
        return Optional.ofNullable(requestContext.getProperty(key));
    }

    /**
     * Check whether the current request should bypass caching
     *
     * @return true if cache should be bypassed, otherwise false
     */
    @Override
    public boolean isCacheBypass() {
        Object attr = requestContext.getProperty(CacheBypassFilter.ATTRIBUTE_NAME);
        // if we have the attribute set on the request, return it. otherwise, false.
        return attr instanceof Boolean ? (boolean) attr : Boolean.FALSE;
    }

    /**
     * Retrieve a request header value.
     *
     * @param key the headers key value
     * @return the value, or an empty optional if missing.
     */
    @Override
    public String getHeader(String key) {
        return request.getHeader(key);
    }

    @Override
    public String getResponseHeader(String key) {
        if (getResponse() != null) {
            return getResponse().headers().get(key);
        }
        return headers.getFirst(key);
    }

    /**
     * Retrieve the request version from the
     *
     * @param key the headers key value
     * @return the version passed from the access version header
     */
    @Override
    public String getRequestVersion() {
        return request.getHeader(RequestHeaderNames.ACCESS_VERSION);
    }

    /**
     * Set a header in the response object for the client.
     *
     * @param name the name of the header to set
     * @param value the headers value
     */
    @Override
    public void setHeader(String name, String value) {
        if (getResponse() != null) {
            getResponse().headers().add(name, value);
        }
        headers.add(name, value);
    }

    protected HttpServerResponse getResponse() {
        return response;
    }

    /**
     * Gets the request URI for the current context.
     * 
     * @return the request URI object, or null if called outside of request context scope.
     */
    @Override
    public URI getURI() {
        return uriInfo != null ? uriInfo.getRequestUri() : null;
    }

    /**
     * Provides easy access to the current user object
     * 
     * @return the current user object for the request
     */
    @Override
    public SecurityIdentity getCurrentUser() {
        return ident;
    }

    @Override
    public int getPageSize() {
        Optional<String> pagesizeRaw = getFirstParam(DefaultUrlParameterNames.PAGESIZE);
        if (pagesizeRaw.isPresent() && StringUtils.isNumeric(pagesizeRaw.get())) {
            return Integer.parseInt(pagesizeRaw.get());
        }
        return config.filter().defaultPageSize();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("RequestWrapper [");
        sb.append("ip=").append(request.remoteAddress());
        sb.append(", uri=").append(request.uri());
        sb.append(", params=").append(getParams());
        return sb.toString();
    }
}
