/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.http.model;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.eclipsefoundation.utils.namespace.UrlParameterNamespace.UrlParameter;

import io.quarkus.security.identity.SecurityIdentity;
import jakarta.ws.rs.core.MultivaluedMap;

public interface RequestWrapper {

    /**
     * Retrieves the first value set in a list from the map for a given key.
     *
     * @param key the key to retrieve the value for
     * @return the first value set in the parameter map for the given key, or null if absent.
     */
    Optional<String> getFirstParam(UrlParameter key);

    /**
     * Retrieves the value list from the map for a given key.
     *
     * @param key the key to retrieve the values for
     * @return the value list for the given key if it exists, or an empty collection if none exists.
     */
    List<String> getParams(UrlParameter key);

    /**
     * Adds the given value for the given key, preserving previous values if they exist.
     *
     * @param key string key to add the value to, must not be null
     * @param value the value to add to the key
     */
    void addParam(UrlParameter key, String value);

    /**
     * Sets the value as the value for the given key, removing previous values if they exist.
     *
     * @param key string key to add the value to, must not be null
     * @param value the value to add to the key
     */
    void setParam(UrlParameter key, String value);

    /**
     * Sets the value as the value for the given key.
     *
     * @param key string key to add the value to, must not be null
     * @param value the values to add to the key
     */
    void setParam(String key, List<String> value);

    /**
     * Returns this QueryParams object as a Map of param values indexed by the param name.
     *
     * @return a copy of the internal param map
     */
    MultivaluedMap<String, String> asMap();

    /**
     * Returns the endpoint for the current call
     *
     * @return
     */
    String getEndpoint();

    /**
     * Retrieve a request attribute
     *
     * @param key attribute key
     * @return the attribute value, or an empty optional if missing.
     */
    Optional<Object> getAttribute(String key);

    /**
     * Check whether the current request should bypass caching
     *
     * @return true if cache should be bypassed, otherwise false
     */
    boolean isCacheBypass();

    /**
     * Retrieve a request header value.
     *
     * @param key the headers key value
     * @return the value, or an empty optional if missing.
     */
    String getHeader(String key);

    /**
     * Retrieve a response header value.
     *
     * @param key the headers key value
     * @return the value, or an empty optional if missing.
     */
    String getResponseHeader(String key);

    /**
     * Retrieve the request version from the
     *
     * @param key the headers key value
     * @return the version passed from the access version header
     */
    String getRequestVersion();

    /**
     * Set a header in the response object for the client.
     *
     * @param name the name of the header to set
     * @param value the headers value
     */
    void setHeader(String name, String value);

    /**
     * Gets the request URI for the current context.
     * 
     * @return the request URI object, or null if called outside of request context scope.
     */
    URI getURI();

    /**
     * Provides easy access to the current user object
     * 
     * @return the current user object for the request
     */
    SecurityIdentity getCurrentUser();

    /**
     * Retrieves the default page size for the current request. Will check the pagesize request parameter, then limit,
     * and then configuration to get the defined pagesize.
     * 
     * Limit support has been deprecated and will be removed in a following update.
     * 
     * @return the max page size for the current request.
     */
    int getPageSize();
}