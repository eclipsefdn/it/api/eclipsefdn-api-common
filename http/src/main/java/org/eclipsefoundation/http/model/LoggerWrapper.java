/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
/**
 * 
 */
package org.eclipsefoundation.http.model;

import io.soabase.recordbuilder.core.RecordBuilder;

/**
 * 
 */
@RecordBuilder
public record LoggerWrapper(String name, String level) {
}
