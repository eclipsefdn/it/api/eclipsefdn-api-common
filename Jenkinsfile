  @Library('common-shared') _

  pipeline {
    agent {
      kubernetes {
        label 'buildenv-agent-java-sdk'
        yaml '''
        apiVersion: v1
        kind: Pod
        spec:
          containers:
          - name: buildcontainer
            image: eclipsefdn/stack-build-agent:h111.3-n18.17-jdk17
            imagePullPolicy: Always
            command:
            - cat
            tty: true
            resources:
              requests:
                cpu: "1"
                memory: "4Gi"
              limits:
                cpu: "2"
                memory: "4Gi"
            env:
            - name: "HOME"
              value: "/home/jenkins"
            - name: "MAVEN_OPTS"
              value: "-Duser.home=/home/jenkins"
            volumeMounts:
            - name: m2-repo
              mountPath: /home/jenkins/.m2/repository
            - name: m2-secret-dir
              mountPath: /home/jenkins/.m2/settings.xml
              subPath: settings.xml
              readOnly: true
            - mountPath: "/home/jenkins/.m2/settings-security.xml"
              name: "m2-secret-dir"
              readOnly: true
              subPath: "settings-security.xml"
            - mountPath: "/home/jenkins/.mavenrc"
              name: "m2-dir"
              readOnly: true
              subPath: ".mavenrc"
            - mountPath: "/home/jenkins/.m2/wrapper"
              name: "m2-wrapper"
              readOnly: false
            - mountPath: "/home/jenkins/.cache"
              name: "yarn-cache"
              readOnly: false
            - mountPath: "/home/jenkins/.sonar"
              name: "sonar-cache"
              readOnly: false
          - name: jnlp
            resources:
              requests:
                memory: "1024Mi"
                cpu: "500m"
              limits:
                memory: "1024Mi"
                cpu: "1000m"
          volumes:
          - name: "m2-dir"
            configMap:
              name: "m2-dir"
          - name: m2-secret-dir
            secret:
              secretName: m2-secret-dir
          - name: m2-repo
            emptyDir: {}
          - name: m2-wrapper
            emptyDir: {}
          - name: yarn-cache
            emptyDir: {}
          - name: sonar-cache
            emptyDir: {}
        '''
      }
    }

    environment {
      ENVIRONMENT = sh(
        script: """
          if [ "${env.BRANCH_NAME}" = "main" ]; then
            printf "production"
          else
            printf "${env.BRANCH_NAME}"
          fi
        """,
        returnStdout: true
      )
      TAG_NAME = sh(
        script: """
          GIT_COMMIT_SHORT=\$(git rev-parse --short ${env.GIT_COMMIT})
          if [ "${env.ENVIRONMENT}" = "" ]; then
            printf \${GIT_COMMIT_SHORT}-${env.BUILD_NUMBER}
          else
            printf ${env.ENVIRONMENT}-\${GIT_COMMIT_SHORT}-${env.BUILD_NUMBER}
          fi
        """,
        returnStdout: true
      )
    }

    options {
      buildDiscarder(logRotator(numToKeepStr: '10'))
      timeout(time: 30, unit: 'MINUTES') 
    }

    triggers { 
      // build once a week to keep up with parents images updates
      cron('H H * * H') 
    }

    stages {
      stage('Build with Sonarcloud scan') {
        when {
          branch 'main'
        }
        steps {
          container('buildcontainer') {
            readTrusted 'pom.xml'

            withCredentials([string(credentialsId: 'sonarcloud-token-eclipsefdn-api-common', variable: 'SONAR_TOKEN')]) {
              sh 'mvn -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn -B clean verify org.sonarsource.scanner.maven:sonar-maven-plugin:sonar -Dsonar.host.url=https://sonarcloud.io -Dsonar.login=${SONAR_TOKEN}'
            }
            stash includes: 'target/', name: 'target'
          }
        }
      }
      
      stage('Build without Sonarcloud scan') {
        when {
          not {
           branch 'main'
          }
        }
        steps {
          container('buildcontainer') {
            readTrusted 'pom.xml'

            sh 'mvn -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn -B package'
          }
        }
      }
            

      stage('Push package image to Nexus') {
        when {
          anyOf {
            environment name: 'ENVIRONMENT', value: 'production'
          }
        }
        steps {
          container('buildcontainer') {
            unstash 'target'

            sh 'mvn -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn -B deploy -Dmaven.test.skip=true'
          }
        }
      }
    }

    post {
      always {
        sendNotifications currentBuild
      }
    }
  }
