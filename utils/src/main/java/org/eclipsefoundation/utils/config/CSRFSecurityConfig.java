/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.utils.config;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;

/**
 * Configuration mapping for all CSRF related values.
 */
@ConfigMapping(prefix = "eclipse.security.csrf")
public interface CSRFSecurityConfig {

    /**
     * @return The flag to enable/disable CSRF
     */
    @WithDefault("false")
    boolean enabled();

    /**
     * @return CSRF distributed mode configs.
     */
    CSRFDistributedDefinition distributedMode();

    /**
     * @return CSRF token configs
     */
    CSRFTokenDefinition token();

    /**
     * Configuration mapping for all CSRF distributed mode values.
     */
    interface CSRFDistributedDefinition {

        /**
         * @return The flag to enable/disable distributed CSRF
         */
        @WithDefault("false")
        boolean enabled();

        /**
         * @return The flag to enable/disable distributed CSRF as the default provider
         */
        @WithDefault("false")
        boolean isDefaultProvider();
    }

    /**
     * Configuration mapping for all CSRF token related configs.
     */
    interface CSRFTokenDefinition {

        /**
         * @return A salt value used in the CSRF token generation process
         */
        @WithDefault("short-salt")
        String salt();
    }
}
