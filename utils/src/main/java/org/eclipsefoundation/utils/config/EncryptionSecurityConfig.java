/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.utils.config;

import java.util.Optional;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;

/**
 * 
 */
@ConfigMapping(prefix = "eclipse.security.encryption")
public interface EncryptionSecurityConfig {

    /**
     * @return The path of the key to use to encrypt values, not needed for every app so set as optional.
     */
    Optional<String> keyFilepath();

    /**
     * @return The type of the PEM file being loaded for encryption
     */
    @WithDefault("RSA/ECB/OAEPWithSHA-1AndMGF1Padding")
    String keyType();

}
