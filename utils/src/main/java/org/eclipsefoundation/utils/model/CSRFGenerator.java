/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.utils.model;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HexFormat;
import java.util.Optional;
import java.util.UUID;

import org.eclipse.microprofile.config.ConfigProvider;
import org.eclipsefoundation.utils.namespace.MicroprofilePropertyNames;

import io.vertx.core.http.HttpServerRequest;
import jakarta.ws.rs.core.SecurityContext;

/**
 * Interface used to generate random hardened tokens for use in requests.
 * 
 * @author Martin Lowe
 *
 */
public interface CSRFGenerator {

    /**
     * Generates a random secure CSRF token to be used in requests. This token should be cryptographically secure and random to ensure that
     * it cannot be generated from an external source.
     * 
     * @param requestContext current request context that can be used for fingerprinting the request if required.
     * @return a random encoded string token
     */
    public String getCSRFToken(HttpServerRequest request, SecurityContext context);

    /**
     * Default implementation of the CSRF generator, will use secure randoms generated on the fly at runtime to generate random seed values
     * as base of token. These values will be hashed and salted to provide extra hardening of the value to make it harder to predict.
     * 
     * @author Martin Lowe
     *
     */
    public static class DefaultCSRFGenerator implements CSRFGenerator {

        @Override
        public String getCSRFToken(HttpServerRequest request, SecurityContext context) {

            // create new digest to hash the result
            MessageDigest md;
            try {
                md = MessageDigest.getInstance("SHA-256");
            } catch (NoSuchAlgorithmException e) {
                throw new IllegalStateException("Could not find SHA-256 algorithm to encode CSRF token", e);
            }

            // use a random value salted with a configured static value
            String secureBase = UUID.randomUUID().toString();
            Optional<String> salt = ConfigProvider.getConfig().getOptionalValue(MicroprofilePropertyNames.CSRF_TOKEN_SALT, String.class);
            // create a secure random secret to embed in the user session
            String preHash = secureBase + salt.orElseGet(() -> "short-salt");

            // hash the results using the message digest
            byte[] array = md.digest(preHash.getBytes());
            // convert back to a hex string to act as a token
            return HexFormat.of().formatHex(array);
        }
    }
}
