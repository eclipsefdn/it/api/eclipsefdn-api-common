/**
 * Copyright (c) 2022 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.utils.providers;

import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import io.quarkus.runtime.Startup;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Singleton;

/**
 * Adds Bouncycastle (BC) as a security provider to enable PKCS1 consumption.
 * 
 * @author Martin Lowe
 *
 */
@Startup
@Singleton
public class BCSecurityProvider {

    @PostConstruct
    public void init() {
        Security.addProvider(new BouncyCastleProvider());
    }
}
