/*********************************************************************
* Copyright (c) 2022, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.utils.helper;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

import org.eclipsefoundation.utils.config.CSRFSecurityConfig;
import org.eclipsefoundation.utils.exception.FinalForbiddenException;
import org.eclipsefoundation.utils.model.AdditionalUserData;
import org.eclipsefoundation.utils.model.CSRFGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.arc.Unremovable;
import io.vertx.core.http.HttpServerRequest;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Singleton;
import jakarta.ws.rs.core.SecurityContext;

/**
 * Helper class for interacting with CSRF tokens within the server. Generates secure CSRF tokens and compares them to the copy that exists
 * within the current session object.
 * 
 * @author Martin Lowe
 *
 */
@Unremovable
@Singleton
public final class CSRFHelper {
    public static final Logger LOGGER = LoggerFactory.getLogger(CSRFHelper.class);

    private final Instance<CSRFSecurityConfig> config;
    private final CSRFGenerator generator;

    /**
     * Default constructor for the CSRF helper, taking the config and generator to be used in fulfilling requests.
     * 
     * @param config mapping for the CSRF configuration values
     * @param generator the CSRF generator implementation to be used for the runtime.
     */
    public CSRFHelper(Instance<CSRFSecurityConfig> config, CSRFGenerator generator) {
        this.config = config;
        this.generator = generator;
    }

    /**
     * Generate a new CSRF token that has been hardened to make it more difficult to predict.
     *
     * @return a cryptographically-secure CSRF token to use in a session.
     */
    public String getNewCSRFToken(HttpServerRequest request, SecurityContext context) {
        return generator.getCSRFToken(request, context);
    }

    /**
     * Checks the current method of CSRF storage (session vs distributed) and returns the stored CSRF value.
     * 
     * @param aud the current local session data
     * @param httpServletRequest the request context
     * @return the CSRF token for the current request.
     */
    public String getSessionCSRFToken(AdditionalUserData aud, HttpServerRequest request, SecurityContext context) {
        if (config.get().distributedMode().enabled()) {
            return generator.getCSRFToken(request, context);
        } else {
            return aud.getCsrf();
        }
    }

    /**
     * Compares the passed CSRF token to the token for the current user session.
     *
     * @param expectedToken the stored CSRF reference value
     * @param passedToken the passed CSRF header data
     * @throws FinalUnauthorizedException when CSRF token is missing in the user data, the passed header value, or does not match
     */
    public void compareCSRF(String expectedToken, String passedToken) {
        if (config.get().enabled()) {
            LOGGER.debug("Comparing following tokens:\n{}\n{}", expectedToken == null ? null : expectedToken, passedToken);
            if (expectedToken == null) {
                throw new FinalForbiddenException("CSRF token not generated for current request and is required, refusing request");
            } else if (passedToken == null) {
                throw new FinalForbiddenException("No CSRF token passed for current request, refusing request");
            } else if (!verifyConstantTime(expectedToken, passedToken)) {
                throw new FinalForbiddenException("CSRF tokens did not match, refusing request");
            }
        }
    }

    private static boolean verifyConstantTime(String expectedToken, String token) {
        if (expectedToken == null || token == null) {
            return false;
        }
        if (expectedToken.equals(token)) {
            return true;
        }
        return MessageDigest.isEqual(expectedToken.getBytes(StandardCharsets.US_ASCII), token.getBytes(StandardCharsets.US_ASCII));
    }

}
