/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.utils.helper;

import java.io.FileReader;
import java.security.KeyFactory;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.crypto.Cipher;

import org.bouncycastle.util.io.pem.PemReader;
import org.eclipsefoundation.utils.config.EncryptionSecurityConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.arc.Unremovable;
import jakarta.enterprise.context.ApplicationScoped;

/**
 * Helper class used to perform string transformation operations, such as sanitation and encryption.
 */
@Unremovable
@ApplicationScoped
public class TransformationHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransformationHelper.class);

    private final EncryptionSecurityConfig encryptionConfig;

    // create a local ref to hold the key
    private byte[] decodedKey;

    /**
     * Default constructor for helper that loads in required configurations.
     * 
     * @param encryptionConfig mapped config object containing information about the public key for transformations
     */
    public TransformationHelper(EncryptionSecurityConfig encryptionConfig) {
        this.encryptionConfig = encryptionConfig;
    }

    /**
     * Encrypts an string using the public key in the application configs and returns the result in a Base64 encoded string.
     * 
     * @param src The given string to encrypt and encode.
     * @return The encrypted and encoded string
     * @throws RuntimeException if there was an error that stops the encryption of the string
     */
    public String encryptAndEncodeValue(String src) {
        Optional<String> path = encryptionConfig.keyFilepath();
        if (path.isEmpty()) {
            throw new IllegalStateException("Encryption utils called, but no encryption key was provided");
        }
        try {
            LOGGER.debug("Encrypting and encoding value: {}", src);

            // Strip key headers and parse content. Caching results
            this.readAndParsePublicKey();
            if (this.decodedKey == null || this.decodedKey.length == 0) {
                LOGGER.error("Error decoding public key while encrypting: {}", src);
                throw new RuntimeException("Could not encrypt value, key could not be loaded");
            }

            // Generate an RSA public key for encryption
            RSAPublicKey publicKey = (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(decodedKey));

            // Initialize a cipher using our public key and encrypt the email
            Cipher cipher = Cipher.getInstance(encryptionConfig.keyType());
            cipher.init(Cipher.PUBLIC_KEY, publicKey);
            byte[] encyptedEmail = cipher.doFinal(src.getBytes());

            // Base64 encode before returning the results
            return Base64.getEncoder().encodeToString(encyptedEmail);
        } catch (Exception e) {
            throw new RuntimeException("Could not encrypt passed value", e);
        }
    }

    /**
     * Formats the message part to remove newline characters. This is meant to be used for user-defined fields to prevent logging
     * injections.
     * 
     * @param msg the message part to format
     * @return the sanitized log part.
     */
    public static String formatLog(String msg) {
        if (msg == null) {
            return "";
        }
        return msg.replaceAll("[\\n\\r]", "__");
    }

    /**
     * Formats the message part to remove newline characters from each item in the list. This is meant to be used for user-defined fields to
     * prevent logging injections.
     * 
     * @param msg the message part to format
     * @return the sanitized log part.
     */
    public static List<String> formatLog(List<String> msg) {
        return msg == null ? Collections.emptyList() : msg.stream().map(TransformationHelper::formatLog).toList();
    }

    /**
     * Reads the public key file, strips the headers, and decodes the public key value, returning the result in an array of bytes.
     * 
     * @return The decoded public key
     */
    private byte[] readAndParsePublicKey() {
        if (this.decodedKey != null) {
            return decodedKey;
        }
        // check that we have the encryption key path before trying to load it
        Optional<String> path = encryptionConfig.keyFilepath();
        if (path.isEmpty()) {
            throw new IllegalStateException("Encryption utils called, but no encryption key was provided");
        }

        // load the key in
        try (PemReader reader = new PemReader(new FileReader(path.get()))) {
            this.decodedKey = reader.readPemObject().getContent();
        } catch (Exception e) {
            LOGGER.error("Error while reading public key", e);
            this.decodedKey = new byte[] {};
        }
        return decodedKey;
    }
}
