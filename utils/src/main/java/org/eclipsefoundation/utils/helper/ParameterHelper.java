/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.utils.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipsefoundation.utils.namespace.UrlParameterNamespace;
import org.eclipsefoundation.utils.namespace.UrlParameterNamespace.UrlParameter;

import io.quarkus.arc.Unremovable;
import io.quarkus.runtime.Startup;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Instance;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * Helper for parameter lookups and filtering.
 * 
 * @author Martin Lowe
 *
 */
@Startup
@Unremovable
@ApplicationScoped
public class ParameterHelper {
    // used
    private static final int PAIRED_INPUT_SIZE = 2;

    private final List<String> urlParamNames;

    /**
     * Default constructor for ParameterHelper, uses instances of parameter namespaces to precompile list of managed names.
     * 
     * @param urlParamImpls CDI instance of parameter namespace implementations
     */
    public ParameterHelper(Instance<UrlParameterNamespace> urlParamImpls) {
        // maintain internal list of key names to reduce churn. These are only created at build time
        this.urlParamNames = urlParamImpls
                .stream()
                .flatMap(namespace -> namespace.getParameters().stream().map(UrlParameter::getName))
                .toList();
    }

    /**
     * Filters out unknown parameters by name/key. Creates and returns a new map with the known/tracked parameters. Tracked parameters are
     * added through implementations of the {@link UrlParameterNamespace} interface.
     * 
     * @param src multivalued parameter map to filter
     * @return a map containing the filtered parameter values
     */
    public MultivaluedMap<String, String> filterUnknownParameters(MultivaluedMap<String, String> src) {
        return src
                .entrySet()
                .stream()
                .filter(e -> urlParamNames.contains(e.getKey()))
                .collect(Collectors.toMap(Entry::getKey, Entry::getValue, (prevVal, addVals) -> {
                    // merge the 2 lists and return
                    prevVal.addAll(addVals);
                    return prevVal;
                }, MultivaluedHashMap<String, String>::new));
    }

    /**
     * Parses a decoded query string to create a query map for use in lookups.
     * 
     * @param query the decoded query string for a request
     * @return map of param names to the associated values
     */
    public static MultivaluedMap<String, String> parseQueryString(String query) {
        if (query == null) {
            return new MultivaluedHashMap<>();
        }
        return Stream
                .of(query.split("&"))
                .map(q -> q.split("=", PAIRED_INPUT_SIZE))
                .filter(a -> a.length == PAIRED_INPUT_SIZE)
                .collect(MultivaluedHashMap::new, (multimap, input) -> multimap.add(input[0], input[1]), MultivaluedMap::putAll);
    }

    /**
     * Returns a list of parameter names, as generated from reading in the parameters present in the {@link UrlParameterNamespace}
     * implementations.
     * 
     * @return list of tracked parameter names.
     */
    public List<String> getValidParameters() {
        return new ArrayList<>(urlParamNames);
    }
}
