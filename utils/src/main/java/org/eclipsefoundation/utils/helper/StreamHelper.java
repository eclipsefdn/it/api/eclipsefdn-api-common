/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.utils.helper;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentHashMap.KeySetView;
import java.util.function.Function;
import java.util.function.Predicate;

import jakarta.inject.Singleton;

/**
 * Contains operations to help with processing and filtering streams.
 */
@Singleton
public class StreamHelper {

    /**
     * Uniqueness check that returns true if the record already exists in the predicate map. Reuse of the resulting predicate is
     * strongly recommended against, as the state is kept within the predicate and will be used in subsequent calls using the same predicate
     * object.
     * 
     * @param <T> type of entry being filtered
     * @param keyExtractor function that retrieves unique keys for entries
     * @return predicate for deduplicating in removal situations
     */
    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        KeySetView<Object, Boolean> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }

    private StreamHelper() {

    }
}
