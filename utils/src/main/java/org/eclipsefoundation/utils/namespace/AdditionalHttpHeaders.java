/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
/**
 * 
 */
package org.eclipsefoundation.utils.namespace;

/**
 * Contains headers that were not mapped in the upstream header namespaces that are used in the application.
 */
public class AdditionalHttpHeaders {
    public static final String X_FORWARDED_FOR = "X-Forwarded-For";

    private AdditionalHttpHeaders() {
    }
}
