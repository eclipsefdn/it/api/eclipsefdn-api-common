/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.utils.namespace;

import java.util.List;

/**
 * Denotes a namespace object containing the URL parameters that are used.
 * Implementations should be scoped and managed through CDI to be detected by
 * other parts of the application.
 * 
 * @author Martin Lowe
 *
 */
public interface UrlParameterNamespace {

	/**
	 * Retrieve the list of managed UrlParameter objects for the current namespace.
	 * 
	 * @return the list of UrlParameters defined by this namespace.
	 */
	List<UrlParameter> getParameters();

	/**
	 * Class representing a named and managed URL parameter.
	 * 
	 * @author Martin Lowe
	 *
	 */
	public static class UrlParameter {
		private final String name;

		public UrlParameter(String name) {
			this.name = name;
		}

		public String getName() {
			return this.name;
		}
	}
}
