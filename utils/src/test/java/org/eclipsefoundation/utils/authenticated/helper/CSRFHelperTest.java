/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.utils.authenticated.helper;

import org.eclipsefoundation.utils.exception.FinalForbiddenException;
import org.eclipsefoundation.utils.helper.CSRFHelper;
import org.eclipsefoundation.utils.test.AuthenticatedTestProfile;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import io.vertx.core.http.HttpServerRequest;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.SecurityContext;

/**
 * Test CSRF functionality from the helper directly using the authentication secured test profile.
 * 
 * @author Martin Lowe
 */
@QuarkusTest
@TestProfile(AuthenticatedTestProfile.class)
class CSRFHelperTest {

    @Inject
    CSRFHelper csrf;

    private static HttpServerRequest mockRequest;
    private static SecurityContext mockContext;

    @BeforeAll
    public static void setup() {
        CSRFHelperTest.mockRequest = Mockito.mock(HttpServerRequest.class);
        CSRFHelperTest.mockContext = Mockito.mock(SecurityContext.class);
    }

    @Test
    void compareCSRF_validToken() {
        // generate a token to use in test
        String csrfToken = csrf.getNewCSRFToken(mockRequest, mockContext);

        // this should not throw as the tokens match
        Assertions.assertDoesNotThrow(() -> csrf.compareCSRF(csrfToken, csrfToken));
    }

    @Test
    void compareCSRF_invalidToken() {
        // generate a token to use in test
        String csrfToken = csrf.getNewCSRFToken(mockRequest, mockContext);

        // this should throw as the tokens are not the same
        Assertions.assertThrows(FinalForbiddenException.class, () -> csrf.compareCSRF(csrfToken, "some-other-value"));
    }

    @Test
    void compareCSRF_noSubmittedToken() {
        // generate a token to use in test
        String csrfToken = csrf.getNewCSRFToken(mockRequest, mockContext);
        // this should throw as the tokens are not the same
        Assertions.assertThrows(FinalForbiddenException.class, () -> csrf.compareCSRF(csrfToken, null));
        Assertions.assertThrows(FinalForbiddenException.class, () -> csrf.compareCSRF(csrfToken, ""));
    }
}
