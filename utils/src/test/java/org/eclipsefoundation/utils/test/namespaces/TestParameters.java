/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.utils.test.namespaces;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipsefoundation.utils.namespace.UrlParameterNamespace;

import jakarta.inject.Singleton;

/**
 * @author Martin Lowe
 *
 */
@Singleton
public final class TestParameters implements UrlParameterNamespace{
    public static final String ID_PARAMETER_NAME = "id";
    public static final String IDS_PARAMETER_NAME = "ids";
    public static final UrlParameter IDS = new UrlParameter(IDS_PARAMETER_NAME);
    public static final UrlParameter ID = new UrlParameter(ID_PARAMETER_NAME);


    private static final List<UrlParameter> params = Collections
            .unmodifiableList(Arrays.asList(IDS, ID));

    @Override
    public List<UrlParameter> getParameters() {
        return new ArrayList<>(params);
    }
}
