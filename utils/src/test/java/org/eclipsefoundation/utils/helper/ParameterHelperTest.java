package org.eclipsefoundation.utils.helper;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipsefoundation.utils.namespace.UrlParameterNamespace;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * Contains base tests for confirming that parameter helper filters as expected
 * based on registered parameters.
 */
@QuarkusTest
class ParameterHelperTest {

    @Inject
    ParameterHelper helper;

    @Test
    void filterUnknownParameters_success_retainsKnownParameters() {
        // set up params with basic params that are tracked
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add("id", "sample");

        MultivaluedMap<String, String> actualParams = helper.filterUnknownParameters(params);
        Assertions.assertEquals(params, actualParams, "Parameter map was unexpectedly filtered");
    }

    @Test
    void filterUnknownParameters_success_removesUnknownParameters() {
        // set up params with basic params that are tracked
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add("some-unknown-parameter", "sample");

        MultivaluedMap<String, String> actualParams = helper.filterUnknownParameters(params);
        Assertions.assertTrue(actualParams.isEmpty(),
                "Parameter map had an unexpected value when it expected value to be filtered");
    }

    @Test
    void filterUnknownParameters_success_detectsCustomNamespaces() {
        // set up params with basic params that are tracked
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();

        params.add(TestUrlParameterNamespace.SAMPLE_URL_PARAM_NAME, "sample");

        MultivaluedMap<String, String> actualParams = helper.filterUnknownParameters(params);
        Assertions.assertEquals(params, actualParams, "Parameter map was unexpectedly filtered");
    }

    @Test
    void getValidParameters_success_containsBakedInParams() {
        Assertions.assertTrue(helper.getValidParameters().contains("id"));
    }

    @Test
    void getValidParameters_success_detectsCustomNamespaces() {
        Assertions.assertTrue(helper.getValidParameters().contains(TestUrlParameterNamespace.SAMPLE_URL_PARAM_NAME));
    }

    @Test
    void parseQueryString_success() {
        MultivaluedMap<String, String> out = ParameterHelper
                .parseQueryString("sample=1&fair_param=simple&sample=some string");
        // check for multiple values on same key
        Assertions.assertNotNull(out.get("sample"));
        Assertions.assertEquals(2, out.get("sample").size());
        Assertions.assertTrue(out.get("sample").contains("1"));
        Assertions.assertTrue(out.get("sample").contains("some string"));

        // check for basic param with non alpha character
        Assertions.assertNotNull(out.get("fair_param"));
        Assertions.assertEquals(1, out.get("fair_param").size());
        Assertions.assertTrue(out.get("fair_param").contains("simple"));
    }

    @Test
    void parseQueryString_success_containsKeyWNoValue() {
        MultivaluedMap<String, String> out = ParameterHelper.parseQueryString("sample=1&fair_param=simple&sample");
        // check that key w/o a value gets filtered out
        Assertions.assertNotNull(out.get("sample"));
        Assertions.assertEquals(1, out.get("sample").size());
        Assertions.assertTrue(out.get("sample").contains("1"));

        // check for basic param with non alpha character
        Assertions.assertNotNull(out.get("fair_param"));
        Assertions.assertEquals(1, out.get("fair_param").size());
        Assertions.assertTrue(out.get("fair_param").contains("simple"));
    }

    @Test
    void parseQueryString_success_containsHangingParam() {
        MultivaluedMap<String, String> out = ParameterHelper.parseQueryString("fair_param=simple&");
        // should only have 1 key
        Assertions.assertEquals(1, out.keySet().size());

        // check for basic param with non alpha character
        Assertions.assertNotNull(out.get("fair_param"));
        Assertions.assertEquals(1, out.get("fair_param").size());
        Assertions.assertTrue(out.get("fair_param").contains("simple"));
    }

    @Test
    void parseQueryString_success_containsEmptyValue() {
        MultivaluedMap<String, String> out = ParameterHelper.parseQueryString("fair_param=simple&sample=");
        // should 2 keys
        Assertions.assertEquals(2, out.keySet().size());

        // check the empty value get's returned
        Assertions.assertNotNull(out.get("sample"));
        Assertions.assertEquals(1, out.get("sample").size());
        Assertions.assertTrue(out.get("sample").contains(""));
    }

    @Test
    void parseQueryString_success_emptyQueryString() {
        MultivaluedMap<String, String> out = ParameterHelper.parseQueryString("");
        // should contain no keys
        Assertions.assertNotNull(out);
        Assertions.assertEquals(0, out.keySet().size());
    }

    @Test
    void parseQueryString_success_nullQueryString() {
        MultivaluedMap<String, String> out = ParameterHelper.parseQueryString(null);
        // should contain no keys
        Assertions.assertNotNull(out);
        Assertions.assertEquals(0, out.keySet().size());
    }

    @ApplicationScoped
    public static class TestUrlParameterNamespace implements UrlParameterNamespace {
        public static final String SAMPLE_URL_PARAM_NAME = "sample_param";
        public static final UrlParameter SAMPLE_URL_PARAM = new UrlParameter(SAMPLE_URL_PARAM_NAME);

        private static final List<UrlParameter> params = Collections.unmodifiableList(Arrays.asList(SAMPLE_URL_PARAM));

        @Override
        public List<UrlParameter> getParameters() {
            return params;
        }
    }
}
