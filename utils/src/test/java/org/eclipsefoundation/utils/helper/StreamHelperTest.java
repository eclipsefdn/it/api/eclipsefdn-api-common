/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.utils.helper;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.logging.Log;
import io.quarkus.test.junit.QuarkusTest;

/**
 * 
 */
@QuarkusTest
class StreamHelperTest {

    @Test
    void distinctByKey_success() {
        List<SampleRecord> records = Arrays
                .asList(new SampleRecord("sample-1", "name-1"), new SampleRecord("sample-1", "name-2"),
                        new SampleRecord("sample-2", "name-1"), new SampleRecord("sample-3", "name-1"));
        // pre test assertion
        Predicate<SampleRecord> p = rec -> "sample-1".equals(rec.id);
        Assertions.assertEquals(2, records.stream().filter(p).count());
        // run the filter
        List<SampleRecord> out = records.stream().filter(StreamHelper.distinctByKey(SampleRecord::id)).toList();
        Log.error(out);
        // check that the output renders as expected
        Assertions.assertEquals(3, out.size());
        Assertions.assertEquals(1, out.stream().filter(p).count());
    }

    @Test
    void distinctByKey_success_caseSensitive() {
        List<SampleRecord> records = Arrays
                .asList(new SampleRecord("sample-1", "name-1"), new SampleRecord("sample-1", "name-2"),
                        new SampleRecord("sample-2", "name-1"), new SampleRecord("sample-3", "name-1"),
                        new SampleRecord("Sample-1", "name-1"));
        // pre test assertion
        Predicate<SampleRecord> caseInsensitivePredicate = rec -> "sample-1".equalsIgnoreCase(rec.id);
        Predicate<SampleRecord> caseSensitivePredicate = rec -> "sample-1".equals(rec.id);
        Assertions.assertEquals(3, records.stream().filter(caseInsensitivePredicate).count());
        Assertions.assertEquals(2, records.stream().filter(caseSensitivePredicate).count());
        // run the filter
        List<SampleRecord> out = records.stream().filter(StreamHelper.distinctByKey(SampleRecord::id)).toList();
        // check that the output renders as expected
        Assertions.assertEquals(4, out.size());
        Assertions.assertEquals(2, out.stream().filter(caseInsensitivePredicate).count());
        Assertions.assertEquals(1, out.stream().filter(caseSensitivePredicate).count());
    }

    public record SampleRecord(String id, String name) {
    }
}
